API SwaggerPetstore {
    description "This is a sample Pet Store Server based on the OpenAPI 3.0 specification."

    datatypes {
        required ObjectDataType Pet {
            datatypes (id, name, friends)
        },
        required IntegerData id,
        required StringData name,
        CollectionDataType friends {
            listItem Pet
            required true
        }
    }

    url "https://petstore3.swagger.io/api/v3"

    operations {
        CreateOperation addPet {
            description "Add a new pet to the store"
            urlSuffix "/pet"
            response Pet
            parameters {
                required Parameter {
                    description "The pet data to add."
                    datatype Pet
                }
            }
        },
        ReadOperation getPetById {
            description "Find pet by ID"
            urlSuffix "/pet/{petId}"
            response Pet
            parameters {
                required Parameter {
                    description "ID of pet to return"
                    datatype id
                }
            }
        }
    }
}
