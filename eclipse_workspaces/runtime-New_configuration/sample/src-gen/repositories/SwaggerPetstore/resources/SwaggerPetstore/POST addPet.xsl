<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
   <xsl:template match="/">
      <ROOT>
         <REQUEST CONTENT-TYPE="application/json" EMBED="true" NAME="addPet" OPERATION="POST" URL="/pet">
            <PARAMS>
            </PARAMS>
            <CONTENT>
            {
              "petId" : "integer",
              "name" : "string",
              "friends" : "list of: Pet"
            }
            <!-- required content: petId, name,  -->
            </CONTENT>
            
         </REQUEST>
      </ROOT>
   </xsl:template>
</xsl:stylesheet>

