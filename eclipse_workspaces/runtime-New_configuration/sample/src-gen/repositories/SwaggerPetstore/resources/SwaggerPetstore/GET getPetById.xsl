<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
   <xsl:template match="/">
      <ROOT>
         <REQUEST CONTENT-TYPE="application/json" EMBED="true" NAME="getPetById" OPERATION="GET" URL="/pet/{petId}">
            <PARAMS>
            <!-- required -->
               <PARAM NAME="petId">integer</PARAM>
                
            </PARAMS>
         </REQUEST>
      </ROOT>
   </xsl:template>
</xsl:stylesheet>

