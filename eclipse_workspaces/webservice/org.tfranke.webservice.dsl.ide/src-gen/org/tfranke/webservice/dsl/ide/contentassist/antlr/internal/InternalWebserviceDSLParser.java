package org.tfranke.webservice.dsl.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import org.tfranke.webservice.dsl.services.WebserviceDSLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalWebserviceDSLParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'API'", "'{'", "'url'", "'datatypes'", "'}'", "'operations'", "'description'", "','", "'Parameter'", "'datatype'", "'ReadOperation'", "'urlSuffix'", "'response'", "'parameters'", "'CreateOperation'", "'UpdateOperation'", "'DeleteOperation'", "'otherOperation'", "'operation'", "'ObjectDataType'", "'('", "')'", "'CollectionDataType'", "'listItem'", "'StringData'", "'IntegerData'", "'FloatData'", "'BooleanData'", "'ByteData'", "'required'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__37=37;
    public static final int T__16=16;
    public static final int T__38=38;
    public static final int T__17=17;
    public static final int T__39=39;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__40=40;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalWebserviceDSLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalWebserviceDSLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalWebserviceDSLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalWebserviceDSL.g"; }


    	private WebserviceDSLGrammarAccess grammarAccess;

    	public void setGrammarAccess(WebserviceDSLGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleAPI"
    // InternalWebserviceDSL.g:53:1: entryRuleAPI : ruleAPI EOF ;
    public final void entryRuleAPI() throws RecognitionException {
        try {
            // InternalWebserviceDSL.g:54:1: ( ruleAPI EOF )
            // InternalWebserviceDSL.g:55:1: ruleAPI EOF
            {
             before(grammarAccess.getAPIRule()); 
            pushFollow(FOLLOW_1);
            ruleAPI();

            state._fsp--;

             after(grammarAccess.getAPIRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAPI"


    // $ANTLR start "ruleAPI"
    // InternalWebserviceDSL.g:62:1: ruleAPI : ( ( rule__API__Group__0 ) ) ;
    public final void ruleAPI() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:66:2: ( ( ( rule__API__Group__0 ) ) )
            // InternalWebserviceDSL.g:67:2: ( ( rule__API__Group__0 ) )
            {
            // InternalWebserviceDSL.g:67:2: ( ( rule__API__Group__0 ) )
            // InternalWebserviceDSL.g:68:3: ( rule__API__Group__0 )
            {
             before(grammarAccess.getAPIAccess().getGroup()); 
            // InternalWebserviceDSL.g:69:3: ( rule__API__Group__0 )
            // InternalWebserviceDSL.g:69:4: rule__API__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__API__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAPIAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAPI"


    // $ANTLR start "entryRuleDataType"
    // InternalWebserviceDSL.g:78:1: entryRuleDataType : ruleDataType EOF ;
    public final void entryRuleDataType() throws RecognitionException {
        try {
            // InternalWebserviceDSL.g:79:1: ( ruleDataType EOF )
            // InternalWebserviceDSL.g:80:1: ruleDataType EOF
            {
             before(grammarAccess.getDataTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleDataType();

            state._fsp--;

             after(grammarAccess.getDataTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDataType"


    // $ANTLR start "ruleDataType"
    // InternalWebserviceDSL.g:87:1: ruleDataType : ( ( rule__DataType__Alternatives ) ) ;
    public final void ruleDataType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:91:2: ( ( ( rule__DataType__Alternatives ) ) )
            // InternalWebserviceDSL.g:92:2: ( ( rule__DataType__Alternatives ) )
            {
            // InternalWebserviceDSL.g:92:2: ( ( rule__DataType__Alternatives ) )
            // InternalWebserviceDSL.g:93:3: ( rule__DataType__Alternatives )
            {
             before(grammarAccess.getDataTypeAccess().getAlternatives()); 
            // InternalWebserviceDSL.g:94:3: ( rule__DataType__Alternatives )
            // InternalWebserviceDSL.g:94:4: rule__DataType__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__DataType__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getDataTypeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDataType"


    // $ANTLR start "entryRuleOperation"
    // InternalWebserviceDSL.g:103:1: entryRuleOperation : ruleOperation EOF ;
    public final void entryRuleOperation() throws RecognitionException {
        try {
            // InternalWebserviceDSL.g:104:1: ( ruleOperation EOF )
            // InternalWebserviceDSL.g:105:1: ruleOperation EOF
            {
             before(grammarAccess.getOperationRule()); 
            pushFollow(FOLLOW_1);
            ruleOperation();

            state._fsp--;

             after(grammarAccess.getOperationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOperation"


    // $ANTLR start "ruleOperation"
    // InternalWebserviceDSL.g:112:1: ruleOperation : ( ( rule__Operation__Alternatives ) ) ;
    public final void ruleOperation() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:116:2: ( ( ( rule__Operation__Alternatives ) ) )
            // InternalWebserviceDSL.g:117:2: ( ( rule__Operation__Alternatives ) )
            {
            // InternalWebserviceDSL.g:117:2: ( ( rule__Operation__Alternatives ) )
            // InternalWebserviceDSL.g:118:3: ( rule__Operation__Alternatives )
            {
             before(grammarAccess.getOperationAccess().getAlternatives()); 
            // InternalWebserviceDSL.g:119:3: ( rule__Operation__Alternatives )
            // InternalWebserviceDSL.g:119:4: rule__Operation__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Operation__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getOperationAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOperation"


    // $ANTLR start "entryRuleEString"
    // InternalWebserviceDSL.g:128:1: entryRuleEString : ruleEString EOF ;
    public final void entryRuleEString() throws RecognitionException {
        try {
            // InternalWebserviceDSL.g:129:1: ( ruleEString EOF )
            // InternalWebserviceDSL.g:130:1: ruleEString EOF
            {
             before(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getEStringRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalWebserviceDSL.g:137:1: ruleEString : ( ( rule__EString__Alternatives ) ) ;
    public final void ruleEString() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:141:2: ( ( ( rule__EString__Alternatives ) ) )
            // InternalWebserviceDSL.g:142:2: ( ( rule__EString__Alternatives ) )
            {
            // InternalWebserviceDSL.g:142:2: ( ( rule__EString__Alternatives ) )
            // InternalWebserviceDSL.g:143:3: ( rule__EString__Alternatives )
            {
             before(grammarAccess.getEStringAccess().getAlternatives()); 
            // InternalWebserviceDSL.g:144:3: ( rule__EString__Alternatives )
            // InternalWebserviceDSL.g:144:4: rule__EString__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__EString__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getEStringAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleParameter"
    // InternalWebserviceDSL.g:153:1: entryRuleParameter : ruleParameter EOF ;
    public final void entryRuleParameter() throws RecognitionException {
        try {
            // InternalWebserviceDSL.g:154:1: ( ruleParameter EOF )
            // InternalWebserviceDSL.g:155:1: ruleParameter EOF
            {
             before(grammarAccess.getParameterRule()); 
            pushFollow(FOLLOW_1);
            ruleParameter();

            state._fsp--;

             after(grammarAccess.getParameterRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleParameter"


    // $ANTLR start "ruleParameter"
    // InternalWebserviceDSL.g:162:1: ruleParameter : ( ( rule__Parameter__Group__0 ) ) ;
    public final void ruleParameter() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:166:2: ( ( ( rule__Parameter__Group__0 ) ) )
            // InternalWebserviceDSL.g:167:2: ( ( rule__Parameter__Group__0 ) )
            {
            // InternalWebserviceDSL.g:167:2: ( ( rule__Parameter__Group__0 ) )
            // InternalWebserviceDSL.g:168:3: ( rule__Parameter__Group__0 )
            {
             before(grammarAccess.getParameterAccess().getGroup()); 
            // InternalWebserviceDSL.g:169:3: ( rule__Parameter__Group__0 )
            // InternalWebserviceDSL.g:169:4: rule__Parameter__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Parameter__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getParameterAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleParameter"


    // $ANTLR start "entryRuleReadOperation"
    // InternalWebserviceDSL.g:178:1: entryRuleReadOperation : ruleReadOperation EOF ;
    public final void entryRuleReadOperation() throws RecognitionException {
        try {
            // InternalWebserviceDSL.g:179:1: ( ruleReadOperation EOF )
            // InternalWebserviceDSL.g:180:1: ruleReadOperation EOF
            {
             before(grammarAccess.getReadOperationRule()); 
            pushFollow(FOLLOW_1);
            ruleReadOperation();

            state._fsp--;

             after(grammarAccess.getReadOperationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleReadOperation"


    // $ANTLR start "ruleReadOperation"
    // InternalWebserviceDSL.g:187:1: ruleReadOperation : ( ( rule__ReadOperation__Group__0 ) ) ;
    public final void ruleReadOperation() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:191:2: ( ( ( rule__ReadOperation__Group__0 ) ) )
            // InternalWebserviceDSL.g:192:2: ( ( rule__ReadOperation__Group__0 ) )
            {
            // InternalWebserviceDSL.g:192:2: ( ( rule__ReadOperation__Group__0 ) )
            // InternalWebserviceDSL.g:193:3: ( rule__ReadOperation__Group__0 )
            {
             before(grammarAccess.getReadOperationAccess().getGroup()); 
            // InternalWebserviceDSL.g:194:3: ( rule__ReadOperation__Group__0 )
            // InternalWebserviceDSL.g:194:4: rule__ReadOperation__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ReadOperation__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getReadOperationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleReadOperation"


    // $ANTLR start "entryRuleCreateOperation"
    // InternalWebserviceDSL.g:203:1: entryRuleCreateOperation : ruleCreateOperation EOF ;
    public final void entryRuleCreateOperation() throws RecognitionException {
        try {
            // InternalWebserviceDSL.g:204:1: ( ruleCreateOperation EOF )
            // InternalWebserviceDSL.g:205:1: ruleCreateOperation EOF
            {
             before(grammarAccess.getCreateOperationRule()); 
            pushFollow(FOLLOW_1);
            ruleCreateOperation();

            state._fsp--;

             after(grammarAccess.getCreateOperationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCreateOperation"


    // $ANTLR start "ruleCreateOperation"
    // InternalWebserviceDSL.g:212:1: ruleCreateOperation : ( ( rule__CreateOperation__Group__0 ) ) ;
    public final void ruleCreateOperation() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:216:2: ( ( ( rule__CreateOperation__Group__0 ) ) )
            // InternalWebserviceDSL.g:217:2: ( ( rule__CreateOperation__Group__0 ) )
            {
            // InternalWebserviceDSL.g:217:2: ( ( rule__CreateOperation__Group__0 ) )
            // InternalWebserviceDSL.g:218:3: ( rule__CreateOperation__Group__0 )
            {
             before(grammarAccess.getCreateOperationAccess().getGroup()); 
            // InternalWebserviceDSL.g:219:3: ( rule__CreateOperation__Group__0 )
            // InternalWebserviceDSL.g:219:4: rule__CreateOperation__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__CreateOperation__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCreateOperationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCreateOperation"


    // $ANTLR start "entryRuleUpdateOperation"
    // InternalWebserviceDSL.g:228:1: entryRuleUpdateOperation : ruleUpdateOperation EOF ;
    public final void entryRuleUpdateOperation() throws RecognitionException {
        try {
            // InternalWebserviceDSL.g:229:1: ( ruleUpdateOperation EOF )
            // InternalWebserviceDSL.g:230:1: ruleUpdateOperation EOF
            {
             before(grammarAccess.getUpdateOperationRule()); 
            pushFollow(FOLLOW_1);
            ruleUpdateOperation();

            state._fsp--;

             after(grammarAccess.getUpdateOperationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleUpdateOperation"


    // $ANTLR start "ruleUpdateOperation"
    // InternalWebserviceDSL.g:237:1: ruleUpdateOperation : ( ( rule__UpdateOperation__Group__0 ) ) ;
    public final void ruleUpdateOperation() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:241:2: ( ( ( rule__UpdateOperation__Group__0 ) ) )
            // InternalWebserviceDSL.g:242:2: ( ( rule__UpdateOperation__Group__0 ) )
            {
            // InternalWebserviceDSL.g:242:2: ( ( rule__UpdateOperation__Group__0 ) )
            // InternalWebserviceDSL.g:243:3: ( rule__UpdateOperation__Group__0 )
            {
             before(grammarAccess.getUpdateOperationAccess().getGroup()); 
            // InternalWebserviceDSL.g:244:3: ( rule__UpdateOperation__Group__0 )
            // InternalWebserviceDSL.g:244:4: rule__UpdateOperation__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__UpdateOperation__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getUpdateOperationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleUpdateOperation"


    // $ANTLR start "entryRuleDeleteOperation"
    // InternalWebserviceDSL.g:253:1: entryRuleDeleteOperation : ruleDeleteOperation EOF ;
    public final void entryRuleDeleteOperation() throws RecognitionException {
        try {
            // InternalWebserviceDSL.g:254:1: ( ruleDeleteOperation EOF )
            // InternalWebserviceDSL.g:255:1: ruleDeleteOperation EOF
            {
             before(grammarAccess.getDeleteOperationRule()); 
            pushFollow(FOLLOW_1);
            ruleDeleteOperation();

            state._fsp--;

             after(grammarAccess.getDeleteOperationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDeleteOperation"


    // $ANTLR start "ruleDeleteOperation"
    // InternalWebserviceDSL.g:262:1: ruleDeleteOperation : ( ( rule__DeleteOperation__Group__0 ) ) ;
    public final void ruleDeleteOperation() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:266:2: ( ( ( rule__DeleteOperation__Group__0 ) ) )
            // InternalWebserviceDSL.g:267:2: ( ( rule__DeleteOperation__Group__0 ) )
            {
            // InternalWebserviceDSL.g:267:2: ( ( rule__DeleteOperation__Group__0 ) )
            // InternalWebserviceDSL.g:268:3: ( rule__DeleteOperation__Group__0 )
            {
             before(grammarAccess.getDeleteOperationAccess().getGroup()); 
            // InternalWebserviceDSL.g:269:3: ( rule__DeleteOperation__Group__0 )
            // InternalWebserviceDSL.g:269:4: rule__DeleteOperation__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__DeleteOperation__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDeleteOperationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDeleteOperation"


    // $ANTLR start "entryRuleOtherOperation"
    // InternalWebserviceDSL.g:278:1: entryRuleOtherOperation : ruleOtherOperation EOF ;
    public final void entryRuleOtherOperation() throws RecognitionException {
        try {
            // InternalWebserviceDSL.g:279:1: ( ruleOtherOperation EOF )
            // InternalWebserviceDSL.g:280:1: ruleOtherOperation EOF
            {
             before(grammarAccess.getOtherOperationRule()); 
            pushFollow(FOLLOW_1);
            ruleOtherOperation();

            state._fsp--;

             after(grammarAccess.getOtherOperationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOtherOperation"


    // $ANTLR start "ruleOtherOperation"
    // InternalWebserviceDSL.g:287:1: ruleOtherOperation : ( ( rule__OtherOperation__Group__0 ) ) ;
    public final void ruleOtherOperation() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:291:2: ( ( ( rule__OtherOperation__Group__0 ) ) )
            // InternalWebserviceDSL.g:292:2: ( ( rule__OtherOperation__Group__0 ) )
            {
            // InternalWebserviceDSL.g:292:2: ( ( rule__OtherOperation__Group__0 ) )
            // InternalWebserviceDSL.g:293:3: ( rule__OtherOperation__Group__0 )
            {
             before(grammarAccess.getOtherOperationAccess().getGroup()); 
            // InternalWebserviceDSL.g:294:3: ( rule__OtherOperation__Group__0 )
            // InternalWebserviceDSL.g:294:4: rule__OtherOperation__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__OtherOperation__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOtherOperationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOtherOperation"


    // $ANTLR start "entryRuleObjectDataType"
    // InternalWebserviceDSL.g:303:1: entryRuleObjectDataType : ruleObjectDataType EOF ;
    public final void entryRuleObjectDataType() throws RecognitionException {
        try {
            // InternalWebserviceDSL.g:304:1: ( ruleObjectDataType EOF )
            // InternalWebserviceDSL.g:305:1: ruleObjectDataType EOF
            {
             before(grammarAccess.getObjectDataTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleObjectDataType();

            state._fsp--;

             after(grammarAccess.getObjectDataTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleObjectDataType"


    // $ANTLR start "ruleObjectDataType"
    // InternalWebserviceDSL.g:312:1: ruleObjectDataType : ( ( rule__ObjectDataType__Group__0 ) ) ;
    public final void ruleObjectDataType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:316:2: ( ( ( rule__ObjectDataType__Group__0 ) ) )
            // InternalWebserviceDSL.g:317:2: ( ( rule__ObjectDataType__Group__0 ) )
            {
            // InternalWebserviceDSL.g:317:2: ( ( rule__ObjectDataType__Group__0 ) )
            // InternalWebserviceDSL.g:318:3: ( rule__ObjectDataType__Group__0 )
            {
             before(grammarAccess.getObjectDataTypeAccess().getGroup()); 
            // InternalWebserviceDSL.g:319:3: ( rule__ObjectDataType__Group__0 )
            // InternalWebserviceDSL.g:319:4: rule__ObjectDataType__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ObjectDataType__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getObjectDataTypeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleObjectDataType"


    // $ANTLR start "entryRuleCollectionDataType"
    // InternalWebserviceDSL.g:328:1: entryRuleCollectionDataType : ruleCollectionDataType EOF ;
    public final void entryRuleCollectionDataType() throws RecognitionException {
        try {
            // InternalWebserviceDSL.g:329:1: ( ruleCollectionDataType EOF )
            // InternalWebserviceDSL.g:330:1: ruleCollectionDataType EOF
            {
             before(grammarAccess.getCollectionDataTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleCollectionDataType();

            state._fsp--;

             after(grammarAccess.getCollectionDataTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCollectionDataType"


    // $ANTLR start "ruleCollectionDataType"
    // InternalWebserviceDSL.g:337:1: ruleCollectionDataType : ( ( rule__CollectionDataType__Group__0 ) ) ;
    public final void ruleCollectionDataType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:341:2: ( ( ( rule__CollectionDataType__Group__0 ) ) )
            // InternalWebserviceDSL.g:342:2: ( ( rule__CollectionDataType__Group__0 ) )
            {
            // InternalWebserviceDSL.g:342:2: ( ( rule__CollectionDataType__Group__0 ) )
            // InternalWebserviceDSL.g:343:3: ( rule__CollectionDataType__Group__0 )
            {
             before(grammarAccess.getCollectionDataTypeAccess().getGroup()); 
            // InternalWebserviceDSL.g:344:3: ( rule__CollectionDataType__Group__0 )
            // InternalWebserviceDSL.g:344:4: rule__CollectionDataType__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__CollectionDataType__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCollectionDataTypeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCollectionDataType"


    // $ANTLR start "entryRuleStringData"
    // InternalWebserviceDSL.g:353:1: entryRuleStringData : ruleStringData EOF ;
    public final void entryRuleStringData() throws RecognitionException {
        try {
            // InternalWebserviceDSL.g:354:1: ( ruleStringData EOF )
            // InternalWebserviceDSL.g:355:1: ruleStringData EOF
            {
             before(grammarAccess.getStringDataRule()); 
            pushFollow(FOLLOW_1);
            ruleStringData();

            state._fsp--;

             after(grammarAccess.getStringDataRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStringData"


    // $ANTLR start "ruleStringData"
    // InternalWebserviceDSL.g:362:1: ruleStringData : ( ( rule__StringData__Group__0 ) ) ;
    public final void ruleStringData() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:366:2: ( ( ( rule__StringData__Group__0 ) ) )
            // InternalWebserviceDSL.g:367:2: ( ( rule__StringData__Group__0 ) )
            {
            // InternalWebserviceDSL.g:367:2: ( ( rule__StringData__Group__0 ) )
            // InternalWebserviceDSL.g:368:3: ( rule__StringData__Group__0 )
            {
             before(grammarAccess.getStringDataAccess().getGroup()); 
            // InternalWebserviceDSL.g:369:3: ( rule__StringData__Group__0 )
            // InternalWebserviceDSL.g:369:4: rule__StringData__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__StringData__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getStringDataAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStringData"


    // $ANTLR start "entryRuleIntegerData"
    // InternalWebserviceDSL.g:378:1: entryRuleIntegerData : ruleIntegerData EOF ;
    public final void entryRuleIntegerData() throws RecognitionException {
        try {
            // InternalWebserviceDSL.g:379:1: ( ruleIntegerData EOF )
            // InternalWebserviceDSL.g:380:1: ruleIntegerData EOF
            {
             before(grammarAccess.getIntegerDataRule()); 
            pushFollow(FOLLOW_1);
            ruleIntegerData();

            state._fsp--;

             after(grammarAccess.getIntegerDataRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIntegerData"


    // $ANTLR start "ruleIntegerData"
    // InternalWebserviceDSL.g:387:1: ruleIntegerData : ( ( rule__IntegerData__Group__0 ) ) ;
    public final void ruleIntegerData() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:391:2: ( ( ( rule__IntegerData__Group__0 ) ) )
            // InternalWebserviceDSL.g:392:2: ( ( rule__IntegerData__Group__0 ) )
            {
            // InternalWebserviceDSL.g:392:2: ( ( rule__IntegerData__Group__0 ) )
            // InternalWebserviceDSL.g:393:3: ( rule__IntegerData__Group__0 )
            {
             before(grammarAccess.getIntegerDataAccess().getGroup()); 
            // InternalWebserviceDSL.g:394:3: ( rule__IntegerData__Group__0 )
            // InternalWebserviceDSL.g:394:4: rule__IntegerData__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__IntegerData__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getIntegerDataAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIntegerData"


    // $ANTLR start "entryRuleFloatData"
    // InternalWebserviceDSL.g:403:1: entryRuleFloatData : ruleFloatData EOF ;
    public final void entryRuleFloatData() throws RecognitionException {
        try {
            // InternalWebserviceDSL.g:404:1: ( ruleFloatData EOF )
            // InternalWebserviceDSL.g:405:1: ruleFloatData EOF
            {
             before(grammarAccess.getFloatDataRule()); 
            pushFollow(FOLLOW_1);
            ruleFloatData();

            state._fsp--;

             after(grammarAccess.getFloatDataRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFloatData"


    // $ANTLR start "ruleFloatData"
    // InternalWebserviceDSL.g:412:1: ruleFloatData : ( ( rule__FloatData__Group__0 ) ) ;
    public final void ruleFloatData() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:416:2: ( ( ( rule__FloatData__Group__0 ) ) )
            // InternalWebserviceDSL.g:417:2: ( ( rule__FloatData__Group__0 ) )
            {
            // InternalWebserviceDSL.g:417:2: ( ( rule__FloatData__Group__0 ) )
            // InternalWebserviceDSL.g:418:3: ( rule__FloatData__Group__0 )
            {
             before(grammarAccess.getFloatDataAccess().getGroup()); 
            // InternalWebserviceDSL.g:419:3: ( rule__FloatData__Group__0 )
            // InternalWebserviceDSL.g:419:4: rule__FloatData__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__FloatData__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFloatDataAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFloatData"


    // $ANTLR start "entryRuleBooleanData"
    // InternalWebserviceDSL.g:428:1: entryRuleBooleanData : ruleBooleanData EOF ;
    public final void entryRuleBooleanData() throws RecognitionException {
        try {
            // InternalWebserviceDSL.g:429:1: ( ruleBooleanData EOF )
            // InternalWebserviceDSL.g:430:1: ruleBooleanData EOF
            {
             before(grammarAccess.getBooleanDataRule()); 
            pushFollow(FOLLOW_1);
            ruleBooleanData();

            state._fsp--;

             after(grammarAccess.getBooleanDataRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBooleanData"


    // $ANTLR start "ruleBooleanData"
    // InternalWebserviceDSL.g:437:1: ruleBooleanData : ( ( rule__BooleanData__Group__0 ) ) ;
    public final void ruleBooleanData() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:441:2: ( ( ( rule__BooleanData__Group__0 ) ) )
            // InternalWebserviceDSL.g:442:2: ( ( rule__BooleanData__Group__0 ) )
            {
            // InternalWebserviceDSL.g:442:2: ( ( rule__BooleanData__Group__0 ) )
            // InternalWebserviceDSL.g:443:3: ( rule__BooleanData__Group__0 )
            {
             before(grammarAccess.getBooleanDataAccess().getGroup()); 
            // InternalWebserviceDSL.g:444:3: ( rule__BooleanData__Group__0 )
            // InternalWebserviceDSL.g:444:4: rule__BooleanData__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__BooleanData__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBooleanDataAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBooleanData"


    // $ANTLR start "entryRuleByteData"
    // InternalWebserviceDSL.g:453:1: entryRuleByteData : ruleByteData EOF ;
    public final void entryRuleByteData() throws RecognitionException {
        try {
            // InternalWebserviceDSL.g:454:1: ( ruleByteData EOF )
            // InternalWebserviceDSL.g:455:1: ruleByteData EOF
            {
             before(grammarAccess.getByteDataRule()); 
            pushFollow(FOLLOW_1);
            ruleByteData();

            state._fsp--;

             after(grammarAccess.getByteDataRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleByteData"


    // $ANTLR start "ruleByteData"
    // InternalWebserviceDSL.g:462:1: ruleByteData : ( ( rule__ByteData__Group__0 ) ) ;
    public final void ruleByteData() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:466:2: ( ( ( rule__ByteData__Group__0 ) ) )
            // InternalWebserviceDSL.g:467:2: ( ( rule__ByteData__Group__0 ) )
            {
            // InternalWebserviceDSL.g:467:2: ( ( rule__ByteData__Group__0 ) )
            // InternalWebserviceDSL.g:468:3: ( rule__ByteData__Group__0 )
            {
             before(grammarAccess.getByteDataAccess().getGroup()); 
            // InternalWebserviceDSL.g:469:3: ( rule__ByteData__Group__0 )
            // InternalWebserviceDSL.g:469:4: rule__ByteData__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ByteData__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getByteDataAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleByteData"


    // $ANTLR start "rule__DataType__Alternatives"
    // InternalWebserviceDSL.g:477:1: rule__DataType__Alternatives : ( ( ruleObjectDataType ) | ( ruleCollectionDataType ) | ( ruleStringData ) | ( ruleIntegerData ) | ( ruleFloatData ) | ( ruleBooleanData ) | ( ruleByteData ) );
    public final void rule__DataType__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:481:1: ( ( ruleObjectDataType ) | ( ruleCollectionDataType ) | ( ruleStringData ) | ( ruleIntegerData ) | ( ruleFloatData ) | ( ruleBooleanData ) | ( ruleByteData ) )
            int alt1=7;
            switch ( input.LA(1) ) {
            case 40:
                {
                switch ( input.LA(2) ) {
                case 36:
                    {
                    alt1=4;
                    }
                    break;
                case 39:
                    {
                    alt1=7;
                    }
                    break;
                case 37:
                    {
                    alt1=5;
                    }
                    break;
                case 30:
                    {
                    alt1=1;
                    }
                    break;
                case 35:
                    {
                    alt1=3;
                    }
                    break;
                case 33:
                    {
                    alt1=2;
                    }
                    break;
                case 38:
                    {
                    alt1=6;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 1, 1, input);

                    throw nvae;
                }

                }
                break;
            case 30:
                {
                alt1=1;
                }
                break;
            case 33:
                {
                alt1=2;
                }
                break;
            case 35:
                {
                alt1=3;
                }
                break;
            case 36:
                {
                alt1=4;
                }
                break;
            case 37:
                {
                alt1=5;
                }
                break;
            case 38:
                {
                alt1=6;
                }
                break;
            case 39:
                {
                alt1=7;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // InternalWebserviceDSL.g:482:2: ( ruleObjectDataType )
                    {
                    // InternalWebserviceDSL.g:482:2: ( ruleObjectDataType )
                    // InternalWebserviceDSL.g:483:3: ruleObjectDataType
                    {
                     before(grammarAccess.getDataTypeAccess().getObjectDataTypeParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleObjectDataType();

                    state._fsp--;

                     after(grammarAccess.getDataTypeAccess().getObjectDataTypeParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalWebserviceDSL.g:488:2: ( ruleCollectionDataType )
                    {
                    // InternalWebserviceDSL.g:488:2: ( ruleCollectionDataType )
                    // InternalWebserviceDSL.g:489:3: ruleCollectionDataType
                    {
                     before(grammarAccess.getDataTypeAccess().getCollectionDataTypeParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleCollectionDataType();

                    state._fsp--;

                     after(grammarAccess.getDataTypeAccess().getCollectionDataTypeParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalWebserviceDSL.g:494:2: ( ruleStringData )
                    {
                    // InternalWebserviceDSL.g:494:2: ( ruleStringData )
                    // InternalWebserviceDSL.g:495:3: ruleStringData
                    {
                     before(grammarAccess.getDataTypeAccess().getStringDataParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleStringData();

                    state._fsp--;

                     after(grammarAccess.getDataTypeAccess().getStringDataParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalWebserviceDSL.g:500:2: ( ruleIntegerData )
                    {
                    // InternalWebserviceDSL.g:500:2: ( ruleIntegerData )
                    // InternalWebserviceDSL.g:501:3: ruleIntegerData
                    {
                     before(grammarAccess.getDataTypeAccess().getIntegerDataParserRuleCall_3()); 
                    pushFollow(FOLLOW_2);
                    ruleIntegerData();

                    state._fsp--;

                     after(grammarAccess.getDataTypeAccess().getIntegerDataParserRuleCall_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalWebserviceDSL.g:506:2: ( ruleFloatData )
                    {
                    // InternalWebserviceDSL.g:506:2: ( ruleFloatData )
                    // InternalWebserviceDSL.g:507:3: ruleFloatData
                    {
                     before(grammarAccess.getDataTypeAccess().getFloatDataParserRuleCall_4()); 
                    pushFollow(FOLLOW_2);
                    ruleFloatData();

                    state._fsp--;

                     after(grammarAccess.getDataTypeAccess().getFloatDataParserRuleCall_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalWebserviceDSL.g:512:2: ( ruleBooleanData )
                    {
                    // InternalWebserviceDSL.g:512:2: ( ruleBooleanData )
                    // InternalWebserviceDSL.g:513:3: ruleBooleanData
                    {
                     before(grammarAccess.getDataTypeAccess().getBooleanDataParserRuleCall_5()); 
                    pushFollow(FOLLOW_2);
                    ruleBooleanData();

                    state._fsp--;

                     after(grammarAccess.getDataTypeAccess().getBooleanDataParserRuleCall_5()); 

                    }


                    }
                    break;
                case 7 :
                    // InternalWebserviceDSL.g:518:2: ( ruleByteData )
                    {
                    // InternalWebserviceDSL.g:518:2: ( ruleByteData )
                    // InternalWebserviceDSL.g:519:3: ruleByteData
                    {
                     before(grammarAccess.getDataTypeAccess().getByteDataParserRuleCall_6()); 
                    pushFollow(FOLLOW_2);
                    ruleByteData();

                    state._fsp--;

                     after(grammarAccess.getDataTypeAccess().getByteDataParserRuleCall_6()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataType__Alternatives"


    // $ANTLR start "rule__Operation__Alternatives"
    // InternalWebserviceDSL.g:528:1: rule__Operation__Alternatives : ( ( ruleReadOperation ) | ( ruleCreateOperation ) | ( ruleUpdateOperation ) | ( ruleDeleteOperation ) | ( ruleOtherOperation ) );
    public final void rule__Operation__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:532:1: ( ( ruleReadOperation ) | ( ruleCreateOperation ) | ( ruleUpdateOperation ) | ( ruleDeleteOperation ) | ( ruleOtherOperation ) )
            int alt2=5;
            switch ( input.LA(1) ) {
            case 21:
                {
                alt2=1;
                }
                break;
            case 25:
                {
                alt2=2;
                }
                break;
            case 26:
                {
                alt2=3;
                }
                break;
            case 27:
                {
                alt2=4;
                }
                break;
            case 28:
                {
                alt2=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // InternalWebserviceDSL.g:533:2: ( ruleReadOperation )
                    {
                    // InternalWebserviceDSL.g:533:2: ( ruleReadOperation )
                    // InternalWebserviceDSL.g:534:3: ruleReadOperation
                    {
                     before(grammarAccess.getOperationAccess().getReadOperationParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleReadOperation();

                    state._fsp--;

                     after(grammarAccess.getOperationAccess().getReadOperationParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalWebserviceDSL.g:539:2: ( ruleCreateOperation )
                    {
                    // InternalWebserviceDSL.g:539:2: ( ruleCreateOperation )
                    // InternalWebserviceDSL.g:540:3: ruleCreateOperation
                    {
                     before(grammarAccess.getOperationAccess().getCreateOperationParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleCreateOperation();

                    state._fsp--;

                     after(grammarAccess.getOperationAccess().getCreateOperationParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalWebserviceDSL.g:545:2: ( ruleUpdateOperation )
                    {
                    // InternalWebserviceDSL.g:545:2: ( ruleUpdateOperation )
                    // InternalWebserviceDSL.g:546:3: ruleUpdateOperation
                    {
                     before(grammarAccess.getOperationAccess().getUpdateOperationParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleUpdateOperation();

                    state._fsp--;

                     after(grammarAccess.getOperationAccess().getUpdateOperationParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalWebserviceDSL.g:551:2: ( ruleDeleteOperation )
                    {
                    // InternalWebserviceDSL.g:551:2: ( ruleDeleteOperation )
                    // InternalWebserviceDSL.g:552:3: ruleDeleteOperation
                    {
                     before(grammarAccess.getOperationAccess().getDeleteOperationParserRuleCall_3()); 
                    pushFollow(FOLLOW_2);
                    ruleDeleteOperation();

                    state._fsp--;

                     after(grammarAccess.getOperationAccess().getDeleteOperationParserRuleCall_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalWebserviceDSL.g:557:2: ( ruleOtherOperation )
                    {
                    // InternalWebserviceDSL.g:557:2: ( ruleOtherOperation )
                    // InternalWebserviceDSL.g:558:3: ruleOtherOperation
                    {
                     before(grammarAccess.getOperationAccess().getOtherOperationParserRuleCall_4()); 
                    pushFollow(FOLLOW_2);
                    ruleOtherOperation();

                    state._fsp--;

                     after(grammarAccess.getOperationAccess().getOtherOperationParserRuleCall_4()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Operation__Alternatives"


    // $ANTLR start "rule__EString__Alternatives"
    // InternalWebserviceDSL.g:567:1: rule__EString__Alternatives : ( ( RULE_STRING ) | ( RULE_ID ) );
    public final void rule__EString__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:571:1: ( ( RULE_STRING ) | ( RULE_ID ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==RULE_STRING) ) {
                alt3=1;
            }
            else if ( (LA3_0==RULE_ID) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalWebserviceDSL.g:572:2: ( RULE_STRING )
                    {
                    // InternalWebserviceDSL.g:572:2: ( RULE_STRING )
                    // InternalWebserviceDSL.g:573:3: RULE_STRING
                    {
                     before(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 
                    match(input,RULE_STRING,FOLLOW_2); 
                     after(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalWebserviceDSL.g:578:2: ( RULE_ID )
                    {
                    // InternalWebserviceDSL.g:578:2: ( RULE_ID )
                    // InternalWebserviceDSL.g:579:3: RULE_ID
                    {
                     before(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 
                    match(input,RULE_ID,FOLLOW_2); 
                     after(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EString__Alternatives"


    // $ANTLR start "rule__API__Group__0"
    // InternalWebserviceDSL.g:588:1: rule__API__Group__0 : rule__API__Group__0__Impl rule__API__Group__1 ;
    public final void rule__API__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:592:1: ( rule__API__Group__0__Impl rule__API__Group__1 )
            // InternalWebserviceDSL.g:593:2: rule__API__Group__0__Impl rule__API__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__API__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__API__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group__0"


    // $ANTLR start "rule__API__Group__0__Impl"
    // InternalWebserviceDSL.g:600:1: rule__API__Group__0__Impl : ( 'API' ) ;
    public final void rule__API__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:604:1: ( ( 'API' ) )
            // InternalWebserviceDSL.g:605:1: ( 'API' )
            {
            // InternalWebserviceDSL.g:605:1: ( 'API' )
            // InternalWebserviceDSL.g:606:2: 'API'
            {
             before(grammarAccess.getAPIAccess().getAPIKeyword_0()); 
            match(input,11,FOLLOW_2); 
             after(grammarAccess.getAPIAccess().getAPIKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group__0__Impl"


    // $ANTLR start "rule__API__Group__1"
    // InternalWebserviceDSL.g:615:1: rule__API__Group__1 : rule__API__Group__1__Impl rule__API__Group__2 ;
    public final void rule__API__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:619:1: ( rule__API__Group__1__Impl rule__API__Group__2 )
            // InternalWebserviceDSL.g:620:2: rule__API__Group__1__Impl rule__API__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__API__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__API__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group__1"


    // $ANTLR start "rule__API__Group__1__Impl"
    // InternalWebserviceDSL.g:627:1: rule__API__Group__1__Impl : ( ( rule__API__NameAssignment_1 ) ) ;
    public final void rule__API__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:631:1: ( ( ( rule__API__NameAssignment_1 ) ) )
            // InternalWebserviceDSL.g:632:1: ( ( rule__API__NameAssignment_1 ) )
            {
            // InternalWebserviceDSL.g:632:1: ( ( rule__API__NameAssignment_1 ) )
            // InternalWebserviceDSL.g:633:2: ( rule__API__NameAssignment_1 )
            {
             before(grammarAccess.getAPIAccess().getNameAssignment_1()); 
            // InternalWebserviceDSL.g:634:2: ( rule__API__NameAssignment_1 )
            // InternalWebserviceDSL.g:634:3: rule__API__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__API__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getAPIAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group__1__Impl"


    // $ANTLR start "rule__API__Group__2"
    // InternalWebserviceDSL.g:642:1: rule__API__Group__2 : rule__API__Group__2__Impl rule__API__Group__3 ;
    public final void rule__API__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:646:1: ( rule__API__Group__2__Impl rule__API__Group__3 )
            // InternalWebserviceDSL.g:647:2: rule__API__Group__2__Impl rule__API__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__API__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__API__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group__2"


    // $ANTLR start "rule__API__Group__2__Impl"
    // InternalWebserviceDSL.g:654:1: rule__API__Group__2__Impl : ( '{' ) ;
    public final void rule__API__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:658:1: ( ( '{' ) )
            // InternalWebserviceDSL.g:659:1: ( '{' )
            {
            // InternalWebserviceDSL.g:659:1: ( '{' )
            // InternalWebserviceDSL.g:660:2: '{'
            {
             before(grammarAccess.getAPIAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getAPIAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group__2__Impl"


    // $ANTLR start "rule__API__Group__3"
    // InternalWebserviceDSL.g:669:1: rule__API__Group__3 : rule__API__Group__3__Impl rule__API__Group__4 ;
    public final void rule__API__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:673:1: ( rule__API__Group__3__Impl rule__API__Group__4 )
            // InternalWebserviceDSL.g:674:2: rule__API__Group__3__Impl rule__API__Group__4
            {
            pushFollow(FOLLOW_5);
            rule__API__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__API__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group__3"


    // $ANTLR start "rule__API__Group__3__Impl"
    // InternalWebserviceDSL.g:681:1: rule__API__Group__3__Impl : ( ( rule__API__Group_3__0 )? ) ;
    public final void rule__API__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:685:1: ( ( ( rule__API__Group_3__0 )? ) )
            // InternalWebserviceDSL.g:686:1: ( ( rule__API__Group_3__0 )? )
            {
            // InternalWebserviceDSL.g:686:1: ( ( rule__API__Group_3__0 )? )
            // InternalWebserviceDSL.g:687:2: ( rule__API__Group_3__0 )?
            {
             before(grammarAccess.getAPIAccess().getGroup_3()); 
            // InternalWebserviceDSL.g:688:2: ( rule__API__Group_3__0 )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==17) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalWebserviceDSL.g:688:3: rule__API__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__API__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAPIAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group__3__Impl"


    // $ANTLR start "rule__API__Group__4"
    // InternalWebserviceDSL.g:696:1: rule__API__Group__4 : rule__API__Group__4__Impl rule__API__Group__5 ;
    public final void rule__API__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:700:1: ( rule__API__Group__4__Impl rule__API__Group__5 )
            // InternalWebserviceDSL.g:701:2: rule__API__Group__4__Impl rule__API__Group__5
            {
            pushFollow(FOLLOW_3);
            rule__API__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__API__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group__4"


    // $ANTLR start "rule__API__Group__4__Impl"
    // InternalWebserviceDSL.g:708:1: rule__API__Group__4__Impl : ( 'url' ) ;
    public final void rule__API__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:712:1: ( ( 'url' ) )
            // InternalWebserviceDSL.g:713:1: ( 'url' )
            {
            // InternalWebserviceDSL.g:713:1: ( 'url' )
            // InternalWebserviceDSL.g:714:2: 'url'
            {
             before(grammarAccess.getAPIAccess().getUrlKeyword_4()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getAPIAccess().getUrlKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group__4__Impl"


    // $ANTLR start "rule__API__Group__5"
    // InternalWebserviceDSL.g:723:1: rule__API__Group__5 : rule__API__Group__5__Impl rule__API__Group__6 ;
    public final void rule__API__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:727:1: ( rule__API__Group__5__Impl rule__API__Group__6 )
            // InternalWebserviceDSL.g:728:2: rule__API__Group__5__Impl rule__API__Group__6
            {
            pushFollow(FOLLOW_6);
            rule__API__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__API__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group__5"


    // $ANTLR start "rule__API__Group__5__Impl"
    // InternalWebserviceDSL.g:735:1: rule__API__Group__5__Impl : ( ( rule__API__UrlAssignment_5 ) ) ;
    public final void rule__API__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:739:1: ( ( ( rule__API__UrlAssignment_5 ) ) )
            // InternalWebserviceDSL.g:740:1: ( ( rule__API__UrlAssignment_5 ) )
            {
            // InternalWebserviceDSL.g:740:1: ( ( rule__API__UrlAssignment_5 ) )
            // InternalWebserviceDSL.g:741:2: ( rule__API__UrlAssignment_5 )
            {
             before(grammarAccess.getAPIAccess().getUrlAssignment_5()); 
            // InternalWebserviceDSL.g:742:2: ( rule__API__UrlAssignment_5 )
            // InternalWebserviceDSL.g:742:3: rule__API__UrlAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__API__UrlAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getAPIAccess().getUrlAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group__5__Impl"


    // $ANTLR start "rule__API__Group__6"
    // InternalWebserviceDSL.g:750:1: rule__API__Group__6 : rule__API__Group__6__Impl rule__API__Group__7 ;
    public final void rule__API__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:754:1: ( rule__API__Group__6__Impl rule__API__Group__7 )
            // InternalWebserviceDSL.g:755:2: rule__API__Group__6__Impl rule__API__Group__7
            {
            pushFollow(FOLLOW_4);
            rule__API__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__API__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group__6"


    // $ANTLR start "rule__API__Group__6__Impl"
    // InternalWebserviceDSL.g:762:1: rule__API__Group__6__Impl : ( 'datatypes' ) ;
    public final void rule__API__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:766:1: ( ( 'datatypes' ) )
            // InternalWebserviceDSL.g:767:1: ( 'datatypes' )
            {
            // InternalWebserviceDSL.g:767:1: ( 'datatypes' )
            // InternalWebserviceDSL.g:768:2: 'datatypes'
            {
             before(grammarAccess.getAPIAccess().getDatatypesKeyword_6()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getAPIAccess().getDatatypesKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group__6__Impl"


    // $ANTLR start "rule__API__Group__7"
    // InternalWebserviceDSL.g:777:1: rule__API__Group__7 : rule__API__Group__7__Impl rule__API__Group__8 ;
    public final void rule__API__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:781:1: ( rule__API__Group__7__Impl rule__API__Group__8 )
            // InternalWebserviceDSL.g:782:2: rule__API__Group__7__Impl rule__API__Group__8
            {
            pushFollow(FOLLOW_7);
            rule__API__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__API__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group__7"


    // $ANTLR start "rule__API__Group__7__Impl"
    // InternalWebserviceDSL.g:789:1: rule__API__Group__7__Impl : ( '{' ) ;
    public final void rule__API__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:793:1: ( ( '{' ) )
            // InternalWebserviceDSL.g:794:1: ( '{' )
            {
            // InternalWebserviceDSL.g:794:1: ( '{' )
            // InternalWebserviceDSL.g:795:2: '{'
            {
             before(grammarAccess.getAPIAccess().getLeftCurlyBracketKeyword_7()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getAPIAccess().getLeftCurlyBracketKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group__7__Impl"


    // $ANTLR start "rule__API__Group__8"
    // InternalWebserviceDSL.g:804:1: rule__API__Group__8 : rule__API__Group__8__Impl rule__API__Group__9 ;
    public final void rule__API__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:808:1: ( rule__API__Group__8__Impl rule__API__Group__9 )
            // InternalWebserviceDSL.g:809:2: rule__API__Group__8__Impl rule__API__Group__9
            {
            pushFollow(FOLLOW_8);
            rule__API__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__API__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group__8"


    // $ANTLR start "rule__API__Group__8__Impl"
    // InternalWebserviceDSL.g:816:1: rule__API__Group__8__Impl : ( ( rule__API__DatatypesAssignment_8 ) ) ;
    public final void rule__API__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:820:1: ( ( ( rule__API__DatatypesAssignment_8 ) ) )
            // InternalWebserviceDSL.g:821:1: ( ( rule__API__DatatypesAssignment_8 ) )
            {
            // InternalWebserviceDSL.g:821:1: ( ( rule__API__DatatypesAssignment_8 ) )
            // InternalWebserviceDSL.g:822:2: ( rule__API__DatatypesAssignment_8 )
            {
             before(grammarAccess.getAPIAccess().getDatatypesAssignment_8()); 
            // InternalWebserviceDSL.g:823:2: ( rule__API__DatatypesAssignment_8 )
            // InternalWebserviceDSL.g:823:3: rule__API__DatatypesAssignment_8
            {
            pushFollow(FOLLOW_2);
            rule__API__DatatypesAssignment_8();

            state._fsp--;


            }

             after(grammarAccess.getAPIAccess().getDatatypesAssignment_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group__8__Impl"


    // $ANTLR start "rule__API__Group__9"
    // InternalWebserviceDSL.g:831:1: rule__API__Group__9 : rule__API__Group__9__Impl rule__API__Group__10 ;
    public final void rule__API__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:835:1: ( rule__API__Group__9__Impl rule__API__Group__10 )
            // InternalWebserviceDSL.g:836:2: rule__API__Group__9__Impl rule__API__Group__10
            {
            pushFollow(FOLLOW_8);
            rule__API__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__API__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group__9"


    // $ANTLR start "rule__API__Group__9__Impl"
    // InternalWebserviceDSL.g:843:1: rule__API__Group__9__Impl : ( ( rule__API__Group_9__0 )* ) ;
    public final void rule__API__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:847:1: ( ( ( rule__API__Group_9__0 )* ) )
            // InternalWebserviceDSL.g:848:1: ( ( rule__API__Group_9__0 )* )
            {
            // InternalWebserviceDSL.g:848:1: ( ( rule__API__Group_9__0 )* )
            // InternalWebserviceDSL.g:849:2: ( rule__API__Group_9__0 )*
            {
             before(grammarAccess.getAPIAccess().getGroup_9()); 
            // InternalWebserviceDSL.g:850:2: ( rule__API__Group_9__0 )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==18) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalWebserviceDSL.g:850:3: rule__API__Group_9__0
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__API__Group_9__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

             after(grammarAccess.getAPIAccess().getGroup_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group__9__Impl"


    // $ANTLR start "rule__API__Group__10"
    // InternalWebserviceDSL.g:858:1: rule__API__Group__10 : rule__API__Group__10__Impl rule__API__Group__11 ;
    public final void rule__API__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:862:1: ( rule__API__Group__10__Impl rule__API__Group__11 )
            // InternalWebserviceDSL.g:863:2: rule__API__Group__10__Impl rule__API__Group__11
            {
            pushFollow(FOLLOW_10);
            rule__API__Group__10__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__API__Group__11();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group__10"


    // $ANTLR start "rule__API__Group__10__Impl"
    // InternalWebserviceDSL.g:870:1: rule__API__Group__10__Impl : ( '}' ) ;
    public final void rule__API__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:874:1: ( ( '}' ) )
            // InternalWebserviceDSL.g:875:1: ( '}' )
            {
            // InternalWebserviceDSL.g:875:1: ( '}' )
            // InternalWebserviceDSL.g:876:2: '}'
            {
             before(grammarAccess.getAPIAccess().getRightCurlyBracketKeyword_10()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getAPIAccess().getRightCurlyBracketKeyword_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group__10__Impl"


    // $ANTLR start "rule__API__Group__11"
    // InternalWebserviceDSL.g:885:1: rule__API__Group__11 : rule__API__Group__11__Impl rule__API__Group__12 ;
    public final void rule__API__Group__11() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:889:1: ( rule__API__Group__11__Impl rule__API__Group__12 )
            // InternalWebserviceDSL.g:890:2: rule__API__Group__11__Impl rule__API__Group__12
            {
            pushFollow(FOLLOW_4);
            rule__API__Group__11__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__API__Group__12();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group__11"


    // $ANTLR start "rule__API__Group__11__Impl"
    // InternalWebserviceDSL.g:897:1: rule__API__Group__11__Impl : ( 'operations' ) ;
    public final void rule__API__Group__11__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:901:1: ( ( 'operations' ) )
            // InternalWebserviceDSL.g:902:1: ( 'operations' )
            {
            // InternalWebserviceDSL.g:902:1: ( 'operations' )
            // InternalWebserviceDSL.g:903:2: 'operations'
            {
             before(grammarAccess.getAPIAccess().getOperationsKeyword_11()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getAPIAccess().getOperationsKeyword_11()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group__11__Impl"


    // $ANTLR start "rule__API__Group__12"
    // InternalWebserviceDSL.g:912:1: rule__API__Group__12 : rule__API__Group__12__Impl rule__API__Group__13 ;
    public final void rule__API__Group__12() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:916:1: ( rule__API__Group__12__Impl rule__API__Group__13 )
            // InternalWebserviceDSL.g:917:2: rule__API__Group__12__Impl rule__API__Group__13
            {
            pushFollow(FOLLOW_11);
            rule__API__Group__12__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__API__Group__13();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group__12"


    // $ANTLR start "rule__API__Group__12__Impl"
    // InternalWebserviceDSL.g:924:1: rule__API__Group__12__Impl : ( '{' ) ;
    public final void rule__API__Group__12__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:928:1: ( ( '{' ) )
            // InternalWebserviceDSL.g:929:1: ( '{' )
            {
            // InternalWebserviceDSL.g:929:1: ( '{' )
            // InternalWebserviceDSL.g:930:2: '{'
            {
             before(grammarAccess.getAPIAccess().getLeftCurlyBracketKeyword_12()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getAPIAccess().getLeftCurlyBracketKeyword_12()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group__12__Impl"


    // $ANTLR start "rule__API__Group__13"
    // InternalWebserviceDSL.g:939:1: rule__API__Group__13 : rule__API__Group__13__Impl rule__API__Group__14 ;
    public final void rule__API__Group__13() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:943:1: ( rule__API__Group__13__Impl rule__API__Group__14 )
            // InternalWebserviceDSL.g:944:2: rule__API__Group__13__Impl rule__API__Group__14
            {
            pushFollow(FOLLOW_8);
            rule__API__Group__13__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__API__Group__14();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group__13"


    // $ANTLR start "rule__API__Group__13__Impl"
    // InternalWebserviceDSL.g:951:1: rule__API__Group__13__Impl : ( ( rule__API__OperationsAssignment_13 ) ) ;
    public final void rule__API__Group__13__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:955:1: ( ( ( rule__API__OperationsAssignment_13 ) ) )
            // InternalWebserviceDSL.g:956:1: ( ( rule__API__OperationsAssignment_13 ) )
            {
            // InternalWebserviceDSL.g:956:1: ( ( rule__API__OperationsAssignment_13 ) )
            // InternalWebserviceDSL.g:957:2: ( rule__API__OperationsAssignment_13 )
            {
             before(grammarAccess.getAPIAccess().getOperationsAssignment_13()); 
            // InternalWebserviceDSL.g:958:2: ( rule__API__OperationsAssignment_13 )
            // InternalWebserviceDSL.g:958:3: rule__API__OperationsAssignment_13
            {
            pushFollow(FOLLOW_2);
            rule__API__OperationsAssignment_13();

            state._fsp--;


            }

             after(grammarAccess.getAPIAccess().getOperationsAssignment_13()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group__13__Impl"


    // $ANTLR start "rule__API__Group__14"
    // InternalWebserviceDSL.g:966:1: rule__API__Group__14 : rule__API__Group__14__Impl rule__API__Group__15 ;
    public final void rule__API__Group__14() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:970:1: ( rule__API__Group__14__Impl rule__API__Group__15 )
            // InternalWebserviceDSL.g:971:2: rule__API__Group__14__Impl rule__API__Group__15
            {
            pushFollow(FOLLOW_8);
            rule__API__Group__14__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__API__Group__15();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group__14"


    // $ANTLR start "rule__API__Group__14__Impl"
    // InternalWebserviceDSL.g:978:1: rule__API__Group__14__Impl : ( ( rule__API__Group_14__0 )* ) ;
    public final void rule__API__Group__14__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:982:1: ( ( ( rule__API__Group_14__0 )* ) )
            // InternalWebserviceDSL.g:983:1: ( ( rule__API__Group_14__0 )* )
            {
            // InternalWebserviceDSL.g:983:1: ( ( rule__API__Group_14__0 )* )
            // InternalWebserviceDSL.g:984:2: ( rule__API__Group_14__0 )*
            {
             before(grammarAccess.getAPIAccess().getGroup_14()); 
            // InternalWebserviceDSL.g:985:2: ( rule__API__Group_14__0 )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==18) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalWebserviceDSL.g:985:3: rule__API__Group_14__0
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__API__Group_14__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

             after(grammarAccess.getAPIAccess().getGroup_14()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group__14__Impl"


    // $ANTLR start "rule__API__Group__15"
    // InternalWebserviceDSL.g:993:1: rule__API__Group__15 : rule__API__Group__15__Impl rule__API__Group__16 ;
    public final void rule__API__Group__15() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:997:1: ( rule__API__Group__15__Impl rule__API__Group__16 )
            // InternalWebserviceDSL.g:998:2: rule__API__Group__15__Impl rule__API__Group__16
            {
            pushFollow(FOLLOW_12);
            rule__API__Group__15__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__API__Group__16();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group__15"


    // $ANTLR start "rule__API__Group__15__Impl"
    // InternalWebserviceDSL.g:1005:1: rule__API__Group__15__Impl : ( '}' ) ;
    public final void rule__API__Group__15__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1009:1: ( ( '}' ) )
            // InternalWebserviceDSL.g:1010:1: ( '}' )
            {
            // InternalWebserviceDSL.g:1010:1: ( '}' )
            // InternalWebserviceDSL.g:1011:2: '}'
            {
             before(grammarAccess.getAPIAccess().getRightCurlyBracketKeyword_15()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getAPIAccess().getRightCurlyBracketKeyword_15()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group__15__Impl"


    // $ANTLR start "rule__API__Group__16"
    // InternalWebserviceDSL.g:1020:1: rule__API__Group__16 : rule__API__Group__16__Impl ;
    public final void rule__API__Group__16() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1024:1: ( rule__API__Group__16__Impl )
            // InternalWebserviceDSL.g:1025:2: rule__API__Group__16__Impl
            {
            pushFollow(FOLLOW_2);
            rule__API__Group__16__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group__16"


    // $ANTLR start "rule__API__Group__16__Impl"
    // InternalWebserviceDSL.g:1031:1: rule__API__Group__16__Impl : ( '}' ) ;
    public final void rule__API__Group__16__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1035:1: ( ( '}' ) )
            // InternalWebserviceDSL.g:1036:1: ( '}' )
            {
            // InternalWebserviceDSL.g:1036:1: ( '}' )
            // InternalWebserviceDSL.g:1037:2: '}'
            {
             before(grammarAccess.getAPIAccess().getRightCurlyBracketKeyword_16()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getAPIAccess().getRightCurlyBracketKeyword_16()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group__16__Impl"


    // $ANTLR start "rule__API__Group_3__0"
    // InternalWebserviceDSL.g:1047:1: rule__API__Group_3__0 : rule__API__Group_3__0__Impl rule__API__Group_3__1 ;
    public final void rule__API__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1051:1: ( rule__API__Group_3__0__Impl rule__API__Group_3__1 )
            // InternalWebserviceDSL.g:1052:2: rule__API__Group_3__0__Impl rule__API__Group_3__1
            {
            pushFollow(FOLLOW_3);
            rule__API__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__API__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group_3__0"


    // $ANTLR start "rule__API__Group_3__0__Impl"
    // InternalWebserviceDSL.g:1059:1: rule__API__Group_3__0__Impl : ( 'description' ) ;
    public final void rule__API__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1063:1: ( ( 'description' ) )
            // InternalWebserviceDSL.g:1064:1: ( 'description' )
            {
            // InternalWebserviceDSL.g:1064:1: ( 'description' )
            // InternalWebserviceDSL.g:1065:2: 'description'
            {
             before(grammarAccess.getAPIAccess().getDescriptionKeyword_3_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getAPIAccess().getDescriptionKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group_3__0__Impl"


    // $ANTLR start "rule__API__Group_3__1"
    // InternalWebserviceDSL.g:1074:1: rule__API__Group_3__1 : rule__API__Group_3__1__Impl ;
    public final void rule__API__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1078:1: ( rule__API__Group_3__1__Impl )
            // InternalWebserviceDSL.g:1079:2: rule__API__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__API__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group_3__1"


    // $ANTLR start "rule__API__Group_3__1__Impl"
    // InternalWebserviceDSL.g:1085:1: rule__API__Group_3__1__Impl : ( ( rule__API__DescriptionAssignment_3_1 ) ) ;
    public final void rule__API__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1089:1: ( ( ( rule__API__DescriptionAssignment_3_1 ) ) )
            // InternalWebserviceDSL.g:1090:1: ( ( rule__API__DescriptionAssignment_3_1 ) )
            {
            // InternalWebserviceDSL.g:1090:1: ( ( rule__API__DescriptionAssignment_3_1 ) )
            // InternalWebserviceDSL.g:1091:2: ( rule__API__DescriptionAssignment_3_1 )
            {
             before(grammarAccess.getAPIAccess().getDescriptionAssignment_3_1()); 
            // InternalWebserviceDSL.g:1092:2: ( rule__API__DescriptionAssignment_3_1 )
            // InternalWebserviceDSL.g:1092:3: rule__API__DescriptionAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__API__DescriptionAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getAPIAccess().getDescriptionAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group_3__1__Impl"


    // $ANTLR start "rule__API__Group_9__0"
    // InternalWebserviceDSL.g:1101:1: rule__API__Group_9__0 : rule__API__Group_9__0__Impl rule__API__Group_9__1 ;
    public final void rule__API__Group_9__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1105:1: ( rule__API__Group_9__0__Impl rule__API__Group_9__1 )
            // InternalWebserviceDSL.g:1106:2: rule__API__Group_9__0__Impl rule__API__Group_9__1
            {
            pushFollow(FOLLOW_7);
            rule__API__Group_9__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__API__Group_9__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group_9__0"


    // $ANTLR start "rule__API__Group_9__0__Impl"
    // InternalWebserviceDSL.g:1113:1: rule__API__Group_9__0__Impl : ( ',' ) ;
    public final void rule__API__Group_9__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1117:1: ( ( ',' ) )
            // InternalWebserviceDSL.g:1118:1: ( ',' )
            {
            // InternalWebserviceDSL.g:1118:1: ( ',' )
            // InternalWebserviceDSL.g:1119:2: ','
            {
             before(grammarAccess.getAPIAccess().getCommaKeyword_9_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getAPIAccess().getCommaKeyword_9_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group_9__0__Impl"


    // $ANTLR start "rule__API__Group_9__1"
    // InternalWebserviceDSL.g:1128:1: rule__API__Group_9__1 : rule__API__Group_9__1__Impl ;
    public final void rule__API__Group_9__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1132:1: ( rule__API__Group_9__1__Impl )
            // InternalWebserviceDSL.g:1133:2: rule__API__Group_9__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__API__Group_9__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group_9__1"


    // $ANTLR start "rule__API__Group_9__1__Impl"
    // InternalWebserviceDSL.g:1139:1: rule__API__Group_9__1__Impl : ( ( rule__API__DatatypesAssignment_9_1 ) ) ;
    public final void rule__API__Group_9__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1143:1: ( ( ( rule__API__DatatypesAssignment_9_1 ) ) )
            // InternalWebserviceDSL.g:1144:1: ( ( rule__API__DatatypesAssignment_9_1 ) )
            {
            // InternalWebserviceDSL.g:1144:1: ( ( rule__API__DatatypesAssignment_9_1 ) )
            // InternalWebserviceDSL.g:1145:2: ( rule__API__DatatypesAssignment_9_1 )
            {
             before(grammarAccess.getAPIAccess().getDatatypesAssignment_9_1()); 
            // InternalWebserviceDSL.g:1146:2: ( rule__API__DatatypesAssignment_9_1 )
            // InternalWebserviceDSL.g:1146:3: rule__API__DatatypesAssignment_9_1
            {
            pushFollow(FOLLOW_2);
            rule__API__DatatypesAssignment_9_1();

            state._fsp--;


            }

             after(grammarAccess.getAPIAccess().getDatatypesAssignment_9_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group_9__1__Impl"


    // $ANTLR start "rule__API__Group_14__0"
    // InternalWebserviceDSL.g:1155:1: rule__API__Group_14__0 : rule__API__Group_14__0__Impl rule__API__Group_14__1 ;
    public final void rule__API__Group_14__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1159:1: ( rule__API__Group_14__0__Impl rule__API__Group_14__1 )
            // InternalWebserviceDSL.g:1160:2: rule__API__Group_14__0__Impl rule__API__Group_14__1
            {
            pushFollow(FOLLOW_11);
            rule__API__Group_14__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__API__Group_14__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group_14__0"


    // $ANTLR start "rule__API__Group_14__0__Impl"
    // InternalWebserviceDSL.g:1167:1: rule__API__Group_14__0__Impl : ( ',' ) ;
    public final void rule__API__Group_14__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1171:1: ( ( ',' ) )
            // InternalWebserviceDSL.g:1172:1: ( ',' )
            {
            // InternalWebserviceDSL.g:1172:1: ( ',' )
            // InternalWebserviceDSL.g:1173:2: ','
            {
             before(grammarAccess.getAPIAccess().getCommaKeyword_14_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getAPIAccess().getCommaKeyword_14_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group_14__0__Impl"


    // $ANTLR start "rule__API__Group_14__1"
    // InternalWebserviceDSL.g:1182:1: rule__API__Group_14__1 : rule__API__Group_14__1__Impl ;
    public final void rule__API__Group_14__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1186:1: ( rule__API__Group_14__1__Impl )
            // InternalWebserviceDSL.g:1187:2: rule__API__Group_14__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__API__Group_14__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group_14__1"


    // $ANTLR start "rule__API__Group_14__1__Impl"
    // InternalWebserviceDSL.g:1193:1: rule__API__Group_14__1__Impl : ( ( rule__API__OperationsAssignment_14_1 ) ) ;
    public final void rule__API__Group_14__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1197:1: ( ( ( rule__API__OperationsAssignment_14_1 ) ) )
            // InternalWebserviceDSL.g:1198:1: ( ( rule__API__OperationsAssignment_14_1 ) )
            {
            // InternalWebserviceDSL.g:1198:1: ( ( rule__API__OperationsAssignment_14_1 ) )
            // InternalWebserviceDSL.g:1199:2: ( rule__API__OperationsAssignment_14_1 )
            {
             before(grammarAccess.getAPIAccess().getOperationsAssignment_14_1()); 
            // InternalWebserviceDSL.g:1200:2: ( rule__API__OperationsAssignment_14_1 )
            // InternalWebserviceDSL.g:1200:3: rule__API__OperationsAssignment_14_1
            {
            pushFollow(FOLLOW_2);
            rule__API__OperationsAssignment_14_1();

            state._fsp--;


            }

             after(grammarAccess.getAPIAccess().getOperationsAssignment_14_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__Group_14__1__Impl"


    // $ANTLR start "rule__Parameter__Group__0"
    // InternalWebserviceDSL.g:1209:1: rule__Parameter__Group__0 : rule__Parameter__Group__0__Impl rule__Parameter__Group__1 ;
    public final void rule__Parameter__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1213:1: ( rule__Parameter__Group__0__Impl rule__Parameter__Group__1 )
            // InternalWebserviceDSL.g:1214:2: rule__Parameter__Group__0__Impl rule__Parameter__Group__1
            {
            pushFollow(FOLLOW_13);
            rule__Parameter__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Parameter__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__0"


    // $ANTLR start "rule__Parameter__Group__0__Impl"
    // InternalWebserviceDSL.g:1221:1: rule__Parameter__Group__0__Impl : ( ( rule__Parameter__RequiredAssignment_0 )? ) ;
    public final void rule__Parameter__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1225:1: ( ( ( rule__Parameter__RequiredAssignment_0 )? ) )
            // InternalWebserviceDSL.g:1226:1: ( ( rule__Parameter__RequiredAssignment_0 )? )
            {
            // InternalWebserviceDSL.g:1226:1: ( ( rule__Parameter__RequiredAssignment_0 )? )
            // InternalWebserviceDSL.g:1227:2: ( rule__Parameter__RequiredAssignment_0 )?
            {
             before(grammarAccess.getParameterAccess().getRequiredAssignment_0()); 
            // InternalWebserviceDSL.g:1228:2: ( rule__Parameter__RequiredAssignment_0 )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==40) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalWebserviceDSL.g:1228:3: rule__Parameter__RequiredAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Parameter__RequiredAssignment_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getParameterAccess().getRequiredAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__0__Impl"


    // $ANTLR start "rule__Parameter__Group__1"
    // InternalWebserviceDSL.g:1236:1: rule__Parameter__Group__1 : rule__Parameter__Group__1__Impl rule__Parameter__Group__2 ;
    public final void rule__Parameter__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1240:1: ( rule__Parameter__Group__1__Impl rule__Parameter__Group__2 )
            // InternalWebserviceDSL.g:1241:2: rule__Parameter__Group__1__Impl rule__Parameter__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Parameter__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Parameter__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__1"


    // $ANTLR start "rule__Parameter__Group__1__Impl"
    // InternalWebserviceDSL.g:1248:1: rule__Parameter__Group__1__Impl : ( 'Parameter' ) ;
    public final void rule__Parameter__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1252:1: ( ( 'Parameter' ) )
            // InternalWebserviceDSL.g:1253:1: ( 'Parameter' )
            {
            // InternalWebserviceDSL.g:1253:1: ( 'Parameter' )
            // InternalWebserviceDSL.g:1254:2: 'Parameter'
            {
             before(grammarAccess.getParameterAccess().getParameterKeyword_1()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getParameterAccess().getParameterKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__1__Impl"


    // $ANTLR start "rule__Parameter__Group__2"
    // InternalWebserviceDSL.g:1263:1: rule__Parameter__Group__2 : rule__Parameter__Group__2__Impl rule__Parameter__Group__3 ;
    public final void rule__Parameter__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1267:1: ( rule__Parameter__Group__2__Impl rule__Parameter__Group__3 )
            // InternalWebserviceDSL.g:1268:2: rule__Parameter__Group__2__Impl rule__Parameter__Group__3
            {
            pushFollow(FOLLOW_14);
            rule__Parameter__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Parameter__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__2"


    // $ANTLR start "rule__Parameter__Group__2__Impl"
    // InternalWebserviceDSL.g:1275:1: rule__Parameter__Group__2__Impl : ( '{' ) ;
    public final void rule__Parameter__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1279:1: ( ( '{' ) )
            // InternalWebserviceDSL.g:1280:1: ( '{' )
            {
            // InternalWebserviceDSL.g:1280:1: ( '{' )
            // InternalWebserviceDSL.g:1281:2: '{'
            {
             before(grammarAccess.getParameterAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getParameterAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__2__Impl"


    // $ANTLR start "rule__Parameter__Group__3"
    // InternalWebserviceDSL.g:1290:1: rule__Parameter__Group__3 : rule__Parameter__Group__3__Impl rule__Parameter__Group__4 ;
    public final void rule__Parameter__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1294:1: ( rule__Parameter__Group__3__Impl rule__Parameter__Group__4 )
            // InternalWebserviceDSL.g:1295:2: rule__Parameter__Group__3__Impl rule__Parameter__Group__4
            {
            pushFollow(FOLLOW_14);
            rule__Parameter__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Parameter__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__3"


    // $ANTLR start "rule__Parameter__Group__3__Impl"
    // InternalWebserviceDSL.g:1302:1: rule__Parameter__Group__3__Impl : ( ( rule__Parameter__Group_3__0 )? ) ;
    public final void rule__Parameter__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1306:1: ( ( ( rule__Parameter__Group_3__0 )? ) )
            // InternalWebserviceDSL.g:1307:1: ( ( rule__Parameter__Group_3__0 )? )
            {
            // InternalWebserviceDSL.g:1307:1: ( ( rule__Parameter__Group_3__0 )? )
            // InternalWebserviceDSL.g:1308:2: ( rule__Parameter__Group_3__0 )?
            {
             before(grammarAccess.getParameterAccess().getGroup_3()); 
            // InternalWebserviceDSL.g:1309:2: ( rule__Parameter__Group_3__0 )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==17) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalWebserviceDSL.g:1309:3: rule__Parameter__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Parameter__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getParameterAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__3__Impl"


    // $ANTLR start "rule__Parameter__Group__4"
    // InternalWebserviceDSL.g:1317:1: rule__Parameter__Group__4 : rule__Parameter__Group__4__Impl rule__Parameter__Group__5 ;
    public final void rule__Parameter__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1321:1: ( rule__Parameter__Group__4__Impl rule__Parameter__Group__5 )
            // InternalWebserviceDSL.g:1322:2: rule__Parameter__Group__4__Impl rule__Parameter__Group__5
            {
            pushFollow(FOLLOW_3);
            rule__Parameter__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Parameter__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__4"


    // $ANTLR start "rule__Parameter__Group__4__Impl"
    // InternalWebserviceDSL.g:1329:1: rule__Parameter__Group__4__Impl : ( 'datatype' ) ;
    public final void rule__Parameter__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1333:1: ( ( 'datatype' ) )
            // InternalWebserviceDSL.g:1334:1: ( 'datatype' )
            {
            // InternalWebserviceDSL.g:1334:1: ( 'datatype' )
            // InternalWebserviceDSL.g:1335:2: 'datatype'
            {
             before(grammarAccess.getParameterAccess().getDatatypeKeyword_4()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getParameterAccess().getDatatypeKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__4__Impl"


    // $ANTLR start "rule__Parameter__Group__5"
    // InternalWebserviceDSL.g:1344:1: rule__Parameter__Group__5 : rule__Parameter__Group__5__Impl rule__Parameter__Group__6 ;
    public final void rule__Parameter__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1348:1: ( rule__Parameter__Group__5__Impl rule__Parameter__Group__6 )
            // InternalWebserviceDSL.g:1349:2: rule__Parameter__Group__5__Impl rule__Parameter__Group__6
            {
            pushFollow(FOLLOW_12);
            rule__Parameter__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Parameter__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__5"


    // $ANTLR start "rule__Parameter__Group__5__Impl"
    // InternalWebserviceDSL.g:1356:1: rule__Parameter__Group__5__Impl : ( ( rule__Parameter__DatatypeAssignment_5 ) ) ;
    public final void rule__Parameter__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1360:1: ( ( ( rule__Parameter__DatatypeAssignment_5 ) ) )
            // InternalWebserviceDSL.g:1361:1: ( ( rule__Parameter__DatatypeAssignment_5 ) )
            {
            // InternalWebserviceDSL.g:1361:1: ( ( rule__Parameter__DatatypeAssignment_5 ) )
            // InternalWebserviceDSL.g:1362:2: ( rule__Parameter__DatatypeAssignment_5 )
            {
             before(grammarAccess.getParameterAccess().getDatatypeAssignment_5()); 
            // InternalWebserviceDSL.g:1363:2: ( rule__Parameter__DatatypeAssignment_5 )
            // InternalWebserviceDSL.g:1363:3: rule__Parameter__DatatypeAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__Parameter__DatatypeAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getParameterAccess().getDatatypeAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__5__Impl"


    // $ANTLR start "rule__Parameter__Group__6"
    // InternalWebserviceDSL.g:1371:1: rule__Parameter__Group__6 : rule__Parameter__Group__6__Impl ;
    public final void rule__Parameter__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1375:1: ( rule__Parameter__Group__6__Impl )
            // InternalWebserviceDSL.g:1376:2: rule__Parameter__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Parameter__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__6"


    // $ANTLR start "rule__Parameter__Group__6__Impl"
    // InternalWebserviceDSL.g:1382:1: rule__Parameter__Group__6__Impl : ( '}' ) ;
    public final void rule__Parameter__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1386:1: ( ( '}' ) )
            // InternalWebserviceDSL.g:1387:1: ( '}' )
            {
            // InternalWebserviceDSL.g:1387:1: ( '}' )
            // InternalWebserviceDSL.g:1388:2: '}'
            {
             before(grammarAccess.getParameterAccess().getRightCurlyBracketKeyword_6()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getParameterAccess().getRightCurlyBracketKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__6__Impl"


    // $ANTLR start "rule__Parameter__Group_3__0"
    // InternalWebserviceDSL.g:1398:1: rule__Parameter__Group_3__0 : rule__Parameter__Group_3__0__Impl rule__Parameter__Group_3__1 ;
    public final void rule__Parameter__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1402:1: ( rule__Parameter__Group_3__0__Impl rule__Parameter__Group_3__1 )
            // InternalWebserviceDSL.g:1403:2: rule__Parameter__Group_3__0__Impl rule__Parameter__Group_3__1
            {
            pushFollow(FOLLOW_3);
            rule__Parameter__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Parameter__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group_3__0"


    // $ANTLR start "rule__Parameter__Group_3__0__Impl"
    // InternalWebserviceDSL.g:1410:1: rule__Parameter__Group_3__0__Impl : ( 'description' ) ;
    public final void rule__Parameter__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1414:1: ( ( 'description' ) )
            // InternalWebserviceDSL.g:1415:1: ( 'description' )
            {
            // InternalWebserviceDSL.g:1415:1: ( 'description' )
            // InternalWebserviceDSL.g:1416:2: 'description'
            {
             before(grammarAccess.getParameterAccess().getDescriptionKeyword_3_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getParameterAccess().getDescriptionKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group_3__0__Impl"


    // $ANTLR start "rule__Parameter__Group_3__1"
    // InternalWebserviceDSL.g:1425:1: rule__Parameter__Group_3__1 : rule__Parameter__Group_3__1__Impl ;
    public final void rule__Parameter__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1429:1: ( rule__Parameter__Group_3__1__Impl )
            // InternalWebserviceDSL.g:1430:2: rule__Parameter__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Parameter__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group_3__1"


    // $ANTLR start "rule__Parameter__Group_3__1__Impl"
    // InternalWebserviceDSL.g:1436:1: rule__Parameter__Group_3__1__Impl : ( ( rule__Parameter__DescriptionAssignment_3_1 ) ) ;
    public final void rule__Parameter__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1440:1: ( ( ( rule__Parameter__DescriptionAssignment_3_1 ) ) )
            // InternalWebserviceDSL.g:1441:1: ( ( rule__Parameter__DescriptionAssignment_3_1 ) )
            {
            // InternalWebserviceDSL.g:1441:1: ( ( rule__Parameter__DescriptionAssignment_3_1 ) )
            // InternalWebserviceDSL.g:1442:2: ( rule__Parameter__DescriptionAssignment_3_1 )
            {
             before(grammarAccess.getParameterAccess().getDescriptionAssignment_3_1()); 
            // InternalWebserviceDSL.g:1443:2: ( rule__Parameter__DescriptionAssignment_3_1 )
            // InternalWebserviceDSL.g:1443:3: rule__Parameter__DescriptionAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Parameter__DescriptionAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getParameterAccess().getDescriptionAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group_3__1__Impl"


    // $ANTLR start "rule__ReadOperation__Group__0"
    // InternalWebserviceDSL.g:1452:1: rule__ReadOperation__Group__0 : rule__ReadOperation__Group__0__Impl rule__ReadOperation__Group__1 ;
    public final void rule__ReadOperation__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1456:1: ( rule__ReadOperation__Group__0__Impl rule__ReadOperation__Group__1 )
            // InternalWebserviceDSL.g:1457:2: rule__ReadOperation__Group__0__Impl rule__ReadOperation__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__ReadOperation__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ReadOperation__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group__0"


    // $ANTLR start "rule__ReadOperation__Group__0__Impl"
    // InternalWebserviceDSL.g:1464:1: rule__ReadOperation__Group__0__Impl : ( 'ReadOperation' ) ;
    public final void rule__ReadOperation__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1468:1: ( ( 'ReadOperation' ) )
            // InternalWebserviceDSL.g:1469:1: ( 'ReadOperation' )
            {
            // InternalWebserviceDSL.g:1469:1: ( 'ReadOperation' )
            // InternalWebserviceDSL.g:1470:2: 'ReadOperation'
            {
             before(grammarAccess.getReadOperationAccess().getReadOperationKeyword_0()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getReadOperationAccess().getReadOperationKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group__0__Impl"


    // $ANTLR start "rule__ReadOperation__Group__1"
    // InternalWebserviceDSL.g:1479:1: rule__ReadOperation__Group__1 : rule__ReadOperation__Group__1__Impl rule__ReadOperation__Group__2 ;
    public final void rule__ReadOperation__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1483:1: ( rule__ReadOperation__Group__1__Impl rule__ReadOperation__Group__2 )
            // InternalWebserviceDSL.g:1484:2: rule__ReadOperation__Group__1__Impl rule__ReadOperation__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__ReadOperation__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ReadOperation__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group__1"


    // $ANTLR start "rule__ReadOperation__Group__1__Impl"
    // InternalWebserviceDSL.g:1491:1: rule__ReadOperation__Group__1__Impl : ( ( rule__ReadOperation__NameAssignment_1 ) ) ;
    public final void rule__ReadOperation__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1495:1: ( ( ( rule__ReadOperation__NameAssignment_1 ) ) )
            // InternalWebserviceDSL.g:1496:1: ( ( rule__ReadOperation__NameAssignment_1 ) )
            {
            // InternalWebserviceDSL.g:1496:1: ( ( rule__ReadOperation__NameAssignment_1 ) )
            // InternalWebserviceDSL.g:1497:2: ( rule__ReadOperation__NameAssignment_1 )
            {
             before(grammarAccess.getReadOperationAccess().getNameAssignment_1()); 
            // InternalWebserviceDSL.g:1498:2: ( rule__ReadOperation__NameAssignment_1 )
            // InternalWebserviceDSL.g:1498:3: rule__ReadOperation__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__ReadOperation__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getReadOperationAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group__1__Impl"


    // $ANTLR start "rule__ReadOperation__Group__2"
    // InternalWebserviceDSL.g:1506:1: rule__ReadOperation__Group__2 : rule__ReadOperation__Group__2__Impl rule__ReadOperation__Group__3 ;
    public final void rule__ReadOperation__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1510:1: ( rule__ReadOperation__Group__2__Impl rule__ReadOperation__Group__3 )
            // InternalWebserviceDSL.g:1511:2: rule__ReadOperation__Group__2__Impl rule__ReadOperation__Group__3
            {
            pushFollow(FOLLOW_15);
            rule__ReadOperation__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ReadOperation__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group__2"


    // $ANTLR start "rule__ReadOperation__Group__2__Impl"
    // InternalWebserviceDSL.g:1518:1: rule__ReadOperation__Group__2__Impl : ( '{' ) ;
    public final void rule__ReadOperation__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1522:1: ( ( '{' ) )
            // InternalWebserviceDSL.g:1523:1: ( '{' )
            {
            // InternalWebserviceDSL.g:1523:1: ( '{' )
            // InternalWebserviceDSL.g:1524:2: '{'
            {
             before(grammarAccess.getReadOperationAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getReadOperationAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group__2__Impl"


    // $ANTLR start "rule__ReadOperation__Group__3"
    // InternalWebserviceDSL.g:1533:1: rule__ReadOperation__Group__3 : rule__ReadOperation__Group__3__Impl rule__ReadOperation__Group__4 ;
    public final void rule__ReadOperation__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1537:1: ( rule__ReadOperation__Group__3__Impl rule__ReadOperation__Group__4 )
            // InternalWebserviceDSL.g:1538:2: rule__ReadOperation__Group__3__Impl rule__ReadOperation__Group__4
            {
            pushFollow(FOLLOW_15);
            rule__ReadOperation__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ReadOperation__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group__3"


    // $ANTLR start "rule__ReadOperation__Group__3__Impl"
    // InternalWebserviceDSL.g:1545:1: rule__ReadOperation__Group__3__Impl : ( ( rule__ReadOperation__Group_3__0 )? ) ;
    public final void rule__ReadOperation__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1549:1: ( ( ( rule__ReadOperation__Group_3__0 )? ) )
            // InternalWebserviceDSL.g:1550:1: ( ( rule__ReadOperation__Group_3__0 )? )
            {
            // InternalWebserviceDSL.g:1550:1: ( ( rule__ReadOperation__Group_3__0 )? )
            // InternalWebserviceDSL.g:1551:2: ( rule__ReadOperation__Group_3__0 )?
            {
             before(grammarAccess.getReadOperationAccess().getGroup_3()); 
            // InternalWebserviceDSL.g:1552:2: ( rule__ReadOperation__Group_3__0 )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==17) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalWebserviceDSL.g:1552:3: rule__ReadOperation__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__ReadOperation__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getReadOperationAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group__3__Impl"


    // $ANTLR start "rule__ReadOperation__Group__4"
    // InternalWebserviceDSL.g:1560:1: rule__ReadOperation__Group__4 : rule__ReadOperation__Group__4__Impl rule__ReadOperation__Group__5 ;
    public final void rule__ReadOperation__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1564:1: ( rule__ReadOperation__Group__4__Impl rule__ReadOperation__Group__5 )
            // InternalWebserviceDSL.g:1565:2: rule__ReadOperation__Group__4__Impl rule__ReadOperation__Group__5
            {
            pushFollow(FOLLOW_15);
            rule__ReadOperation__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ReadOperation__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group__4"


    // $ANTLR start "rule__ReadOperation__Group__4__Impl"
    // InternalWebserviceDSL.g:1572:1: rule__ReadOperation__Group__4__Impl : ( ( rule__ReadOperation__Group_4__0 )? ) ;
    public final void rule__ReadOperation__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1576:1: ( ( ( rule__ReadOperation__Group_4__0 )? ) )
            // InternalWebserviceDSL.g:1577:1: ( ( rule__ReadOperation__Group_4__0 )? )
            {
            // InternalWebserviceDSL.g:1577:1: ( ( rule__ReadOperation__Group_4__0 )? )
            // InternalWebserviceDSL.g:1578:2: ( rule__ReadOperation__Group_4__0 )?
            {
             before(grammarAccess.getReadOperationAccess().getGroup_4()); 
            // InternalWebserviceDSL.g:1579:2: ( rule__ReadOperation__Group_4__0 )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==22) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalWebserviceDSL.g:1579:3: rule__ReadOperation__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__ReadOperation__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getReadOperationAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group__4__Impl"


    // $ANTLR start "rule__ReadOperation__Group__5"
    // InternalWebserviceDSL.g:1587:1: rule__ReadOperation__Group__5 : rule__ReadOperation__Group__5__Impl rule__ReadOperation__Group__6 ;
    public final void rule__ReadOperation__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1591:1: ( rule__ReadOperation__Group__5__Impl rule__ReadOperation__Group__6 )
            // InternalWebserviceDSL.g:1592:2: rule__ReadOperation__Group__5__Impl rule__ReadOperation__Group__6
            {
            pushFollow(FOLLOW_15);
            rule__ReadOperation__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ReadOperation__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group__5"


    // $ANTLR start "rule__ReadOperation__Group__5__Impl"
    // InternalWebserviceDSL.g:1599:1: rule__ReadOperation__Group__5__Impl : ( ( rule__ReadOperation__Group_5__0 )? ) ;
    public final void rule__ReadOperation__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1603:1: ( ( ( rule__ReadOperation__Group_5__0 )? ) )
            // InternalWebserviceDSL.g:1604:1: ( ( rule__ReadOperation__Group_5__0 )? )
            {
            // InternalWebserviceDSL.g:1604:1: ( ( rule__ReadOperation__Group_5__0 )? )
            // InternalWebserviceDSL.g:1605:2: ( rule__ReadOperation__Group_5__0 )?
            {
             before(grammarAccess.getReadOperationAccess().getGroup_5()); 
            // InternalWebserviceDSL.g:1606:2: ( rule__ReadOperation__Group_5__0 )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==23) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalWebserviceDSL.g:1606:3: rule__ReadOperation__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__ReadOperation__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getReadOperationAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group__5__Impl"


    // $ANTLR start "rule__ReadOperation__Group__6"
    // InternalWebserviceDSL.g:1614:1: rule__ReadOperation__Group__6 : rule__ReadOperation__Group__6__Impl rule__ReadOperation__Group__7 ;
    public final void rule__ReadOperation__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1618:1: ( rule__ReadOperation__Group__6__Impl rule__ReadOperation__Group__7 )
            // InternalWebserviceDSL.g:1619:2: rule__ReadOperation__Group__6__Impl rule__ReadOperation__Group__7
            {
            pushFollow(FOLLOW_15);
            rule__ReadOperation__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ReadOperation__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group__6"


    // $ANTLR start "rule__ReadOperation__Group__6__Impl"
    // InternalWebserviceDSL.g:1626:1: rule__ReadOperation__Group__6__Impl : ( ( rule__ReadOperation__Group_6__0 )? ) ;
    public final void rule__ReadOperation__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1630:1: ( ( ( rule__ReadOperation__Group_6__0 )? ) )
            // InternalWebserviceDSL.g:1631:1: ( ( rule__ReadOperation__Group_6__0 )? )
            {
            // InternalWebserviceDSL.g:1631:1: ( ( rule__ReadOperation__Group_6__0 )? )
            // InternalWebserviceDSL.g:1632:2: ( rule__ReadOperation__Group_6__0 )?
            {
             before(grammarAccess.getReadOperationAccess().getGroup_6()); 
            // InternalWebserviceDSL.g:1633:2: ( rule__ReadOperation__Group_6__0 )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==24) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalWebserviceDSL.g:1633:3: rule__ReadOperation__Group_6__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__ReadOperation__Group_6__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getReadOperationAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group__6__Impl"


    // $ANTLR start "rule__ReadOperation__Group__7"
    // InternalWebserviceDSL.g:1641:1: rule__ReadOperation__Group__7 : rule__ReadOperation__Group__7__Impl ;
    public final void rule__ReadOperation__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1645:1: ( rule__ReadOperation__Group__7__Impl )
            // InternalWebserviceDSL.g:1646:2: rule__ReadOperation__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ReadOperation__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group__7"


    // $ANTLR start "rule__ReadOperation__Group__7__Impl"
    // InternalWebserviceDSL.g:1652:1: rule__ReadOperation__Group__7__Impl : ( '}' ) ;
    public final void rule__ReadOperation__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1656:1: ( ( '}' ) )
            // InternalWebserviceDSL.g:1657:1: ( '}' )
            {
            // InternalWebserviceDSL.g:1657:1: ( '}' )
            // InternalWebserviceDSL.g:1658:2: '}'
            {
             before(grammarAccess.getReadOperationAccess().getRightCurlyBracketKeyword_7()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getReadOperationAccess().getRightCurlyBracketKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group__7__Impl"


    // $ANTLR start "rule__ReadOperation__Group_3__0"
    // InternalWebserviceDSL.g:1668:1: rule__ReadOperation__Group_3__0 : rule__ReadOperation__Group_3__0__Impl rule__ReadOperation__Group_3__1 ;
    public final void rule__ReadOperation__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1672:1: ( rule__ReadOperation__Group_3__0__Impl rule__ReadOperation__Group_3__1 )
            // InternalWebserviceDSL.g:1673:2: rule__ReadOperation__Group_3__0__Impl rule__ReadOperation__Group_3__1
            {
            pushFollow(FOLLOW_3);
            rule__ReadOperation__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ReadOperation__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group_3__0"


    // $ANTLR start "rule__ReadOperation__Group_3__0__Impl"
    // InternalWebserviceDSL.g:1680:1: rule__ReadOperation__Group_3__0__Impl : ( 'description' ) ;
    public final void rule__ReadOperation__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1684:1: ( ( 'description' ) )
            // InternalWebserviceDSL.g:1685:1: ( 'description' )
            {
            // InternalWebserviceDSL.g:1685:1: ( 'description' )
            // InternalWebserviceDSL.g:1686:2: 'description'
            {
             before(grammarAccess.getReadOperationAccess().getDescriptionKeyword_3_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getReadOperationAccess().getDescriptionKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group_3__0__Impl"


    // $ANTLR start "rule__ReadOperation__Group_3__1"
    // InternalWebserviceDSL.g:1695:1: rule__ReadOperation__Group_3__1 : rule__ReadOperation__Group_3__1__Impl ;
    public final void rule__ReadOperation__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1699:1: ( rule__ReadOperation__Group_3__1__Impl )
            // InternalWebserviceDSL.g:1700:2: rule__ReadOperation__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ReadOperation__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group_3__1"


    // $ANTLR start "rule__ReadOperation__Group_3__1__Impl"
    // InternalWebserviceDSL.g:1706:1: rule__ReadOperation__Group_3__1__Impl : ( ( rule__ReadOperation__DescriptionAssignment_3_1 ) ) ;
    public final void rule__ReadOperation__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1710:1: ( ( ( rule__ReadOperation__DescriptionAssignment_3_1 ) ) )
            // InternalWebserviceDSL.g:1711:1: ( ( rule__ReadOperation__DescriptionAssignment_3_1 ) )
            {
            // InternalWebserviceDSL.g:1711:1: ( ( rule__ReadOperation__DescriptionAssignment_3_1 ) )
            // InternalWebserviceDSL.g:1712:2: ( rule__ReadOperation__DescriptionAssignment_3_1 )
            {
             before(grammarAccess.getReadOperationAccess().getDescriptionAssignment_3_1()); 
            // InternalWebserviceDSL.g:1713:2: ( rule__ReadOperation__DescriptionAssignment_3_1 )
            // InternalWebserviceDSL.g:1713:3: rule__ReadOperation__DescriptionAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__ReadOperation__DescriptionAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getReadOperationAccess().getDescriptionAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group_3__1__Impl"


    // $ANTLR start "rule__ReadOperation__Group_4__0"
    // InternalWebserviceDSL.g:1722:1: rule__ReadOperation__Group_4__0 : rule__ReadOperation__Group_4__0__Impl rule__ReadOperation__Group_4__1 ;
    public final void rule__ReadOperation__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1726:1: ( rule__ReadOperation__Group_4__0__Impl rule__ReadOperation__Group_4__1 )
            // InternalWebserviceDSL.g:1727:2: rule__ReadOperation__Group_4__0__Impl rule__ReadOperation__Group_4__1
            {
            pushFollow(FOLLOW_3);
            rule__ReadOperation__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ReadOperation__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group_4__0"


    // $ANTLR start "rule__ReadOperation__Group_4__0__Impl"
    // InternalWebserviceDSL.g:1734:1: rule__ReadOperation__Group_4__0__Impl : ( 'urlSuffix' ) ;
    public final void rule__ReadOperation__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1738:1: ( ( 'urlSuffix' ) )
            // InternalWebserviceDSL.g:1739:1: ( 'urlSuffix' )
            {
            // InternalWebserviceDSL.g:1739:1: ( 'urlSuffix' )
            // InternalWebserviceDSL.g:1740:2: 'urlSuffix'
            {
             before(grammarAccess.getReadOperationAccess().getUrlSuffixKeyword_4_0()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getReadOperationAccess().getUrlSuffixKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group_4__0__Impl"


    // $ANTLR start "rule__ReadOperation__Group_4__1"
    // InternalWebserviceDSL.g:1749:1: rule__ReadOperation__Group_4__1 : rule__ReadOperation__Group_4__1__Impl ;
    public final void rule__ReadOperation__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1753:1: ( rule__ReadOperation__Group_4__1__Impl )
            // InternalWebserviceDSL.g:1754:2: rule__ReadOperation__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ReadOperation__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group_4__1"


    // $ANTLR start "rule__ReadOperation__Group_4__1__Impl"
    // InternalWebserviceDSL.g:1760:1: rule__ReadOperation__Group_4__1__Impl : ( ( rule__ReadOperation__UrlSuffixAssignment_4_1 ) ) ;
    public final void rule__ReadOperation__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1764:1: ( ( ( rule__ReadOperation__UrlSuffixAssignment_4_1 ) ) )
            // InternalWebserviceDSL.g:1765:1: ( ( rule__ReadOperation__UrlSuffixAssignment_4_1 ) )
            {
            // InternalWebserviceDSL.g:1765:1: ( ( rule__ReadOperation__UrlSuffixAssignment_4_1 ) )
            // InternalWebserviceDSL.g:1766:2: ( rule__ReadOperation__UrlSuffixAssignment_4_1 )
            {
             before(grammarAccess.getReadOperationAccess().getUrlSuffixAssignment_4_1()); 
            // InternalWebserviceDSL.g:1767:2: ( rule__ReadOperation__UrlSuffixAssignment_4_1 )
            // InternalWebserviceDSL.g:1767:3: rule__ReadOperation__UrlSuffixAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__ReadOperation__UrlSuffixAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getReadOperationAccess().getUrlSuffixAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group_4__1__Impl"


    // $ANTLR start "rule__ReadOperation__Group_5__0"
    // InternalWebserviceDSL.g:1776:1: rule__ReadOperation__Group_5__0 : rule__ReadOperation__Group_5__0__Impl rule__ReadOperation__Group_5__1 ;
    public final void rule__ReadOperation__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1780:1: ( rule__ReadOperation__Group_5__0__Impl rule__ReadOperation__Group_5__1 )
            // InternalWebserviceDSL.g:1781:2: rule__ReadOperation__Group_5__0__Impl rule__ReadOperation__Group_5__1
            {
            pushFollow(FOLLOW_3);
            rule__ReadOperation__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ReadOperation__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group_5__0"


    // $ANTLR start "rule__ReadOperation__Group_5__0__Impl"
    // InternalWebserviceDSL.g:1788:1: rule__ReadOperation__Group_5__0__Impl : ( 'response' ) ;
    public final void rule__ReadOperation__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1792:1: ( ( 'response' ) )
            // InternalWebserviceDSL.g:1793:1: ( 'response' )
            {
            // InternalWebserviceDSL.g:1793:1: ( 'response' )
            // InternalWebserviceDSL.g:1794:2: 'response'
            {
             before(grammarAccess.getReadOperationAccess().getResponseKeyword_5_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getReadOperationAccess().getResponseKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group_5__0__Impl"


    // $ANTLR start "rule__ReadOperation__Group_5__1"
    // InternalWebserviceDSL.g:1803:1: rule__ReadOperation__Group_5__1 : rule__ReadOperation__Group_5__1__Impl ;
    public final void rule__ReadOperation__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1807:1: ( rule__ReadOperation__Group_5__1__Impl )
            // InternalWebserviceDSL.g:1808:2: rule__ReadOperation__Group_5__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ReadOperation__Group_5__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group_5__1"


    // $ANTLR start "rule__ReadOperation__Group_5__1__Impl"
    // InternalWebserviceDSL.g:1814:1: rule__ReadOperation__Group_5__1__Impl : ( ( rule__ReadOperation__ResponseAssignment_5_1 ) ) ;
    public final void rule__ReadOperation__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1818:1: ( ( ( rule__ReadOperation__ResponseAssignment_5_1 ) ) )
            // InternalWebserviceDSL.g:1819:1: ( ( rule__ReadOperation__ResponseAssignment_5_1 ) )
            {
            // InternalWebserviceDSL.g:1819:1: ( ( rule__ReadOperation__ResponseAssignment_5_1 ) )
            // InternalWebserviceDSL.g:1820:2: ( rule__ReadOperation__ResponseAssignment_5_1 )
            {
             before(grammarAccess.getReadOperationAccess().getResponseAssignment_5_1()); 
            // InternalWebserviceDSL.g:1821:2: ( rule__ReadOperation__ResponseAssignment_5_1 )
            // InternalWebserviceDSL.g:1821:3: rule__ReadOperation__ResponseAssignment_5_1
            {
            pushFollow(FOLLOW_2);
            rule__ReadOperation__ResponseAssignment_5_1();

            state._fsp--;


            }

             after(grammarAccess.getReadOperationAccess().getResponseAssignment_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group_5__1__Impl"


    // $ANTLR start "rule__ReadOperation__Group_6__0"
    // InternalWebserviceDSL.g:1830:1: rule__ReadOperation__Group_6__0 : rule__ReadOperation__Group_6__0__Impl rule__ReadOperation__Group_6__1 ;
    public final void rule__ReadOperation__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1834:1: ( rule__ReadOperation__Group_6__0__Impl rule__ReadOperation__Group_6__1 )
            // InternalWebserviceDSL.g:1835:2: rule__ReadOperation__Group_6__0__Impl rule__ReadOperation__Group_6__1
            {
            pushFollow(FOLLOW_4);
            rule__ReadOperation__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ReadOperation__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group_6__0"


    // $ANTLR start "rule__ReadOperation__Group_6__0__Impl"
    // InternalWebserviceDSL.g:1842:1: rule__ReadOperation__Group_6__0__Impl : ( 'parameters' ) ;
    public final void rule__ReadOperation__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1846:1: ( ( 'parameters' ) )
            // InternalWebserviceDSL.g:1847:1: ( 'parameters' )
            {
            // InternalWebserviceDSL.g:1847:1: ( 'parameters' )
            // InternalWebserviceDSL.g:1848:2: 'parameters'
            {
             before(grammarAccess.getReadOperationAccess().getParametersKeyword_6_0()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getReadOperationAccess().getParametersKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group_6__0__Impl"


    // $ANTLR start "rule__ReadOperation__Group_6__1"
    // InternalWebserviceDSL.g:1857:1: rule__ReadOperation__Group_6__1 : rule__ReadOperation__Group_6__1__Impl rule__ReadOperation__Group_6__2 ;
    public final void rule__ReadOperation__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1861:1: ( rule__ReadOperation__Group_6__1__Impl rule__ReadOperation__Group_6__2 )
            // InternalWebserviceDSL.g:1862:2: rule__ReadOperation__Group_6__1__Impl rule__ReadOperation__Group_6__2
            {
            pushFollow(FOLLOW_16);
            rule__ReadOperation__Group_6__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ReadOperation__Group_6__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group_6__1"


    // $ANTLR start "rule__ReadOperation__Group_6__1__Impl"
    // InternalWebserviceDSL.g:1869:1: rule__ReadOperation__Group_6__1__Impl : ( '{' ) ;
    public final void rule__ReadOperation__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1873:1: ( ( '{' ) )
            // InternalWebserviceDSL.g:1874:1: ( '{' )
            {
            // InternalWebserviceDSL.g:1874:1: ( '{' )
            // InternalWebserviceDSL.g:1875:2: '{'
            {
             before(grammarAccess.getReadOperationAccess().getLeftCurlyBracketKeyword_6_1()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getReadOperationAccess().getLeftCurlyBracketKeyword_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group_6__1__Impl"


    // $ANTLR start "rule__ReadOperation__Group_6__2"
    // InternalWebserviceDSL.g:1884:1: rule__ReadOperation__Group_6__2 : rule__ReadOperation__Group_6__2__Impl rule__ReadOperation__Group_6__3 ;
    public final void rule__ReadOperation__Group_6__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1888:1: ( rule__ReadOperation__Group_6__2__Impl rule__ReadOperation__Group_6__3 )
            // InternalWebserviceDSL.g:1889:2: rule__ReadOperation__Group_6__2__Impl rule__ReadOperation__Group_6__3
            {
            pushFollow(FOLLOW_8);
            rule__ReadOperation__Group_6__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ReadOperation__Group_6__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group_6__2"


    // $ANTLR start "rule__ReadOperation__Group_6__2__Impl"
    // InternalWebserviceDSL.g:1896:1: rule__ReadOperation__Group_6__2__Impl : ( ( rule__ReadOperation__ParametersAssignment_6_2 ) ) ;
    public final void rule__ReadOperation__Group_6__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1900:1: ( ( ( rule__ReadOperation__ParametersAssignment_6_2 ) ) )
            // InternalWebserviceDSL.g:1901:1: ( ( rule__ReadOperation__ParametersAssignment_6_2 ) )
            {
            // InternalWebserviceDSL.g:1901:1: ( ( rule__ReadOperation__ParametersAssignment_6_2 ) )
            // InternalWebserviceDSL.g:1902:2: ( rule__ReadOperation__ParametersAssignment_6_2 )
            {
             before(grammarAccess.getReadOperationAccess().getParametersAssignment_6_2()); 
            // InternalWebserviceDSL.g:1903:2: ( rule__ReadOperation__ParametersAssignment_6_2 )
            // InternalWebserviceDSL.g:1903:3: rule__ReadOperation__ParametersAssignment_6_2
            {
            pushFollow(FOLLOW_2);
            rule__ReadOperation__ParametersAssignment_6_2();

            state._fsp--;


            }

             after(grammarAccess.getReadOperationAccess().getParametersAssignment_6_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group_6__2__Impl"


    // $ANTLR start "rule__ReadOperation__Group_6__3"
    // InternalWebserviceDSL.g:1911:1: rule__ReadOperation__Group_6__3 : rule__ReadOperation__Group_6__3__Impl rule__ReadOperation__Group_6__4 ;
    public final void rule__ReadOperation__Group_6__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1915:1: ( rule__ReadOperation__Group_6__3__Impl rule__ReadOperation__Group_6__4 )
            // InternalWebserviceDSL.g:1916:2: rule__ReadOperation__Group_6__3__Impl rule__ReadOperation__Group_6__4
            {
            pushFollow(FOLLOW_8);
            rule__ReadOperation__Group_6__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ReadOperation__Group_6__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group_6__3"


    // $ANTLR start "rule__ReadOperation__Group_6__3__Impl"
    // InternalWebserviceDSL.g:1923:1: rule__ReadOperation__Group_6__3__Impl : ( ( rule__ReadOperation__Group_6_3__0 )* ) ;
    public final void rule__ReadOperation__Group_6__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1927:1: ( ( ( rule__ReadOperation__Group_6_3__0 )* ) )
            // InternalWebserviceDSL.g:1928:1: ( ( rule__ReadOperation__Group_6_3__0 )* )
            {
            // InternalWebserviceDSL.g:1928:1: ( ( rule__ReadOperation__Group_6_3__0 )* )
            // InternalWebserviceDSL.g:1929:2: ( rule__ReadOperation__Group_6_3__0 )*
            {
             before(grammarAccess.getReadOperationAccess().getGroup_6_3()); 
            // InternalWebserviceDSL.g:1930:2: ( rule__ReadOperation__Group_6_3__0 )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==18) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // InternalWebserviceDSL.g:1930:3: rule__ReadOperation__Group_6_3__0
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__ReadOperation__Group_6_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

             after(grammarAccess.getReadOperationAccess().getGroup_6_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group_6__3__Impl"


    // $ANTLR start "rule__ReadOperation__Group_6__4"
    // InternalWebserviceDSL.g:1938:1: rule__ReadOperation__Group_6__4 : rule__ReadOperation__Group_6__4__Impl ;
    public final void rule__ReadOperation__Group_6__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1942:1: ( rule__ReadOperation__Group_6__4__Impl )
            // InternalWebserviceDSL.g:1943:2: rule__ReadOperation__Group_6__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ReadOperation__Group_6__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group_6__4"


    // $ANTLR start "rule__ReadOperation__Group_6__4__Impl"
    // InternalWebserviceDSL.g:1949:1: rule__ReadOperation__Group_6__4__Impl : ( '}' ) ;
    public final void rule__ReadOperation__Group_6__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1953:1: ( ( '}' ) )
            // InternalWebserviceDSL.g:1954:1: ( '}' )
            {
            // InternalWebserviceDSL.g:1954:1: ( '}' )
            // InternalWebserviceDSL.g:1955:2: '}'
            {
             before(grammarAccess.getReadOperationAccess().getRightCurlyBracketKeyword_6_4()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getReadOperationAccess().getRightCurlyBracketKeyword_6_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group_6__4__Impl"


    // $ANTLR start "rule__ReadOperation__Group_6_3__0"
    // InternalWebserviceDSL.g:1965:1: rule__ReadOperation__Group_6_3__0 : rule__ReadOperation__Group_6_3__0__Impl rule__ReadOperation__Group_6_3__1 ;
    public final void rule__ReadOperation__Group_6_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1969:1: ( rule__ReadOperation__Group_6_3__0__Impl rule__ReadOperation__Group_6_3__1 )
            // InternalWebserviceDSL.g:1970:2: rule__ReadOperation__Group_6_3__0__Impl rule__ReadOperation__Group_6_3__1
            {
            pushFollow(FOLLOW_16);
            rule__ReadOperation__Group_6_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ReadOperation__Group_6_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group_6_3__0"


    // $ANTLR start "rule__ReadOperation__Group_6_3__0__Impl"
    // InternalWebserviceDSL.g:1977:1: rule__ReadOperation__Group_6_3__0__Impl : ( ',' ) ;
    public final void rule__ReadOperation__Group_6_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1981:1: ( ( ',' ) )
            // InternalWebserviceDSL.g:1982:1: ( ',' )
            {
            // InternalWebserviceDSL.g:1982:1: ( ',' )
            // InternalWebserviceDSL.g:1983:2: ','
            {
             before(grammarAccess.getReadOperationAccess().getCommaKeyword_6_3_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getReadOperationAccess().getCommaKeyword_6_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group_6_3__0__Impl"


    // $ANTLR start "rule__ReadOperation__Group_6_3__1"
    // InternalWebserviceDSL.g:1992:1: rule__ReadOperation__Group_6_3__1 : rule__ReadOperation__Group_6_3__1__Impl ;
    public final void rule__ReadOperation__Group_6_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:1996:1: ( rule__ReadOperation__Group_6_3__1__Impl )
            // InternalWebserviceDSL.g:1997:2: rule__ReadOperation__Group_6_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ReadOperation__Group_6_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group_6_3__1"


    // $ANTLR start "rule__ReadOperation__Group_6_3__1__Impl"
    // InternalWebserviceDSL.g:2003:1: rule__ReadOperation__Group_6_3__1__Impl : ( ( rule__ReadOperation__ParametersAssignment_6_3_1 ) ) ;
    public final void rule__ReadOperation__Group_6_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2007:1: ( ( ( rule__ReadOperation__ParametersAssignment_6_3_1 ) ) )
            // InternalWebserviceDSL.g:2008:1: ( ( rule__ReadOperation__ParametersAssignment_6_3_1 ) )
            {
            // InternalWebserviceDSL.g:2008:1: ( ( rule__ReadOperation__ParametersAssignment_6_3_1 ) )
            // InternalWebserviceDSL.g:2009:2: ( rule__ReadOperation__ParametersAssignment_6_3_1 )
            {
             before(grammarAccess.getReadOperationAccess().getParametersAssignment_6_3_1()); 
            // InternalWebserviceDSL.g:2010:2: ( rule__ReadOperation__ParametersAssignment_6_3_1 )
            // InternalWebserviceDSL.g:2010:3: rule__ReadOperation__ParametersAssignment_6_3_1
            {
            pushFollow(FOLLOW_2);
            rule__ReadOperation__ParametersAssignment_6_3_1();

            state._fsp--;


            }

             after(grammarAccess.getReadOperationAccess().getParametersAssignment_6_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__Group_6_3__1__Impl"


    // $ANTLR start "rule__CreateOperation__Group__0"
    // InternalWebserviceDSL.g:2019:1: rule__CreateOperation__Group__0 : rule__CreateOperation__Group__0__Impl rule__CreateOperation__Group__1 ;
    public final void rule__CreateOperation__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2023:1: ( rule__CreateOperation__Group__0__Impl rule__CreateOperation__Group__1 )
            // InternalWebserviceDSL.g:2024:2: rule__CreateOperation__Group__0__Impl rule__CreateOperation__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__CreateOperation__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CreateOperation__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group__0"


    // $ANTLR start "rule__CreateOperation__Group__0__Impl"
    // InternalWebserviceDSL.g:2031:1: rule__CreateOperation__Group__0__Impl : ( 'CreateOperation' ) ;
    public final void rule__CreateOperation__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2035:1: ( ( 'CreateOperation' ) )
            // InternalWebserviceDSL.g:2036:1: ( 'CreateOperation' )
            {
            // InternalWebserviceDSL.g:2036:1: ( 'CreateOperation' )
            // InternalWebserviceDSL.g:2037:2: 'CreateOperation'
            {
             before(grammarAccess.getCreateOperationAccess().getCreateOperationKeyword_0()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getCreateOperationAccess().getCreateOperationKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group__0__Impl"


    // $ANTLR start "rule__CreateOperation__Group__1"
    // InternalWebserviceDSL.g:2046:1: rule__CreateOperation__Group__1 : rule__CreateOperation__Group__1__Impl rule__CreateOperation__Group__2 ;
    public final void rule__CreateOperation__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2050:1: ( rule__CreateOperation__Group__1__Impl rule__CreateOperation__Group__2 )
            // InternalWebserviceDSL.g:2051:2: rule__CreateOperation__Group__1__Impl rule__CreateOperation__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__CreateOperation__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CreateOperation__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group__1"


    // $ANTLR start "rule__CreateOperation__Group__1__Impl"
    // InternalWebserviceDSL.g:2058:1: rule__CreateOperation__Group__1__Impl : ( ( rule__CreateOperation__NameAssignment_1 ) ) ;
    public final void rule__CreateOperation__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2062:1: ( ( ( rule__CreateOperation__NameAssignment_1 ) ) )
            // InternalWebserviceDSL.g:2063:1: ( ( rule__CreateOperation__NameAssignment_1 ) )
            {
            // InternalWebserviceDSL.g:2063:1: ( ( rule__CreateOperation__NameAssignment_1 ) )
            // InternalWebserviceDSL.g:2064:2: ( rule__CreateOperation__NameAssignment_1 )
            {
             before(grammarAccess.getCreateOperationAccess().getNameAssignment_1()); 
            // InternalWebserviceDSL.g:2065:2: ( rule__CreateOperation__NameAssignment_1 )
            // InternalWebserviceDSL.g:2065:3: rule__CreateOperation__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__CreateOperation__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getCreateOperationAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group__1__Impl"


    // $ANTLR start "rule__CreateOperation__Group__2"
    // InternalWebserviceDSL.g:2073:1: rule__CreateOperation__Group__2 : rule__CreateOperation__Group__2__Impl rule__CreateOperation__Group__3 ;
    public final void rule__CreateOperation__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2077:1: ( rule__CreateOperation__Group__2__Impl rule__CreateOperation__Group__3 )
            // InternalWebserviceDSL.g:2078:2: rule__CreateOperation__Group__2__Impl rule__CreateOperation__Group__3
            {
            pushFollow(FOLLOW_15);
            rule__CreateOperation__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CreateOperation__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group__2"


    // $ANTLR start "rule__CreateOperation__Group__2__Impl"
    // InternalWebserviceDSL.g:2085:1: rule__CreateOperation__Group__2__Impl : ( '{' ) ;
    public final void rule__CreateOperation__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2089:1: ( ( '{' ) )
            // InternalWebserviceDSL.g:2090:1: ( '{' )
            {
            // InternalWebserviceDSL.g:2090:1: ( '{' )
            // InternalWebserviceDSL.g:2091:2: '{'
            {
             before(grammarAccess.getCreateOperationAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getCreateOperationAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group__2__Impl"


    // $ANTLR start "rule__CreateOperation__Group__3"
    // InternalWebserviceDSL.g:2100:1: rule__CreateOperation__Group__3 : rule__CreateOperation__Group__3__Impl rule__CreateOperation__Group__4 ;
    public final void rule__CreateOperation__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2104:1: ( rule__CreateOperation__Group__3__Impl rule__CreateOperation__Group__4 )
            // InternalWebserviceDSL.g:2105:2: rule__CreateOperation__Group__3__Impl rule__CreateOperation__Group__4
            {
            pushFollow(FOLLOW_15);
            rule__CreateOperation__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CreateOperation__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group__3"


    // $ANTLR start "rule__CreateOperation__Group__3__Impl"
    // InternalWebserviceDSL.g:2112:1: rule__CreateOperation__Group__3__Impl : ( ( rule__CreateOperation__Group_3__0 )? ) ;
    public final void rule__CreateOperation__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2116:1: ( ( ( rule__CreateOperation__Group_3__0 )? ) )
            // InternalWebserviceDSL.g:2117:1: ( ( rule__CreateOperation__Group_3__0 )? )
            {
            // InternalWebserviceDSL.g:2117:1: ( ( rule__CreateOperation__Group_3__0 )? )
            // InternalWebserviceDSL.g:2118:2: ( rule__CreateOperation__Group_3__0 )?
            {
             before(grammarAccess.getCreateOperationAccess().getGroup_3()); 
            // InternalWebserviceDSL.g:2119:2: ( rule__CreateOperation__Group_3__0 )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==17) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalWebserviceDSL.g:2119:3: rule__CreateOperation__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__CreateOperation__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getCreateOperationAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group__3__Impl"


    // $ANTLR start "rule__CreateOperation__Group__4"
    // InternalWebserviceDSL.g:2127:1: rule__CreateOperation__Group__4 : rule__CreateOperation__Group__4__Impl rule__CreateOperation__Group__5 ;
    public final void rule__CreateOperation__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2131:1: ( rule__CreateOperation__Group__4__Impl rule__CreateOperation__Group__5 )
            // InternalWebserviceDSL.g:2132:2: rule__CreateOperation__Group__4__Impl rule__CreateOperation__Group__5
            {
            pushFollow(FOLLOW_15);
            rule__CreateOperation__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CreateOperation__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group__4"


    // $ANTLR start "rule__CreateOperation__Group__4__Impl"
    // InternalWebserviceDSL.g:2139:1: rule__CreateOperation__Group__4__Impl : ( ( rule__CreateOperation__Group_4__0 )? ) ;
    public final void rule__CreateOperation__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2143:1: ( ( ( rule__CreateOperation__Group_4__0 )? ) )
            // InternalWebserviceDSL.g:2144:1: ( ( rule__CreateOperation__Group_4__0 )? )
            {
            // InternalWebserviceDSL.g:2144:1: ( ( rule__CreateOperation__Group_4__0 )? )
            // InternalWebserviceDSL.g:2145:2: ( rule__CreateOperation__Group_4__0 )?
            {
             before(grammarAccess.getCreateOperationAccess().getGroup_4()); 
            // InternalWebserviceDSL.g:2146:2: ( rule__CreateOperation__Group_4__0 )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==22) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalWebserviceDSL.g:2146:3: rule__CreateOperation__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__CreateOperation__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getCreateOperationAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group__4__Impl"


    // $ANTLR start "rule__CreateOperation__Group__5"
    // InternalWebserviceDSL.g:2154:1: rule__CreateOperation__Group__5 : rule__CreateOperation__Group__5__Impl rule__CreateOperation__Group__6 ;
    public final void rule__CreateOperation__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2158:1: ( rule__CreateOperation__Group__5__Impl rule__CreateOperation__Group__6 )
            // InternalWebserviceDSL.g:2159:2: rule__CreateOperation__Group__5__Impl rule__CreateOperation__Group__6
            {
            pushFollow(FOLLOW_15);
            rule__CreateOperation__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CreateOperation__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group__5"


    // $ANTLR start "rule__CreateOperation__Group__5__Impl"
    // InternalWebserviceDSL.g:2166:1: rule__CreateOperation__Group__5__Impl : ( ( rule__CreateOperation__Group_5__0 )? ) ;
    public final void rule__CreateOperation__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2170:1: ( ( ( rule__CreateOperation__Group_5__0 )? ) )
            // InternalWebserviceDSL.g:2171:1: ( ( rule__CreateOperation__Group_5__0 )? )
            {
            // InternalWebserviceDSL.g:2171:1: ( ( rule__CreateOperation__Group_5__0 )? )
            // InternalWebserviceDSL.g:2172:2: ( rule__CreateOperation__Group_5__0 )?
            {
             before(grammarAccess.getCreateOperationAccess().getGroup_5()); 
            // InternalWebserviceDSL.g:2173:2: ( rule__CreateOperation__Group_5__0 )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==23) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalWebserviceDSL.g:2173:3: rule__CreateOperation__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__CreateOperation__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getCreateOperationAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group__5__Impl"


    // $ANTLR start "rule__CreateOperation__Group__6"
    // InternalWebserviceDSL.g:2181:1: rule__CreateOperation__Group__6 : rule__CreateOperation__Group__6__Impl rule__CreateOperation__Group__7 ;
    public final void rule__CreateOperation__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2185:1: ( rule__CreateOperation__Group__6__Impl rule__CreateOperation__Group__7 )
            // InternalWebserviceDSL.g:2186:2: rule__CreateOperation__Group__6__Impl rule__CreateOperation__Group__7
            {
            pushFollow(FOLLOW_15);
            rule__CreateOperation__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CreateOperation__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group__6"


    // $ANTLR start "rule__CreateOperation__Group__6__Impl"
    // InternalWebserviceDSL.g:2193:1: rule__CreateOperation__Group__6__Impl : ( ( rule__CreateOperation__Group_6__0 )? ) ;
    public final void rule__CreateOperation__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2197:1: ( ( ( rule__CreateOperation__Group_6__0 )? ) )
            // InternalWebserviceDSL.g:2198:1: ( ( rule__CreateOperation__Group_6__0 )? )
            {
            // InternalWebserviceDSL.g:2198:1: ( ( rule__CreateOperation__Group_6__0 )? )
            // InternalWebserviceDSL.g:2199:2: ( rule__CreateOperation__Group_6__0 )?
            {
             before(grammarAccess.getCreateOperationAccess().getGroup_6()); 
            // InternalWebserviceDSL.g:2200:2: ( rule__CreateOperation__Group_6__0 )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==24) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalWebserviceDSL.g:2200:3: rule__CreateOperation__Group_6__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__CreateOperation__Group_6__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getCreateOperationAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group__6__Impl"


    // $ANTLR start "rule__CreateOperation__Group__7"
    // InternalWebserviceDSL.g:2208:1: rule__CreateOperation__Group__7 : rule__CreateOperation__Group__7__Impl ;
    public final void rule__CreateOperation__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2212:1: ( rule__CreateOperation__Group__7__Impl )
            // InternalWebserviceDSL.g:2213:2: rule__CreateOperation__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CreateOperation__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group__7"


    // $ANTLR start "rule__CreateOperation__Group__7__Impl"
    // InternalWebserviceDSL.g:2219:1: rule__CreateOperation__Group__7__Impl : ( '}' ) ;
    public final void rule__CreateOperation__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2223:1: ( ( '}' ) )
            // InternalWebserviceDSL.g:2224:1: ( '}' )
            {
            // InternalWebserviceDSL.g:2224:1: ( '}' )
            // InternalWebserviceDSL.g:2225:2: '}'
            {
             before(grammarAccess.getCreateOperationAccess().getRightCurlyBracketKeyword_7()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getCreateOperationAccess().getRightCurlyBracketKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group__7__Impl"


    // $ANTLR start "rule__CreateOperation__Group_3__0"
    // InternalWebserviceDSL.g:2235:1: rule__CreateOperation__Group_3__0 : rule__CreateOperation__Group_3__0__Impl rule__CreateOperation__Group_3__1 ;
    public final void rule__CreateOperation__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2239:1: ( rule__CreateOperation__Group_3__0__Impl rule__CreateOperation__Group_3__1 )
            // InternalWebserviceDSL.g:2240:2: rule__CreateOperation__Group_3__0__Impl rule__CreateOperation__Group_3__1
            {
            pushFollow(FOLLOW_3);
            rule__CreateOperation__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CreateOperation__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group_3__0"


    // $ANTLR start "rule__CreateOperation__Group_3__0__Impl"
    // InternalWebserviceDSL.g:2247:1: rule__CreateOperation__Group_3__0__Impl : ( 'description' ) ;
    public final void rule__CreateOperation__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2251:1: ( ( 'description' ) )
            // InternalWebserviceDSL.g:2252:1: ( 'description' )
            {
            // InternalWebserviceDSL.g:2252:1: ( 'description' )
            // InternalWebserviceDSL.g:2253:2: 'description'
            {
             before(grammarAccess.getCreateOperationAccess().getDescriptionKeyword_3_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getCreateOperationAccess().getDescriptionKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group_3__0__Impl"


    // $ANTLR start "rule__CreateOperation__Group_3__1"
    // InternalWebserviceDSL.g:2262:1: rule__CreateOperation__Group_3__1 : rule__CreateOperation__Group_3__1__Impl ;
    public final void rule__CreateOperation__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2266:1: ( rule__CreateOperation__Group_3__1__Impl )
            // InternalWebserviceDSL.g:2267:2: rule__CreateOperation__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CreateOperation__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group_3__1"


    // $ANTLR start "rule__CreateOperation__Group_3__1__Impl"
    // InternalWebserviceDSL.g:2273:1: rule__CreateOperation__Group_3__1__Impl : ( ( rule__CreateOperation__DescriptionAssignment_3_1 ) ) ;
    public final void rule__CreateOperation__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2277:1: ( ( ( rule__CreateOperation__DescriptionAssignment_3_1 ) ) )
            // InternalWebserviceDSL.g:2278:1: ( ( rule__CreateOperation__DescriptionAssignment_3_1 ) )
            {
            // InternalWebserviceDSL.g:2278:1: ( ( rule__CreateOperation__DescriptionAssignment_3_1 ) )
            // InternalWebserviceDSL.g:2279:2: ( rule__CreateOperation__DescriptionAssignment_3_1 )
            {
             before(grammarAccess.getCreateOperationAccess().getDescriptionAssignment_3_1()); 
            // InternalWebserviceDSL.g:2280:2: ( rule__CreateOperation__DescriptionAssignment_3_1 )
            // InternalWebserviceDSL.g:2280:3: rule__CreateOperation__DescriptionAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__CreateOperation__DescriptionAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getCreateOperationAccess().getDescriptionAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group_3__1__Impl"


    // $ANTLR start "rule__CreateOperation__Group_4__0"
    // InternalWebserviceDSL.g:2289:1: rule__CreateOperation__Group_4__0 : rule__CreateOperation__Group_4__0__Impl rule__CreateOperation__Group_4__1 ;
    public final void rule__CreateOperation__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2293:1: ( rule__CreateOperation__Group_4__0__Impl rule__CreateOperation__Group_4__1 )
            // InternalWebserviceDSL.g:2294:2: rule__CreateOperation__Group_4__0__Impl rule__CreateOperation__Group_4__1
            {
            pushFollow(FOLLOW_3);
            rule__CreateOperation__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CreateOperation__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group_4__0"


    // $ANTLR start "rule__CreateOperation__Group_4__0__Impl"
    // InternalWebserviceDSL.g:2301:1: rule__CreateOperation__Group_4__0__Impl : ( 'urlSuffix' ) ;
    public final void rule__CreateOperation__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2305:1: ( ( 'urlSuffix' ) )
            // InternalWebserviceDSL.g:2306:1: ( 'urlSuffix' )
            {
            // InternalWebserviceDSL.g:2306:1: ( 'urlSuffix' )
            // InternalWebserviceDSL.g:2307:2: 'urlSuffix'
            {
             before(grammarAccess.getCreateOperationAccess().getUrlSuffixKeyword_4_0()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getCreateOperationAccess().getUrlSuffixKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group_4__0__Impl"


    // $ANTLR start "rule__CreateOperation__Group_4__1"
    // InternalWebserviceDSL.g:2316:1: rule__CreateOperation__Group_4__1 : rule__CreateOperation__Group_4__1__Impl ;
    public final void rule__CreateOperation__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2320:1: ( rule__CreateOperation__Group_4__1__Impl )
            // InternalWebserviceDSL.g:2321:2: rule__CreateOperation__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CreateOperation__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group_4__1"


    // $ANTLR start "rule__CreateOperation__Group_4__1__Impl"
    // InternalWebserviceDSL.g:2327:1: rule__CreateOperation__Group_4__1__Impl : ( ( rule__CreateOperation__UrlSuffixAssignment_4_1 ) ) ;
    public final void rule__CreateOperation__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2331:1: ( ( ( rule__CreateOperation__UrlSuffixAssignment_4_1 ) ) )
            // InternalWebserviceDSL.g:2332:1: ( ( rule__CreateOperation__UrlSuffixAssignment_4_1 ) )
            {
            // InternalWebserviceDSL.g:2332:1: ( ( rule__CreateOperation__UrlSuffixAssignment_4_1 ) )
            // InternalWebserviceDSL.g:2333:2: ( rule__CreateOperation__UrlSuffixAssignment_4_1 )
            {
             before(grammarAccess.getCreateOperationAccess().getUrlSuffixAssignment_4_1()); 
            // InternalWebserviceDSL.g:2334:2: ( rule__CreateOperation__UrlSuffixAssignment_4_1 )
            // InternalWebserviceDSL.g:2334:3: rule__CreateOperation__UrlSuffixAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__CreateOperation__UrlSuffixAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getCreateOperationAccess().getUrlSuffixAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group_4__1__Impl"


    // $ANTLR start "rule__CreateOperation__Group_5__0"
    // InternalWebserviceDSL.g:2343:1: rule__CreateOperation__Group_5__0 : rule__CreateOperation__Group_5__0__Impl rule__CreateOperation__Group_5__1 ;
    public final void rule__CreateOperation__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2347:1: ( rule__CreateOperation__Group_5__0__Impl rule__CreateOperation__Group_5__1 )
            // InternalWebserviceDSL.g:2348:2: rule__CreateOperation__Group_5__0__Impl rule__CreateOperation__Group_5__1
            {
            pushFollow(FOLLOW_3);
            rule__CreateOperation__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CreateOperation__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group_5__0"


    // $ANTLR start "rule__CreateOperation__Group_5__0__Impl"
    // InternalWebserviceDSL.g:2355:1: rule__CreateOperation__Group_5__0__Impl : ( 'response' ) ;
    public final void rule__CreateOperation__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2359:1: ( ( 'response' ) )
            // InternalWebserviceDSL.g:2360:1: ( 'response' )
            {
            // InternalWebserviceDSL.g:2360:1: ( 'response' )
            // InternalWebserviceDSL.g:2361:2: 'response'
            {
             before(grammarAccess.getCreateOperationAccess().getResponseKeyword_5_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getCreateOperationAccess().getResponseKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group_5__0__Impl"


    // $ANTLR start "rule__CreateOperation__Group_5__1"
    // InternalWebserviceDSL.g:2370:1: rule__CreateOperation__Group_5__1 : rule__CreateOperation__Group_5__1__Impl ;
    public final void rule__CreateOperation__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2374:1: ( rule__CreateOperation__Group_5__1__Impl )
            // InternalWebserviceDSL.g:2375:2: rule__CreateOperation__Group_5__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CreateOperation__Group_5__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group_5__1"


    // $ANTLR start "rule__CreateOperation__Group_5__1__Impl"
    // InternalWebserviceDSL.g:2381:1: rule__CreateOperation__Group_5__1__Impl : ( ( rule__CreateOperation__ResponseAssignment_5_1 ) ) ;
    public final void rule__CreateOperation__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2385:1: ( ( ( rule__CreateOperation__ResponseAssignment_5_1 ) ) )
            // InternalWebserviceDSL.g:2386:1: ( ( rule__CreateOperation__ResponseAssignment_5_1 ) )
            {
            // InternalWebserviceDSL.g:2386:1: ( ( rule__CreateOperation__ResponseAssignment_5_1 ) )
            // InternalWebserviceDSL.g:2387:2: ( rule__CreateOperation__ResponseAssignment_5_1 )
            {
             before(grammarAccess.getCreateOperationAccess().getResponseAssignment_5_1()); 
            // InternalWebserviceDSL.g:2388:2: ( rule__CreateOperation__ResponseAssignment_5_1 )
            // InternalWebserviceDSL.g:2388:3: rule__CreateOperation__ResponseAssignment_5_1
            {
            pushFollow(FOLLOW_2);
            rule__CreateOperation__ResponseAssignment_5_1();

            state._fsp--;


            }

             after(grammarAccess.getCreateOperationAccess().getResponseAssignment_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group_5__1__Impl"


    // $ANTLR start "rule__CreateOperation__Group_6__0"
    // InternalWebserviceDSL.g:2397:1: rule__CreateOperation__Group_6__0 : rule__CreateOperation__Group_6__0__Impl rule__CreateOperation__Group_6__1 ;
    public final void rule__CreateOperation__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2401:1: ( rule__CreateOperation__Group_6__0__Impl rule__CreateOperation__Group_6__1 )
            // InternalWebserviceDSL.g:2402:2: rule__CreateOperation__Group_6__0__Impl rule__CreateOperation__Group_6__1
            {
            pushFollow(FOLLOW_4);
            rule__CreateOperation__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CreateOperation__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group_6__0"


    // $ANTLR start "rule__CreateOperation__Group_6__0__Impl"
    // InternalWebserviceDSL.g:2409:1: rule__CreateOperation__Group_6__0__Impl : ( 'parameters' ) ;
    public final void rule__CreateOperation__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2413:1: ( ( 'parameters' ) )
            // InternalWebserviceDSL.g:2414:1: ( 'parameters' )
            {
            // InternalWebserviceDSL.g:2414:1: ( 'parameters' )
            // InternalWebserviceDSL.g:2415:2: 'parameters'
            {
             before(grammarAccess.getCreateOperationAccess().getParametersKeyword_6_0()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getCreateOperationAccess().getParametersKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group_6__0__Impl"


    // $ANTLR start "rule__CreateOperation__Group_6__1"
    // InternalWebserviceDSL.g:2424:1: rule__CreateOperation__Group_6__1 : rule__CreateOperation__Group_6__1__Impl rule__CreateOperation__Group_6__2 ;
    public final void rule__CreateOperation__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2428:1: ( rule__CreateOperation__Group_6__1__Impl rule__CreateOperation__Group_6__2 )
            // InternalWebserviceDSL.g:2429:2: rule__CreateOperation__Group_6__1__Impl rule__CreateOperation__Group_6__2
            {
            pushFollow(FOLLOW_16);
            rule__CreateOperation__Group_6__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CreateOperation__Group_6__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group_6__1"


    // $ANTLR start "rule__CreateOperation__Group_6__1__Impl"
    // InternalWebserviceDSL.g:2436:1: rule__CreateOperation__Group_6__1__Impl : ( '{' ) ;
    public final void rule__CreateOperation__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2440:1: ( ( '{' ) )
            // InternalWebserviceDSL.g:2441:1: ( '{' )
            {
            // InternalWebserviceDSL.g:2441:1: ( '{' )
            // InternalWebserviceDSL.g:2442:2: '{'
            {
             before(grammarAccess.getCreateOperationAccess().getLeftCurlyBracketKeyword_6_1()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getCreateOperationAccess().getLeftCurlyBracketKeyword_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group_6__1__Impl"


    // $ANTLR start "rule__CreateOperation__Group_6__2"
    // InternalWebserviceDSL.g:2451:1: rule__CreateOperation__Group_6__2 : rule__CreateOperation__Group_6__2__Impl rule__CreateOperation__Group_6__3 ;
    public final void rule__CreateOperation__Group_6__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2455:1: ( rule__CreateOperation__Group_6__2__Impl rule__CreateOperation__Group_6__3 )
            // InternalWebserviceDSL.g:2456:2: rule__CreateOperation__Group_6__2__Impl rule__CreateOperation__Group_6__3
            {
            pushFollow(FOLLOW_8);
            rule__CreateOperation__Group_6__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CreateOperation__Group_6__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group_6__2"


    // $ANTLR start "rule__CreateOperation__Group_6__2__Impl"
    // InternalWebserviceDSL.g:2463:1: rule__CreateOperation__Group_6__2__Impl : ( ( rule__CreateOperation__ParametersAssignment_6_2 ) ) ;
    public final void rule__CreateOperation__Group_6__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2467:1: ( ( ( rule__CreateOperation__ParametersAssignment_6_2 ) ) )
            // InternalWebserviceDSL.g:2468:1: ( ( rule__CreateOperation__ParametersAssignment_6_2 ) )
            {
            // InternalWebserviceDSL.g:2468:1: ( ( rule__CreateOperation__ParametersAssignment_6_2 ) )
            // InternalWebserviceDSL.g:2469:2: ( rule__CreateOperation__ParametersAssignment_6_2 )
            {
             before(grammarAccess.getCreateOperationAccess().getParametersAssignment_6_2()); 
            // InternalWebserviceDSL.g:2470:2: ( rule__CreateOperation__ParametersAssignment_6_2 )
            // InternalWebserviceDSL.g:2470:3: rule__CreateOperation__ParametersAssignment_6_2
            {
            pushFollow(FOLLOW_2);
            rule__CreateOperation__ParametersAssignment_6_2();

            state._fsp--;


            }

             after(grammarAccess.getCreateOperationAccess().getParametersAssignment_6_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group_6__2__Impl"


    // $ANTLR start "rule__CreateOperation__Group_6__3"
    // InternalWebserviceDSL.g:2478:1: rule__CreateOperation__Group_6__3 : rule__CreateOperation__Group_6__3__Impl rule__CreateOperation__Group_6__4 ;
    public final void rule__CreateOperation__Group_6__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2482:1: ( rule__CreateOperation__Group_6__3__Impl rule__CreateOperation__Group_6__4 )
            // InternalWebserviceDSL.g:2483:2: rule__CreateOperation__Group_6__3__Impl rule__CreateOperation__Group_6__4
            {
            pushFollow(FOLLOW_8);
            rule__CreateOperation__Group_6__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CreateOperation__Group_6__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group_6__3"


    // $ANTLR start "rule__CreateOperation__Group_6__3__Impl"
    // InternalWebserviceDSL.g:2490:1: rule__CreateOperation__Group_6__3__Impl : ( ( rule__CreateOperation__Group_6_3__0 )* ) ;
    public final void rule__CreateOperation__Group_6__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2494:1: ( ( ( rule__CreateOperation__Group_6_3__0 )* ) )
            // InternalWebserviceDSL.g:2495:1: ( ( rule__CreateOperation__Group_6_3__0 )* )
            {
            // InternalWebserviceDSL.g:2495:1: ( ( rule__CreateOperation__Group_6_3__0 )* )
            // InternalWebserviceDSL.g:2496:2: ( rule__CreateOperation__Group_6_3__0 )*
            {
             before(grammarAccess.getCreateOperationAccess().getGroup_6_3()); 
            // InternalWebserviceDSL.g:2497:2: ( rule__CreateOperation__Group_6_3__0 )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( (LA18_0==18) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // InternalWebserviceDSL.g:2497:3: rule__CreateOperation__Group_6_3__0
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__CreateOperation__Group_6_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);

             after(grammarAccess.getCreateOperationAccess().getGroup_6_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group_6__3__Impl"


    // $ANTLR start "rule__CreateOperation__Group_6__4"
    // InternalWebserviceDSL.g:2505:1: rule__CreateOperation__Group_6__4 : rule__CreateOperation__Group_6__4__Impl ;
    public final void rule__CreateOperation__Group_6__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2509:1: ( rule__CreateOperation__Group_6__4__Impl )
            // InternalWebserviceDSL.g:2510:2: rule__CreateOperation__Group_6__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CreateOperation__Group_6__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group_6__4"


    // $ANTLR start "rule__CreateOperation__Group_6__4__Impl"
    // InternalWebserviceDSL.g:2516:1: rule__CreateOperation__Group_6__4__Impl : ( '}' ) ;
    public final void rule__CreateOperation__Group_6__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2520:1: ( ( '}' ) )
            // InternalWebserviceDSL.g:2521:1: ( '}' )
            {
            // InternalWebserviceDSL.g:2521:1: ( '}' )
            // InternalWebserviceDSL.g:2522:2: '}'
            {
             before(grammarAccess.getCreateOperationAccess().getRightCurlyBracketKeyword_6_4()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getCreateOperationAccess().getRightCurlyBracketKeyword_6_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group_6__4__Impl"


    // $ANTLR start "rule__CreateOperation__Group_6_3__0"
    // InternalWebserviceDSL.g:2532:1: rule__CreateOperation__Group_6_3__0 : rule__CreateOperation__Group_6_3__0__Impl rule__CreateOperation__Group_6_3__1 ;
    public final void rule__CreateOperation__Group_6_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2536:1: ( rule__CreateOperation__Group_6_3__0__Impl rule__CreateOperation__Group_6_3__1 )
            // InternalWebserviceDSL.g:2537:2: rule__CreateOperation__Group_6_3__0__Impl rule__CreateOperation__Group_6_3__1
            {
            pushFollow(FOLLOW_16);
            rule__CreateOperation__Group_6_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CreateOperation__Group_6_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group_6_3__0"


    // $ANTLR start "rule__CreateOperation__Group_6_3__0__Impl"
    // InternalWebserviceDSL.g:2544:1: rule__CreateOperation__Group_6_3__0__Impl : ( ',' ) ;
    public final void rule__CreateOperation__Group_6_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2548:1: ( ( ',' ) )
            // InternalWebserviceDSL.g:2549:1: ( ',' )
            {
            // InternalWebserviceDSL.g:2549:1: ( ',' )
            // InternalWebserviceDSL.g:2550:2: ','
            {
             before(grammarAccess.getCreateOperationAccess().getCommaKeyword_6_3_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getCreateOperationAccess().getCommaKeyword_6_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group_6_3__0__Impl"


    // $ANTLR start "rule__CreateOperation__Group_6_3__1"
    // InternalWebserviceDSL.g:2559:1: rule__CreateOperation__Group_6_3__1 : rule__CreateOperation__Group_6_3__1__Impl ;
    public final void rule__CreateOperation__Group_6_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2563:1: ( rule__CreateOperation__Group_6_3__1__Impl )
            // InternalWebserviceDSL.g:2564:2: rule__CreateOperation__Group_6_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CreateOperation__Group_6_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group_6_3__1"


    // $ANTLR start "rule__CreateOperation__Group_6_3__1__Impl"
    // InternalWebserviceDSL.g:2570:1: rule__CreateOperation__Group_6_3__1__Impl : ( ( rule__CreateOperation__ParametersAssignment_6_3_1 ) ) ;
    public final void rule__CreateOperation__Group_6_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2574:1: ( ( ( rule__CreateOperation__ParametersAssignment_6_3_1 ) ) )
            // InternalWebserviceDSL.g:2575:1: ( ( rule__CreateOperation__ParametersAssignment_6_3_1 ) )
            {
            // InternalWebserviceDSL.g:2575:1: ( ( rule__CreateOperation__ParametersAssignment_6_3_1 ) )
            // InternalWebserviceDSL.g:2576:2: ( rule__CreateOperation__ParametersAssignment_6_3_1 )
            {
             before(grammarAccess.getCreateOperationAccess().getParametersAssignment_6_3_1()); 
            // InternalWebserviceDSL.g:2577:2: ( rule__CreateOperation__ParametersAssignment_6_3_1 )
            // InternalWebserviceDSL.g:2577:3: rule__CreateOperation__ParametersAssignment_6_3_1
            {
            pushFollow(FOLLOW_2);
            rule__CreateOperation__ParametersAssignment_6_3_1();

            state._fsp--;


            }

             after(grammarAccess.getCreateOperationAccess().getParametersAssignment_6_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__Group_6_3__1__Impl"


    // $ANTLR start "rule__UpdateOperation__Group__0"
    // InternalWebserviceDSL.g:2586:1: rule__UpdateOperation__Group__0 : rule__UpdateOperation__Group__0__Impl rule__UpdateOperation__Group__1 ;
    public final void rule__UpdateOperation__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2590:1: ( rule__UpdateOperation__Group__0__Impl rule__UpdateOperation__Group__1 )
            // InternalWebserviceDSL.g:2591:2: rule__UpdateOperation__Group__0__Impl rule__UpdateOperation__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__UpdateOperation__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UpdateOperation__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group__0"


    // $ANTLR start "rule__UpdateOperation__Group__0__Impl"
    // InternalWebserviceDSL.g:2598:1: rule__UpdateOperation__Group__0__Impl : ( 'UpdateOperation' ) ;
    public final void rule__UpdateOperation__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2602:1: ( ( 'UpdateOperation' ) )
            // InternalWebserviceDSL.g:2603:1: ( 'UpdateOperation' )
            {
            // InternalWebserviceDSL.g:2603:1: ( 'UpdateOperation' )
            // InternalWebserviceDSL.g:2604:2: 'UpdateOperation'
            {
             before(grammarAccess.getUpdateOperationAccess().getUpdateOperationKeyword_0()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getUpdateOperationAccess().getUpdateOperationKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group__0__Impl"


    // $ANTLR start "rule__UpdateOperation__Group__1"
    // InternalWebserviceDSL.g:2613:1: rule__UpdateOperation__Group__1 : rule__UpdateOperation__Group__1__Impl rule__UpdateOperation__Group__2 ;
    public final void rule__UpdateOperation__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2617:1: ( rule__UpdateOperation__Group__1__Impl rule__UpdateOperation__Group__2 )
            // InternalWebserviceDSL.g:2618:2: rule__UpdateOperation__Group__1__Impl rule__UpdateOperation__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__UpdateOperation__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UpdateOperation__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group__1"


    // $ANTLR start "rule__UpdateOperation__Group__1__Impl"
    // InternalWebserviceDSL.g:2625:1: rule__UpdateOperation__Group__1__Impl : ( ( rule__UpdateOperation__NameAssignment_1 ) ) ;
    public final void rule__UpdateOperation__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2629:1: ( ( ( rule__UpdateOperation__NameAssignment_1 ) ) )
            // InternalWebserviceDSL.g:2630:1: ( ( rule__UpdateOperation__NameAssignment_1 ) )
            {
            // InternalWebserviceDSL.g:2630:1: ( ( rule__UpdateOperation__NameAssignment_1 ) )
            // InternalWebserviceDSL.g:2631:2: ( rule__UpdateOperation__NameAssignment_1 )
            {
             before(grammarAccess.getUpdateOperationAccess().getNameAssignment_1()); 
            // InternalWebserviceDSL.g:2632:2: ( rule__UpdateOperation__NameAssignment_1 )
            // InternalWebserviceDSL.g:2632:3: rule__UpdateOperation__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__UpdateOperation__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getUpdateOperationAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group__1__Impl"


    // $ANTLR start "rule__UpdateOperation__Group__2"
    // InternalWebserviceDSL.g:2640:1: rule__UpdateOperation__Group__2 : rule__UpdateOperation__Group__2__Impl rule__UpdateOperation__Group__3 ;
    public final void rule__UpdateOperation__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2644:1: ( rule__UpdateOperation__Group__2__Impl rule__UpdateOperation__Group__3 )
            // InternalWebserviceDSL.g:2645:2: rule__UpdateOperation__Group__2__Impl rule__UpdateOperation__Group__3
            {
            pushFollow(FOLLOW_15);
            rule__UpdateOperation__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UpdateOperation__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group__2"


    // $ANTLR start "rule__UpdateOperation__Group__2__Impl"
    // InternalWebserviceDSL.g:2652:1: rule__UpdateOperation__Group__2__Impl : ( '{' ) ;
    public final void rule__UpdateOperation__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2656:1: ( ( '{' ) )
            // InternalWebserviceDSL.g:2657:1: ( '{' )
            {
            // InternalWebserviceDSL.g:2657:1: ( '{' )
            // InternalWebserviceDSL.g:2658:2: '{'
            {
             before(grammarAccess.getUpdateOperationAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getUpdateOperationAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group__2__Impl"


    // $ANTLR start "rule__UpdateOperation__Group__3"
    // InternalWebserviceDSL.g:2667:1: rule__UpdateOperation__Group__3 : rule__UpdateOperation__Group__3__Impl rule__UpdateOperation__Group__4 ;
    public final void rule__UpdateOperation__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2671:1: ( rule__UpdateOperation__Group__3__Impl rule__UpdateOperation__Group__4 )
            // InternalWebserviceDSL.g:2672:2: rule__UpdateOperation__Group__3__Impl rule__UpdateOperation__Group__4
            {
            pushFollow(FOLLOW_15);
            rule__UpdateOperation__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UpdateOperation__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group__3"


    // $ANTLR start "rule__UpdateOperation__Group__3__Impl"
    // InternalWebserviceDSL.g:2679:1: rule__UpdateOperation__Group__3__Impl : ( ( rule__UpdateOperation__Group_3__0 )? ) ;
    public final void rule__UpdateOperation__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2683:1: ( ( ( rule__UpdateOperation__Group_3__0 )? ) )
            // InternalWebserviceDSL.g:2684:1: ( ( rule__UpdateOperation__Group_3__0 )? )
            {
            // InternalWebserviceDSL.g:2684:1: ( ( rule__UpdateOperation__Group_3__0 )? )
            // InternalWebserviceDSL.g:2685:2: ( rule__UpdateOperation__Group_3__0 )?
            {
             before(grammarAccess.getUpdateOperationAccess().getGroup_3()); 
            // InternalWebserviceDSL.g:2686:2: ( rule__UpdateOperation__Group_3__0 )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==17) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // InternalWebserviceDSL.g:2686:3: rule__UpdateOperation__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__UpdateOperation__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getUpdateOperationAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group__3__Impl"


    // $ANTLR start "rule__UpdateOperation__Group__4"
    // InternalWebserviceDSL.g:2694:1: rule__UpdateOperation__Group__4 : rule__UpdateOperation__Group__4__Impl rule__UpdateOperation__Group__5 ;
    public final void rule__UpdateOperation__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2698:1: ( rule__UpdateOperation__Group__4__Impl rule__UpdateOperation__Group__5 )
            // InternalWebserviceDSL.g:2699:2: rule__UpdateOperation__Group__4__Impl rule__UpdateOperation__Group__5
            {
            pushFollow(FOLLOW_15);
            rule__UpdateOperation__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UpdateOperation__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group__4"


    // $ANTLR start "rule__UpdateOperation__Group__4__Impl"
    // InternalWebserviceDSL.g:2706:1: rule__UpdateOperation__Group__4__Impl : ( ( rule__UpdateOperation__Group_4__0 )? ) ;
    public final void rule__UpdateOperation__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2710:1: ( ( ( rule__UpdateOperation__Group_4__0 )? ) )
            // InternalWebserviceDSL.g:2711:1: ( ( rule__UpdateOperation__Group_4__0 )? )
            {
            // InternalWebserviceDSL.g:2711:1: ( ( rule__UpdateOperation__Group_4__0 )? )
            // InternalWebserviceDSL.g:2712:2: ( rule__UpdateOperation__Group_4__0 )?
            {
             before(grammarAccess.getUpdateOperationAccess().getGroup_4()); 
            // InternalWebserviceDSL.g:2713:2: ( rule__UpdateOperation__Group_4__0 )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==22) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // InternalWebserviceDSL.g:2713:3: rule__UpdateOperation__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__UpdateOperation__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getUpdateOperationAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group__4__Impl"


    // $ANTLR start "rule__UpdateOperation__Group__5"
    // InternalWebserviceDSL.g:2721:1: rule__UpdateOperation__Group__5 : rule__UpdateOperation__Group__5__Impl rule__UpdateOperation__Group__6 ;
    public final void rule__UpdateOperation__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2725:1: ( rule__UpdateOperation__Group__5__Impl rule__UpdateOperation__Group__6 )
            // InternalWebserviceDSL.g:2726:2: rule__UpdateOperation__Group__5__Impl rule__UpdateOperation__Group__6
            {
            pushFollow(FOLLOW_15);
            rule__UpdateOperation__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UpdateOperation__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group__5"


    // $ANTLR start "rule__UpdateOperation__Group__5__Impl"
    // InternalWebserviceDSL.g:2733:1: rule__UpdateOperation__Group__5__Impl : ( ( rule__UpdateOperation__Group_5__0 )? ) ;
    public final void rule__UpdateOperation__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2737:1: ( ( ( rule__UpdateOperation__Group_5__0 )? ) )
            // InternalWebserviceDSL.g:2738:1: ( ( rule__UpdateOperation__Group_5__0 )? )
            {
            // InternalWebserviceDSL.g:2738:1: ( ( rule__UpdateOperation__Group_5__0 )? )
            // InternalWebserviceDSL.g:2739:2: ( rule__UpdateOperation__Group_5__0 )?
            {
             before(grammarAccess.getUpdateOperationAccess().getGroup_5()); 
            // InternalWebserviceDSL.g:2740:2: ( rule__UpdateOperation__Group_5__0 )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==23) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalWebserviceDSL.g:2740:3: rule__UpdateOperation__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__UpdateOperation__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getUpdateOperationAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group__5__Impl"


    // $ANTLR start "rule__UpdateOperation__Group__6"
    // InternalWebserviceDSL.g:2748:1: rule__UpdateOperation__Group__6 : rule__UpdateOperation__Group__6__Impl rule__UpdateOperation__Group__7 ;
    public final void rule__UpdateOperation__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2752:1: ( rule__UpdateOperation__Group__6__Impl rule__UpdateOperation__Group__7 )
            // InternalWebserviceDSL.g:2753:2: rule__UpdateOperation__Group__6__Impl rule__UpdateOperation__Group__7
            {
            pushFollow(FOLLOW_15);
            rule__UpdateOperation__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UpdateOperation__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group__6"


    // $ANTLR start "rule__UpdateOperation__Group__6__Impl"
    // InternalWebserviceDSL.g:2760:1: rule__UpdateOperation__Group__6__Impl : ( ( rule__UpdateOperation__Group_6__0 )? ) ;
    public final void rule__UpdateOperation__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2764:1: ( ( ( rule__UpdateOperation__Group_6__0 )? ) )
            // InternalWebserviceDSL.g:2765:1: ( ( rule__UpdateOperation__Group_6__0 )? )
            {
            // InternalWebserviceDSL.g:2765:1: ( ( rule__UpdateOperation__Group_6__0 )? )
            // InternalWebserviceDSL.g:2766:2: ( rule__UpdateOperation__Group_6__0 )?
            {
             before(grammarAccess.getUpdateOperationAccess().getGroup_6()); 
            // InternalWebserviceDSL.g:2767:2: ( rule__UpdateOperation__Group_6__0 )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==24) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // InternalWebserviceDSL.g:2767:3: rule__UpdateOperation__Group_6__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__UpdateOperation__Group_6__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getUpdateOperationAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group__6__Impl"


    // $ANTLR start "rule__UpdateOperation__Group__7"
    // InternalWebserviceDSL.g:2775:1: rule__UpdateOperation__Group__7 : rule__UpdateOperation__Group__7__Impl ;
    public final void rule__UpdateOperation__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2779:1: ( rule__UpdateOperation__Group__7__Impl )
            // InternalWebserviceDSL.g:2780:2: rule__UpdateOperation__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__UpdateOperation__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group__7"


    // $ANTLR start "rule__UpdateOperation__Group__7__Impl"
    // InternalWebserviceDSL.g:2786:1: rule__UpdateOperation__Group__7__Impl : ( '}' ) ;
    public final void rule__UpdateOperation__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2790:1: ( ( '}' ) )
            // InternalWebserviceDSL.g:2791:1: ( '}' )
            {
            // InternalWebserviceDSL.g:2791:1: ( '}' )
            // InternalWebserviceDSL.g:2792:2: '}'
            {
             before(grammarAccess.getUpdateOperationAccess().getRightCurlyBracketKeyword_7()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getUpdateOperationAccess().getRightCurlyBracketKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group__7__Impl"


    // $ANTLR start "rule__UpdateOperation__Group_3__0"
    // InternalWebserviceDSL.g:2802:1: rule__UpdateOperation__Group_3__0 : rule__UpdateOperation__Group_3__0__Impl rule__UpdateOperation__Group_3__1 ;
    public final void rule__UpdateOperation__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2806:1: ( rule__UpdateOperation__Group_3__0__Impl rule__UpdateOperation__Group_3__1 )
            // InternalWebserviceDSL.g:2807:2: rule__UpdateOperation__Group_3__0__Impl rule__UpdateOperation__Group_3__1
            {
            pushFollow(FOLLOW_3);
            rule__UpdateOperation__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UpdateOperation__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group_3__0"


    // $ANTLR start "rule__UpdateOperation__Group_3__0__Impl"
    // InternalWebserviceDSL.g:2814:1: rule__UpdateOperation__Group_3__0__Impl : ( 'description' ) ;
    public final void rule__UpdateOperation__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2818:1: ( ( 'description' ) )
            // InternalWebserviceDSL.g:2819:1: ( 'description' )
            {
            // InternalWebserviceDSL.g:2819:1: ( 'description' )
            // InternalWebserviceDSL.g:2820:2: 'description'
            {
             before(grammarAccess.getUpdateOperationAccess().getDescriptionKeyword_3_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getUpdateOperationAccess().getDescriptionKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group_3__0__Impl"


    // $ANTLR start "rule__UpdateOperation__Group_3__1"
    // InternalWebserviceDSL.g:2829:1: rule__UpdateOperation__Group_3__1 : rule__UpdateOperation__Group_3__1__Impl ;
    public final void rule__UpdateOperation__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2833:1: ( rule__UpdateOperation__Group_3__1__Impl )
            // InternalWebserviceDSL.g:2834:2: rule__UpdateOperation__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__UpdateOperation__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group_3__1"


    // $ANTLR start "rule__UpdateOperation__Group_3__1__Impl"
    // InternalWebserviceDSL.g:2840:1: rule__UpdateOperation__Group_3__1__Impl : ( ( rule__UpdateOperation__DescriptionAssignment_3_1 ) ) ;
    public final void rule__UpdateOperation__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2844:1: ( ( ( rule__UpdateOperation__DescriptionAssignment_3_1 ) ) )
            // InternalWebserviceDSL.g:2845:1: ( ( rule__UpdateOperation__DescriptionAssignment_3_1 ) )
            {
            // InternalWebserviceDSL.g:2845:1: ( ( rule__UpdateOperation__DescriptionAssignment_3_1 ) )
            // InternalWebserviceDSL.g:2846:2: ( rule__UpdateOperation__DescriptionAssignment_3_1 )
            {
             before(grammarAccess.getUpdateOperationAccess().getDescriptionAssignment_3_1()); 
            // InternalWebserviceDSL.g:2847:2: ( rule__UpdateOperation__DescriptionAssignment_3_1 )
            // InternalWebserviceDSL.g:2847:3: rule__UpdateOperation__DescriptionAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__UpdateOperation__DescriptionAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getUpdateOperationAccess().getDescriptionAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group_3__1__Impl"


    // $ANTLR start "rule__UpdateOperation__Group_4__0"
    // InternalWebserviceDSL.g:2856:1: rule__UpdateOperation__Group_4__0 : rule__UpdateOperation__Group_4__0__Impl rule__UpdateOperation__Group_4__1 ;
    public final void rule__UpdateOperation__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2860:1: ( rule__UpdateOperation__Group_4__0__Impl rule__UpdateOperation__Group_4__1 )
            // InternalWebserviceDSL.g:2861:2: rule__UpdateOperation__Group_4__0__Impl rule__UpdateOperation__Group_4__1
            {
            pushFollow(FOLLOW_3);
            rule__UpdateOperation__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UpdateOperation__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group_4__0"


    // $ANTLR start "rule__UpdateOperation__Group_4__0__Impl"
    // InternalWebserviceDSL.g:2868:1: rule__UpdateOperation__Group_4__0__Impl : ( 'urlSuffix' ) ;
    public final void rule__UpdateOperation__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2872:1: ( ( 'urlSuffix' ) )
            // InternalWebserviceDSL.g:2873:1: ( 'urlSuffix' )
            {
            // InternalWebserviceDSL.g:2873:1: ( 'urlSuffix' )
            // InternalWebserviceDSL.g:2874:2: 'urlSuffix'
            {
             before(grammarAccess.getUpdateOperationAccess().getUrlSuffixKeyword_4_0()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getUpdateOperationAccess().getUrlSuffixKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group_4__0__Impl"


    // $ANTLR start "rule__UpdateOperation__Group_4__1"
    // InternalWebserviceDSL.g:2883:1: rule__UpdateOperation__Group_4__1 : rule__UpdateOperation__Group_4__1__Impl ;
    public final void rule__UpdateOperation__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2887:1: ( rule__UpdateOperation__Group_4__1__Impl )
            // InternalWebserviceDSL.g:2888:2: rule__UpdateOperation__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__UpdateOperation__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group_4__1"


    // $ANTLR start "rule__UpdateOperation__Group_4__1__Impl"
    // InternalWebserviceDSL.g:2894:1: rule__UpdateOperation__Group_4__1__Impl : ( ( rule__UpdateOperation__UrlSuffixAssignment_4_1 ) ) ;
    public final void rule__UpdateOperation__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2898:1: ( ( ( rule__UpdateOperation__UrlSuffixAssignment_4_1 ) ) )
            // InternalWebserviceDSL.g:2899:1: ( ( rule__UpdateOperation__UrlSuffixAssignment_4_1 ) )
            {
            // InternalWebserviceDSL.g:2899:1: ( ( rule__UpdateOperation__UrlSuffixAssignment_4_1 ) )
            // InternalWebserviceDSL.g:2900:2: ( rule__UpdateOperation__UrlSuffixAssignment_4_1 )
            {
             before(grammarAccess.getUpdateOperationAccess().getUrlSuffixAssignment_4_1()); 
            // InternalWebserviceDSL.g:2901:2: ( rule__UpdateOperation__UrlSuffixAssignment_4_1 )
            // InternalWebserviceDSL.g:2901:3: rule__UpdateOperation__UrlSuffixAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__UpdateOperation__UrlSuffixAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getUpdateOperationAccess().getUrlSuffixAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group_4__1__Impl"


    // $ANTLR start "rule__UpdateOperation__Group_5__0"
    // InternalWebserviceDSL.g:2910:1: rule__UpdateOperation__Group_5__0 : rule__UpdateOperation__Group_5__0__Impl rule__UpdateOperation__Group_5__1 ;
    public final void rule__UpdateOperation__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2914:1: ( rule__UpdateOperation__Group_5__0__Impl rule__UpdateOperation__Group_5__1 )
            // InternalWebserviceDSL.g:2915:2: rule__UpdateOperation__Group_5__0__Impl rule__UpdateOperation__Group_5__1
            {
            pushFollow(FOLLOW_3);
            rule__UpdateOperation__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UpdateOperation__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group_5__0"


    // $ANTLR start "rule__UpdateOperation__Group_5__0__Impl"
    // InternalWebserviceDSL.g:2922:1: rule__UpdateOperation__Group_5__0__Impl : ( 'response' ) ;
    public final void rule__UpdateOperation__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2926:1: ( ( 'response' ) )
            // InternalWebserviceDSL.g:2927:1: ( 'response' )
            {
            // InternalWebserviceDSL.g:2927:1: ( 'response' )
            // InternalWebserviceDSL.g:2928:2: 'response'
            {
             before(grammarAccess.getUpdateOperationAccess().getResponseKeyword_5_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getUpdateOperationAccess().getResponseKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group_5__0__Impl"


    // $ANTLR start "rule__UpdateOperation__Group_5__1"
    // InternalWebserviceDSL.g:2937:1: rule__UpdateOperation__Group_5__1 : rule__UpdateOperation__Group_5__1__Impl ;
    public final void rule__UpdateOperation__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2941:1: ( rule__UpdateOperation__Group_5__1__Impl )
            // InternalWebserviceDSL.g:2942:2: rule__UpdateOperation__Group_5__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__UpdateOperation__Group_5__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group_5__1"


    // $ANTLR start "rule__UpdateOperation__Group_5__1__Impl"
    // InternalWebserviceDSL.g:2948:1: rule__UpdateOperation__Group_5__1__Impl : ( ( rule__UpdateOperation__ResponseAssignment_5_1 ) ) ;
    public final void rule__UpdateOperation__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2952:1: ( ( ( rule__UpdateOperation__ResponseAssignment_5_1 ) ) )
            // InternalWebserviceDSL.g:2953:1: ( ( rule__UpdateOperation__ResponseAssignment_5_1 ) )
            {
            // InternalWebserviceDSL.g:2953:1: ( ( rule__UpdateOperation__ResponseAssignment_5_1 ) )
            // InternalWebserviceDSL.g:2954:2: ( rule__UpdateOperation__ResponseAssignment_5_1 )
            {
             before(grammarAccess.getUpdateOperationAccess().getResponseAssignment_5_1()); 
            // InternalWebserviceDSL.g:2955:2: ( rule__UpdateOperation__ResponseAssignment_5_1 )
            // InternalWebserviceDSL.g:2955:3: rule__UpdateOperation__ResponseAssignment_5_1
            {
            pushFollow(FOLLOW_2);
            rule__UpdateOperation__ResponseAssignment_5_1();

            state._fsp--;


            }

             after(grammarAccess.getUpdateOperationAccess().getResponseAssignment_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group_5__1__Impl"


    // $ANTLR start "rule__UpdateOperation__Group_6__0"
    // InternalWebserviceDSL.g:2964:1: rule__UpdateOperation__Group_6__0 : rule__UpdateOperation__Group_6__0__Impl rule__UpdateOperation__Group_6__1 ;
    public final void rule__UpdateOperation__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2968:1: ( rule__UpdateOperation__Group_6__0__Impl rule__UpdateOperation__Group_6__1 )
            // InternalWebserviceDSL.g:2969:2: rule__UpdateOperation__Group_6__0__Impl rule__UpdateOperation__Group_6__1
            {
            pushFollow(FOLLOW_4);
            rule__UpdateOperation__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UpdateOperation__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group_6__0"


    // $ANTLR start "rule__UpdateOperation__Group_6__0__Impl"
    // InternalWebserviceDSL.g:2976:1: rule__UpdateOperation__Group_6__0__Impl : ( 'parameters' ) ;
    public final void rule__UpdateOperation__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2980:1: ( ( 'parameters' ) )
            // InternalWebserviceDSL.g:2981:1: ( 'parameters' )
            {
            // InternalWebserviceDSL.g:2981:1: ( 'parameters' )
            // InternalWebserviceDSL.g:2982:2: 'parameters'
            {
             before(grammarAccess.getUpdateOperationAccess().getParametersKeyword_6_0()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getUpdateOperationAccess().getParametersKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group_6__0__Impl"


    // $ANTLR start "rule__UpdateOperation__Group_6__1"
    // InternalWebserviceDSL.g:2991:1: rule__UpdateOperation__Group_6__1 : rule__UpdateOperation__Group_6__1__Impl rule__UpdateOperation__Group_6__2 ;
    public final void rule__UpdateOperation__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:2995:1: ( rule__UpdateOperation__Group_6__1__Impl rule__UpdateOperation__Group_6__2 )
            // InternalWebserviceDSL.g:2996:2: rule__UpdateOperation__Group_6__1__Impl rule__UpdateOperation__Group_6__2
            {
            pushFollow(FOLLOW_16);
            rule__UpdateOperation__Group_6__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UpdateOperation__Group_6__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group_6__1"


    // $ANTLR start "rule__UpdateOperation__Group_6__1__Impl"
    // InternalWebserviceDSL.g:3003:1: rule__UpdateOperation__Group_6__1__Impl : ( '{' ) ;
    public final void rule__UpdateOperation__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3007:1: ( ( '{' ) )
            // InternalWebserviceDSL.g:3008:1: ( '{' )
            {
            // InternalWebserviceDSL.g:3008:1: ( '{' )
            // InternalWebserviceDSL.g:3009:2: '{'
            {
             before(grammarAccess.getUpdateOperationAccess().getLeftCurlyBracketKeyword_6_1()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getUpdateOperationAccess().getLeftCurlyBracketKeyword_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group_6__1__Impl"


    // $ANTLR start "rule__UpdateOperation__Group_6__2"
    // InternalWebserviceDSL.g:3018:1: rule__UpdateOperation__Group_6__2 : rule__UpdateOperation__Group_6__2__Impl rule__UpdateOperation__Group_6__3 ;
    public final void rule__UpdateOperation__Group_6__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3022:1: ( rule__UpdateOperation__Group_6__2__Impl rule__UpdateOperation__Group_6__3 )
            // InternalWebserviceDSL.g:3023:2: rule__UpdateOperation__Group_6__2__Impl rule__UpdateOperation__Group_6__3
            {
            pushFollow(FOLLOW_8);
            rule__UpdateOperation__Group_6__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UpdateOperation__Group_6__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group_6__2"


    // $ANTLR start "rule__UpdateOperation__Group_6__2__Impl"
    // InternalWebserviceDSL.g:3030:1: rule__UpdateOperation__Group_6__2__Impl : ( ( rule__UpdateOperation__ParametersAssignment_6_2 ) ) ;
    public final void rule__UpdateOperation__Group_6__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3034:1: ( ( ( rule__UpdateOperation__ParametersAssignment_6_2 ) ) )
            // InternalWebserviceDSL.g:3035:1: ( ( rule__UpdateOperation__ParametersAssignment_6_2 ) )
            {
            // InternalWebserviceDSL.g:3035:1: ( ( rule__UpdateOperation__ParametersAssignment_6_2 ) )
            // InternalWebserviceDSL.g:3036:2: ( rule__UpdateOperation__ParametersAssignment_6_2 )
            {
             before(grammarAccess.getUpdateOperationAccess().getParametersAssignment_6_2()); 
            // InternalWebserviceDSL.g:3037:2: ( rule__UpdateOperation__ParametersAssignment_6_2 )
            // InternalWebserviceDSL.g:3037:3: rule__UpdateOperation__ParametersAssignment_6_2
            {
            pushFollow(FOLLOW_2);
            rule__UpdateOperation__ParametersAssignment_6_2();

            state._fsp--;


            }

             after(grammarAccess.getUpdateOperationAccess().getParametersAssignment_6_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group_6__2__Impl"


    // $ANTLR start "rule__UpdateOperation__Group_6__3"
    // InternalWebserviceDSL.g:3045:1: rule__UpdateOperation__Group_6__3 : rule__UpdateOperation__Group_6__3__Impl rule__UpdateOperation__Group_6__4 ;
    public final void rule__UpdateOperation__Group_6__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3049:1: ( rule__UpdateOperation__Group_6__3__Impl rule__UpdateOperation__Group_6__4 )
            // InternalWebserviceDSL.g:3050:2: rule__UpdateOperation__Group_6__3__Impl rule__UpdateOperation__Group_6__4
            {
            pushFollow(FOLLOW_8);
            rule__UpdateOperation__Group_6__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UpdateOperation__Group_6__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group_6__3"


    // $ANTLR start "rule__UpdateOperation__Group_6__3__Impl"
    // InternalWebserviceDSL.g:3057:1: rule__UpdateOperation__Group_6__3__Impl : ( ( rule__UpdateOperation__Group_6_3__0 )* ) ;
    public final void rule__UpdateOperation__Group_6__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3061:1: ( ( ( rule__UpdateOperation__Group_6_3__0 )* ) )
            // InternalWebserviceDSL.g:3062:1: ( ( rule__UpdateOperation__Group_6_3__0 )* )
            {
            // InternalWebserviceDSL.g:3062:1: ( ( rule__UpdateOperation__Group_6_3__0 )* )
            // InternalWebserviceDSL.g:3063:2: ( rule__UpdateOperation__Group_6_3__0 )*
            {
             before(grammarAccess.getUpdateOperationAccess().getGroup_6_3()); 
            // InternalWebserviceDSL.g:3064:2: ( rule__UpdateOperation__Group_6_3__0 )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( (LA23_0==18) ) {
                    alt23=1;
                }


                switch (alt23) {
            	case 1 :
            	    // InternalWebserviceDSL.g:3064:3: rule__UpdateOperation__Group_6_3__0
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__UpdateOperation__Group_6_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);

             after(grammarAccess.getUpdateOperationAccess().getGroup_6_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group_6__3__Impl"


    // $ANTLR start "rule__UpdateOperation__Group_6__4"
    // InternalWebserviceDSL.g:3072:1: rule__UpdateOperation__Group_6__4 : rule__UpdateOperation__Group_6__4__Impl ;
    public final void rule__UpdateOperation__Group_6__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3076:1: ( rule__UpdateOperation__Group_6__4__Impl )
            // InternalWebserviceDSL.g:3077:2: rule__UpdateOperation__Group_6__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__UpdateOperation__Group_6__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group_6__4"


    // $ANTLR start "rule__UpdateOperation__Group_6__4__Impl"
    // InternalWebserviceDSL.g:3083:1: rule__UpdateOperation__Group_6__4__Impl : ( '}' ) ;
    public final void rule__UpdateOperation__Group_6__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3087:1: ( ( '}' ) )
            // InternalWebserviceDSL.g:3088:1: ( '}' )
            {
            // InternalWebserviceDSL.g:3088:1: ( '}' )
            // InternalWebserviceDSL.g:3089:2: '}'
            {
             before(grammarAccess.getUpdateOperationAccess().getRightCurlyBracketKeyword_6_4()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getUpdateOperationAccess().getRightCurlyBracketKeyword_6_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group_6__4__Impl"


    // $ANTLR start "rule__UpdateOperation__Group_6_3__0"
    // InternalWebserviceDSL.g:3099:1: rule__UpdateOperation__Group_6_3__0 : rule__UpdateOperation__Group_6_3__0__Impl rule__UpdateOperation__Group_6_3__1 ;
    public final void rule__UpdateOperation__Group_6_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3103:1: ( rule__UpdateOperation__Group_6_3__0__Impl rule__UpdateOperation__Group_6_3__1 )
            // InternalWebserviceDSL.g:3104:2: rule__UpdateOperation__Group_6_3__0__Impl rule__UpdateOperation__Group_6_3__1
            {
            pushFollow(FOLLOW_16);
            rule__UpdateOperation__Group_6_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UpdateOperation__Group_6_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group_6_3__0"


    // $ANTLR start "rule__UpdateOperation__Group_6_3__0__Impl"
    // InternalWebserviceDSL.g:3111:1: rule__UpdateOperation__Group_6_3__0__Impl : ( ',' ) ;
    public final void rule__UpdateOperation__Group_6_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3115:1: ( ( ',' ) )
            // InternalWebserviceDSL.g:3116:1: ( ',' )
            {
            // InternalWebserviceDSL.g:3116:1: ( ',' )
            // InternalWebserviceDSL.g:3117:2: ','
            {
             before(grammarAccess.getUpdateOperationAccess().getCommaKeyword_6_3_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getUpdateOperationAccess().getCommaKeyword_6_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group_6_3__0__Impl"


    // $ANTLR start "rule__UpdateOperation__Group_6_3__1"
    // InternalWebserviceDSL.g:3126:1: rule__UpdateOperation__Group_6_3__1 : rule__UpdateOperation__Group_6_3__1__Impl ;
    public final void rule__UpdateOperation__Group_6_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3130:1: ( rule__UpdateOperation__Group_6_3__1__Impl )
            // InternalWebserviceDSL.g:3131:2: rule__UpdateOperation__Group_6_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__UpdateOperation__Group_6_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group_6_3__1"


    // $ANTLR start "rule__UpdateOperation__Group_6_3__1__Impl"
    // InternalWebserviceDSL.g:3137:1: rule__UpdateOperation__Group_6_3__1__Impl : ( ( rule__UpdateOperation__ParametersAssignment_6_3_1 ) ) ;
    public final void rule__UpdateOperation__Group_6_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3141:1: ( ( ( rule__UpdateOperation__ParametersAssignment_6_3_1 ) ) )
            // InternalWebserviceDSL.g:3142:1: ( ( rule__UpdateOperation__ParametersAssignment_6_3_1 ) )
            {
            // InternalWebserviceDSL.g:3142:1: ( ( rule__UpdateOperation__ParametersAssignment_6_3_1 ) )
            // InternalWebserviceDSL.g:3143:2: ( rule__UpdateOperation__ParametersAssignment_6_3_1 )
            {
             before(grammarAccess.getUpdateOperationAccess().getParametersAssignment_6_3_1()); 
            // InternalWebserviceDSL.g:3144:2: ( rule__UpdateOperation__ParametersAssignment_6_3_1 )
            // InternalWebserviceDSL.g:3144:3: rule__UpdateOperation__ParametersAssignment_6_3_1
            {
            pushFollow(FOLLOW_2);
            rule__UpdateOperation__ParametersAssignment_6_3_1();

            state._fsp--;


            }

             after(grammarAccess.getUpdateOperationAccess().getParametersAssignment_6_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__Group_6_3__1__Impl"


    // $ANTLR start "rule__DeleteOperation__Group__0"
    // InternalWebserviceDSL.g:3153:1: rule__DeleteOperation__Group__0 : rule__DeleteOperation__Group__0__Impl rule__DeleteOperation__Group__1 ;
    public final void rule__DeleteOperation__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3157:1: ( rule__DeleteOperation__Group__0__Impl rule__DeleteOperation__Group__1 )
            // InternalWebserviceDSL.g:3158:2: rule__DeleteOperation__Group__0__Impl rule__DeleteOperation__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__DeleteOperation__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DeleteOperation__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group__0"


    // $ANTLR start "rule__DeleteOperation__Group__0__Impl"
    // InternalWebserviceDSL.g:3165:1: rule__DeleteOperation__Group__0__Impl : ( 'DeleteOperation' ) ;
    public final void rule__DeleteOperation__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3169:1: ( ( 'DeleteOperation' ) )
            // InternalWebserviceDSL.g:3170:1: ( 'DeleteOperation' )
            {
            // InternalWebserviceDSL.g:3170:1: ( 'DeleteOperation' )
            // InternalWebserviceDSL.g:3171:2: 'DeleteOperation'
            {
             before(grammarAccess.getDeleteOperationAccess().getDeleteOperationKeyword_0()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getDeleteOperationAccess().getDeleteOperationKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group__0__Impl"


    // $ANTLR start "rule__DeleteOperation__Group__1"
    // InternalWebserviceDSL.g:3180:1: rule__DeleteOperation__Group__1 : rule__DeleteOperation__Group__1__Impl rule__DeleteOperation__Group__2 ;
    public final void rule__DeleteOperation__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3184:1: ( rule__DeleteOperation__Group__1__Impl rule__DeleteOperation__Group__2 )
            // InternalWebserviceDSL.g:3185:2: rule__DeleteOperation__Group__1__Impl rule__DeleteOperation__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__DeleteOperation__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DeleteOperation__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group__1"


    // $ANTLR start "rule__DeleteOperation__Group__1__Impl"
    // InternalWebserviceDSL.g:3192:1: rule__DeleteOperation__Group__1__Impl : ( ( rule__DeleteOperation__NameAssignment_1 ) ) ;
    public final void rule__DeleteOperation__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3196:1: ( ( ( rule__DeleteOperation__NameAssignment_1 ) ) )
            // InternalWebserviceDSL.g:3197:1: ( ( rule__DeleteOperation__NameAssignment_1 ) )
            {
            // InternalWebserviceDSL.g:3197:1: ( ( rule__DeleteOperation__NameAssignment_1 ) )
            // InternalWebserviceDSL.g:3198:2: ( rule__DeleteOperation__NameAssignment_1 )
            {
             before(grammarAccess.getDeleteOperationAccess().getNameAssignment_1()); 
            // InternalWebserviceDSL.g:3199:2: ( rule__DeleteOperation__NameAssignment_1 )
            // InternalWebserviceDSL.g:3199:3: rule__DeleteOperation__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__DeleteOperation__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getDeleteOperationAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group__1__Impl"


    // $ANTLR start "rule__DeleteOperation__Group__2"
    // InternalWebserviceDSL.g:3207:1: rule__DeleteOperation__Group__2 : rule__DeleteOperation__Group__2__Impl rule__DeleteOperation__Group__3 ;
    public final void rule__DeleteOperation__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3211:1: ( rule__DeleteOperation__Group__2__Impl rule__DeleteOperation__Group__3 )
            // InternalWebserviceDSL.g:3212:2: rule__DeleteOperation__Group__2__Impl rule__DeleteOperation__Group__3
            {
            pushFollow(FOLLOW_15);
            rule__DeleteOperation__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DeleteOperation__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group__2"


    // $ANTLR start "rule__DeleteOperation__Group__2__Impl"
    // InternalWebserviceDSL.g:3219:1: rule__DeleteOperation__Group__2__Impl : ( '{' ) ;
    public final void rule__DeleteOperation__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3223:1: ( ( '{' ) )
            // InternalWebserviceDSL.g:3224:1: ( '{' )
            {
            // InternalWebserviceDSL.g:3224:1: ( '{' )
            // InternalWebserviceDSL.g:3225:2: '{'
            {
             before(grammarAccess.getDeleteOperationAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getDeleteOperationAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group__2__Impl"


    // $ANTLR start "rule__DeleteOperation__Group__3"
    // InternalWebserviceDSL.g:3234:1: rule__DeleteOperation__Group__3 : rule__DeleteOperation__Group__3__Impl rule__DeleteOperation__Group__4 ;
    public final void rule__DeleteOperation__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3238:1: ( rule__DeleteOperation__Group__3__Impl rule__DeleteOperation__Group__4 )
            // InternalWebserviceDSL.g:3239:2: rule__DeleteOperation__Group__3__Impl rule__DeleteOperation__Group__4
            {
            pushFollow(FOLLOW_15);
            rule__DeleteOperation__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DeleteOperation__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group__3"


    // $ANTLR start "rule__DeleteOperation__Group__3__Impl"
    // InternalWebserviceDSL.g:3246:1: rule__DeleteOperation__Group__3__Impl : ( ( rule__DeleteOperation__Group_3__0 )? ) ;
    public final void rule__DeleteOperation__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3250:1: ( ( ( rule__DeleteOperation__Group_3__0 )? ) )
            // InternalWebserviceDSL.g:3251:1: ( ( rule__DeleteOperation__Group_3__0 )? )
            {
            // InternalWebserviceDSL.g:3251:1: ( ( rule__DeleteOperation__Group_3__0 )? )
            // InternalWebserviceDSL.g:3252:2: ( rule__DeleteOperation__Group_3__0 )?
            {
             before(grammarAccess.getDeleteOperationAccess().getGroup_3()); 
            // InternalWebserviceDSL.g:3253:2: ( rule__DeleteOperation__Group_3__0 )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==17) ) {
                alt24=1;
            }
            switch (alt24) {
                case 1 :
                    // InternalWebserviceDSL.g:3253:3: rule__DeleteOperation__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__DeleteOperation__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDeleteOperationAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group__3__Impl"


    // $ANTLR start "rule__DeleteOperation__Group__4"
    // InternalWebserviceDSL.g:3261:1: rule__DeleteOperation__Group__4 : rule__DeleteOperation__Group__4__Impl rule__DeleteOperation__Group__5 ;
    public final void rule__DeleteOperation__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3265:1: ( rule__DeleteOperation__Group__4__Impl rule__DeleteOperation__Group__5 )
            // InternalWebserviceDSL.g:3266:2: rule__DeleteOperation__Group__4__Impl rule__DeleteOperation__Group__5
            {
            pushFollow(FOLLOW_15);
            rule__DeleteOperation__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DeleteOperation__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group__4"


    // $ANTLR start "rule__DeleteOperation__Group__4__Impl"
    // InternalWebserviceDSL.g:3273:1: rule__DeleteOperation__Group__4__Impl : ( ( rule__DeleteOperation__Group_4__0 )? ) ;
    public final void rule__DeleteOperation__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3277:1: ( ( ( rule__DeleteOperation__Group_4__0 )? ) )
            // InternalWebserviceDSL.g:3278:1: ( ( rule__DeleteOperation__Group_4__0 )? )
            {
            // InternalWebserviceDSL.g:3278:1: ( ( rule__DeleteOperation__Group_4__0 )? )
            // InternalWebserviceDSL.g:3279:2: ( rule__DeleteOperation__Group_4__0 )?
            {
             before(grammarAccess.getDeleteOperationAccess().getGroup_4()); 
            // InternalWebserviceDSL.g:3280:2: ( rule__DeleteOperation__Group_4__0 )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==22) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // InternalWebserviceDSL.g:3280:3: rule__DeleteOperation__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__DeleteOperation__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDeleteOperationAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group__4__Impl"


    // $ANTLR start "rule__DeleteOperation__Group__5"
    // InternalWebserviceDSL.g:3288:1: rule__DeleteOperation__Group__5 : rule__DeleteOperation__Group__5__Impl rule__DeleteOperation__Group__6 ;
    public final void rule__DeleteOperation__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3292:1: ( rule__DeleteOperation__Group__5__Impl rule__DeleteOperation__Group__6 )
            // InternalWebserviceDSL.g:3293:2: rule__DeleteOperation__Group__5__Impl rule__DeleteOperation__Group__6
            {
            pushFollow(FOLLOW_15);
            rule__DeleteOperation__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DeleteOperation__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group__5"


    // $ANTLR start "rule__DeleteOperation__Group__5__Impl"
    // InternalWebserviceDSL.g:3300:1: rule__DeleteOperation__Group__5__Impl : ( ( rule__DeleteOperation__Group_5__0 )? ) ;
    public final void rule__DeleteOperation__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3304:1: ( ( ( rule__DeleteOperation__Group_5__0 )? ) )
            // InternalWebserviceDSL.g:3305:1: ( ( rule__DeleteOperation__Group_5__0 )? )
            {
            // InternalWebserviceDSL.g:3305:1: ( ( rule__DeleteOperation__Group_5__0 )? )
            // InternalWebserviceDSL.g:3306:2: ( rule__DeleteOperation__Group_5__0 )?
            {
             before(grammarAccess.getDeleteOperationAccess().getGroup_5()); 
            // InternalWebserviceDSL.g:3307:2: ( rule__DeleteOperation__Group_5__0 )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==23) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // InternalWebserviceDSL.g:3307:3: rule__DeleteOperation__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__DeleteOperation__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDeleteOperationAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group__5__Impl"


    // $ANTLR start "rule__DeleteOperation__Group__6"
    // InternalWebserviceDSL.g:3315:1: rule__DeleteOperation__Group__6 : rule__DeleteOperation__Group__6__Impl rule__DeleteOperation__Group__7 ;
    public final void rule__DeleteOperation__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3319:1: ( rule__DeleteOperation__Group__6__Impl rule__DeleteOperation__Group__7 )
            // InternalWebserviceDSL.g:3320:2: rule__DeleteOperation__Group__6__Impl rule__DeleteOperation__Group__7
            {
            pushFollow(FOLLOW_15);
            rule__DeleteOperation__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DeleteOperation__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group__6"


    // $ANTLR start "rule__DeleteOperation__Group__6__Impl"
    // InternalWebserviceDSL.g:3327:1: rule__DeleteOperation__Group__6__Impl : ( ( rule__DeleteOperation__Group_6__0 )? ) ;
    public final void rule__DeleteOperation__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3331:1: ( ( ( rule__DeleteOperation__Group_6__0 )? ) )
            // InternalWebserviceDSL.g:3332:1: ( ( rule__DeleteOperation__Group_6__0 )? )
            {
            // InternalWebserviceDSL.g:3332:1: ( ( rule__DeleteOperation__Group_6__0 )? )
            // InternalWebserviceDSL.g:3333:2: ( rule__DeleteOperation__Group_6__0 )?
            {
             before(grammarAccess.getDeleteOperationAccess().getGroup_6()); 
            // InternalWebserviceDSL.g:3334:2: ( rule__DeleteOperation__Group_6__0 )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==24) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // InternalWebserviceDSL.g:3334:3: rule__DeleteOperation__Group_6__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__DeleteOperation__Group_6__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDeleteOperationAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group__6__Impl"


    // $ANTLR start "rule__DeleteOperation__Group__7"
    // InternalWebserviceDSL.g:3342:1: rule__DeleteOperation__Group__7 : rule__DeleteOperation__Group__7__Impl ;
    public final void rule__DeleteOperation__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3346:1: ( rule__DeleteOperation__Group__7__Impl )
            // InternalWebserviceDSL.g:3347:2: rule__DeleteOperation__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DeleteOperation__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group__7"


    // $ANTLR start "rule__DeleteOperation__Group__7__Impl"
    // InternalWebserviceDSL.g:3353:1: rule__DeleteOperation__Group__7__Impl : ( '}' ) ;
    public final void rule__DeleteOperation__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3357:1: ( ( '}' ) )
            // InternalWebserviceDSL.g:3358:1: ( '}' )
            {
            // InternalWebserviceDSL.g:3358:1: ( '}' )
            // InternalWebserviceDSL.g:3359:2: '}'
            {
             before(grammarAccess.getDeleteOperationAccess().getRightCurlyBracketKeyword_7()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getDeleteOperationAccess().getRightCurlyBracketKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group__7__Impl"


    // $ANTLR start "rule__DeleteOperation__Group_3__0"
    // InternalWebserviceDSL.g:3369:1: rule__DeleteOperation__Group_3__0 : rule__DeleteOperation__Group_3__0__Impl rule__DeleteOperation__Group_3__1 ;
    public final void rule__DeleteOperation__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3373:1: ( rule__DeleteOperation__Group_3__0__Impl rule__DeleteOperation__Group_3__1 )
            // InternalWebserviceDSL.g:3374:2: rule__DeleteOperation__Group_3__0__Impl rule__DeleteOperation__Group_3__1
            {
            pushFollow(FOLLOW_3);
            rule__DeleteOperation__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DeleteOperation__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group_3__0"


    // $ANTLR start "rule__DeleteOperation__Group_3__0__Impl"
    // InternalWebserviceDSL.g:3381:1: rule__DeleteOperation__Group_3__0__Impl : ( 'description' ) ;
    public final void rule__DeleteOperation__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3385:1: ( ( 'description' ) )
            // InternalWebserviceDSL.g:3386:1: ( 'description' )
            {
            // InternalWebserviceDSL.g:3386:1: ( 'description' )
            // InternalWebserviceDSL.g:3387:2: 'description'
            {
             before(grammarAccess.getDeleteOperationAccess().getDescriptionKeyword_3_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getDeleteOperationAccess().getDescriptionKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group_3__0__Impl"


    // $ANTLR start "rule__DeleteOperation__Group_3__1"
    // InternalWebserviceDSL.g:3396:1: rule__DeleteOperation__Group_3__1 : rule__DeleteOperation__Group_3__1__Impl ;
    public final void rule__DeleteOperation__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3400:1: ( rule__DeleteOperation__Group_3__1__Impl )
            // InternalWebserviceDSL.g:3401:2: rule__DeleteOperation__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DeleteOperation__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group_3__1"


    // $ANTLR start "rule__DeleteOperation__Group_3__1__Impl"
    // InternalWebserviceDSL.g:3407:1: rule__DeleteOperation__Group_3__1__Impl : ( ( rule__DeleteOperation__DescriptionAssignment_3_1 ) ) ;
    public final void rule__DeleteOperation__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3411:1: ( ( ( rule__DeleteOperation__DescriptionAssignment_3_1 ) ) )
            // InternalWebserviceDSL.g:3412:1: ( ( rule__DeleteOperation__DescriptionAssignment_3_1 ) )
            {
            // InternalWebserviceDSL.g:3412:1: ( ( rule__DeleteOperation__DescriptionAssignment_3_1 ) )
            // InternalWebserviceDSL.g:3413:2: ( rule__DeleteOperation__DescriptionAssignment_3_1 )
            {
             before(grammarAccess.getDeleteOperationAccess().getDescriptionAssignment_3_1()); 
            // InternalWebserviceDSL.g:3414:2: ( rule__DeleteOperation__DescriptionAssignment_3_1 )
            // InternalWebserviceDSL.g:3414:3: rule__DeleteOperation__DescriptionAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__DeleteOperation__DescriptionAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getDeleteOperationAccess().getDescriptionAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group_3__1__Impl"


    // $ANTLR start "rule__DeleteOperation__Group_4__0"
    // InternalWebserviceDSL.g:3423:1: rule__DeleteOperation__Group_4__0 : rule__DeleteOperation__Group_4__0__Impl rule__DeleteOperation__Group_4__1 ;
    public final void rule__DeleteOperation__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3427:1: ( rule__DeleteOperation__Group_4__0__Impl rule__DeleteOperation__Group_4__1 )
            // InternalWebserviceDSL.g:3428:2: rule__DeleteOperation__Group_4__0__Impl rule__DeleteOperation__Group_4__1
            {
            pushFollow(FOLLOW_3);
            rule__DeleteOperation__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DeleteOperation__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group_4__0"


    // $ANTLR start "rule__DeleteOperation__Group_4__0__Impl"
    // InternalWebserviceDSL.g:3435:1: rule__DeleteOperation__Group_4__0__Impl : ( 'urlSuffix' ) ;
    public final void rule__DeleteOperation__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3439:1: ( ( 'urlSuffix' ) )
            // InternalWebserviceDSL.g:3440:1: ( 'urlSuffix' )
            {
            // InternalWebserviceDSL.g:3440:1: ( 'urlSuffix' )
            // InternalWebserviceDSL.g:3441:2: 'urlSuffix'
            {
             before(grammarAccess.getDeleteOperationAccess().getUrlSuffixKeyword_4_0()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getDeleteOperationAccess().getUrlSuffixKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group_4__0__Impl"


    // $ANTLR start "rule__DeleteOperation__Group_4__1"
    // InternalWebserviceDSL.g:3450:1: rule__DeleteOperation__Group_4__1 : rule__DeleteOperation__Group_4__1__Impl ;
    public final void rule__DeleteOperation__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3454:1: ( rule__DeleteOperation__Group_4__1__Impl )
            // InternalWebserviceDSL.g:3455:2: rule__DeleteOperation__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DeleteOperation__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group_4__1"


    // $ANTLR start "rule__DeleteOperation__Group_4__1__Impl"
    // InternalWebserviceDSL.g:3461:1: rule__DeleteOperation__Group_4__1__Impl : ( ( rule__DeleteOperation__UrlSuffixAssignment_4_1 ) ) ;
    public final void rule__DeleteOperation__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3465:1: ( ( ( rule__DeleteOperation__UrlSuffixAssignment_4_1 ) ) )
            // InternalWebserviceDSL.g:3466:1: ( ( rule__DeleteOperation__UrlSuffixAssignment_4_1 ) )
            {
            // InternalWebserviceDSL.g:3466:1: ( ( rule__DeleteOperation__UrlSuffixAssignment_4_1 ) )
            // InternalWebserviceDSL.g:3467:2: ( rule__DeleteOperation__UrlSuffixAssignment_4_1 )
            {
             before(grammarAccess.getDeleteOperationAccess().getUrlSuffixAssignment_4_1()); 
            // InternalWebserviceDSL.g:3468:2: ( rule__DeleteOperation__UrlSuffixAssignment_4_1 )
            // InternalWebserviceDSL.g:3468:3: rule__DeleteOperation__UrlSuffixAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__DeleteOperation__UrlSuffixAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getDeleteOperationAccess().getUrlSuffixAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group_4__1__Impl"


    // $ANTLR start "rule__DeleteOperation__Group_5__0"
    // InternalWebserviceDSL.g:3477:1: rule__DeleteOperation__Group_5__0 : rule__DeleteOperation__Group_5__0__Impl rule__DeleteOperation__Group_5__1 ;
    public final void rule__DeleteOperation__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3481:1: ( rule__DeleteOperation__Group_5__0__Impl rule__DeleteOperation__Group_5__1 )
            // InternalWebserviceDSL.g:3482:2: rule__DeleteOperation__Group_5__0__Impl rule__DeleteOperation__Group_5__1
            {
            pushFollow(FOLLOW_3);
            rule__DeleteOperation__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DeleteOperation__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group_5__0"


    // $ANTLR start "rule__DeleteOperation__Group_5__0__Impl"
    // InternalWebserviceDSL.g:3489:1: rule__DeleteOperation__Group_5__0__Impl : ( 'response' ) ;
    public final void rule__DeleteOperation__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3493:1: ( ( 'response' ) )
            // InternalWebserviceDSL.g:3494:1: ( 'response' )
            {
            // InternalWebserviceDSL.g:3494:1: ( 'response' )
            // InternalWebserviceDSL.g:3495:2: 'response'
            {
             before(grammarAccess.getDeleteOperationAccess().getResponseKeyword_5_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getDeleteOperationAccess().getResponseKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group_5__0__Impl"


    // $ANTLR start "rule__DeleteOperation__Group_5__1"
    // InternalWebserviceDSL.g:3504:1: rule__DeleteOperation__Group_5__1 : rule__DeleteOperation__Group_5__1__Impl ;
    public final void rule__DeleteOperation__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3508:1: ( rule__DeleteOperation__Group_5__1__Impl )
            // InternalWebserviceDSL.g:3509:2: rule__DeleteOperation__Group_5__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DeleteOperation__Group_5__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group_5__1"


    // $ANTLR start "rule__DeleteOperation__Group_5__1__Impl"
    // InternalWebserviceDSL.g:3515:1: rule__DeleteOperation__Group_5__1__Impl : ( ( rule__DeleteOperation__ResponseAssignment_5_1 ) ) ;
    public final void rule__DeleteOperation__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3519:1: ( ( ( rule__DeleteOperation__ResponseAssignment_5_1 ) ) )
            // InternalWebserviceDSL.g:3520:1: ( ( rule__DeleteOperation__ResponseAssignment_5_1 ) )
            {
            // InternalWebserviceDSL.g:3520:1: ( ( rule__DeleteOperation__ResponseAssignment_5_1 ) )
            // InternalWebserviceDSL.g:3521:2: ( rule__DeleteOperation__ResponseAssignment_5_1 )
            {
             before(grammarAccess.getDeleteOperationAccess().getResponseAssignment_5_1()); 
            // InternalWebserviceDSL.g:3522:2: ( rule__DeleteOperation__ResponseAssignment_5_1 )
            // InternalWebserviceDSL.g:3522:3: rule__DeleteOperation__ResponseAssignment_5_1
            {
            pushFollow(FOLLOW_2);
            rule__DeleteOperation__ResponseAssignment_5_1();

            state._fsp--;


            }

             after(grammarAccess.getDeleteOperationAccess().getResponseAssignment_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group_5__1__Impl"


    // $ANTLR start "rule__DeleteOperation__Group_6__0"
    // InternalWebserviceDSL.g:3531:1: rule__DeleteOperation__Group_6__0 : rule__DeleteOperation__Group_6__0__Impl rule__DeleteOperation__Group_6__1 ;
    public final void rule__DeleteOperation__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3535:1: ( rule__DeleteOperation__Group_6__0__Impl rule__DeleteOperation__Group_6__1 )
            // InternalWebserviceDSL.g:3536:2: rule__DeleteOperation__Group_6__0__Impl rule__DeleteOperation__Group_6__1
            {
            pushFollow(FOLLOW_4);
            rule__DeleteOperation__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DeleteOperation__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group_6__0"


    // $ANTLR start "rule__DeleteOperation__Group_6__0__Impl"
    // InternalWebserviceDSL.g:3543:1: rule__DeleteOperation__Group_6__0__Impl : ( 'parameters' ) ;
    public final void rule__DeleteOperation__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3547:1: ( ( 'parameters' ) )
            // InternalWebserviceDSL.g:3548:1: ( 'parameters' )
            {
            // InternalWebserviceDSL.g:3548:1: ( 'parameters' )
            // InternalWebserviceDSL.g:3549:2: 'parameters'
            {
             before(grammarAccess.getDeleteOperationAccess().getParametersKeyword_6_0()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getDeleteOperationAccess().getParametersKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group_6__0__Impl"


    // $ANTLR start "rule__DeleteOperation__Group_6__1"
    // InternalWebserviceDSL.g:3558:1: rule__DeleteOperation__Group_6__1 : rule__DeleteOperation__Group_6__1__Impl rule__DeleteOperation__Group_6__2 ;
    public final void rule__DeleteOperation__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3562:1: ( rule__DeleteOperation__Group_6__1__Impl rule__DeleteOperation__Group_6__2 )
            // InternalWebserviceDSL.g:3563:2: rule__DeleteOperation__Group_6__1__Impl rule__DeleteOperation__Group_6__2
            {
            pushFollow(FOLLOW_16);
            rule__DeleteOperation__Group_6__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DeleteOperation__Group_6__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group_6__1"


    // $ANTLR start "rule__DeleteOperation__Group_6__1__Impl"
    // InternalWebserviceDSL.g:3570:1: rule__DeleteOperation__Group_6__1__Impl : ( '{' ) ;
    public final void rule__DeleteOperation__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3574:1: ( ( '{' ) )
            // InternalWebserviceDSL.g:3575:1: ( '{' )
            {
            // InternalWebserviceDSL.g:3575:1: ( '{' )
            // InternalWebserviceDSL.g:3576:2: '{'
            {
             before(grammarAccess.getDeleteOperationAccess().getLeftCurlyBracketKeyword_6_1()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getDeleteOperationAccess().getLeftCurlyBracketKeyword_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group_6__1__Impl"


    // $ANTLR start "rule__DeleteOperation__Group_6__2"
    // InternalWebserviceDSL.g:3585:1: rule__DeleteOperation__Group_6__2 : rule__DeleteOperation__Group_6__2__Impl rule__DeleteOperation__Group_6__3 ;
    public final void rule__DeleteOperation__Group_6__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3589:1: ( rule__DeleteOperation__Group_6__2__Impl rule__DeleteOperation__Group_6__3 )
            // InternalWebserviceDSL.g:3590:2: rule__DeleteOperation__Group_6__2__Impl rule__DeleteOperation__Group_6__3
            {
            pushFollow(FOLLOW_8);
            rule__DeleteOperation__Group_6__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DeleteOperation__Group_6__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group_6__2"


    // $ANTLR start "rule__DeleteOperation__Group_6__2__Impl"
    // InternalWebserviceDSL.g:3597:1: rule__DeleteOperation__Group_6__2__Impl : ( ( rule__DeleteOperation__ParametersAssignment_6_2 ) ) ;
    public final void rule__DeleteOperation__Group_6__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3601:1: ( ( ( rule__DeleteOperation__ParametersAssignment_6_2 ) ) )
            // InternalWebserviceDSL.g:3602:1: ( ( rule__DeleteOperation__ParametersAssignment_6_2 ) )
            {
            // InternalWebserviceDSL.g:3602:1: ( ( rule__DeleteOperation__ParametersAssignment_6_2 ) )
            // InternalWebserviceDSL.g:3603:2: ( rule__DeleteOperation__ParametersAssignment_6_2 )
            {
             before(grammarAccess.getDeleteOperationAccess().getParametersAssignment_6_2()); 
            // InternalWebserviceDSL.g:3604:2: ( rule__DeleteOperation__ParametersAssignment_6_2 )
            // InternalWebserviceDSL.g:3604:3: rule__DeleteOperation__ParametersAssignment_6_2
            {
            pushFollow(FOLLOW_2);
            rule__DeleteOperation__ParametersAssignment_6_2();

            state._fsp--;


            }

             after(grammarAccess.getDeleteOperationAccess().getParametersAssignment_6_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group_6__2__Impl"


    // $ANTLR start "rule__DeleteOperation__Group_6__3"
    // InternalWebserviceDSL.g:3612:1: rule__DeleteOperation__Group_6__3 : rule__DeleteOperation__Group_6__3__Impl rule__DeleteOperation__Group_6__4 ;
    public final void rule__DeleteOperation__Group_6__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3616:1: ( rule__DeleteOperation__Group_6__3__Impl rule__DeleteOperation__Group_6__4 )
            // InternalWebserviceDSL.g:3617:2: rule__DeleteOperation__Group_6__3__Impl rule__DeleteOperation__Group_6__4
            {
            pushFollow(FOLLOW_8);
            rule__DeleteOperation__Group_6__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DeleteOperation__Group_6__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group_6__3"


    // $ANTLR start "rule__DeleteOperation__Group_6__3__Impl"
    // InternalWebserviceDSL.g:3624:1: rule__DeleteOperation__Group_6__3__Impl : ( ( rule__DeleteOperation__Group_6_3__0 )* ) ;
    public final void rule__DeleteOperation__Group_6__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3628:1: ( ( ( rule__DeleteOperation__Group_6_3__0 )* ) )
            // InternalWebserviceDSL.g:3629:1: ( ( rule__DeleteOperation__Group_6_3__0 )* )
            {
            // InternalWebserviceDSL.g:3629:1: ( ( rule__DeleteOperation__Group_6_3__0 )* )
            // InternalWebserviceDSL.g:3630:2: ( rule__DeleteOperation__Group_6_3__0 )*
            {
             before(grammarAccess.getDeleteOperationAccess().getGroup_6_3()); 
            // InternalWebserviceDSL.g:3631:2: ( rule__DeleteOperation__Group_6_3__0 )*
            loop28:
            do {
                int alt28=2;
                int LA28_0 = input.LA(1);

                if ( (LA28_0==18) ) {
                    alt28=1;
                }


                switch (alt28) {
            	case 1 :
            	    // InternalWebserviceDSL.g:3631:3: rule__DeleteOperation__Group_6_3__0
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__DeleteOperation__Group_6_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop28;
                }
            } while (true);

             after(grammarAccess.getDeleteOperationAccess().getGroup_6_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group_6__3__Impl"


    // $ANTLR start "rule__DeleteOperation__Group_6__4"
    // InternalWebserviceDSL.g:3639:1: rule__DeleteOperation__Group_6__4 : rule__DeleteOperation__Group_6__4__Impl ;
    public final void rule__DeleteOperation__Group_6__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3643:1: ( rule__DeleteOperation__Group_6__4__Impl )
            // InternalWebserviceDSL.g:3644:2: rule__DeleteOperation__Group_6__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DeleteOperation__Group_6__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group_6__4"


    // $ANTLR start "rule__DeleteOperation__Group_6__4__Impl"
    // InternalWebserviceDSL.g:3650:1: rule__DeleteOperation__Group_6__4__Impl : ( '}' ) ;
    public final void rule__DeleteOperation__Group_6__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3654:1: ( ( '}' ) )
            // InternalWebserviceDSL.g:3655:1: ( '}' )
            {
            // InternalWebserviceDSL.g:3655:1: ( '}' )
            // InternalWebserviceDSL.g:3656:2: '}'
            {
             before(grammarAccess.getDeleteOperationAccess().getRightCurlyBracketKeyword_6_4()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getDeleteOperationAccess().getRightCurlyBracketKeyword_6_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group_6__4__Impl"


    // $ANTLR start "rule__DeleteOperation__Group_6_3__0"
    // InternalWebserviceDSL.g:3666:1: rule__DeleteOperation__Group_6_3__0 : rule__DeleteOperation__Group_6_3__0__Impl rule__DeleteOperation__Group_6_3__1 ;
    public final void rule__DeleteOperation__Group_6_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3670:1: ( rule__DeleteOperation__Group_6_3__0__Impl rule__DeleteOperation__Group_6_3__1 )
            // InternalWebserviceDSL.g:3671:2: rule__DeleteOperation__Group_6_3__0__Impl rule__DeleteOperation__Group_6_3__1
            {
            pushFollow(FOLLOW_16);
            rule__DeleteOperation__Group_6_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DeleteOperation__Group_6_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group_6_3__0"


    // $ANTLR start "rule__DeleteOperation__Group_6_3__0__Impl"
    // InternalWebserviceDSL.g:3678:1: rule__DeleteOperation__Group_6_3__0__Impl : ( ',' ) ;
    public final void rule__DeleteOperation__Group_6_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3682:1: ( ( ',' ) )
            // InternalWebserviceDSL.g:3683:1: ( ',' )
            {
            // InternalWebserviceDSL.g:3683:1: ( ',' )
            // InternalWebserviceDSL.g:3684:2: ','
            {
             before(grammarAccess.getDeleteOperationAccess().getCommaKeyword_6_3_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getDeleteOperationAccess().getCommaKeyword_6_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group_6_3__0__Impl"


    // $ANTLR start "rule__DeleteOperation__Group_6_3__1"
    // InternalWebserviceDSL.g:3693:1: rule__DeleteOperation__Group_6_3__1 : rule__DeleteOperation__Group_6_3__1__Impl ;
    public final void rule__DeleteOperation__Group_6_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3697:1: ( rule__DeleteOperation__Group_6_3__1__Impl )
            // InternalWebserviceDSL.g:3698:2: rule__DeleteOperation__Group_6_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DeleteOperation__Group_6_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group_6_3__1"


    // $ANTLR start "rule__DeleteOperation__Group_6_3__1__Impl"
    // InternalWebserviceDSL.g:3704:1: rule__DeleteOperation__Group_6_3__1__Impl : ( ( rule__DeleteOperation__ParametersAssignment_6_3_1 ) ) ;
    public final void rule__DeleteOperation__Group_6_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3708:1: ( ( ( rule__DeleteOperation__ParametersAssignment_6_3_1 ) ) )
            // InternalWebserviceDSL.g:3709:1: ( ( rule__DeleteOperation__ParametersAssignment_6_3_1 ) )
            {
            // InternalWebserviceDSL.g:3709:1: ( ( rule__DeleteOperation__ParametersAssignment_6_3_1 ) )
            // InternalWebserviceDSL.g:3710:2: ( rule__DeleteOperation__ParametersAssignment_6_3_1 )
            {
             before(grammarAccess.getDeleteOperationAccess().getParametersAssignment_6_3_1()); 
            // InternalWebserviceDSL.g:3711:2: ( rule__DeleteOperation__ParametersAssignment_6_3_1 )
            // InternalWebserviceDSL.g:3711:3: rule__DeleteOperation__ParametersAssignment_6_3_1
            {
            pushFollow(FOLLOW_2);
            rule__DeleteOperation__ParametersAssignment_6_3_1();

            state._fsp--;


            }

             after(grammarAccess.getDeleteOperationAccess().getParametersAssignment_6_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__Group_6_3__1__Impl"


    // $ANTLR start "rule__OtherOperation__Group__0"
    // InternalWebserviceDSL.g:3720:1: rule__OtherOperation__Group__0 : rule__OtherOperation__Group__0__Impl rule__OtherOperation__Group__1 ;
    public final void rule__OtherOperation__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3724:1: ( rule__OtherOperation__Group__0__Impl rule__OtherOperation__Group__1 )
            // InternalWebserviceDSL.g:3725:2: rule__OtherOperation__Group__0__Impl rule__OtherOperation__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__OtherOperation__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OtherOperation__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group__0"


    // $ANTLR start "rule__OtherOperation__Group__0__Impl"
    // InternalWebserviceDSL.g:3732:1: rule__OtherOperation__Group__0__Impl : ( 'otherOperation' ) ;
    public final void rule__OtherOperation__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3736:1: ( ( 'otherOperation' ) )
            // InternalWebserviceDSL.g:3737:1: ( 'otherOperation' )
            {
            // InternalWebserviceDSL.g:3737:1: ( 'otherOperation' )
            // InternalWebserviceDSL.g:3738:2: 'otherOperation'
            {
             before(grammarAccess.getOtherOperationAccess().getOtherOperationKeyword_0()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getOtherOperationAccess().getOtherOperationKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group__0__Impl"


    // $ANTLR start "rule__OtherOperation__Group__1"
    // InternalWebserviceDSL.g:3747:1: rule__OtherOperation__Group__1 : rule__OtherOperation__Group__1__Impl rule__OtherOperation__Group__2 ;
    public final void rule__OtherOperation__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3751:1: ( rule__OtherOperation__Group__1__Impl rule__OtherOperation__Group__2 )
            // InternalWebserviceDSL.g:3752:2: rule__OtherOperation__Group__1__Impl rule__OtherOperation__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__OtherOperation__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OtherOperation__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group__1"


    // $ANTLR start "rule__OtherOperation__Group__1__Impl"
    // InternalWebserviceDSL.g:3759:1: rule__OtherOperation__Group__1__Impl : ( ( rule__OtherOperation__NameAssignment_1 ) ) ;
    public final void rule__OtherOperation__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3763:1: ( ( ( rule__OtherOperation__NameAssignment_1 ) ) )
            // InternalWebserviceDSL.g:3764:1: ( ( rule__OtherOperation__NameAssignment_1 ) )
            {
            // InternalWebserviceDSL.g:3764:1: ( ( rule__OtherOperation__NameAssignment_1 ) )
            // InternalWebserviceDSL.g:3765:2: ( rule__OtherOperation__NameAssignment_1 )
            {
             before(grammarAccess.getOtherOperationAccess().getNameAssignment_1()); 
            // InternalWebserviceDSL.g:3766:2: ( rule__OtherOperation__NameAssignment_1 )
            // InternalWebserviceDSL.g:3766:3: rule__OtherOperation__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__OtherOperation__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getOtherOperationAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group__1__Impl"


    // $ANTLR start "rule__OtherOperation__Group__2"
    // InternalWebserviceDSL.g:3774:1: rule__OtherOperation__Group__2 : rule__OtherOperation__Group__2__Impl rule__OtherOperation__Group__3 ;
    public final void rule__OtherOperation__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3778:1: ( rule__OtherOperation__Group__2__Impl rule__OtherOperation__Group__3 )
            // InternalWebserviceDSL.g:3779:2: rule__OtherOperation__Group__2__Impl rule__OtherOperation__Group__3
            {
            pushFollow(FOLLOW_17);
            rule__OtherOperation__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OtherOperation__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group__2"


    // $ANTLR start "rule__OtherOperation__Group__2__Impl"
    // InternalWebserviceDSL.g:3786:1: rule__OtherOperation__Group__2__Impl : ( '{' ) ;
    public final void rule__OtherOperation__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3790:1: ( ( '{' ) )
            // InternalWebserviceDSL.g:3791:1: ( '{' )
            {
            // InternalWebserviceDSL.g:3791:1: ( '{' )
            // InternalWebserviceDSL.g:3792:2: '{'
            {
             before(grammarAccess.getOtherOperationAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getOtherOperationAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group__2__Impl"


    // $ANTLR start "rule__OtherOperation__Group__3"
    // InternalWebserviceDSL.g:3801:1: rule__OtherOperation__Group__3 : rule__OtherOperation__Group__3__Impl rule__OtherOperation__Group__4 ;
    public final void rule__OtherOperation__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3805:1: ( rule__OtherOperation__Group__3__Impl rule__OtherOperation__Group__4 )
            // InternalWebserviceDSL.g:3806:2: rule__OtherOperation__Group__3__Impl rule__OtherOperation__Group__4
            {
            pushFollow(FOLLOW_17);
            rule__OtherOperation__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OtherOperation__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group__3"


    // $ANTLR start "rule__OtherOperation__Group__3__Impl"
    // InternalWebserviceDSL.g:3813:1: rule__OtherOperation__Group__3__Impl : ( ( rule__OtherOperation__Group_3__0 )? ) ;
    public final void rule__OtherOperation__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3817:1: ( ( ( rule__OtherOperation__Group_3__0 )? ) )
            // InternalWebserviceDSL.g:3818:1: ( ( rule__OtherOperation__Group_3__0 )? )
            {
            // InternalWebserviceDSL.g:3818:1: ( ( rule__OtherOperation__Group_3__0 )? )
            // InternalWebserviceDSL.g:3819:2: ( rule__OtherOperation__Group_3__0 )?
            {
             before(grammarAccess.getOtherOperationAccess().getGroup_3()); 
            // InternalWebserviceDSL.g:3820:2: ( rule__OtherOperation__Group_3__0 )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==17) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // InternalWebserviceDSL.g:3820:3: rule__OtherOperation__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__OtherOperation__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getOtherOperationAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group__3__Impl"


    // $ANTLR start "rule__OtherOperation__Group__4"
    // InternalWebserviceDSL.g:3828:1: rule__OtherOperation__Group__4 : rule__OtherOperation__Group__4__Impl rule__OtherOperation__Group__5 ;
    public final void rule__OtherOperation__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3832:1: ( rule__OtherOperation__Group__4__Impl rule__OtherOperation__Group__5 )
            // InternalWebserviceDSL.g:3833:2: rule__OtherOperation__Group__4__Impl rule__OtherOperation__Group__5
            {
            pushFollow(FOLLOW_17);
            rule__OtherOperation__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OtherOperation__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group__4"


    // $ANTLR start "rule__OtherOperation__Group__4__Impl"
    // InternalWebserviceDSL.g:3840:1: rule__OtherOperation__Group__4__Impl : ( ( rule__OtherOperation__Group_4__0 )? ) ;
    public final void rule__OtherOperation__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3844:1: ( ( ( rule__OtherOperation__Group_4__0 )? ) )
            // InternalWebserviceDSL.g:3845:1: ( ( rule__OtherOperation__Group_4__0 )? )
            {
            // InternalWebserviceDSL.g:3845:1: ( ( rule__OtherOperation__Group_4__0 )? )
            // InternalWebserviceDSL.g:3846:2: ( rule__OtherOperation__Group_4__0 )?
            {
             before(grammarAccess.getOtherOperationAccess().getGroup_4()); 
            // InternalWebserviceDSL.g:3847:2: ( rule__OtherOperation__Group_4__0 )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==22) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // InternalWebserviceDSL.g:3847:3: rule__OtherOperation__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__OtherOperation__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getOtherOperationAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group__4__Impl"


    // $ANTLR start "rule__OtherOperation__Group__5"
    // InternalWebserviceDSL.g:3855:1: rule__OtherOperation__Group__5 : rule__OtherOperation__Group__5__Impl rule__OtherOperation__Group__6 ;
    public final void rule__OtherOperation__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3859:1: ( rule__OtherOperation__Group__5__Impl rule__OtherOperation__Group__6 )
            // InternalWebserviceDSL.g:3860:2: rule__OtherOperation__Group__5__Impl rule__OtherOperation__Group__6
            {
            pushFollow(FOLLOW_17);
            rule__OtherOperation__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OtherOperation__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group__5"


    // $ANTLR start "rule__OtherOperation__Group__5__Impl"
    // InternalWebserviceDSL.g:3867:1: rule__OtherOperation__Group__5__Impl : ( ( rule__OtherOperation__Group_5__0 )? ) ;
    public final void rule__OtherOperation__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3871:1: ( ( ( rule__OtherOperation__Group_5__0 )? ) )
            // InternalWebserviceDSL.g:3872:1: ( ( rule__OtherOperation__Group_5__0 )? )
            {
            // InternalWebserviceDSL.g:3872:1: ( ( rule__OtherOperation__Group_5__0 )? )
            // InternalWebserviceDSL.g:3873:2: ( rule__OtherOperation__Group_5__0 )?
            {
             before(grammarAccess.getOtherOperationAccess().getGroup_5()); 
            // InternalWebserviceDSL.g:3874:2: ( rule__OtherOperation__Group_5__0 )?
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==29) ) {
                alt31=1;
            }
            switch (alt31) {
                case 1 :
                    // InternalWebserviceDSL.g:3874:3: rule__OtherOperation__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__OtherOperation__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getOtherOperationAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group__5__Impl"


    // $ANTLR start "rule__OtherOperation__Group__6"
    // InternalWebserviceDSL.g:3882:1: rule__OtherOperation__Group__6 : rule__OtherOperation__Group__6__Impl rule__OtherOperation__Group__7 ;
    public final void rule__OtherOperation__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3886:1: ( rule__OtherOperation__Group__6__Impl rule__OtherOperation__Group__7 )
            // InternalWebserviceDSL.g:3887:2: rule__OtherOperation__Group__6__Impl rule__OtherOperation__Group__7
            {
            pushFollow(FOLLOW_17);
            rule__OtherOperation__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OtherOperation__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group__6"


    // $ANTLR start "rule__OtherOperation__Group__6__Impl"
    // InternalWebserviceDSL.g:3894:1: rule__OtherOperation__Group__6__Impl : ( ( rule__OtherOperation__Group_6__0 )? ) ;
    public final void rule__OtherOperation__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3898:1: ( ( ( rule__OtherOperation__Group_6__0 )? ) )
            // InternalWebserviceDSL.g:3899:1: ( ( rule__OtherOperation__Group_6__0 )? )
            {
            // InternalWebserviceDSL.g:3899:1: ( ( rule__OtherOperation__Group_6__0 )? )
            // InternalWebserviceDSL.g:3900:2: ( rule__OtherOperation__Group_6__0 )?
            {
             before(grammarAccess.getOtherOperationAccess().getGroup_6()); 
            // InternalWebserviceDSL.g:3901:2: ( rule__OtherOperation__Group_6__0 )?
            int alt32=2;
            int LA32_0 = input.LA(1);

            if ( (LA32_0==23) ) {
                alt32=1;
            }
            switch (alt32) {
                case 1 :
                    // InternalWebserviceDSL.g:3901:3: rule__OtherOperation__Group_6__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__OtherOperation__Group_6__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getOtherOperationAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group__6__Impl"


    // $ANTLR start "rule__OtherOperation__Group__7"
    // InternalWebserviceDSL.g:3909:1: rule__OtherOperation__Group__7 : rule__OtherOperation__Group__7__Impl rule__OtherOperation__Group__8 ;
    public final void rule__OtherOperation__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3913:1: ( rule__OtherOperation__Group__7__Impl rule__OtherOperation__Group__8 )
            // InternalWebserviceDSL.g:3914:2: rule__OtherOperation__Group__7__Impl rule__OtherOperation__Group__8
            {
            pushFollow(FOLLOW_12);
            rule__OtherOperation__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OtherOperation__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group__7"


    // $ANTLR start "rule__OtherOperation__Group__7__Impl"
    // InternalWebserviceDSL.g:3921:1: rule__OtherOperation__Group__7__Impl : ( ( rule__OtherOperation__Group_7__0 ) ) ;
    public final void rule__OtherOperation__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3925:1: ( ( ( rule__OtherOperation__Group_7__0 ) ) )
            // InternalWebserviceDSL.g:3926:1: ( ( rule__OtherOperation__Group_7__0 ) )
            {
            // InternalWebserviceDSL.g:3926:1: ( ( rule__OtherOperation__Group_7__0 ) )
            // InternalWebserviceDSL.g:3927:2: ( rule__OtherOperation__Group_7__0 )
            {
             before(grammarAccess.getOtherOperationAccess().getGroup_7()); 
            // InternalWebserviceDSL.g:3928:2: ( rule__OtherOperation__Group_7__0 )
            // InternalWebserviceDSL.g:3928:3: rule__OtherOperation__Group_7__0
            {
            pushFollow(FOLLOW_2);
            rule__OtherOperation__Group_7__0();

            state._fsp--;


            }

             after(grammarAccess.getOtherOperationAccess().getGroup_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group__7__Impl"


    // $ANTLR start "rule__OtherOperation__Group__8"
    // InternalWebserviceDSL.g:3936:1: rule__OtherOperation__Group__8 : rule__OtherOperation__Group__8__Impl ;
    public final void rule__OtherOperation__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3940:1: ( rule__OtherOperation__Group__8__Impl )
            // InternalWebserviceDSL.g:3941:2: rule__OtherOperation__Group__8__Impl
            {
            pushFollow(FOLLOW_2);
            rule__OtherOperation__Group__8__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group__8"


    // $ANTLR start "rule__OtherOperation__Group__8__Impl"
    // InternalWebserviceDSL.g:3947:1: rule__OtherOperation__Group__8__Impl : ( '}' ) ;
    public final void rule__OtherOperation__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3951:1: ( ( '}' ) )
            // InternalWebserviceDSL.g:3952:1: ( '}' )
            {
            // InternalWebserviceDSL.g:3952:1: ( '}' )
            // InternalWebserviceDSL.g:3953:2: '}'
            {
             before(grammarAccess.getOtherOperationAccess().getRightCurlyBracketKeyword_8()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getOtherOperationAccess().getRightCurlyBracketKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group__8__Impl"


    // $ANTLR start "rule__OtherOperation__Group_3__0"
    // InternalWebserviceDSL.g:3963:1: rule__OtherOperation__Group_3__0 : rule__OtherOperation__Group_3__0__Impl rule__OtherOperation__Group_3__1 ;
    public final void rule__OtherOperation__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3967:1: ( rule__OtherOperation__Group_3__0__Impl rule__OtherOperation__Group_3__1 )
            // InternalWebserviceDSL.g:3968:2: rule__OtherOperation__Group_3__0__Impl rule__OtherOperation__Group_3__1
            {
            pushFollow(FOLLOW_3);
            rule__OtherOperation__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OtherOperation__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group_3__0"


    // $ANTLR start "rule__OtherOperation__Group_3__0__Impl"
    // InternalWebserviceDSL.g:3975:1: rule__OtherOperation__Group_3__0__Impl : ( 'description' ) ;
    public final void rule__OtherOperation__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3979:1: ( ( 'description' ) )
            // InternalWebserviceDSL.g:3980:1: ( 'description' )
            {
            // InternalWebserviceDSL.g:3980:1: ( 'description' )
            // InternalWebserviceDSL.g:3981:2: 'description'
            {
             before(grammarAccess.getOtherOperationAccess().getDescriptionKeyword_3_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getOtherOperationAccess().getDescriptionKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group_3__0__Impl"


    // $ANTLR start "rule__OtherOperation__Group_3__1"
    // InternalWebserviceDSL.g:3990:1: rule__OtherOperation__Group_3__1 : rule__OtherOperation__Group_3__1__Impl ;
    public final void rule__OtherOperation__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:3994:1: ( rule__OtherOperation__Group_3__1__Impl )
            // InternalWebserviceDSL.g:3995:2: rule__OtherOperation__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__OtherOperation__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group_3__1"


    // $ANTLR start "rule__OtherOperation__Group_3__1__Impl"
    // InternalWebserviceDSL.g:4001:1: rule__OtherOperation__Group_3__1__Impl : ( ( rule__OtherOperation__DescriptionAssignment_3_1 ) ) ;
    public final void rule__OtherOperation__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4005:1: ( ( ( rule__OtherOperation__DescriptionAssignment_3_1 ) ) )
            // InternalWebserviceDSL.g:4006:1: ( ( rule__OtherOperation__DescriptionAssignment_3_1 ) )
            {
            // InternalWebserviceDSL.g:4006:1: ( ( rule__OtherOperation__DescriptionAssignment_3_1 ) )
            // InternalWebserviceDSL.g:4007:2: ( rule__OtherOperation__DescriptionAssignment_3_1 )
            {
             before(grammarAccess.getOtherOperationAccess().getDescriptionAssignment_3_1()); 
            // InternalWebserviceDSL.g:4008:2: ( rule__OtherOperation__DescriptionAssignment_3_1 )
            // InternalWebserviceDSL.g:4008:3: rule__OtherOperation__DescriptionAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__OtherOperation__DescriptionAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getOtherOperationAccess().getDescriptionAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group_3__1__Impl"


    // $ANTLR start "rule__OtherOperation__Group_4__0"
    // InternalWebserviceDSL.g:4017:1: rule__OtherOperation__Group_4__0 : rule__OtherOperation__Group_4__0__Impl rule__OtherOperation__Group_4__1 ;
    public final void rule__OtherOperation__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4021:1: ( rule__OtherOperation__Group_4__0__Impl rule__OtherOperation__Group_4__1 )
            // InternalWebserviceDSL.g:4022:2: rule__OtherOperation__Group_4__0__Impl rule__OtherOperation__Group_4__1
            {
            pushFollow(FOLLOW_3);
            rule__OtherOperation__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OtherOperation__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group_4__0"


    // $ANTLR start "rule__OtherOperation__Group_4__0__Impl"
    // InternalWebserviceDSL.g:4029:1: rule__OtherOperation__Group_4__0__Impl : ( 'urlSuffix' ) ;
    public final void rule__OtherOperation__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4033:1: ( ( 'urlSuffix' ) )
            // InternalWebserviceDSL.g:4034:1: ( 'urlSuffix' )
            {
            // InternalWebserviceDSL.g:4034:1: ( 'urlSuffix' )
            // InternalWebserviceDSL.g:4035:2: 'urlSuffix'
            {
             before(grammarAccess.getOtherOperationAccess().getUrlSuffixKeyword_4_0()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getOtherOperationAccess().getUrlSuffixKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group_4__0__Impl"


    // $ANTLR start "rule__OtherOperation__Group_4__1"
    // InternalWebserviceDSL.g:4044:1: rule__OtherOperation__Group_4__1 : rule__OtherOperation__Group_4__1__Impl ;
    public final void rule__OtherOperation__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4048:1: ( rule__OtherOperation__Group_4__1__Impl )
            // InternalWebserviceDSL.g:4049:2: rule__OtherOperation__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__OtherOperation__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group_4__1"


    // $ANTLR start "rule__OtherOperation__Group_4__1__Impl"
    // InternalWebserviceDSL.g:4055:1: rule__OtherOperation__Group_4__1__Impl : ( ( rule__OtherOperation__UrlSuffixAssignment_4_1 ) ) ;
    public final void rule__OtherOperation__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4059:1: ( ( ( rule__OtherOperation__UrlSuffixAssignment_4_1 ) ) )
            // InternalWebserviceDSL.g:4060:1: ( ( rule__OtherOperation__UrlSuffixAssignment_4_1 ) )
            {
            // InternalWebserviceDSL.g:4060:1: ( ( rule__OtherOperation__UrlSuffixAssignment_4_1 ) )
            // InternalWebserviceDSL.g:4061:2: ( rule__OtherOperation__UrlSuffixAssignment_4_1 )
            {
             before(grammarAccess.getOtherOperationAccess().getUrlSuffixAssignment_4_1()); 
            // InternalWebserviceDSL.g:4062:2: ( rule__OtherOperation__UrlSuffixAssignment_4_1 )
            // InternalWebserviceDSL.g:4062:3: rule__OtherOperation__UrlSuffixAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__OtherOperation__UrlSuffixAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getOtherOperationAccess().getUrlSuffixAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group_4__1__Impl"


    // $ANTLR start "rule__OtherOperation__Group_5__0"
    // InternalWebserviceDSL.g:4071:1: rule__OtherOperation__Group_5__0 : rule__OtherOperation__Group_5__0__Impl rule__OtherOperation__Group_5__1 ;
    public final void rule__OtherOperation__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4075:1: ( rule__OtherOperation__Group_5__0__Impl rule__OtherOperation__Group_5__1 )
            // InternalWebserviceDSL.g:4076:2: rule__OtherOperation__Group_5__0__Impl rule__OtherOperation__Group_5__1
            {
            pushFollow(FOLLOW_3);
            rule__OtherOperation__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OtherOperation__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group_5__0"


    // $ANTLR start "rule__OtherOperation__Group_5__0__Impl"
    // InternalWebserviceDSL.g:4083:1: rule__OtherOperation__Group_5__0__Impl : ( 'operation' ) ;
    public final void rule__OtherOperation__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4087:1: ( ( 'operation' ) )
            // InternalWebserviceDSL.g:4088:1: ( 'operation' )
            {
            // InternalWebserviceDSL.g:4088:1: ( 'operation' )
            // InternalWebserviceDSL.g:4089:2: 'operation'
            {
             before(grammarAccess.getOtherOperationAccess().getOperationKeyword_5_0()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getOtherOperationAccess().getOperationKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group_5__0__Impl"


    // $ANTLR start "rule__OtherOperation__Group_5__1"
    // InternalWebserviceDSL.g:4098:1: rule__OtherOperation__Group_5__1 : rule__OtherOperation__Group_5__1__Impl ;
    public final void rule__OtherOperation__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4102:1: ( rule__OtherOperation__Group_5__1__Impl )
            // InternalWebserviceDSL.g:4103:2: rule__OtherOperation__Group_5__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__OtherOperation__Group_5__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group_5__1"


    // $ANTLR start "rule__OtherOperation__Group_5__1__Impl"
    // InternalWebserviceDSL.g:4109:1: rule__OtherOperation__Group_5__1__Impl : ( ( rule__OtherOperation__OperationAssignment_5_1 ) ) ;
    public final void rule__OtherOperation__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4113:1: ( ( ( rule__OtherOperation__OperationAssignment_5_1 ) ) )
            // InternalWebserviceDSL.g:4114:1: ( ( rule__OtherOperation__OperationAssignment_5_1 ) )
            {
            // InternalWebserviceDSL.g:4114:1: ( ( rule__OtherOperation__OperationAssignment_5_1 ) )
            // InternalWebserviceDSL.g:4115:2: ( rule__OtherOperation__OperationAssignment_5_1 )
            {
             before(grammarAccess.getOtherOperationAccess().getOperationAssignment_5_1()); 
            // InternalWebserviceDSL.g:4116:2: ( rule__OtherOperation__OperationAssignment_5_1 )
            // InternalWebserviceDSL.g:4116:3: rule__OtherOperation__OperationAssignment_5_1
            {
            pushFollow(FOLLOW_2);
            rule__OtherOperation__OperationAssignment_5_1();

            state._fsp--;


            }

             after(grammarAccess.getOtherOperationAccess().getOperationAssignment_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group_5__1__Impl"


    // $ANTLR start "rule__OtherOperation__Group_6__0"
    // InternalWebserviceDSL.g:4125:1: rule__OtherOperation__Group_6__0 : rule__OtherOperation__Group_6__0__Impl rule__OtherOperation__Group_6__1 ;
    public final void rule__OtherOperation__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4129:1: ( rule__OtherOperation__Group_6__0__Impl rule__OtherOperation__Group_6__1 )
            // InternalWebserviceDSL.g:4130:2: rule__OtherOperation__Group_6__0__Impl rule__OtherOperation__Group_6__1
            {
            pushFollow(FOLLOW_3);
            rule__OtherOperation__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OtherOperation__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group_6__0"


    // $ANTLR start "rule__OtherOperation__Group_6__0__Impl"
    // InternalWebserviceDSL.g:4137:1: rule__OtherOperation__Group_6__0__Impl : ( 'response' ) ;
    public final void rule__OtherOperation__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4141:1: ( ( 'response' ) )
            // InternalWebserviceDSL.g:4142:1: ( 'response' )
            {
            // InternalWebserviceDSL.g:4142:1: ( 'response' )
            // InternalWebserviceDSL.g:4143:2: 'response'
            {
             before(grammarAccess.getOtherOperationAccess().getResponseKeyword_6_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getOtherOperationAccess().getResponseKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group_6__0__Impl"


    // $ANTLR start "rule__OtherOperation__Group_6__1"
    // InternalWebserviceDSL.g:4152:1: rule__OtherOperation__Group_6__1 : rule__OtherOperation__Group_6__1__Impl ;
    public final void rule__OtherOperation__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4156:1: ( rule__OtherOperation__Group_6__1__Impl )
            // InternalWebserviceDSL.g:4157:2: rule__OtherOperation__Group_6__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__OtherOperation__Group_6__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group_6__1"


    // $ANTLR start "rule__OtherOperation__Group_6__1__Impl"
    // InternalWebserviceDSL.g:4163:1: rule__OtherOperation__Group_6__1__Impl : ( ( rule__OtherOperation__ResponseAssignment_6_1 ) ) ;
    public final void rule__OtherOperation__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4167:1: ( ( ( rule__OtherOperation__ResponseAssignment_6_1 ) ) )
            // InternalWebserviceDSL.g:4168:1: ( ( rule__OtherOperation__ResponseAssignment_6_1 ) )
            {
            // InternalWebserviceDSL.g:4168:1: ( ( rule__OtherOperation__ResponseAssignment_6_1 ) )
            // InternalWebserviceDSL.g:4169:2: ( rule__OtherOperation__ResponseAssignment_6_1 )
            {
             before(grammarAccess.getOtherOperationAccess().getResponseAssignment_6_1()); 
            // InternalWebserviceDSL.g:4170:2: ( rule__OtherOperation__ResponseAssignment_6_1 )
            // InternalWebserviceDSL.g:4170:3: rule__OtherOperation__ResponseAssignment_6_1
            {
            pushFollow(FOLLOW_2);
            rule__OtherOperation__ResponseAssignment_6_1();

            state._fsp--;


            }

             after(grammarAccess.getOtherOperationAccess().getResponseAssignment_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group_6__1__Impl"


    // $ANTLR start "rule__OtherOperation__Group_7__0"
    // InternalWebserviceDSL.g:4179:1: rule__OtherOperation__Group_7__0 : rule__OtherOperation__Group_7__0__Impl rule__OtherOperation__Group_7__1 ;
    public final void rule__OtherOperation__Group_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4183:1: ( rule__OtherOperation__Group_7__0__Impl rule__OtherOperation__Group_7__1 )
            // InternalWebserviceDSL.g:4184:2: rule__OtherOperation__Group_7__0__Impl rule__OtherOperation__Group_7__1
            {
            pushFollow(FOLLOW_4);
            rule__OtherOperation__Group_7__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OtherOperation__Group_7__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group_7__0"


    // $ANTLR start "rule__OtherOperation__Group_7__0__Impl"
    // InternalWebserviceDSL.g:4191:1: rule__OtherOperation__Group_7__0__Impl : ( 'parameters' ) ;
    public final void rule__OtherOperation__Group_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4195:1: ( ( 'parameters' ) )
            // InternalWebserviceDSL.g:4196:1: ( 'parameters' )
            {
            // InternalWebserviceDSL.g:4196:1: ( 'parameters' )
            // InternalWebserviceDSL.g:4197:2: 'parameters'
            {
             before(grammarAccess.getOtherOperationAccess().getParametersKeyword_7_0()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getOtherOperationAccess().getParametersKeyword_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group_7__0__Impl"


    // $ANTLR start "rule__OtherOperation__Group_7__1"
    // InternalWebserviceDSL.g:4206:1: rule__OtherOperation__Group_7__1 : rule__OtherOperation__Group_7__1__Impl rule__OtherOperation__Group_7__2 ;
    public final void rule__OtherOperation__Group_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4210:1: ( rule__OtherOperation__Group_7__1__Impl rule__OtherOperation__Group_7__2 )
            // InternalWebserviceDSL.g:4211:2: rule__OtherOperation__Group_7__1__Impl rule__OtherOperation__Group_7__2
            {
            pushFollow(FOLLOW_16);
            rule__OtherOperation__Group_7__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OtherOperation__Group_7__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group_7__1"


    // $ANTLR start "rule__OtherOperation__Group_7__1__Impl"
    // InternalWebserviceDSL.g:4218:1: rule__OtherOperation__Group_7__1__Impl : ( '{' ) ;
    public final void rule__OtherOperation__Group_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4222:1: ( ( '{' ) )
            // InternalWebserviceDSL.g:4223:1: ( '{' )
            {
            // InternalWebserviceDSL.g:4223:1: ( '{' )
            // InternalWebserviceDSL.g:4224:2: '{'
            {
             before(grammarAccess.getOtherOperationAccess().getLeftCurlyBracketKeyword_7_1()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getOtherOperationAccess().getLeftCurlyBracketKeyword_7_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group_7__1__Impl"


    // $ANTLR start "rule__OtherOperation__Group_7__2"
    // InternalWebserviceDSL.g:4233:1: rule__OtherOperation__Group_7__2 : rule__OtherOperation__Group_7__2__Impl rule__OtherOperation__Group_7__3 ;
    public final void rule__OtherOperation__Group_7__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4237:1: ( rule__OtherOperation__Group_7__2__Impl rule__OtherOperation__Group_7__3 )
            // InternalWebserviceDSL.g:4238:2: rule__OtherOperation__Group_7__2__Impl rule__OtherOperation__Group_7__3
            {
            pushFollow(FOLLOW_8);
            rule__OtherOperation__Group_7__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OtherOperation__Group_7__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group_7__2"


    // $ANTLR start "rule__OtherOperation__Group_7__2__Impl"
    // InternalWebserviceDSL.g:4245:1: rule__OtherOperation__Group_7__2__Impl : ( ( rule__OtherOperation__ParametersAssignment_7_2 ) ) ;
    public final void rule__OtherOperation__Group_7__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4249:1: ( ( ( rule__OtherOperation__ParametersAssignment_7_2 ) ) )
            // InternalWebserviceDSL.g:4250:1: ( ( rule__OtherOperation__ParametersAssignment_7_2 ) )
            {
            // InternalWebserviceDSL.g:4250:1: ( ( rule__OtherOperation__ParametersAssignment_7_2 ) )
            // InternalWebserviceDSL.g:4251:2: ( rule__OtherOperation__ParametersAssignment_7_2 )
            {
             before(grammarAccess.getOtherOperationAccess().getParametersAssignment_7_2()); 
            // InternalWebserviceDSL.g:4252:2: ( rule__OtherOperation__ParametersAssignment_7_2 )
            // InternalWebserviceDSL.g:4252:3: rule__OtherOperation__ParametersAssignment_7_2
            {
            pushFollow(FOLLOW_2);
            rule__OtherOperation__ParametersAssignment_7_2();

            state._fsp--;


            }

             after(grammarAccess.getOtherOperationAccess().getParametersAssignment_7_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group_7__2__Impl"


    // $ANTLR start "rule__OtherOperation__Group_7__3"
    // InternalWebserviceDSL.g:4260:1: rule__OtherOperation__Group_7__3 : rule__OtherOperation__Group_7__3__Impl rule__OtherOperation__Group_7__4 ;
    public final void rule__OtherOperation__Group_7__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4264:1: ( rule__OtherOperation__Group_7__3__Impl rule__OtherOperation__Group_7__4 )
            // InternalWebserviceDSL.g:4265:2: rule__OtherOperation__Group_7__3__Impl rule__OtherOperation__Group_7__4
            {
            pushFollow(FOLLOW_8);
            rule__OtherOperation__Group_7__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OtherOperation__Group_7__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group_7__3"


    // $ANTLR start "rule__OtherOperation__Group_7__3__Impl"
    // InternalWebserviceDSL.g:4272:1: rule__OtherOperation__Group_7__3__Impl : ( ( rule__OtherOperation__Group_7_3__0 )* ) ;
    public final void rule__OtherOperation__Group_7__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4276:1: ( ( ( rule__OtherOperation__Group_7_3__0 )* ) )
            // InternalWebserviceDSL.g:4277:1: ( ( rule__OtherOperation__Group_7_3__0 )* )
            {
            // InternalWebserviceDSL.g:4277:1: ( ( rule__OtherOperation__Group_7_3__0 )* )
            // InternalWebserviceDSL.g:4278:2: ( rule__OtherOperation__Group_7_3__0 )*
            {
             before(grammarAccess.getOtherOperationAccess().getGroup_7_3()); 
            // InternalWebserviceDSL.g:4279:2: ( rule__OtherOperation__Group_7_3__0 )*
            loop33:
            do {
                int alt33=2;
                int LA33_0 = input.LA(1);

                if ( (LA33_0==18) ) {
                    alt33=1;
                }


                switch (alt33) {
            	case 1 :
            	    // InternalWebserviceDSL.g:4279:3: rule__OtherOperation__Group_7_3__0
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__OtherOperation__Group_7_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop33;
                }
            } while (true);

             after(grammarAccess.getOtherOperationAccess().getGroup_7_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group_7__3__Impl"


    // $ANTLR start "rule__OtherOperation__Group_7__4"
    // InternalWebserviceDSL.g:4287:1: rule__OtherOperation__Group_7__4 : rule__OtherOperation__Group_7__4__Impl ;
    public final void rule__OtherOperation__Group_7__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4291:1: ( rule__OtherOperation__Group_7__4__Impl )
            // InternalWebserviceDSL.g:4292:2: rule__OtherOperation__Group_7__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__OtherOperation__Group_7__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group_7__4"


    // $ANTLR start "rule__OtherOperation__Group_7__4__Impl"
    // InternalWebserviceDSL.g:4298:1: rule__OtherOperation__Group_7__4__Impl : ( '}' ) ;
    public final void rule__OtherOperation__Group_7__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4302:1: ( ( '}' ) )
            // InternalWebserviceDSL.g:4303:1: ( '}' )
            {
            // InternalWebserviceDSL.g:4303:1: ( '}' )
            // InternalWebserviceDSL.g:4304:2: '}'
            {
             before(grammarAccess.getOtherOperationAccess().getRightCurlyBracketKeyword_7_4()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getOtherOperationAccess().getRightCurlyBracketKeyword_7_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group_7__4__Impl"


    // $ANTLR start "rule__OtherOperation__Group_7_3__0"
    // InternalWebserviceDSL.g:4314:1: rule__OtherOperation__Group_7_3__0 : rule__OtherOperation__Group_7_3__0__Impl rule__OtherOperation__Group_7_3__1 ;
    public final void rule__OtherOperation__Group_7_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4318:1: ( rule__OtherOperation__Group_7_3__0__Impl rule__OtherOperation__Group_7_3__1 )
            // InternalWebserviceDSL.g:4319:2: rule__OtherOperation__Group_7_3__0__Impl rule__OtherOperation__Group_7_3__1
            {
            pushFollow(FOLLOW_16);
            rule__OtherOperation__Group_7_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OtherOperation__Group_7_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group_7_3__0"


    // $ANTLR start "rule__OtherOperation__Group_7_3__0__Impl"
    // InternalWebserviceDSL.g:4326:1: rule__OtherOperation__Group_7_3__0__Impl : ( ',' ) ;
    public final void rule__OtherOperation__Group_7_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4330:1: ( ( ',' ) )
            // InternalWebserviceDSL.g:4331:1: ( ',' )
            {
            // InternalWebserviceDSL.g:4331:1: ( ',' )
            // InternalWebserviceDSL.g:4332:2: ','
            {
             before(grammarAccess.getOtherOperationAccess().getCommaKeyword_7_3_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getOtherOperationAccess().getCommaKeyword_7_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group_7_3__0__Impl"


    // $ANTLR start "rule__OtherOperation__Group_7_3__1"
    // InternalWebserviceDSL.g:4341:1: rule__OtherOperation__Group_7_3__1 : rule__OtherOperation__Group_7_3__1__Impl ;
    public final void rule__OtherOperation__Group_7_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4345:1: ( rule__OtherOperation__Group_7_3__1__Impl )
            // InternalWebserviceDSL.g:4346:2: rule__OtherOperation__Group_7_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__OtherOperation__Group_7_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group_7_3__1"


    // $ANTLR start "rule__OtherOperation__Group_7_3__1__Impl"
    // InternalWebserviceDSL.g:4352:1: rule__OtherOperation__Group_7_3__1__Impl : ( ( rule__OtherOperation__ParametersAssignment_7_3_1 ) ) ;
    public final void rule__OtherOperation__Group_7_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4356:1: ( ( ( rule__OtherOperation__ParametersAssignment_7_3_1 ) ) )
            // InternalWebserviceDSL.g:4357:1: ( ( rule__OtherOperation__ParametersAssignment_7_3_1 ) )
            {
            // InternalWebserviceDSL.g:4357:1: ( ( rule__OtherOperation__ParametersAssignment_7_3_1 ) )
            // InternalWebserviceDSL.g:4358:2: ( rule__OtherOperation__ParametersAssignment_7_3_1 )
            {
             before(grammarAccess.getOtherOperationAccess().getParametersAssignment_7_3_1()); 
            // InternalWebserviceDSL.g:4359:2: ( rule__OtherOperation__ParametersAssignment_7_3_1 )
            // InternalWebserviceDSL.g:4359:3: rule__OtherOperation__ParametersAssignment_7_3_1
            {
            pushFollow(FOLLOW_2);
            rule__OtherOperation__ParametersAssignment_7_3_1();

            state._fsp--;


            }

             after(grammarAccess.getOtherOperationAccess().getParametersAssignment_7_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__Group_7_3__1__Impl"


    // $ANTLR start "rule__ObjectDataType__Group__0"
    // InternalWebserviceDSL.g:4368:1: rule__ObjectDataType__Group__0 : rule__ObjectDataType__Group__0__Impl rule__ObjectDataType__Group__1 ;
    public final void rule__ObjectDataType__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4372:1: ( rule__ObjectDataType__Group__0__Impl rule__ObjectDataType__Group__1 )
            // InternalWebserviceDSL.g:4373:2: rule__ObjectDataType__Group__0__Impl rule__ObjectDataType__Group__1
            {
            pushFollow(FOLLOW_18);
            rule__ObjectDataType__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ObjectDataType__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectDataType__Group__0"


    // $ANTLR start "rule__ObjectDataType__Group__0__Impl"
    // InternalWebserviceDSL.g:4380:1: rule__ObjectDataType__Group__0__Impl : ( ( rule__ObjectDataType__RequiredAssignment_0 )? ) ;
    public final void rule__ObjectDataType__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4384:1: ( ( ( rule__ObjectDataType__RequiredAssignment_0 )? ) )
            // InternalWebserviceDSL.g:4385:1: ( ( rule__ObjectDataType__RequiredAssignment_0 )? )
            {
            // InternalWebserviceDSL.g:4385:1: ( ( rule__ObjectDataType__RequiredAssignment_0 )? )
            // InternalWebserviceDSL.g:4386:2: ( rule__ObjectDataType__RequiredAssignment_0 )?
            {
             before(grammarAccess.getObjectDataTypeAccess().getRequiredAssignment_0()); 
            // InternalWebserviceDSL.g:4387:2: ( rule__ObjectDataType__RequiredAssignment_0 )?
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==40) ) {
                alt34=1;
            }
            switch (alt34) {
                case 1 :
                    // InternalWebserviceDSL.g:4387:3: rule__ObjectDataType__RequiredAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__ObjectDataType__RequiredAssignment_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getObjectDataTypeAccess().getRequiredAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectDataType__Group__0__Impl"


    // $ANTLR start "rule__ObjectDataType__Group__1"
    // InternalWebserviceDSL.g:4395:1: rule__ObjectDataType__Group__1 : rule__ObjectDataType__Group__1__Impl rule__ObjectDataType__Group__2 ;
    public final void rule__ObjectDataType__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4399:1: ( rule__ObjectDataType__Group__1__Impl rule__ObjectDataType__Group__2 )
            // InternalWebserviceDSL.g:4400:2: rule__ObjectDataType__Group__1__Impl rule__ObjectDataType__Group__2
            {
            pushFollow(FOLLOW_3);
            rule__ObjectDataType__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ObjectDataType__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectDataType__Group__1"


    // $ANTLR start "rule__ObjectDataType__Group__1__Impl"
    // InternalWebserviceDSL.g:4407:1: rule__ObjectDataType__Group__1__Impl : ( 'ObjectDataType' ) ;
    public final void rule__ObjectDataType__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4411:1: ( ( 'ObjectDataType' ) )
            // InternalWebserviceDSL.g:4412:1: ( 'ObjectDataType' )
            {
            // InternalWebserviceDSL.g:4412:1: ( 'ObjectDataType' )
            // InternalWebserviceDSL.g:4413:2: 'ObjectDataType'
            {
             before(grammarAccess.getObjectDataTypeAccess().getObjectDataTypeKeyword_1()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getObjectDataTypeAccess().getObjectDataTypeKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectDataType__Group__1__Impl"


    // $ANTLR start "rule__ObjectDataType__Group__2"
    // InternalWebserviceDSL.g:4422:1: rule__ObjectDataType__Group__2 : rule__ObjectDataType__Group__2__Impl rule__ObjectDataType__Group__3 ;
    public final void rule__ObjectDataType__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4426:1: ( rule__ObjectDataType__Group__2__Impl rule__ObjectDataType__Group__3 )
            // InternalWebserviceDSL.g:4427:2: rule__ObjectDataType__Group__2__Impl rule__ObjectDataType__Group__3
            {
            pushFollow(FOLLOW_4);
            rule__ObjectDataType__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ObjectDataType__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectDataType__Group__2"


    // $ANTLR start "rule__ObjectDataType__Group__2__Impl"
    // InternalWebserviceDSL.g:4434:1: rule__ObjectDataType__Group__2__Impl : ( ( rule__ObjectDataType__NameAssignment_2 ) ) ;
    public final void rule__ObjectDataType__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4438:1: ( ( ( rule__ObjectDataType__NameAssignment_2 ) ) )
            // InternalWebserviceDSL.g:4439:1: ( ( rule__ObjectDataType__NameAssignment_2 ) )
            {
            // InternalWebserviceDSL.g:4439:1: ( ( rule__ObjectDataType__NameAssignment_2 ) )
            // InternalWebserviceDSL.g:4440:2: ( rule__ObjectDataType__NameAssignment_2 )
            {
             before(grammarAccess.getObjectDataTypeAccess().getNameAssignment_2()); 
            // InternalWebserviceDSL.g:4441:2: ( rule__ObjectDataType__NameAssignment_2 )
            // InternalWebserviceDSL.g:4441:3: rule__ObjectDataType__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__ObjectDataType__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getObjectDataTypeAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectDataType__Group__2__Impl"


    // $ANTLR start "rule__ObjectDataType__Group__3"
    // InternalWebserviceDSL.g:4449:1: rule__ObjectDataType__Group__3 : rule__ObjectDataType__Group__3__Impl rule__ObjectDataType__Group__4 ;
    public final void rule__ObjectDataType__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4453:1: ( rule__ObjectDataType__Group__3__Impl rule__ObjectDataType__Group__4 )
            // InternalWebserviceDSL.g:4454:2: rule__ObjectDataType__Group__3__Impl rule__ObjectDataType__Group__4
            {
            pushFollow(FOLLOW_6);
            rule__ObjectDataType__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ObjectDataType__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectDataType__Group__3"


    // $ANTLR start "rule__ObjectDataType__Group__3__Impl"
    // InternalWebserviceDSL.g:4461:1: rule__ObjectDataType__Group__3__Impl : ( '{' ) ;
    public final void rule__ObjectDataType__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4465:1: ( ( '{' ) )
            // InternalWebserviceDSL.g:4466:1: ( '{' )
            {
            // InternalWebserviceDSL.g:4466:1: ( '{' )
            // InternalWebserviceDSL.g:4467:2: '{'
            {
             before(grammarAccess.getObjectDataTypeAccess().getLeftCurlyBracketKeyword_3()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getObjectDataTypeAccess().getLeftCurlyBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectDataType__Group__3__Impl"


    // $ANTLR start "rule__ObjectDataType__Group__4"
    // InternalWebserviceDSL.g:4476:1: rule__ObjectDataType__Group__4 : rule__ObjectDataType__Group__4__Impl rule__ObjectDataType__Group__5 ;
    public final void rule__ObjectDataType__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4480:1: ( rule__ObjectDataType__Group__4__Impl rule__ObjectDataType__Group__5 )
            // InternalWebserviceDSL.g:4481:2: rule__ObjectDataType__Group__4__Impl rule__ObjectDataType__Group__5
            {
            pushFollow(FOLLOW_19);
            rule__ObjectDataType__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ObjectDataType__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectDataType__Group__4"


    // $ANTLR start "rule__ObjectDataType__Group__4__Impl"
    // InternalWebserviceDSL.g:4488:1: rule__ObjectDataType__Group__4__Impl : ( 'datatypes' ) ;
    public final void rule__ObjectDataType__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4492:1: ( ( 'datatypes' ) )
            // InternalWebserviceDSL.g:4493:1: ( 'datatypes' )
            {
            // InternalWebserviceDSL.g:4493:1: ( 'datatypes' )
            // InternalWebserviceDSL.g:4494:2: 'datatypes'
            {
             before(grammarAccess.getObjectDataTypeAccess().getDatatypesKeyword_4()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getObjectDataTypeAccess().getDatatypesKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectDataType__Group__4__Impl"


    // $ANTLR start "rule__ObjectDataType__Group__5"
    // InternalWebserviceDSL.g:4503:1: rule__ObjectDataType__Group__5 : rule__ObjectDataType__Group__5__Impl rule__ObjectDataType__Group__6 ;
    public final void rule__ObjectDataType__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4507:1: ( rule__ObjectDataType__Group__5__Impl rule__ObjectDataType__Group__6 )
            // InternalWebserviceDSL.g:4508:2: rule__ObjectDataType__Group__5__Impl rule__ObjectDataType__Group__6
            {
            pushFollow(FOLLOW_3);
            rule__ObjectDataType__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ObjectDataType__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectDataType__Group__5"


    // $ANTLR start "rule__ObjectDataType__Group__5__Impl"
    // InternalWebserviceDSL.g:4515:1: rule__ObjectDataType__Group__5__Impl : ( '(' ) ;
    public final void rule__ObjectDataType__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4519:1: ( ( '(' ) )
            // InternalWebserviceDSL.g:4520:1: ( '(' )
            {
            // InternalWebserviceDSL.g:4520:1: ( '(' )
            // InternalWebserviceDSL.g:4521:2: '('
            {
             before(grammarAccess.getObjectDataTypeAccess().getLeftParenthesisKeyword_5()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getObjectDataTypeAccess().getLeftParenthesisKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectDataType__Group__5__Impl"


    // $ANTLR start "rule__ObjectDataType__Group__6"
    // InternalWebserviceDSL.g:4530:1: rule__ObjectDataType__Group__6 : rule__ObjectDataType__Group__6__Impl rule__ObjectDataType__Group__7 ;
    public final void rule__ObjectDataType__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4534:1: ( rule__ObjectDataType__Group__6__Impl rule__ObjectDataType__Group__7 )
            // InternalWebserviceDSL.g:4535:2: rule__ObjectDataType__Group__6__Impl rule__ObjectDataType__Group__7
            {
            pushFollow(FOLLOW_20);
            rule__ObjectDataType__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ObjectDataType__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectDataType__Group__6"


    // $ANTLR start "rule__ObjectDataType__Group__6__Impl"
    // InternalWebserviceDSL.g:4542:1: rule__ObjectDataType__Group__6__Impl : ( ( rule__ObjectDataType__DatatypesAssignment_6 ) ) ;
    public final void rule__ObjectDataType__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4546:1: ( ( ( rule__ObjectDataType__DatatypesAssignment_6 ) ) )
            // InternalWebserviceDSL.g:4547:1: ( ( rule__ObjectDataType__DatatypesAssignment_6 ) )
            {
            // InternalWebserviceDSL.g:4547:1: ( ( rule__ObjectDataType__DatatypesAssignment_6 ) )
            // InternalWebserviceDSL.g:4548:2: ( rule__ObjectDataType__DatatypesAssignment_6 )
            {
             before(grammarAccess.getObjectDataTypeAccess().getDatatypesAssignment_6()); 
            // InternalWebserviceDSL.g:4549:2: ( rule__ObjectDataType__DatatypesAssignment_6 )
            // InternalWebserviceDSL.g:4549:3: rule__ObjectDataType__DatatypesAssignment_6
            {
            pushFollow(FOLLOW_2);
            rule__ObjectDataType__DatatypesAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getObjectDataTypeAccess().getDatatypesAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectDataType__Group__6__Impl"


    // $ANTLR start "rule__ObjectDataType__Group__7"
    // InternalWebserviceDSL.g:4557:1: rule__ObjectDataType__Group__7 : rule__ObjectDataType__Group__7__Impl rule__ObjectDataType__Group__8 ;
    public final void rule__ObjectDataType__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4561:1: ( rule__ObjectDataType__Group__7__Impl rule__ObjectDataType__Group__8 )
            // InternalWebserviceDSL.g:4562:2: rule__ObjectDataType__Group__7__Impl rule__ObjectDataType__Group__8
            {
            pushFollow(FOLLOW_20);
            rule__ObjectDataType__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ObjectDataType__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectDataType__Group__7"


    // $ANTLR start "rule__ObjectDataType__Group__7__Impl"
    // InternalWebserviceDSL.g:4569:1: rule__ObjectDataType__Group__7__Impl : ( ( rule__ObjectDataType__Group_7__0 )* ) ;
    public final void rule__ObjectDataType__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4573:1: ( ( ( rule__ObjectDataType__Group_7__0 )* ) )
            // InternalWebserviceDSL.g:4574:1: ( ( rule__ObjectDataType__Group_7__0 )* )
            {
            // InternalWebserviceDSL.g:4574:1: ( ( rule__ObjectDataType__Group_7__0 )* )
            // InternalWebserviceDSL.g:4575:2: ( rule__ObjectDataType__Group_7__0 )*
            {
             before(grammarAccess.getObjectDataTypeAccess().getGroup_7()); 
            // InternalWebserviceDSL.g:4576:2: ( rule__ObjectDataType__Group_7__0 )*
            loop35:
            do {
                int alt35=2;
                int LA35_0 = input.LA(1);

                if ( (LA35_0==18) ) {
                    alt35=1;
                }


                switch (alt35) {
            	case 1 :
            	    // InternalWebserviceDSL.g:4576:3: rule__ObjectDataType__Group_7__0
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__ObjectDataType__Group_7__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop35;
                }
            } while (true);

             after(grammarAccess.getObjectDataTypeAccess().getGroup_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectDataType__Group__7__Impl"


    // $ANTLR start "rule__ObjectDataType__Group__8"
    // InternalWebserviceDSL.g:4584:1: rule__ObjectDataType__Group__8 : rule__ObjectDataType__Group__8__Impl rule__ObjectDataType__Group__9 ;
    public final void rule__ObjectDataType__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4588:1: ( rule__ObjectDataType__Group__8__Impl rule__ObjectDataType__Group__9 )
            // InternalWebserviceDSL.g:4589:2: rule__ObjectDataType__Group__8__Impl rule__ObjectDataType__Group__9
            {
            pushFollow(FOLLOW_12);
            rule__ObjectDataType__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ObjectDataType__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectDataType__Group__8"


    // $ANTLR start "rule__ObjectDataType__Group__8__Impl"
    // InternalWebserviceDSL.g:4596:1: rule__ObjectDataType__Group__8__Impl : ( ')' ) ;
    public final void rule__ObjectDataType__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4600:1: ( ( ')' ) )
            // InternalWebserviceDSL.g:4601:1: ( ')' )
            {
            // InternalWebserviceDSL.g:4601:1: ( ')' )
            // InternalWebserviceDSL.g:4602:2: ')'
            {
             before(grammarAccess.getObjectDataTypeAccess().getRightParenthesisKeyword_8()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getObjectDataTypeAccess().getRightParenthesisKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectDataType__Group__8__Impl"


    // $ANTLR start "rule__ObjectDataType__Group__9"
    // InternalWebserviceDSL.g:4611:1: rule__ObjectDataType__Group__9 : rule__ObjectDataType__Group__9__Impl ;
    public final void rule__ObjectDataType__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4615:1: ( rule__ObjectDataType__Group__9__Impl )
            // InternalWebserviceDSL.g:4616:2: rule__ObjectDataType__Group__9__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ObjectDataType__Group__9__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectDataType__Group__9"


    // $ANTLR start "rule__ObjectDataType__Group__9__Impl"
    // InternalWebserviceDSL.g:4622:1: rule__ObjectDataType__Group__9__Impl : ( '}' ) ;
    public final void rule__ObjectDataType__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4626:1: ( ( '}' ) )
            // InternalWebserviceDSL.g:4627:1: ( '}' )
            {
            // InternalWebserviceDSL.g:4627:1: ( '}' )
            // InternalWebserviceDSL.g:4628:2: '}'
            {
             before(grammarAccess.getObjectDataTypeAccess().getRightCurlyBracketKeyword_9()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getObjectDataTypeAccess().getRightCurlyBracketKeyword_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectDataType__Group__9__Impl"


    // $ANTLR start "rule__ObjectDataType__Group_7__0"
    // InternalWebserviceDSL.g:4638:1: rule__ObjectDataType__Group_7__0 : rule__ObjectDataType__Group_7__0__Impl rule__ObjectDataType__Group_7__1 ;
    public final void rule__ObjectDataType__Group_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4642:1: ( rule__ObjectDataType__Group_7__0__Impl rule__ObjectDataType__Group_7__1 )
            // InternalWebserviceDSL.g:4643:2: rule__ObjectDataType__Group_7__0__Impl rule__ObjectDataType__Group_7__1
            {
            pushFollow(FOLLOW_3);
            rule__ObjectDataType__Group_7__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ObjectDataType__Group_7__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectDataType__Group_7__0"


    // $ANTLR start "rule__ObjectDataType__Group_7__0__Impl"
    // InternalWebserviceDSL.g:4650:1: rule__ObjectDataType__Group_7__0__Impl : ( ',' ) ;
    public final void rule__ObjectDataType__Group_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4654:1: ( ( ',' ) )
            // InternalWebserviceDSL.g:4655:1: ( ',' )
            {
            // InternalWebserviceDSL.g:4655:1: ( ',' )
            // InternalWebserviceDSL.g:4656:2: ','
            {
             before(grammarAccess.getObjectDataTypeAccess().getCommaKeyword_7_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getObjectDataTypeAccess().getCommaKeyword_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectDataType__Group_7__0__Impl"


    // $ANTLR start "rule__ObjectDataType__Group_7__1"
    // InternalWebserviceDSL.g:4665:1: rule__ObjectDataType__Group_7__1 : rule__ObjectDataType__Group_7__1__Impl ;
    public final void rule__ObjectDataType__Group_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4669:1: ( rule__ObjectDataType__Group_7__1__Impl )
            // InternalWebserviceDSL.g:4670:2: rule__ObjectDataType__Group_7__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ObjectDataType__Group_7__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectDataType__Group_7__1"


    // $ANTLR start "rule__ObjectDataType__Group_7__1__Impl"
    // InternalWebserviceDSL.g:4676:1: rule__ObjectDataType__Group_7__1__Impl : ( ( rule__ObjectDataType__DatatypesAssignment_7_1 ) ) ;
    public final void rule__ObjectDataType__Group_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4680:1: ( ( ( rule__ObjectDataType__DatatypesAssignment_7_1 ) ) )
            // InternalWebserviceDSL.g:4681:1: ( ( rule__ObjectDataType__DatatypesAssignment_7_1 ) )
            {
            // InternalWebserviceDSL.g:4681:1: ( ( rule__ObjectDataType__DatatypesAssignment_7_1 ) )
            // InternalWebserviceDSL.g:4682:2: ( rule__ObjectDataType__DatatypesAssignment_7_1 )
            {
             before(grammarAccess.getObjectDataTypeAccess().getDatatypesAssignment_7_1()); 
            // InternalWebserviceDSL.g:4683:2: ( rule__ObjectDataType__DatatypesAssignment_7_1 )
            // InternalWebserviceDSL.g:4683:3: rule__ObjectDataType__DatatypesAssignment_7_1
            {
            pushFollow(FOLLOW_2);
            rule__ObjectDataType__DatatypesAssignment_7_1();

            state._fsp--;


            }

             after(grammarAccess.getObjectDataTypeAccess().getDatatypesAssignment_7_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectDataType__Group_7__1__Impl"


    // $ANTLR start "rule__CollectionDataType__Group__0"
    // InternalWebserviceDSL.g:4692:1: rule__CollectionDataType__Group__0 : rule__CollectionDataType__Group__0__Impl rule__CollectionDataType__Group__1 ;
    public final void rule__CollectionDataType__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4696:1: ( rule__CollectionDataType__Group__0__Impl rule__CollectionDataType__Group__1 )
            // InternalWebserviceDSL.g:4697:2: rule__CollectionDataType__Group__0__Impl rule__CollectionDataType__Group__1
            {
            pushFollow(FOLLOW_21);
            rule__CollectionDataType__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CollectionDataType__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionDataType__Group__0"


    // $ANTLR start "rule__CollectionDataType__Group__0__Impl"
    // InternalWebserviceDSL.g:4704:1: rule__CollectionDataType__Group__0__Impl : ( ( rule__CollectionDataType__RequiredAssignment_0 )? ) ;
    public final void rule__CollectionDataType__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4708:1: ( ( ( rule__CollectionDataType__RequiredAssignment_0 )? ) )
            // InternalWebserviceDSL.g:4709:1: ( ( rule__CollectionDataType__RequiredAssignment_0 )? )
            {
            // InternalWebserviceDSL.g:4709:1: ( ( rule__CollectionDataType__RequiredAssignment_0 )? )
            // InternalWebserviceDSL.g:4710:2: ( rule__CollectionDataType__RequiredAssignment_0 )?
            {
             before(grammarAccess.getCollectionDataTypeAccess().getRequiredAssignment_0()); 
            // InternalWebserviceDSL.g:4711:2: ( rule__CollectionDataType__RequiredAssignment_0 )?
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==40) ) {
                alt36=1;
            }
            switch (alt36) {
                case 1 :
                    // InternalWebserviceDSL.g:4711:3: rule__CollectionDataType__RequiredAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__CollectionDataType__RequiredAssignment_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getCollectionDataTypeAccess().getRequiredAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionDataType__Group__0__Impl"


    // $ANTLR start "rule__CollectionDataType__Group__1"
    // InternalWebserviceDSL.g:4719:1: rule__CollectionDataType__Group__1 : rule__CollectionDataType__Group__1__Impl rule__CollectionDataType__Group__2 ;
    public final void rule__CollectionDataType__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4723:1: ( rule__CollectionDataType__Group__1__Impl rule__CollectionDataType__Group__2 )
            // InternalWebserviceDSL.g:4724:2: rule__CollectionDataType__Group__1__Impl rule__CollectionDataType__Group__2
            {
            pushFollow(FOLLOW_3);
            rule__CollectionDataType__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CollectionDataType__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionDataType__Group__1"


    // $ANTLR start "rule__CollectionDataType__Group__1__Impl"
    // InternalWebserviceDSL.g:4731:1: rule__CollectionDataType__Group__1__Impl : ( 'CollectionDataType' ) ;
    public final void rule__CollectionDataType__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4735:1: ( ( 'CollectionDataType' ) )
            // InternalWebserviceDSL.g:4736:1: ( 'CollectionDataType' )
            {
            // InternalWebserviceDSL.g:4736:1: ( 'CollectionDataType' )
            // InternalWebserviceDSL.g:4737:2: 'CollectionDataType'
            {
             before(grammarAccess.getCollectionDataTypeAccess().getCollectionDataTypeKeyword_1()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getCollectionDataTypeAccess().getCollectionDataTypeKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionDataType__Group__1__Impl"


    // $ANTLR start "rule__CollectionDataType__Group__2"
    // InternalWebserviceDSL.g:4746:1: rule__CollectionDataType__Group__2 : rule__CollectionDataType__Group__2__Impl rule__CollectionDataType__Group__3 ;
    public final void rule__CollectionDataType__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4750:1: ( rule__CollectionDataType__Group__2__Impl rule__CollectionDataType__Group__3 )
            // InternalWebserviceDSL.g:4751:2: rule__CollectionDataType__Group__2__Impl rule__CollectionDataType__Group__3
            {
            pushFollow(FOLLOW_4);
            rule__CollectionDataType__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CollectionDataType__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionDataType__Group__2"


    // $ANTLR start "rule__CollectionDataType__Group__2__Impl"
    // InternalWebserviceDSL.g:4758:1: rule__CollectionDataType__Group__2__Impl : ( ( rule__CollectionDataType__NameAssignment_2 ) ) ;
    public final void rule__CollectionDataType__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4762:1: ( ( ( rule__CollectionDataType__NameAssignment_2 ) ) )
            // InternalWebserviceDSL.g:4763:1: ( ( rule__CollectionDataType__NameAssignment_2 ) )
            {
            // InternalWebserviceDSL.g:4763:1: ( ( rule__CollectionDataType__NameAssignment_2 ) )
            // InternalWebserviceDSL.g:4764:2: ( rule__CollectionDataType__NameAssignment_2 )
            {
             before(grammarAccess.getCollectionDataTypeAccess().getNameAssignment_2()); 
            // InternalWebserviceDSL.g:4765:2: ( rule__CollectionDataType__NameAssignment_2 )
            // InternalWebserviceDSL.g:4765:3: rule__CollectionDataType__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__CollectionDataType__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getCollectionDataTypeAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionDataType__Group__2__Impl"


    // $ANTLR start "rule__CollectionDataType__Group__3"
    // InternalWebserviceDSL.g:4773:1: rule__CollectionDataType__Group__3 : rule__CollectionDataType__Group__3__Impl rule__CollectionDataType__Group__4 ;
    public final void rule__CollectionDataType__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4777:1: ( rule__CollectionDataType__Group__3__Impl rule__CollectionDataType__Group__4 )
            // InternalWebserviceDSL.g:4778:2: rule__CollectionDataType__Group__3__Impl rule__CollectionDataType__Group__4
            {
            pushFollow(FOLLOW_22);
            rule__CollectionDataType__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CollectionDataType__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionDataType__Group__3"


    // $ANTLR start "rule__CollectionDataType__Group__3__Impl"
    // InternalWebserviceDSL.g:4785:1: rule__CollectionDataType__Group__3__Impl : ( '{' ) ;
    public final void rule__CollectionDataType__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4789:1: ( ( '{' ) )
            // InternalWebserviceDSL.g:4790:1: ( '{' )
            {
            // InternalWebserviceDSL.g:4790:1: ( '{' )
            // InternalWebserviceDSL.g:4791:2: '{'
            {
             before(grammarAccess.getCollectionDataTypeAccess().getLeftCurlyBracketKeyword_3()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getCollectionDataTypeAccess().getLeftCurlyBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionDataType__Group__3__Impl"


    // $ANTLR start "rule__CollectionDataType__Group__4"
    // InternalWebserviceDSL.g:4800:1: rule__CollectionDataType__Group__4 : rule__CollectionDataType__Group__4__Impl rule__CollectionDataType__Group__5 ;
    public final void rule__CollectionDataType__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4804:1: ( rule__CollectionDataType__Group__4__Impl rule__CollectionDataType__Group__5 )
            // InternalWebserviceDSL.g:4805:2: rule__CollectionDataType__Group__4__Impl rule__CollectionDataType__Group__5
            {
            pushFollow(FOLLOW_3);
            rule__CollectionDataType__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CollectionDataType__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionDataType__Group__4"


    // $ANTLR start "rule__CollectionDataType__Group__4__Impl"
    // InternalWebserviceDSL.g:4812:1: rule__CollectionDataType__Group__4__Impl : ( 'listItem' ) ;
    public final void rule__CollectionDataType__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4816:1: ( ( 'listItem' ) )
            // InternalWebserviceDSL.g:4817:1: ( 'listItem' )
            {
            // InternalWebserviceDSL.g:4817:1: ( 'listItem' )
            // InternalWebserviceDSL.g:4818:2: 'listItem'
            {
             before(grammarAccess.getCollectionDataTypeAccess().getListItemKeyword_4()); 
            match(input,34,FOLLOW_2); 
             after(grammarAccess.getCollectionDataTypeAccess().getListItemKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionDataType__Group__4__Impl"


    // $ANTLR start "rule__CollectionDataType__Group__5"
    // InternalWebserviceDSL.g:4827:1: rule__CollectionDataType__Group__5 : rule__CollectionDataType__Group__5__Impl rule__CollectionDataType__Group__6 ;
    public final void rule__CollectionDataType__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4831:1: ( rule__CollectionDataType__Group__5__Impl rule__CollectionDataType__Group__6 )
            // InternalWebserviceDSL.g:4832:2: rule__CollectionDataType__Group__5__Impl rule__CollectionDataType__Group__6
            {
            pushFollow(FOLLOW_12);
            rule__CollectionDataType__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CollectionDataType__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionDataType__Group__5"


    // $ANTLR start "rule__CollectionDataType__Group__5__Impl"
    // InternalWebserviceDSL.g:4839:1: rule__CollectionDataType__Group__5__Impl : ( ( rule__CollectionDataType__ListItemAssignment_5 ) ) ;
    public final void rule__CollectionDataType__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4843:1: ( ( ( rule__CollectionDataType__ListItemAssignment_5 ) ) )
            // InternalWebserviceDSL.g:4844:1: ( ( rule__CollectionDataType__ListItemAssignment_5 ) )
            {
            // InternalWebserviceDSL.g:4844:1: ( ( rule__CollectionDataType__ListItemAssignment_5 ) )
            // InternalWebserviceDSL.g:4845:2: ( rule__CollectionDataType__ListItemAssignment_5 )
            {
             before(grammarAccess.getCollectionDataTypeAccess().getListItemAssignment_5()); 
            // InternalWebserviceDSL.g:4846:2: ( rule__CollectionDataType__ListItemAssignment_5 )
            // InternalWebserviceDSL.g:4846:3: rule__CollectionDataType__ListItemAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__CollectionDataType__ListItemAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getCollectionDataTypeAccess().getListItemAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionDataType__Group__5__Impl"


    // $ANTLR start "rule__CollectionDataType__Group__6"
    // InternalWebserviceDSL.g:4854:1: rule__CollectionDataType__Group__6 : rule__CollectionDataType__Group__6__Impl ;
    public final void rule__CollectionDataType__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4858:1: ( rule__CollectionDataType__Group__6__Impl )
            // InternalWebserviceDSL.g:4859:2: rule__CollectionDataType__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CollectionDataType__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionDataType__Group__6"


    // $ANTLR start "rule__CollectionDataType__Group__6__Impl"
    // InternalWebserviceDSL.g:4865:1: rule__CollectionDataType__Group__6__Impl : ( '}' ) ;
    public final void rule__CollectionDataType__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4869:1: ( ( '}' ) )
            // InternalWebserviceDSL.g:4870:1: ( '}' )
            {
            // InternalWebserviceDSL.g:4870:1: ( '}' )
            // InternalWebserviceDSL.g:4871:2: '}'
            {
             before(grammarAccess.getCollectionDataTypeAccess().getRightCurlyBracketKeyword_6()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getCollectionDataTypeAccess().getRightCurlyBracketKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionDataType__Group__6__Impl"


    // $ANTLR start "rule__StringData__Group__0"
    // InternalWebserviceDSL.g:4881:1: rule__StringData__Group__0 : rule__StringData__Group__0__Impl rule__StringData__Group__1 ;
    public final void rule__StringData__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4885:1: ( rule__StringData__Group__0__Impl rule__StringData__Group__1 )
            // InternalWebserviceDSL.g:4886:2: rule__StringData__Group__0__Impl rule__StringData__Group__1
            {
            pushFollow(FOLLOW_23);
            rule__StringData__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StringData__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringData__Group__0"


    // $ANTLR start "rule__StringData__Group__0__Impl"
    // InternalWebserviceDSL.g:4893:1: rule__StringData__Group__0__Impl : ( () ) ;
    public final void rule__StringData__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4897:1: ( ( () ) )
            // InternalWebserviceDSL.g:4898:1: ( () )
            {
            // InternalWebserviceDSL.g:4898:1: ( () )
            // InternalWebserviceDSL.g:4899:2: ()
            {
             before(grammarAccess.getStringDataAccess().getStringDataAction_0()); 
            // InternalWebserviceDSL.g:4900:2: ()
            // InternalWebserviceDSL.g:4900:3: 
            {
            }

             after(grammarAccess.getStringDataAccess().getStringDataAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringData__Group__0__Impl"


    // $ANTLR start "rule__StringData__Group__1"
    // InternalWebserviceDSL.g:4908:1: rule__StringData__Group__1 : rule__StringData__Group__1__Impl rule__StringData__Group__2 ;
    public final void rule__StringData__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4912:1: ( rule__StringData__Group__1__Impl rule__StringData__Group__2 )
            // InternalWebserviceDSL.g:4913:2: rule__StringData__Group__1__Impl rule__StringData__Group__2
            {
            pushFollow(FOLLOW_23);
            rule__StringData__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StringData__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringData__Group__1"


    // $ANTLR start "rule__StringData__Group__1__Impl"
    // InternalWebserviceDSL.g:4920:1: rule__StringData__Group__1__Impl : ( ( rule__StringData__RequiredAssignment_1 )? ) ;
    public final void rule__StringData__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4924:1: ( ( ( rule__StringData__RequiredAssignment_1 )? ) )
            // InternalWebserviceDSL.g:4925:1: ( ( rule__StringData__RequiredAssignment_1 )? )
            {
            // InternalWebserviceDSL.g:4925:1: ( ( rule__StringData__RequiredAssignment_1 )? )
            // InternalWebserviceDSL.g:4926:2: ( rule__StringData__RequiredAssignment_1 )?
            {
             before(grammarAccess.getStringDataAccess().getRequiredAssignment_1()); 
            // InternalWebserviceDSL.g:4927:2: ( rule__StringData__RequiredAssignment_1 )?
            int alt37=2;
            int LA37_0 = input.LA(1);

            if ( (LA37_0==40) ) {
                alt37=1;
            }
            switch (alt37) {
                case 1 :
                    // InternalWebserviceDSL.g:4927:3: rule__StringData__RequiredAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__StringData__RequiredAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getStringDataAccess().getRequiredAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringData__Group__1__Impl"


    // $ANTLR start "rule__StringData__Group__2"
    // InternalWebserviceDSL.g:4935:1: rule__StringData__Group__2 : rule__StringData__Group__2__Impl rule__StringData__Group__3 ;
    public final void rule__StringData__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4939:1: ( rule__StringData__Group__2__Impl rule__StringData__Group__3 )
            // InternalWebserviceDSL.g:4940:2: rule__StringData__Group__2__Impl rule__StringData__Group__3
            {
            pushFollow(FOLLOW_3);
            rule__StringData__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StringData__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringData__Group__2"


    // $ANTLR start "rule__StringData__Group__2__Impl"
    // InternalWebserviceDSL.g:4947:1: rule__StringData__Group__2__Impl : ( 'StringData' ) ;
    public final void rule__StringData__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4951:1: ( ( 'StringData' ) )
            // InternalWebserviceDSL.g:4952:1: ( 'StringData' )
            {
            // InternalWebserviceDSL.g:4952:1: ( 'StringData' )
            // InternalWebserviceDSL.g:4953:2: 'StringData'
            {
             before(grammarAccess.getStringDataAccess().getStringDataKeyword_2()); 
            match(input,35,FOLLOW_2); 
             after(grammarAccess.getStringDataAccess().getStringDataKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringData__Group__2__Impl"


    // $ANTLR start "rule__StringData__Group__3"
    // InternalWebserviceDSL.g:4962:1: rule__StringData__Group__3 : rule__StringData__Group__3__Impl ;
    public final void rule__StringData__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4966:1: ( rule__StringData__Group__3__Impl )
            // InternalWebserviceDSL.g:4967:2: rule__StringData__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__StringData__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringData__Group__3"


    // $ANTLR start "rule__StringData__Group__3__Impl"
    // InternalWebserviceDSL.g:4973:1: rule__StringData__Group__3__Impl : ( ( rule__StringData__NameAssignment_3 ) ) ;
    public final void rule__StringData__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4977:1: ( ( ( rule__StringData__NameAssignment_3 ) ) )
            // InternalWebserviceDSL.g:4978:1: ( ( rule__StringData__NameAssignment_3 ) )
            {
            // InternalWebserviceDSL.g:4978:1: ( ( rule__StringData__NameAssignment_3 ) )
            // InternalWebserviceDSL.g:4979:2: ( rule__StringData__NameAssignment_3 )
            {
             before(grammarAccess.getStringDataAccess().getNameAssignment_3()); 
            // InternalWebserviceDSL.g:4980:2: ( rule__StringData__NameAssignment_3 )
            // InternalWebserviceDSL.g:4980:3: rule__StringData__NameAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__StringData__NameAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getStringDataAccess().getNameAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringData__Group__3__Impl"


    // $ANTLR start "rule__IntegerData__Group__0"
    // InternalWebserviceDSL.g:4989:1: rule__IntegerData__Group__0 : rule__IntegerData__Group__0__Impl rule__IntegerData__Group__1 ;
    public final void rule__IntegerData__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:4993:1: ( rule__IntegerData__Group__0__Impl rule__IntegerData__Group__1 )
            // InternalWebserviceDSL.g:4994:2: rule__IntegerData__Group__0__Impl rule__IntegerData__Group__1
            {
            pushFollow(FOLLOW_24);
            rule__IntegerData__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IntegerData__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerData__Group__0"


    // $ANTLR start "rule__IntegerData__Group__0__Impl"
    // InternalWebserviceDSL.g:5001:1: rule__IntegerData__Group__0__Impl : ( () ) ;
    public final void rule__IntegerData__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5005:1: ( ( () ) )
            // InternalWebserviceDSL.g:5006:1: ( () )
            {
            // InternalWebserviceDSL.g:5006:1: ( () )
            // InternalWebserviceDSL.g:5007:2: ()
            {
             before(grammarAccess.getIntegerDataAccess().getIntegerDataAction_0()); 
            // InternalWebserviceDSL.g:5008:2: ()
            // InternalWebserviceDSL.g:5008:3: 
            {
            }

             after(grammarAccess.getIntegerDataAccess().getIntegerDataAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerData__Group__0__Impl"


    // $ANTLR start "rule__IntegerData__Group__1"
    // InternalWebserviceDSL.g:5016:1: rule__IntegerData__Group__1 : rule__IntegerData__Group__1__Impl rule__IntegerData__Group__2 ;
    public final void rule__IntegerData__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5020:1: ( rule__IntegerData__Group__1__Impl rule__IntegerData__Group__2 )
            // InternalWebserviceDSL.g:5021:2: rule__IntegerData__Group__1__Impl rule__IntegerData__Group__2
            {
            pushFollow(FOLLOW_24);
            rule__IntegerData__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IntegerData__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerData__Group__1"


    // $ANTLR start "rule__IntegerData__Group__1__Impl"
    // InternalWebserviceDSL.g:5028:1: rule__IntegerData__Group__1__Impl : ( ( rule__IntegerData__RequiredAssignment_1 )? ) ;
    public final void rule__IntegerData__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5032:1: ( ( ( rule__IntegerData__RequiredAssignment_1 )? ) )
            // InternalWebserviceDSL.g:5033:1: ( ( rule__IntegerData__RequiredAssignment_1 )? )
            {
            // InternalWebserviceDSL.g:5033:1: ( ( rule__IntegerData__RequiredAssignment_1 )? )
            // InternalWebserviceDSL.g:5034:2: ( rule__IntegerData__RequiredAssignment_1 )?
            {
             before(grammarAccess.getIntegerDataAccess().getRequiredAssignment_1()); 
            // InternalWebserviceDSL.g:5035:2: ( rule__IntegerData__RequiredAssignment_1 )?
            int alt38=2;
            int LA38_0 = input.LA(1);

            if ( (LA38_0==40) ) {
                alt38=1;
            }
            switch (alt38) {
                case 1 :
                    // InternalWebserviceDSL.g:5035:3: rule__IntegerData__RequiredAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__IntegerData__RequiredAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getIntegerDataAccess().getRequiredAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerData__Group__1__Impl"


    // $ANTLR start "rule__IntegerData__Group__2"
    // InternalWebserviceDSL.g:5043:1: rule__IntegerData__Group__2 : rule__IntegerData__Group__2__Impl rule__IntegerData__Group__3 ;
    public final void rule__IntegerData__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5047:1: ( rule__IntegerData__Group__2__Impl rule__IntegerData__Group__3 )
            // InternalWebserviceDSL.g:5048:2: rule__IntegerData__Group__2__Impl rule__IntegerData__Group__3
            {
            pushFollow(FOLLOW_3);
            rule__IntegerData__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IntegerData__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerData__Group__2"


    // $ANTLR start "rule__IntegerData__Group__2__Impl"
    // InternalWebserviceDSL.g:5055:1: rule__IntegerData__Group__2__Impl : ( 'IntegerData' ) ;
    public final void rule__IntegerData__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5059:1: ( ( 'IntegerData' ) )
            // InternalWebserviceDSL.g:5060:1: ( 'IntegerData' )
            {
            // InternalWebserviceDSL.g:5060:1: ( 'IntegerData' )
            // InternalWebserviceDSL.g:5061:2: 'IntegerData'
            {
             before(grammarAccess.getIntegerDataAccess().getIntegerDataKeyword_2()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getIntegerDataAccess().getIntegerDataKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerData__Group__2__Impl"


    // $ANTLR start "rule__IntegerData__Group__3"
    // InternalWebserviceDSL.g:5070:1: rule__IntegerData__Group__3 : rule__IntegerData__Group__3__Impl ;
    public final void rule__IntegerData__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5074:1: ( rule__IntegerData__Group__3__Impl )
            // InternalWebserviceDSL.g:5075:2: rule__IntegerData__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__IntegerData__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerData__Group__3"


    // $ANTLR start "rule__IntegerData__Group__3__Impl"
    // InternalWebserviceDSL.g:5081:1: rule__IntegerData__Group__3__Impl : ( ( rule__IntegerData__NameAssignment_3 ) ) ;
    public final void rule__IntegerData__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5085:1: ( ( ( rule__IntegerData__NameAssignment_3 ) ) )
            // InternalWebserviceDSL.g:5086:1: ( ( rule__IntegerData__NameAssignment_3 ) )
            {
            // InternalWebserviceDSL.g:5086:1: ( ( rule__IntegerData__NameAssignment_3 ) )
            // InternalWebserviceDSL.g:5087:2: ( rule__IntegerData__NameAssignment_3 )
            {
             before(grammarAccess.getIntegerDataAccess().getNameAssignment_3()); 
            // InternalWebserviceDSL.g:5088:2: ( rule__IntegerData__NameAssignment_3 )
            // InternalWebserviceDSL.g:5088:3: rule__IntegerData__NameAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__IntegerData__NameAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getIntegerDataAccess().getNameAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerData__Group__3__Impl"


    // $ANTLR start "rule__FloatData__Group__0"
    // InternalWebserviceDSL.g:5097:1: rule__FloatData__Group__0 : rule__FloatData__Group__0__Impl rule__FloatData__Group__1 ;
    public final void rule__FloatData__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5101:1: ( rule__FloatData__Group__0__Impl rule__FloatData__Group__1 )
            // InternalWebserviceDSL.g:5102:2: rule__FloatData__Group__0__Impl rule__FloatData__Group__1
            {
            pushFollow(FOLLOW_25);
            rule__FloatData__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FloatData__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FloatData__Group__0"


    // $ANTLR start "rule__FloatData__Group__0__Impl"
    // InternalWebserviceDSL.g:5109:1: rule__FloatData__Group__0__Impl : ( () ) ;
    public final void rule__FloatData__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5113:1: ( ( () ) )
            // InternalWebserviceDSL.g:5114:1: ( () )
            {
            // InternalWebserviceDSL.g:5114:1: ( () )
            // InternalWebserviceDSL.g:5115:2: ()
            {
             before(grammarAccess.getFloatDataAccess().getFloatDataAction_0()); 
            // InternalWebserviceDSL.g:5116:2: ()
            // InternalWebserviceDSL.g:5116:3: 
            {
            }

             after(grammarAccess.getFloatDataAccess().getFloatDataAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FloatData__Group__0__Impl"


    // $ANTLR start "rule__FloatData__Group__1"
    // InternalWebserviceDSL.g:5124:1: rule__FloatData__Group__1 : rule__FloatData__Group__1__Impl rule__FloatData__Group__2 ;
    public final void rule__FloatData__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5128:1: ( rule__FloatData__Group__1__Impl rule__FloatData__Group__2 )
            // InternalWebserviceDSL.g:5129:2: rule__FloatData__Group__1__Impl rule__FloatData__Group__2
            {
            pushFollow(FOLLOW_25);
            rule__FloatData__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FloatData__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FloatData__Group__1"


    // $ANTLR start "rule__FloatData__Group__1__Impl"
    // InternalWebserviceDSL.g:5136:1: rule__FloatData__Group__1__Impl : ( ( rule__FloatData__RequiredAssignment_1 )? ) ;
    public final void rule__FloatData__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5140:1: ( ( ( rule__FloatData__RequiredAssignment_1 )? ) )
            // InternalWebserviceDSL.g:5141:1: ( ( rule__FloatData__RequiredAssignment_1 )? )
            {
            // InternalWebserviceDSL.g:5141:1: ( ( rule__FloatData__RequiredAssignment_1 )? )
            // InternalWebserviceDSL.g:5142:2: ( rule__FloatData__RequiredAssignment_1 )?
            {
             before(grammarAccess.getFloatDataAccess().getRequiredAssignment_1()); 
            // InternalWebserviceDSL.g:5143:2: ( rule__FloatData__RequiredAssignment_1 )?
            int alt39=2;
            int LA39_0 = input.LA(1);

            if ( (LA39_0==40) ) {
                alt39=1;
            }
            switch (alt39) {
                case 1 :
                    // InternalWebserviceDSL.g:5143:3: rule__FloatData__RequiredAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__FloatData__RequiredAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFloatDataAccess().getRequiredAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FloatData__Group__1__Impl"


    // $ANTLR start "rule__FloatData__Group__2"
    // InternalWebserviceDSL.g:5151:1: rule__FloatData__Group__2 : rule__FloatData__Group__2__Impl rule__FloatData__Group__3 ;
    public final void rule__FloatData__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5155:1: ( rule__FloatData__Group__2__Impl rule__FloatData__Group__3 )
            // InternalWebserviceDSL.g:5156:2: rule__FloatData__Group__2__Impl rule__FloatData__Group__3
            {
            pushFollow(FOLLOW_3);
            rule__FloatData__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FloatData__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FloatData__Group__2"


    // $ANTLR start "rule__FloatData__Group__2__Impl"
    // InternalWebserviceDSL.g:5163:1: rule__FloatData__Group__2__Impl : ( 'FloatData' ) ;
    public final void rule__FloatData__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5167:1: ( ( 'FloatData' ) )
            // InternalWebserviceDSL.g:5168:1: ( 'FloatData' )
            {
            // InternalWebserviceDSL.g:5168:1: ( 'FloatData' )
            // InternalWebserviceDSL.g:5169:2: 'FloatData'
            {
             before(grammarAccess.getFloatDataAccess().getFloatDataKeyword_2()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getFloatDataAccess().getFloatDataKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FloatData__Group__2__Impl"


    // $ANTLR start "rule__FloatData__Group__3"
    // InternalWebserviceDSL.g:5178:1: rule__FloatData__Group__3 : rule__FloatData__Group__3__Impl ;
    public final void rule__FloatData__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5182:1: ( rule__FloatData__Group__3__Impl )
            // InternalWebserviceDSL.g:5183:2: rule__FloatData__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__FloatData__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FloatData__Group__3"


    // $ANTLR start "rule__FloatData__Group__3__Impl"
    // InternalWebserviceDSL.g:5189:1: rule__FloatData__Group__3__Impl : ( ( rule__FloatData__NameAssignment_3 ) ) ;
    public final void rule__FloatData__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5193:1: ( ( ( rule__FloatData__NameAssignment_3 ) ) )
            // InternalWebserviceDSL.g:5194:1: ( ( rule__FloatData__NameAssignment_3 ) )
            {
            // InternalWebserviceDSL.g:5194:1: ( ( rule__FloatData__NameAssignment_3 ) )
            // InternalWebserviceDSL.g:5195:2: ( rule__FloatData__NameAssignment_3 )
            {
             before(grammarAccess.getFloatDataAccess().getNameAssignment_3()); 
            // InternalWebserviceDSL.g:5196:2: ( rule__FloatData__NameAssignment_3 )
            // InternalWebserviceDSL.g:5196:3: rule__FloatData__NameAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__FloatData__NameAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getFloatDataAccess().getNameAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FloatData__Group__3__Impl"


    // $ANTLR start "rule__BooleanData__Group__0"
    // InternalWebserviceDSL.g:5205:1: rule__BooleanData__Group__0 : rule__BooleanData__Group__0__Impl rule__BooleanData__Group__1 ;
    public final void rule__BooleanData__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5209:1: ( rule__BooleanData__Group__0__Impl rule__BooleanData__Group__1 )
            // InternalWebserviceDSL.g:5210:2: rule__BooleanData__Group__0__Impl rule__BooleanData__Group__1
            {
            pushFollow(FOLLOW_26);
            rule__BooleanData__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BooleanData__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanData__Group__0"


    // $ANTLR start "rule__BooleanData__Group__0__Impl"
    // InternalWebserviceDSL.g:5217:1: rule__BooleanData__Group__0__Impl : ( () ) ;
    public final void rule__BooleanData__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5221:1: ( ( () ) )
            // InternalWebserviceDSL.g:5222:1: ( () )
            {
            // InternalWebserviceDSL.g:5222:1: ( () )
            // InternalWebserviceDSL.g:5223:2: ()
            {
             before(grammarAccess.getBooleanDataAccess().getBooleanDataAction_0()); 
            // InternalWebserviceDSL.g:5224:2: ()
            // InternalWebserviceDSL.g:5224:3: 
            {
            }

             after(grammarAccess.getBooleanDataAccess().getBooleanDataAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanData__Group__0__Impl"


    // $ANTLR start "rule__BooleanData__Group__1"
    // InternalWebserviceDSL.g:5232:1: rule__BooleanData__Group__1 : rule__BooleanData__Group__1__Impl rule__BooleanData__Group__2 ;
    public final void rule__BooleanData__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5236:1: ( rule__BooleanData__Group__1__Impl rule__BooleanData__Group__2 )
            // InternalWebserviceDSL.g:5237:2: rule__BooleanData__Group__1__Impl rule__BooleanData__Group__2
            {
            pushFollow(FOLLOW_26);
            rule__BooleanData__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BooleanData__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanData__Group__1"


    // $ANTLR start "rule__BooleanData__Group__1__Impl"
    // InternalWebserviceDSL.g:5244:1: rule__BooleanData__Group__1__Impl : ( ( rule__BooleanData__RequiredAssignment_1 )? ) ;
    public final void rule__BooleanData__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5248:1: ( ( ( rule__BooleanData__RequiredAssignment_1 )? ) )
            // InternalWebserviceDSL.g:5249:1: ( ( rule__BooleanData__RequiredAssignment_1 )? )
            {
            // InternalWebserviceDSL.g:5249:1: ( ( rule__BooleanData__RequiredAssignment_1 )? )
            // InternalWebserviceDSL.g:5250:2: ( rule__BooleanData__RequiredAssignment_1 )?
            {
             before(grammarAccess.getBooleanDataAccess().getRequiredAssignment_1()); 
            // InternalWebserviceDSL.g:5251:2: ( rule__BooleanData__RequiredAssignment_1 )?
            int alt40=2;
            int LA40_0 = input.LA(1);

            if ( (LA40_0==40) ) {
                alt40=1;
            }
            switch (alt40) {
                case 1 :
                    // InternalWebserviceDSL.g:5251:3: rule__BooleanData__RequiredAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__BooleanData__RequiredAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getBooleanDataAccess().getRequiredAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanData__Group__1__Impl"


    // $ANTLR start "rule__BooleanData__Group__2"
    // InternalWebserviceDSL.g:5259:1: rule__BooleanData__Group__2 : rule__BooleanData__Group__2__Impl rule__BooleanData__Group__3 ;
    public final void rule__BooleanData__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5263:1: ( rule__BooleanData__Group__2__Impl rule__BooleanData__Group__3 )
            // InternalWebserviceDSL.g:5264:2: rule__BooleanData__Group__2__Impl rule__BooleanData__Group__3
            {
            pushFollow(FOLLOW_3);
            rule__BooleanData__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BooleanData__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanData__Group__2"


    // $ANTLR start "rule__BooleanData__Group__2__Impl"
    // InternalWebserviceDSL.g:5271:1: rule__BooleanData__Group__2__Impl : ( 'BooleanData' ) ;
    public final void rule__BooleanData__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5275:1: ( ( 'BooleanData' ) )
            // InternalWebserviceDSL.g:5276:1: ( 'BooleanData' )
            {
            // InternalWebserviceDSL.g:5276:1: ( 'BooleanData' )
            // InternalWebserviceDSL.g:5277:2: 'BooleanData'
            {
             before(grammarAccess.getBooleanDataAccess().getBooleanDataKeyword_2()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getBooleanDataAccess().getBooleanDataKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanData__Group__2__Impl"


    // $ANTLR start "rule__BooleanData__Group__3"
    // InternalWebserviceDSL.g:5286:1: rule__BooleanData__Group__3 : rule__BooleanData__Group__3__Impl ;
    public final void rule__BooleanData__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5290:1: ( rule__BooleanData__Group__3__Impl )
            // InternalWebserviceDSL.g:5291:2: rule__BooleanData__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BooleanData__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanData__Group__3"


    // $ANTLR start "rule__BooleanData__Group__3__Impl"
    // InternalWebserviceDSL.g:5297:1: rule__BooleanData__Group__3__Impl : ( ( rule__BooleanData__NameAssignment_3 ) ) ;
    public final void rule__BooleanData__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5301:1: ( ( ( rule__BooleanData__NameAssignment_3 ) ) )
            // InternalWebserviceDSL.g:5302:1: ( ( rule__BooleanData__NameAssignment_3 ) )
            {
            // InternalWebserviceDSL.g:5302:1: ( ( rule__BooleanData__NameAssignment_3 ) )
            // InternalWebserviceDSL.g:5303:2: ( rule__BooleanData__NameAssignment_3 )
            {
             before(grammarAccess.getBooleanDataAccess().getNameAssignment_3()); 
            // InternalWebserviceDSL.g:5304:2: ( rule__BooleanData__NameAssignment_3 )
            // InternalWebserviceDSL.g:5304:3: rule__BooleanData__NameAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__BooleanData__NameAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getBooleanDataAccess().getNameAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanData__Group__3__Impl"


    // $ANTLR start "rule__ByteData__Group__0"
    // InternalWebserviceDSL.g:5313:1: rule__ByteData__Group__0 : rule__ByteData__Group__0__Impl rule__ByteData__Group__1 ;
    public final void rule__ByteData__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5317:1: ( rule__ByteData__Group__0__Impl rule__ByteData__Group__1 )
            // InternalWebserviceDSL.g:5318:2: rule__ByteData__Group__0__Impl rule__ByteData__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__ByteData__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ByteData__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ByteData__Group__0"


    // $ANTLR start "rule__ByteData__Group__0__Impl"
    // InternalWebserviceDSL.g:5325:1: rule__ByteData__Group__0__Impl : ( () ) ;
    public final void rule__ByteData__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5329:1: ( ( () ) )
            // InternalWebserviceDSL.g:5330:1: ( () )
            {
            // InternalWebserviceDSL.g:5330:1: ( () )
            // InternalWebserviceDSL.g:5331:2: ()
            {
             before(grammarAccess.getByteDataAccess().getByteDataAction_0()); 
            // InternalWebserviceDSL.g:5332:2: ()
            // InternalWebserviceDSL.g:5332:3: 
            {
            }

             after(grammarAccess.getByteDataAccess().getByteDataAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ByteData__Group__0__Impl"


    // $ANTLR start "rule__ByteData__Group__1"
    // InternalWebserviceDSL.g:5340:1: rule__ByteData__Group__1 : rule__ByteData__Group__1__Impl rule__ByteData__Group__2 ;
    public final void rule__ByteData__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5344:1: ( rule__ByteData__Group__1__Impl rule__ByteData__Group__2 )
            // InternalWebserviceDSL.g:5345:2: rule__ByteData__Group__1__Impl rule__ByteData__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__ByteData__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ByteData__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ByteData__Group__1"


    // $ANTLR start "rule__ByteData__Group__1__Impl"
    // InternalWebserviceDSL.g:5352:1: rule__ByteData__Group__1__Impl : ( ( rule__ByteData__RequiredAssignment_1 )? ) ;
    public final void rule__ByteData__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5356:1: ( ( ( rule__ByteData__RequiredAssignment_1 )? ) )
            // InternalWebserviceDSL.g:5357:1: ( ( rule__ByteData__RequiredAssignment_1 )? )
            {
            // InternalWebserviceDSL.g:5357:1: ( ( rule__ByteData__RequiredAssignment_1 )? )
            // InternalWebserviceDSL.g:5358:2: ( rule__ByteData__RequiredAssignment_1 )?
            {
             before(grammarAccess.getByteDataAccess().getRequiredAssignment_1()); 
            // InternalWebserviceDSL.g:5359:2: ( rule__ByteData__RequiredAssignment_1 )?
            int alt41=2;
            int LA41_0 = input.LA(1);

            if ( (LA41_0==40) ) {
                alt41=1;
            }
            switch (alt41) {
                case 1 :
                    // InternalWebserviceDSL.g:5359:3: rule__ByteData__RequiredAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__ByteData__RequiredAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getByteDataAccess().getRequiredAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ByteData__Group__1__Impl"


    // $ANTLR start "rule__ByteData__Group__2"
    // InternalWebserviceDSL.g:5367:1: rule__ByteData__Group__2 : rule__ByteData__Group__2__Impl rule__ByteData__Group__3 ;
    public final void rule__ByteData__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5371:1: ( rule__ByteData__Group__2__Impl rule__ByteData__Group__3 )
            // InternalWebserviceDSL.g:5372:2: rule__ByteData__Group__2__Impl rule__ByteData__Group__3
            {
            pushFollow(FOLLOW_3);
            rule__ByteData__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ByteData__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ByteData__Group__2"


    // $ANTLR start "rule__ByteData__Group__2__Impl"
    // InternalWebserviceDSL.g:5379:1: rule__ByteData__Group__2__Impl : ( 'ByteData' ) ;
    public final void rule__ByteData__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5383:1: ( ( 'ByteData' ) )
            // InternalWebserviceDSL.g:5384:1: ( 'ByteData' )
            {
            // InternalWebserviceDSL.g:5384:1: ( 'ByteData' )
            // InternalWebserviceDSL.g:5385:2: 'ByteData'
            {
             before(grammarAccess.getByteDataAccess().getByteDataKeyword_2()); 
            match(input,39,FOLLOW_2); 
             after(grammarAccess.getByteDataAccess().getByteDataKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ByteData__Group__2__Impl"


    // $ANTLR start "rule__ByteData__Group__3"
    // InternalWebserviceDSL.g:5394:1: rule__ByteData__Group__3 : rule__ByteData__Group__3__Impl ;
    public final void rule__ByteData__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5398:1: ( rule__ByteData__Group__3__Impl )
            // InternalWebserviceDSL.g:5399:2: rule__ByteData__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ByteData__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ByteData__Group__3"


    // $ANTLR start "rule__ByteData__Group__3__Impl"
    // InternalWebserviceDSL.g:5405:1: rule__ByteData__Group__3__Impl : ( ( rule__ByteData__NameAssignment_3 ) ) ;
    public final void rule__ByteData__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5409:1: ( ( ( rule__ByteData__NameAssignment_3 ) ) )
            // InternalWebserviceDSL.g:5410:1: ( ( rule__ByteData__NameAssignment_3 ) )
            {
            // InternalWebserviceDSL.g:5410:1: ( ( rule__ByteData__NameAssignment_3 ) )
            // InternalWebserviceDSL.g:5411:2: ( rule__ByteData__NameAssignment_3 )
            {
             before(grammarAccess.getByteDataAccess().getNameAssignment_3()); 
            // InternalWebserviceDSL.g:5412:2: ( rule__ByteData__NameAssignment_3 )
            // InternalWebserviceDSL.g:5412:3: rule__ByteData__NameAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__ByteData__NameAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getByteDataAccess().getNameAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ByteData__Group__3__Impl"


    // $ANTLR start "rule__API__NameAssignment_1"
    // InternalWebserviceDSL.g:5421:1: rule__API__NameAssignment_1 : ( ruleEString ) ;
    public final void rule__API__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5425:1: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:5426:2: ( ruleEString )
            {
            // InternalWebserviceDSL.g:5426:2: ( ruleEString )
            // InternalWebserviceDSL.g:5427:3: ruleEString
            {
             before(grammarAccess.getAPIAccess().getNameEStringParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getAPIAccess().getNameEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__NameAssignment_1"


    // $ANTLR start "rule__API__DescriptionAssignment_3_1"
    // InternalWebserviceDSL.g:5436:1: rule__API__DescriptionAssignment_3_1 : ( ruleEString ) ;
    public final void rule__API__DescriptionAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5440:1: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:5441:2: ( ruleEString )
            {
            // InternalWebserviceDSL.g:5441:2: ( ruleEString )
            // InternalWebserviceDSL.g:5442:3: ruleEString
            {
             before(grammarAccess.getAPIAccess().getDescriptionEStringParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getAPIAccess().getDescriptionEStringParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__DescriptionAssignment_3_1"


    // $ANTLR start "rule__API__UrlAssignment_5"
    // InternalWebserviceDSL.g:5451:1: rule__API__UrlAssignment_5 : ( ruleEString ) ;
    public final void rule__API__UrlAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5455:1: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:5456:2: ( ruleEString )
            {
            // InternalWebserviceDSL.g:5456:2: ( ruleEString )
            // InternalWebserviceDSL.g:5457:3: ruleEString
            {
             before(grammarAccess.getAPIAccess().getUrlEStringParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getAPIAccess().getUrlEStringParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__UrlAssignment_5"


    // $ANTLR start "rule__API__DatatypesAssignment_8"
    // InternalWebserviceDSL.g:5466:1: rule__API__DatatypesAssignment_8 : ( ruleDataType ) ;
    public final void rule__API__DatatypesAssignment_8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5470:1: ( ( ruleDataType ) )
            // InternalWebserviceDSL.g:5471:2: ( ruleDataType )
            {
            // InternalWebserviceDSL.g:5471:2: ( ruleDataType )
            // InternalWebserviceDSL.g:5472:3: ruleDataType
            {
             before(grammarAccess.getAPIAccess().getDatatypesDataTypeParserRuleCall_8_0()); 
            pushFollow(FOLLOW_2);
            ruleDataType();

            state._fsp--;

             after(grammarAccess.getAPIAccess().getDatatypesDataTypeParserRuleCall_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__DatatypesAssignment_8"


    // $ANTLR start "rule__API__DatatypesAssignment_9_1"
    // InternalWebserviceDSL.g:5481:1: rule__API__DatatypesAssignment_9_1 : ( ruleDataType ) ;
    public final void rule__API__DatatypesAssignment_9_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5485:1: ( ( ruleDataType ) )
            // InternalWebserviceDSL.g:5486:2: ( ruleDataType )
            {
            // InternalWebserviceDSL.g:5486:2: ( ruleDataType )
            // InternalWebserviceDSL.g:5487:3: ruleDataType
            {
             before(grammarAccess.getAPIAccess().getDatatypesDataTypeParserRuleCall_9_1_0()); 
            pushFollow(FOLLOW_2);
            ruleDataType();

            state._fsp--;

             after(grammarAccess.getAPIAccess().getDatatypesDataTypeParserRuleCall_9_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__DatatypesAssignment_9_1"


    // $ANTLR start "rule__API__OperationsAssignment_13"
    // InternalWebserviceDSL.g:5496:1: rule__API__OperationsAssignment_13 : ( ruleOperation ) ;
    public final void rule__API__OperationsAssignment_13() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5500:1: ( ( ruleOperation ) )
            // InternalWebserviceDSL.g:5501:2: ( ruleOperation )
            {
            // InternalWebserviceDSL.g:5501:2: ( ruleOperation )
            // InternalWebserviceDSL.g:5502:3: ruleOperation
            {
             before(grammarAccess.getAPIAccess().getOperationsOperationParserRuleCall_13_0()); 
            pushFollow(FOLLOW_2);
            ruleOperation();

            state._fsp--;

             after(grammarAccess.getAPIAccess().getOperationsOperationParserRuleCall_13_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__OperationsAssignment_13"


    // $ANTLR start "rule__API__OperationsAssignment_14_1"
    // InternalWebserviceDSL.g:5511:1: rule__API__OperationsAssignment_14_1 : ( ruleOperation ) ;
    public final void rule__API__OperationsAssignment_14_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5515:1: ( ( ruleOperation ) )
            // InternalWebserviceDSL.g:5516:2: ( ruleOperation )
            {
            // InternalWebserviceDSL.g:5516:2: ( ruleOperation )
            // InternalWebserviceDSL.g:5517:3: ruleOperation
            {
             before(grammarAccess.getAPIAccess().getOperationsOperationParserRuleCall_14_1_0()); 
            pushFollow(FOLLOW_2);
            ruleOperation();

            state._fsp--;

             after(grammarAccess.getAPIAccess().getOperationsOperationParserRuleCall_14_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__API__OperationsAssignment_14_1"


    // $ANTLR start "rule__Parameter__RequiredAssignment_0"
    // InternalWebserviceDSL.g:5526:1: rule__Parameter__RequiredAssignment_0 : ( ( 'required' ) ) ;
    public final void rule__Parameter__RequiredAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5530:1: ( ( ( 'required' ) ) )
            // InternalWebserviceDSL.g:5531:2: ( ( 'required' ) )
            {
            // InternalWebserviceDSL.g:5531:2: ( ( 'required' ) )
            // InternalWebserviceDSL.g:5532:3: ( 'required' )
            {
             before(grammarAccess.getParameterAccess().getRequiredRequiredKeyword_0_0()); 
            // InternalWebserviceDSL.g:5533:3: ( 'required' )
            // InternalWebserviceDSL.g:5534:4: 'required'
            {
             before(grammarAccess.getParameterAccess().getRequiredRequiredKeyword_0_0()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getParameterAccess().getRequiredRequiredKeyword_0_0()); 

            }

             after(grammarAccess.getParameterAccess().getRequiredRequiredKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__RequiredAssignment_0"


    // $ANTLR start "rule__Parameter__DescriptionAssignment_3_1"
    // InternalWebserviceDSL.g:5545:1: rule__Parameter__DescriptionAssignment_3_1 : ( ruleEString ) ;
    public final void rule__Parameter__DescriptionAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5549:1: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:5550:2: ( ruleEString )
            {
            // InternalWebserviceDSL.g:5550:2: ( ruleEString )
            // InternalWebserviceDSL.g:5551:3: ruleEString
            {
             before(grammarAccess.getParameterAccess().getDescriptionEStringParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getParameterAccess().getDescriptionEStringParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__DescriptionAssignment_3_1"


    // $ANTLR start "rule__Parameter__DatatypeAssignment_5"
    // InternalWebserviceDSL.g:5560:1: rule__Parameter__DatatypeAssignment_5 : ( ( ruleEString ) ) ;
    public final void rule__Parameter__DatatypeAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5564:1: ( ( ( ruleEString ) ) )
            // InternalWebserviceDSL.g:5565:2: ( ( ruleEString ) )
            {
            // InternalWebserviceDSL.g:5565:2: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:5566:3: ( ruleEString )
            {
             before(grammarAccess.getParameterAccess().getDatatypeDataTypeCrossReference_5_0()); 
            // InternalWebserviceDSL.g:5567:3: ( ruleEString )
            // InternalWebserviceDSL.g:5568:4: ruleEString
            {
             before(grammarAccess.getParameterAccess().getDatatypeDataTypeEStringParserRuleCall_5_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getParameterAccess().getDatatypeDataTypeEStringParserRuleCall_5_0_1()); 

            }

             after(grammarAccess.getParameterAccess().getDatatypeDataTypeCrossReference_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__DatatypeAssignment_5"


    // $ANTLR start "rule__ReadOperation__NameAssignment_1"
    // InternalWebserviceDSL.g:5579:1: rule__ReadOperation__NameAssignment_1 : ( ruleEString ) ;
    public final void rule__ReadOperation__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5583:1: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:5584:2: ( ruleEString )
            {
            // InternalWebserviceDSL.g:5584:2: ( ruleEString )
            // InternalWebserviceDSL.g:5585:3: ruleEString
            {
             before(grammarAccess.getReadOperationAccess().getNameEStringParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getReadOperationAccess().getNameEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__NameAssignment_1"


    // $ANTLR start "rule__ReadOperation__DescriptionAssignment_3_1"
    // InternalWebserviceDSL.g:5594:1: rule__ReadOperation__DescriptionAssignment_3_1 : ( ruleEString ) ;
    public final void rule__ReadOperation__DescriptionAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5598:1: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:5599:2: ( ruleEString )
            {
            // InternalWebserviceDSL.g:5599:2: ( ruleEString )
            // InternalWebserviceDSL.g:5600:3: ruleEString
            {
             before(grammarAccess.getReadOperationAccess().getDescriptionEStringParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getReadOperationAccess().getDescriptionEStringParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__DescriptionAssignment_3_1"


    // $ANTLR start "rule__ReadOperation__UrlSuffixAssignment_4_1"
    // InternalWebserviceDSL.g:5609:1: rule__ReadOperation__UrlSuffixAssignment_4_1 : ( ruleEString ) ;
    public final void rule__ReadOperation__UrlSuffixAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5613:1: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:5614:2: ( ruleEString )
            {
            // InternalWebserviceDSL.g:5614:2: ( ruleEString )
            // InternalWebserviceDSL.g:5615:3: ruleEString
            {
             before(grammarAccess.getReadOperationAccess().getUrlSuffixEStringParserRuleCall_4_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getReadOperationAccess().getUrlSuffixEStringParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__UrlSuffixAssignment_4_1"


    // $ANTLR start "rule__ReadOperation__ResponseAssignment_5_1"
    // InternalWebserviceDSL.g:5624:1: rule__ReadOperation__ResponseAssignment_5_1 : ( ( ruleEString ) ) ;
    public final void rule__ReadOperation__ResponseAssignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5628:1: ( ( ( ruleEString ) ) )
            // InternalWebserviceDSL.g:5629:2: ( ( ruleEString ) )
            {
            // InternalWebserviceDSL.g:5629:2: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:5630:3: ( ruleEString )
            {
             before(grammarAccess.getReadOperationAccess().getResponseDataTypeCrossReference_5_1_0()); 
            // InternalWebserviceDSL.g:5631:3: ( ruleEString )
            // InternalWebserviceDSL.g:5632:4: ruleEString
            {
             before(grammarAccess.getReadOperationAccess().getResponseDataTypeEStringParserRuleCall_5_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getReadOperationAccess().getResponseDataTypeEStringParserRuleCall_5_1_0_1()); 

            }

             after(grammarAccess.getReadOperationAccess().getResponseDataTypeCrossReference_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__ResponseAssignment_5_1"


    // $ANTLR start "rule__ReadOperation__ParametersAssignment_6_2"
    // InternalWebserviceDSL.g:5643:1: rule__ReadOperation__ParametersAssignment_6_2 : ( ruleParameter ) ;
    public final void rule__ReadOperation__ParametersAssignment_6_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5647:1: ( ( ruleParameter ) )
            // InternalWebserviceDSL.g:5648:2: ( ruleParameter )
            {
            // InternalWebserviceDSL.g:5648:2: ( ruleParameter )
            // InternalWebserviceDSL.g:5649:3: ruleParameter
            {
             before(grammarAccess.getReadOperationAccess().getParametersParameterParserRuleCall_6_2_0()); 
            pushFollow(FOLLOW_2);
            ruleParameter();

            state._fsp--;

             after(grammarAccess.getReadOperationAccess().getParametersParameterParserRuleCall_6_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__ParametersAssignment_6_2"


    // $ANTLR start "rule__ReadOperation__ParametersAssignment_6_3_1"
    // InternalWebserviceDSL.g:5658:1: rule__ReadOperation__ParametersAssignment_6_3_1 : ( ruleParameter ) ;
    public final void rule__ReadOperation__ParametersAssignment_6_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5662:1: ( ( ruleParameter ) )
            // InternalWebserviceDSL.g:5663:2: ( ruleParameter )
            {
            // InternalWebserviceDSL.g:5663:2: ( ruleParameter )
            // InternalWebserviceDSL.g:5664:3: ruleParameter
            {
             before(grammarAccess.getReadOperationAccess().getParametersParameterParserRuleCall_6_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleParameter();

            state._fsp--;

             after(grammarAccess.getReadOperationAccess().getParametersParameterParserRuleCall_6_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReadOperation__ParametersAssignment_6_3_1"


    // $ANTLR start "rule__CreateOperation__NameAssignment_1"
    // InternalWebserviceDSL.g:5673:1: rule__CreateOperation__NameAssignment_1 : ( ruleEString ) ;
    public final void rule__CreateOperation__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5677:1: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:5678:2: ( ruleEString )
            {
            // InternalWebserviceDSL.g:5678:2: ( ruleEString )
            // InternalWebserviceDSL.g:5679:3: ruleEString
            {
             before(grammarAccess.getCreateOperationAccess().getNameEStringParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getCreateOperationAccess().getNameEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__NameAssignment_1"


    // $ANTLR start "rule__CreateOperation__DescriptionAssignment_3_1"
    // InternalWebserviceDSL.g:5688:1: rule__CreateOperation__DescriptionAssignment_3_1 : ( ruleEString ) ;
    public final void rule__CreateOperation__DescriptionAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5692:1: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:5693:2: ( ruleEString )
            {
            // InternalWebserviceDSL.g:5693:2: ( ruleEString )
            // InternalWebserviceDSL.g:5694:3: ruleEString
            {
             before(grammarAccess.getCreateOperationAccess().getDescriptionEStringParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getCreateOperationAccess().getDescriptionEStringParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__DescriptionAssignment_3_1"


    // $ANTLR start "rule__CreateOperation__UrlSuffixAssignment_4_1"
    // InternalWebserviceDSL.g:5703:1: rule__CreateOperation__UrlSuffixAssignment_4_1 : ( ruleEString ) ;
    public final void rule__CreateOperation__UrlSuffixAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5707:1: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:5708:2: ( ruleEString )
            {
            // InternalWebserviceDSL.g:5708:2: ( ruleEString )
            // InternalWebserviceDSL.g:5709:3: ruleEString
            {
             before(grammarAccess.getCreateOperationAccess().getUrlSuffixEStringParserRuleCall_4_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getCreateOperationAccess().getUrlSuffixEStringParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__UrlSuffixAssignment_4_1"


    // $ANTLR start "rule__CreateOperation__ResponseAssignment_5_1"
    // InternalWebserviceDSL.g:5718:1: rule__CreateOperation__ResponseAssignment_5_1 : ( ( ruleEString ) ) ;
    public final void rule__CreateOperation__ResponseAssignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5722:1: ( ( ( ruleEString ) ) )
            // InternalWebserviceDSL.g:5723:2: ( ( ruleEString ) )
            {
            // InternalWebserviceDSL.g:5723:2: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:5724:3: ( ruleEString )
            {
             before(grammarAccess.getCreateOperationAccess().getResponseDataTypeCrossReference_5_1_0()); 
            // InternalWebserviceDSL.g:5725:3: ( ruleEString )
            // InternalWebserviceDSL.g:5726:4: ruleEString
            {
             before(grammarAccess.getCreateOperationAccess().getResponseDataTypeEStringParserRuleCall_5_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getCreateOperationAccess().getResponseDataTypeEStringParserRuleCall_5_1_0_1()); 

            }

             after(grammarAccess.getCreateOperationAccess().getResponseDataTypeCrossReference_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__ResponseAssignment_5_1"


    // $ANTLR start "rule__CreateOperation__ParametersAssignment_6_2"
    // InternalWebserviceDSL.g:5737:1: rule__CreateOperation__ParametersAssignment_6_2 : ( ruleParameter ) ;
    public final void rule__CreateOperation__ParametersAssignment_6_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5741:1: ( ( ruleParameter ) )
            // InternalWebserviceDSL.g:5742:2: ( ruleParameter )
            {
            // InternalWebserviceDSL.g:5742:2: ( ruleParameter )
            // InternalWebserviceDSL.g:5743:3: ruleParameter
            {
             before(grammarAccess.getCreateOperationAccess().getParametersParameterParserRuleCall_6_2_0()); 
            pushFollow(FOLLOW_2);
            ruleParameter();

            state._fsp--;

             after(grammarAccess.getCreateOperationAccess().getParametersParameterParserRuleCall_6_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__ParametersAssignment_6_2"


    // $ANTLR start "rule__CreateOperation__ParametersAssignment_6_3_1"
    // InternalWebserviceDSL.g:5752:1: rule__CreateOperation__ParametersAssignment_6_3_1 : ( ruleParameter ) ;
    public final void rule__CreateOperation__ParametersAssignment_6_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5756:1: ( ( ruleParameter ) )
            // InternalWebserviceDSL.g:5757:2: ( ruleParameter )
            {
            // InternalWebserviceDSL.g:5757:2: ( ruleParameter )
            // InternalWebserviceDSL.g:5758:3: ruleParameter
            {
             before(grammarAccess.getCreateOperationAccess().getParametersParameterParserRuleCall_6_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleParameter();

            state._fsp--;

             after(grammarAccess.getCreateOperationAccess().getParametersParameterParserRuleCall_6_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CreateOperation__ParametersAssignment_6_3_1"


    // $ANTLR start "rule__UpdateOperation__NameAssignment_1"
    // InternalWebserviceDSL.g:5767:1: rule__UpdateOperation__NameAssignment_1 : ( ruleEString ) ;
    public final void rule__UpdateOperation__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5771:1: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:5772:2: ( ruleEString )
            {
            // InternalWebserviceDSL.g:5772:2: ( ruleEString )
            // InternalWebserviceDSL.g:5773:3: ruleEString
            {
             before(grammarAccess.getUpdateOperationAccess().getNameEStringParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getUpdateOperationAccess().getNameEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__NameAssignment_1"


    // $ANTLR start "rule__UpdateOperation__DescriptionAssignment_3_1"
    // InternalWebserviceDSL.g:5782:1: rule__UpdateOperation__DescriptionAssignment_3_1 : ( ruleEString ) ;
    public final void rule__UpdateOperation__DescriptionAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5786:1: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:5787:2: ( ruleEString )
            {
            // InternalWebserviceDSL.g:5787:2: ( ruleEString )
            // InternalWebserviceDSL.g:5788:3: ruleEString
            {
             before(grammarAccess.getUpdateOperationAccess().getDescriptionEStringParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getUpdateOperationAccess().getDescriptionEStringParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__DescriptionAssignment_3_1"


    // $ANTLR start "rule__UpdateOperation__UrlSuffixAssignment_4_1"
    // InternalWebserviceDSL.g:5797:1: rule__UpdateOperation__UrlSuffixAssignment_4_1 : ( ruleEString ) ;
    public final void rule__UpdateOperation__UrlSuffixAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5801:1: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:5802:2: ( ruleEString )
            {
            // InternalWebserviceDSL.g:5802:2: ( ruleEString )
            // InternalWebserviceDSL.g:5803:3: ruleEString
            {
             before(grammarAccess.getUpdateOperationAccess().getUrlSuffixEStringParserRuleCall_4_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getUpdateOperationAccess().getUrlSuffixEStringParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__UrlSuffixAssignment_4_1"


    // $ANTLR start "rule__UpdateOperation__ResponseAssignment_5_1"
    // InternalWebserviceDSL.g:5812:1: rule__UpdateOperation__ResponseAssignment_5_1 : ( ( ruleEString ) ) ;
    public final void rule__UpdateOperation__ResponseAssignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5816:1: ( ( ( ruleEString ) ) )
            // InternalWebserviceDSL.g:5817:2: ( ( ruleEString ) )
            {
            // InternalWebserviceDSL.g:5817:2: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:5818:3: ( ruleEString )
            {
             before(grammarAccess.getUpdateOperationAccess().getResponseDataTypeCrossReference_5_1_0()); 
            // InternalWebserviceDSL.g:5819:3: ( ruleEString )
            // InternalWebserviceDSL.g:5820:4: ruleEString
            {
             before(grammarAccess.getUpdateOperationAccess().getResponseDataTypeEStringParserRuleCall_5_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getUpdateOperationAccess().getResponseDataTypeEStringParserRuleCall_5_1_0_1()); 

            }

             after(grammarAccess.getUpdateOperationAccess().getResponseDataTypeCrossReference_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__ResponseAssignment_5_1"


    // $ANTLR start "rule__UpdateOperation__ParametersAssignment_6_2"
    // InternalWebserviceDSL.g:5831:1: rule__UpdateOperation__ParametersAssignment_6_2 : ( ruleParameter ) ;
    public final void rule__UpdateOperation__ParametersAssignment_6_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5835:1: ( ( ruleParameter ) )
            // InternalWebserviceDSL.g:5836:2: ( ruleParameter )
            {
            // InternalWebserviceDSL.g:5836:2: ( ruleParameter )
            // InternalWebserviceDSL.g:5837:3: ruleParameter
            {
             before(grammarAccess.getUpdateOperationAccess().getParametersParameterParserRuleCall_6_2_0()); 
            pushFollow(FOLLOW_2);
            ruleParameter();

            state._fsp--;

             after(grammarAccess.getUpdateOperationAccess().getParametersParameterParserRuleCall_6_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__ParametersAssignment_6_2"


    // $ANTLR start "rule__UpdateOperation__ParametersAssignment_6_3_1"
    // InternalWebserviceDSL.g:5846:1: rule__UpdateOperation__ParametersAssignment_6_3_1 : ( ruleParameter ) ;
    public final void rule__UpdateOperation__ParametersAssignment_6_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5850:1: ( ( ruleParameter ) )
            // InternalWebserviceDSL.g:5851:2: ( ruleParameter )
            {
            // InternalWebserviceDSL.g:5851:2: ( ruleParameter )
            // InternalWebserviceDSL.g:5852:3: ruleParameter
            {
             before(grammarAccess.getUpdateOperationAccess().getParametersParameterParserRuleCall_6_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleParameter();

            state._fsp--;

             after(grammarAccess.getUpdateOperationAccess().getParametersParameterParserRuleCall_6_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UpdateOperation__ParametersAssignment_6_3_1"


    // $ANTLR start "rule__DeleteOperation__NameAssignment_1"
    // InternalWebserviceDSL.g:5861:1: rule__DeleteOperation__NameAssignment_1 : ( ruleEString ) ;
    public final void rule__DeleteOperation__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5865:1: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:5866:2: ( ruleEString )
            {
            // InternalWebserviceDSL.g:5866:2: ( ruleEString )
            // InternalWebserviceDSL.g:5867:3: ruleEString
            {
             before(grammarAccess.getDeleteOperationAccess().getNameEStringParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getDeleteOperationAccess().getNameEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__NameAssignment_1"


    // $ANTLR start "rule__DeleteOperation__DescriptionAssignment_3_1"
    // InternalWebserviceDSL.g:5876:1: rule__DeleteOperation__DescriptionAssignment_3_1 : ( ruleEString ) ;
    public final void rule__DeleteOperation__DescriptionAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5880:1: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:5881:2: ( ruleEString )
            {
            // InternalWebserviceDSL.g:5881:2: ( ruleEString )
            // InternalWebserviceDSL.g:5882:3: ruleEString
            {
             before(grammarAccess.getDeleteOperationAccess().getDescriptionEStringParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getDeleteOperationAccess().getDescriptionEStringParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__DescriptionAssignment_3_1"


    // $ANTLR start "rule__DeleteOperation__UrlSuffixAssignment_4_1"
    // InternalWebserviceDSL.g:5891:1: rule__DeleteOperation__UrlSuffixAssignment_4_1 : ( ruleEString ) ;
    public final void rule__DeleteOperation__UrlSuffixAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5895:1: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:5896:2: ( ruleEString )
            {
            // InternalWebserviceDSL.g:5896:2: ( ruleEString )
            // InternalWebserviceDSL.g:5897:3: ruleEString
            {
             before(grammarAccess.getDeleteOperationAccess().getUrlSuffixEStringParserRuleCall_4_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getDeleteOperationAccess().getUrlSuffixEStringParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__UrlSuffixAssignment_4_1"


    // $ANTLR start "rule__DeleteOperation__ResponseAssignment_5_1"
    // InternalWebserviceDSL.g:5906:1: rule__DeleteOperation__ResponseAssignment_5_1 : ( ( ruleEString ) ) ;
    public final void rule__DeleteOperation__ResponseAssignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5910:1: ( ( ( ruleEString ) ) )
            // InternalWebserviceDSL.g:5911:2: ( ( ruleEString ) )
            {
            // InternalWebserviceDSL.g:5911:2: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:5912:3: ( ruleEString )
            {
             before(grammarAccess.getDeleteOperationAccess().getResponseDataTypeCrossReference_5_1_0()); 
            // InternalWebserviceDSL.g:5913:3: ( ruleEString )
            // InternalWebserviceDSL.g:5914:4: ruleEString
            {
             before(grammarAccess.getDeleteOperationAccess().getResponseDataTypeEStringParserRuleCall_5_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getDeleteOperationAccess().getResponseDataTypeEStringParserRuleCall_5_1_0_1()); 

            }

             after(grammarAccess.getDeleteOperationAccess().getResponseDataTypeCrossReference_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__ResponseAssignment_5_1"


    // $ANTLR start "rule__DeleteOperation__ParametersAssignment_6_2"
    // InternalWebserviceDSL.g:5925:1: rule__DeleteOperation__ParametersAssignment_6_2 : ( ruleParameter ) ;
    public final void rule__DeleteOperation__ParametersAssignment_6_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5929:1: ( ( ruleParameter ) )
            // InternalWebserviceDSL.g:5930:2: ( ruleParameter )
            {
            // InternalWebserviceDSL.g:5930:2: ( ruleParameter )
            // InternalWebserviceDSL.g:5931:3: ruleParameter
            {
             before(grammarAccess.getDeleteOperationAccess().getParametersParameterParserRuleCall_6_2_0()); 
            pushFollow(FOLLOW_2);
            ruleParameter();

            state._fsp--;

             after(grammarAccess.getDeleteOperationAccess().getParametersParameterParserRuleCall_6_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__ParametersAssignment_6_2"


    // $ANTLR start "rule__DeleteOperation__ParametersAssignment_6_3_1"
    // InternalWebserviceDSL.g:5940:1: rule__DeleteOperation__ParametersAssignment_6_3_1 : ( ruleParameter ) ;
    public final void rule__DeleteOperation__ParametersAssignment_6_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5944:1: ( ( ruleParameter ) )
            // InternalWebserviceDSL.g:5945:2: ( ruleParameter )
            {
            // InternalWebserviceDSL.g:5945:2: ( ruleParameter )
            // InternalWebserviceDSL.g:5946:3: ruleParameter
            {
             before(grammarAccess.getDeleteOperationAccess().getParametersParameterParserRuleCall_6_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleParameter();

            state._fsp--;

             after(grammarAccess.getDeleteOperationAccess().getParametersParameterParserRuleCall_6_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeleteOperation__ParametersAssignment_6_3_1"


    // $ANTLR start "rule__OtherOperation__NameAssignment_1"
    // InternalWebserviceDSL.g:5955:1: rule__OtherOperation__NameAssignment_1 : ( ruleEString ) ;
    public final void rule__OtherOperation__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5959:1: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:5960:2: ( ruleEString )
            {
            // InternalWebserviceDSL.g:5960:2: ( ruleEString )
            // InternalWebserviceDSL.g:5961:3: ruleEString
            {
             before(grammarAccess.getOtherOperationAccess().getNameEStringParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getOtherOperationAccess().getNameEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__NameAssignment_1"


    // $ANTLR start "rule__OtherOperation__DescriptionAssignment_3_1"
    // InternalWebserviceDSL.g:5970:1: rule__OtherOperation__DescriptionAssignment_3_1 : ( ruleEString ) ;
    public final void rule__OtherOperation__DescriptionAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5974:1: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:5975:2: ( ruleEString )
            {
            // InternalWebserviceDSL.g:5975:2: ( ruleEString )
            // InternalWebserviceDSL.g:5976:3: ruleEString
            {
             before(grammarAccess.getOtherOperationAccess().getDescriptionEStringParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getOtherOperationAccess().getDescriptionEStringParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__DescriptionAssignment_3_1"


    // $ANTLR start "rule__OtherOperation__UrlSuffixAssignment_4_1"
    // InternalWebserviceDSL.g:5985:1: rule__OtherOperation__UrlSuffixAssignment_4_1 : ( ruleEString ) ;
    public final void rule__OtherOperation__UrlSuffixAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:5989:1: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:5990:2: ( ruleEString )
            {
            // InternalWebserviceDSL.g:5990:2: ( ruleEString )
            // InternalWebserviceDSL.g:5991:3: ruleEString
            {
             before(grammarAccess.getOtherOperationAccess().getUrlSuffixEStringParserRuleCall_4_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getOtherOperationAccess().getUrlSuffixEStringParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__UrlSuffixAssignment_4_1"


    // $ANTLR start "rule__OtherOperation__OperationAssignment_5_1"
    // InternalWebserviceDSL.g:6000:1: rule__OtherOperation__OperationAssignment_5_1 : ( ruleEString ) ;
    public final void rule__OtherOperation__OperationAssignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:6004:1: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:6005:2: ( ruleEString )
            {
            // InternalWebserviceDSL.g:6005:2: ( ruleEString )
            // InternalWebserviceDSL.g:6006:3: ruleEString
            {
             before(grammarAccess.getOtherOperationAccess().getOperationEStringParserRuleCall_5_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getOtherOperationAccess().getOperationEStringParserRuleCall_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__OperationAssignment_5_1"


    // $ANTLR start "rule__OtherOperation__ResponseAssignment_6_1"
    // InternalWebserviceDSL.g:6015:1: rule__OtherOperation__ResponseAssignment_6_1 : ( ( ruleEString ) ) ;
    public final void rule__OtherOperation__ResponseAssignment_6_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:6019:1: ( ( ( ruleEString ) ) )
            // InternalWebserviceDSL.g:6020:2: ( ( ruleEString ) )
            {
            // InternalWebserviceDSL.g:6020:2: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:6021:3: ( ruleEString )
            {
             before(grammarAccess.getOtherOperationAccess().getResponseDataTypeCrossReference_6_1_0()); 
            // InternalWebserviceDSL.g:6022:3: ( ruleEString )
            // InternalWebserviceDSL.g:6023:4: ruleEString
            {
             before(grammarAccess.getOtherOperationAccess().getResponseDataTypeEStringParserRuleCall_6_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getOtherOperationAccess().getResponseDataTypeEStringParserRuleCall_6_1_0_1()); 

            }

             after(grammarAccess.getOtherOperationAccess().getResponseDataTypeCrossReference_6_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__ResponseAssignment_6_1"


    // $ANTLR start "rule__OtherOperation__ParametersAssignment_7_2"
    // InternalWebserviceDSL.g:6034:1: rule__OtherOperation__ParametersAssignment_7_2 : ( ruleParameter ) ;
    public final void rule__OtherOperation__ParametersAssignment_7_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:6038:1: ( ( ruleParameter ) )
            // InternalWebserviceDSL.g:6039:2: ( ruleParameter )
            {
            // InternalWebserviceDSL.g:6039:2: ( ruleParameter )
            // InternalWebserviceDSL.g:6040:3: ruleParameter
            {
             before(grammarAccess.getOtherOperationAccess().getParametersParameterParserRuleCall_7_2_0()); 
            pushFollow(FOLLOW_2);
            ruleParameter();

            state._fsp--;

             after(grammarAccess.getOtherOperationAccess().getParametersParameterParserRuleCall_7_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__ParametersAssignment_7_2"


    // $ANTLR start "rule__OtherOperation__ParametersAssignment_7_3_1"
    // InternalWebserviceDSL.g:6049:1: rule__OtherOperation__ParametersAssignment_7_3_1 : ( ruleParameter ) ;
    public final void rule__OtherOperation__ParametersAssignment_7_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:6053:1: ( ( ruleParameter ) )
            // InternalWebserviceDSL.g:6054:2: ( ruleParameter )
            {
            // InternalWebserviceDSL.g:6054:2: ( ruleParameter )
            // InternalWebserviceDSL.g:6055:3: ruleParameter
            {
             before(grammarAccess.getOtherOperationAccess().getParametersParameterParserRuleCall_7_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleParameter();

            state._fsp--;

             after(grammarAccess.getOtherOperationAccess().getParametersParameterParserRuleCall_7_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OtherOperation__ParametersAssignment_7_3_1"


    // $ANTLR start "rule__ObjectDataType__RequiredAssignment_0"
    // InternalWebserviceDSL.g:6064:1: rule__ObjectDataType__RequiredAssignment_0 : ( ( 'required' ) ) ;
    public final void rule__ObjectDataType__RequiredAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:6068:1: ( ( ( 'required' ) ) )
            // InternalWebserviceDSL.g:6069:2: ( ( 'required' ) )
            {
            // InternalWebserviceDSL.g:6069:2: ( ( 'required' ) )
            // InternalWebserviceDSL.g:6070:3: ( 'required' )
            {
             before(grammarAccess.getObjectDataTypeAccess().getRequiredRequiredKeyword_0_0()); 
            // InternalWebserviceDSL.g:6071:3: ( 'required' )
            // InternalWebserviceDSL.g:6072:4: 'required'
            {
             before(grammarAccess.getObjectDataTypeAccess().getRequiredRequiredKeyword_0_0()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getObjectDataTypeAccess().getRequiredRequiredKeyword_0_0()); 

            }

             after(grammarAccess.getObjectDataTypeAccess().getRequiredRequiredKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectDataType__RequiredAssignment_0"


    // $ANTLR start "rule__ObjectDataType__NameAssignment_2"
    // InternalWebserviceDSL.g:6083:1: rule__ObjectDataType__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__ObjectDataType__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:6087:1: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:6088:2: ( ruleEString )
            {
            // InternalWebserviceDSL.g:6088:2: ( ruleEString )
            // InternalWebserviceDSL.g:6089:3: ruleEString
            {
             before(grammarAccess.getObjectDataTypeAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getObjectDataTypeAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectDataType__NameAssignment_2"


    // $ANTLR start "rule__ObjectDataType__DatatypesAssignment_6"
    // InternalWebserviceDSL.g:6098:1: rule__ObjectDataType__DatatypesAssignment_6 : ( ( ruleEString ) ) ;
    public final void rule__ObjectDataType__DatatypesAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:6102:1: ( ( ( ruleEString ) ) )
            // InternalWebserviceDSL.g:6103:2: ( ( ruleEString ) )
            {
            // InternalWebserviceDSL.g:6103:2: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:6104:3: ( ruleEString )
            {
             before(grammarAccess.getObjectDataTypeAccess().getDatatypesDataTypeCrossReference_6_0()); 
            // InternalWebserviceDSL.g:6105:3: ( ruleEString )
            // InternalWebserviceDSL.g:6106:4: ruleEString
            {
             before(grammarAccess.getObjectDataTypeAccess().getDatatypesDataTypeEStringParserRuleCall_6_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getObjectDataTypeAccess().getDatatypesDataTypeEStringParserRuleCall_6_0_1()); 

            }

             after(grammarAccess.getObjectDataTypeAccess().getDatatypesDataTypeCrossReference_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectDataType__DatatypesAssignment_6"


    // $ANTLR start "rule__ObjectDataType__DatatypesAssignment_7_1"
    // InternalWebserviceDSL.g:6117:1: rule__ObjectDataType__DatatypesAssignment_7_1 : ( ( ruleEString ) ) ;
    public final void rule__ObjectDataType__DatatypesAssignment_7_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:6121:1: ( ( ( ruleEString ) ) )
            // InternalWebserviceDSL.g:6122:2: ( ( ruleEString ) )
            {
            // InternalWebserviceDSL.g:6122:2: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:6123:3: ( ruleEString )
            {
             before(grammarAccess.getObjectDataTypeAccess().getDatatypesDataTypeCrossReference_7_1_0()); 
            // InternalWebserviceDSL.g:6124:3: ( ruleEString )
            // InternalWebserviceDSL.g:6125:4: ruleEString
            {
             before(grammarAccess.getObjectDataTypeAccess().getDatatypesDataTypeEStringParserRuleCall_7_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getObjectDataTypeAccess().getDatatypesDataTypeEStringParserRuleCall_7_1_0_1()); 

            }

             after(grammarAccess.getObjectDataTypeAccess().getDatatypesDataTypeCrossReference_7_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectDataType__DatatypesAssignment_7_1"


    // $ANTLR start "rule__CollectionDataType__RequiredAssignment_0"
    // InternalWebserviceDSL.g:6136:1: rule__CollectionDataType__RequiredAssignment_0 : ( ( 'required' ) ) ;
    public final void rule__CollectionDataType__RequiredAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:6140:1: ( ( ( 'required' ) ) )
            // InternalWebserviceDSL.g:6141:2: ( ( 'required' ) )
            {
            // InternalWebserviceDSL.g:6141:2: ( ( 'required' ) )
            // InternalWebserviceDSL.g:6142:3: ( 'required' )
            {
             before(grammarAccess.getCollectionDataTypeAccess().getRequiredRequiredKeyword_0_0()); 
            // InternalWebserviceDSL.g:6143:3: ( 'required' )
            // InternalWebserviceDSL.g:6144:4: 'required'
            {
             before(grammarAccess.getCollectionDataTypeAccess().getRequiredRequiredKeyword_0_0()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getCollectionDataTypeAccess().getRequiredRequiredKeyword_0_0()); 

            }

             after(grammarAccess.getCollectionDataTypeAccess().getRequiredRequiredKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionDataType__RequiredAssignment_0"


    // $ANTLR start "rule__CollectionDataType__NameAssignment_2"
    // InternalWebserviceDSL.g:6155:1: rule__CollectionDataType__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__CollectionDataType__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:6159:1: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:6160:2: ( ruleEString )
            {
            // InternalWebserviceDSL.g:6160:2: ( ruleEString )
            // InternalWebserviceDSL.g:6161:3: ruleEString
            {
             before(grammarAccess.getCollectionDataTypeAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getCollectionDataTypeAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionDataType__NameAssignment_2"


    // $ANTLR start "rule__CollectionDataType__ListItemAssignment_5"
    // InternalWebserviceDSL.g:6170:1: rule__CollectionDataType__ListItemAssignment_5 : ( ( ruleEString ) ) ;
    public final void rule__CollectionDataType__ListItemAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:6174:1: ( ( ( ruleEString ) ) )
            // InternalWebserviceDSL.g:6175:2: ( ( ruleEString ) )
            {
            // InternalWebserviceDSL.g:6175:2: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:6176:3: ( ruleEString )
            {
             before(grammarAccess.getCollectionDataTypeAccess().getListItemDataTypeCrossReference_5_0()); 
            // InternalWebserviceDSL.g:6177:3: ( ruleEString )
            // InternalWebserviceDSL.g:6178:4: ruleEString
            {
             before(grammarAccess.getCollectionDataTypeAccess().getListItemDataTypeEStringParserRuleCall_5_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getCollectionDataTypeAccess().getListItemDataTypeEStringParserRuleCall_5_0_1()); 

            }

             after(grammarAccess.getCollectionDataTypeAccess().getListItemDataTypeCrossReference_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CollectionDataType__ListItemAssignment_5"


    // $ANTLR start "rule__StringData__RequiredAssignment_1"
    // InternalWebserviceDSL.g:6189:1: rule__StringData__RequiredAssignment_1 : ( ( 'required' ) ) ;
    public final void rule__StringData__RequiredAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:6193:1: ( ( ( 'required' ) ) )
            // InternalWebserviceDSL.g:6194:2: ( ( 'required' ) )
            {
            // InternalWebserviceDSL.g:6194:2: ( ( 'required' ) )
            // InternalWebserviceDSL.g:6195:3: ( 'required' )
            {
             before(grammarAccess.getStringDataAccess().getRequiredRequiredKeyword_1_0()); 
            // InternalWebserviceDSL.g:6196:3: ( 'required' )
            // InternalWebserviceDSL.g:6197:4: 'required'
            {
             before(grammarAccess.getStringDataAccess().getRequiredRequiredKeyword_1_0()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getStringDataAccess().getRequiredRequiredKeyword_1_0()); 

            }

             after(grammarAccess.getStringDataAccess().getRequiredRequiredKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringData__RequiredAssignment_1"


    // $ANTLR start "rule__StringData__NameAssignment_3"
    // InternalWebserviceDSL.g:6208:1: rule__StringData__NameAssignment_3 : ( ruleEString ) ;
    public final void rule__StringData__NameAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:6212:1: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:6213:2: ( ruleEString )
            {
            // InternalWebserviceDSL.g:6213:2: ( ruleEString )
            // InternalWebserviceDSL.g:6214:3: ruleEString
            {
             before(grammarAccess.getStringDataAccess().getNameEStringParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getStringDataAccess().getNameEStringParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringData__NameAssignment_3"


    // $ANTLR start "rule__IntegerData__RequiredAssignment_1"
    // InternalWebserviceDSL.g:6223:1: rule__IntegerData__RequiredAssignment_1 : ( ( 'required' ) ) ;
    public final void rule__IntegerData__RequiredAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:6227:1: ( ( ( 'required' ) ) )
            // InternalWebserviceDSL.g:6228:2: ( ( 'required' ) )
            {
            // InternalWebserviceDSL.g:6228:2: ( ( 'required' ) )
            // InternalWebserviceDSL.g:6229:3: ( 'required' )
            {
             before(grammarAccess.getIntegerDataAccess().getRequiredRequiredKeyword_1_0()); 
            // InternalWebserviceDSL.g:6230:3: ( 'required' )
            // InternalWebserviceDSL.g:6231:4: 'required'
            {
             before(grammarAccess.getIntegerDataAccess().getRequiredRequiredKeyword_1_0()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getIntegerDataAccess().getRequiredRequiredKeyword_1_0()); 

            }

             after(grammarAccess.getIntegerDataAccess().getRequiredRequiredKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerData__RequiredAssignment_1"


    // $ANTLR start "rule__IntegerData__NameAssignment_3"
    // InternalWebserviceDSL.g:6242:1: rule__IntegerData__NameAssignment_3 : ( ruleEString ) ;
    public final void rule__IntegerData__NameAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:6246:1: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:6247:2: ( ruleEString )
            {
            // InternalWebserviceDSL.g:6247:2: ( ruleEString )
            // InternalWebserviceDSL.g:6248:3: ruleEString
            {
             before(grammarAccess.getIntegerDataAccess().getNameEStringParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getIntegerDataAccess().getNameEStringParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerData__NameAssignment_3"


    // $ANTLR start "rule__FloatData__RequiredAssignment_1"
    // InternalWebserviceDSL.g:6257:1: rule__FloatData__RequiredAssignment_1 : ( ( 'required' ) ) ;
    public final void rule__FloatData__RequiredAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:6261:1: ( ( ( 'required' ) ) )
            // InternalWebserviceDSL.g:6262:2: ( ( 'required' ) )
            {
            // InternalWebserviceDSL.g:6262:2: ( ( 'required' ) )
            // InternalWebserviceDSL.g:6263:3: ( 'required' )
            {
             before(grammarAccess.getFloatDataAccess().getRequiredRequiredKeyword_1_0()); 
            // InternalWebserviceDSL.g:6264:3: ( 'required' )
            // InternalWebserviceDSL.g:6265:4: 'required'
            {
             before(grammarAccess.getFloatDataAccess().getRequiredRequiredKeyword_1_0()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getFloatDataAccess().getRequiredRequiredKeyword_1_0()); 

            }

             after(grammarAccess.getFloatDataAccess().getRequiredRequiredKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FloatData__RequiredAssignment_1"


    // $ANTLR start "rule__FloatData__NameAssignment_3"
    // InternalWebserviceDSL.g:6276:1: rule__FloatData__NameAssignment_3 : ( ruleEString ) ;
    public final void rule__FloatData__NameAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:6280:1: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:6281:2: ( ruleEString )
            {
            // InternalWebserviceDSL.g:6281:2: ( ruleEString )
            // InternalWebserviceDSL.g:6282:3: ruleEString
            {
             before(grammarAccess.getFloatDataAccess().getNameEStringParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getFloatDataAccess().getNameEStringParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FloatData__NameAssignment_3"


    // $ANTLR start "rule__BooleanData__RequiredAssignment_1"
    // InternalWebserviceDSL.g:6291:1: rule__BooleanData__RequiredAssignment_1 : ( ( 'required' ) ) ;
    public final void rule__BooleanData__RequiredAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:6295:1: ( ( ( 'required' ) ) )
            // InternalWebserviceDSL.g:6296:2: ( ( 'required' ) )
            {
            // InternalWebserviceDSL.g:6296:2: ( ( 'required' ) )
            // InternalWebserviceDSL.g:6297:3: ( 'required' )
            {
             before(grammarAccess.getBooleanDataAccess().getRequiredRequiredKeyword_1_0()); 
            // InternalWebserviceDSL.g:6298:3: ( 'required' )
            // InternalWebserviceDSL.g:6299:4: 'required'
            {
             before(grammarAccess.getBooleanDataAccess().getRequiredRequiredKeyword_1_0()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getBooleanDataAccess().getRequiredRequiredKeyword_1_0()); 

            }

             after(grammarAccess.getBooleanDataAccess().getRequiredRequiredKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanData__RequiredAssignment_1"


    // $ANTLR start "rule__BooleanData__NameAssignment_3"
    // InternalWebserviceDSL.g:6310:1: rule__BooleanData__NameAssignment_3 : ( ruleEString ) ;
    public final void rule__BooleanData__NameAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:6314:1: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:6315:2: ( ruleEString )
            {
            // InternalWebserviceDSL.g:6315:2: ( ruleEString )
            // InternalWebserviceDSL.g:6316:3: ruleEString
            {
             before(grammarAccess.getBooleanDataAccess().getNameEStringParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getBooleanDataAccess().getNameEStringParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanData__NameAssignment_3"


    // $ANTLR start "rule__ByteData__RequiredAssignment_1"
    // InternalWebserviceDSL.g:6325:1: rule__ByteData__RequiredAssignment_1 : ( ( 'required' ) ) ;
    public final void rule__ByteData__RequiredAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:6329:1: ( ( ( 'required' ) ) )
            // InternalWebserviceDSL.g:6330:2: ( ( 'required' ) )
            {
            // InternalWebserviceDSL.g:6330:2: ( ( 'required' ) )
            // InternalWebserviceDSL.g:6331:3: ( 'required' )
            {
             before(grammarAccess.getByteDataAccess().getRequiredRequiredKeyword_1_0()); 
            // InternalWebserviceDSL.g:6332:3: ( 'required' )
            // InternalWebserviceDSL.g:6333:4: 'required'
            {
             before(grammarAccess.getByteDataAccess().getRequiredRequiredKeyword_1_0()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getByteDataAccess().getRequiredRequiredKeyword_1_0()); 

            }

             after(grammarAccess.getByteDataAccess().getRequiredRequiredKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ByteData__RequiredAssignment_1"


    // $ANTLR start "rule__ByteData__NameAssignment_3"
    // InternalWebserviceDSL.g:6344:1: rule__ByteData__NameAssignment_3 : ( ruleEString ) ;
    public final void rule__ByteData__NameAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalWebserviceDSL.g:6348:1: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:6349:2: ( ruleEString )
            {
            // InternalWebserviceDSL.g:6349:2: ( ruleEString )
            // InternalWebserviceDSL.g:6350:3: ruleEString
            {
             before(grammarAccess.getByteDataAccess().getNameEStringParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getByteDataAccess().getNameEStringParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ByteData__NameAssignment_3"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000022000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x000001FA40000000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000048000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x000000001E200000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000120000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000001C28000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000010000080000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000021C20000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000010040000000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000100040000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000010200000000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000010800000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000011000000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000012000000000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000014000000000L});

}