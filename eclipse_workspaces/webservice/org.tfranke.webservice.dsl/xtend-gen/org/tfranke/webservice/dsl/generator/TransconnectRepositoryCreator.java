package org.tfranke.webservice.dsl.generator;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.generator.IFileSystemAccess2;
import org.eclipse.xtext.xbase.lib.Exceptions;
import webservice.API;
import webservice.BooleanData;
import webservice.CollectionDataType;
import webservice.CreateOperation;
import webservice.DataType;
import webservice.DeleteOperation;
import webservice.FloatData;
import webservice.IntegerData;
import webservice.ObjectDataType;
import webservice.Operation;
import webservice.Parameter;
import webservice.ReadOperation;
import webservice.StringData;
import webservice.UpdateOperation;

/**
 * This class contains methods to create a Transconnect repository from a Webservice model.
 */
@SuppressWarnings("all")
public class TransconnectRepositoryCreator {
  /**
   * Creates a Transconnect repository for the given Webservice model.
   */
  public static void createRepository(final API api, final IFileSystemAccess2 fsa) {
    List<String> fileNames = new ArrayList<String>();
    String folderName = api.getName();
    EList<Operation> _operations = api.getOperations();
    for (final Operation operation : _operations) {
      {
        String _httpMethodFromOperation = TransconnectRepositoryCreator.getHttpMethodFromOperation(operation);
        String _plus = (_httpMethodFromOperation + " ");
        String _name = operation.getName();
        String _plus_1 = (_plus + _name);
        String fileName = (_plus_1 + ".xsl");
        fileNames.add(fileName);
        fsa.generateFile(((((("repositories/" + folderName) + "/resources/") + folderName) + "/") + fileName), TransconnectRepositoryCreator.createResourceFile(operation, api));
      }
    }
    fsa.generateFile((("repositories/" + folderName) + "/repository.xml"), TransconnectRepositoryCreator.createRepositoryXML(api, fileNames, folderName));
  }

  /**
   * Creates the contents of a resource file for the given Operation.
   */
  public static CharSequence createResourceFile(final Operation operation, final API api) {
    CharSequence _xblockexpression = null;
    {
      List<ObjectDataType> contentObjects = new ArrayList<ObjectDataType>();
      List<Parameter> queryParams = new ArrayList<Parameter>();
      EList<Parameter> _parameters = operation.getParameters();
      for (final Parameter param : _parameters) {
        {
          DataType datatype = param.getDatatype();
          if ((datatype instanceof ObjectDataType)) {
            contentObjects.add(((ObjectDataType)datatype));
          } else {
            queryParams.add(param);
          }
        }
      }
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("<xsl:stylesheet xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" version=\"1.0\">");
      _builder.newLine();
      _builder.append("   ");
      _builder.append("<xsl:template match=\"/\">");
      _builder.newLine();
      _builder.append("      ");
      _builder.append("<ROOT>");
      _builder.newLine();
      _builder.append("         ");
      _builder.append("<REQUEST CONTENT-TYPE=\"application/json\" EMBED=\"true\" NAME=\"");
      String _name = operation.getName();
      _builder.append(_name, "         ");
      _builder.append("\" OPERATION=\"");
      String _httpMethodFromOperation = TransconnectRepositoryCreator.getHttpMethodFromOperation(operation);
      _builder.append(_httpMethodFromOperation, "         ");
      _builder.append("\" URL=\"");
      String _urlSuffix = operation.getUrlSuffix();
      _builder.append(_urlSuffix, "         ");
      _builder.append("\">");
      _builder.newLineIfNotEmpty();
      _builder.append("            ");
      _builder.append("<PARAMS>");
      _builder.newLine();
      {
        for(final Parameter param_1 : queryParams) {
          {
            boolean _isRequired = param_1.isRequired();
            if (_isRequired) {
              _builder.append("            ");
              _builder.append("<!-- required -->");
              _builder.newLine();
            }
          }
          _builder.append("            ");
          _builder.append("   ");
          _builder.append("<PARAM NAME=\"");
          String _name_1 = param_1.getDatatype().getName();
          _builder.append(_name_1, "               ");
          _builder.append("\">");
          String _typeOfData = TransconnectRepositoryCreator.getTypeOfData(param_1.getDatatype());
          _builder.append(_typeOfData, "               ");
          _builder.append("</PARAM>");
          _builder.newLineIfNotEmpty();
          _builder.append("            ");
          _builder.append("    ");
          _builder.newLine();
        }
      }
      _builder.append("            ");
      _builder.append("</PARAMS>");
      _builder.newLine();
      {
        boolean _isEmpty = contentObjects.isEmpty();
        boolean _not = (!_isEmpty);
        if (_not) {
          _builder.append("            ");
          _builder.append("<CONTENT>");
          _builder.newLine();
          {
            boolean _hasElements = false;
            for(final ObjectDataType object : contentObjects) {
              if (!_hasElements) {
                _hasElements = true;
              } else {
                _builder.appendImmediate(",", "            ");
              }
              _builder.append("            ");
              String _resolveObjectDatatypeToJson = TransconnectRepositoryCreator.resolveObjectDatatypeToJson(object);
              _builder.append(_resolveObjectDatatypeToJson, "            ");
              _builder.newLineIfNotEmpty();
              _builder.append("            ");
              _builder.append("<!-- required content: ");
              {
                EList<DataType> _datatypes = object.getDatatypes();
                boolean _hasElements_1 = false;
                for(final DataType datatype : _datatypes) {
                  if (!_hasElements_1) {
                    _hasElements_1 = true;
                  } else {
                    _builder.appendImmediate(",", "            ");
                  }
                  {
                    boolean _isRequired_1 = datatype.isRequired();
                    if (_isRequired_1) {
                      String _name_2 = datatype.getName();
                      _builder.append(_name_2, "            ");
                      _builder.append(" ");
                    }
                  }
                }
              }
              _builder.append(" -->");
              _builder.newLineIfNotEmpty();
            }
          }
          _builder.append("            ");
          _builder.append("</CONTENT>");
          _builder.newLine();
          _builder.append("            ");
          _builder.newLine();
        }
      }
      _builder.append("         ");
      _builder.append("</REQUEST>");
      _builder.newLine();
      _builder.append("      ");
      _builder.append("</ROOT>");
      _builder.newLine();
      _builder.append("   ");
      _builder.append("</xsl:template>");
      _builder.newLine();
      _builder.append("</xsl:stylesheet>");
      _builder.newLine();
      _builder.newLine();
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }

  /**
   * Creates the contents of the repository.xml file for the given API.
   */
  public static CharSequence createRepositoryXML(final API api, final List<String> fileNames, final String folderName) {
    CharSequence _xblockexpression = null;
    {
      int id = 100;
      LocalDateTime dateTimeNow = LocalDateTime.now();
      DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
      String dateTimeNowFormatted = dateTimeNow.format(formatter);
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>");
      _builder.newLine();
      _builder.append("<REPOSITORY>");
      _builder.newLine();
      _builder.append("  ");
      _builder.append("<RESOURCES>");
      _builder.newLine();
      _builder.append("    ");
      _builder.append("<RESOURCE EMPTYFOLDER=\"true\" FILENAME=\"");
      _builder.append(folderName, "    ");
      _builder.append("\" ID=\"");
      _builder.append(id, "    ");
      _builder.append("\" IMPORT=\"true\" NAME=\"SwaggerPetstore\">");
      _builder.newLineIfNotEmpty();
      _builder.append("      ");
      _builder.append("<DESCRIPTION/>");
      _builder.newLine();
      _builder.append("    ");
      _builder.append("</RESOURCE>");
      _builder.newLine();
      {
        for(final String fileName : fileNames) {
          _builder.append("    ");
          _builder.append("<RESOURCE FILENAME=\"");
          _builder.append(folderName, "    ");
          _builder.append("/");
          _builder.append(fileName, "    ");
          _builder.append("\" ID=\"");
          int _id = id;
          int _plus = id = (_id + 1);
          _builder.append(_plus, "    ");
          _builder.append("\" IMPORT=\"true\" LASTMODIFIED=\"");
          _builder.append(dateTimeNowFormatted, "    ");
          _builder.append("\" NAME=\"");
          _builder.append(fileName, "    ");
          _builder.append("\" TYPE=\"XsltDocument\">");
          _builder.newLineIfNotEmpty();
          _builder.append("    ");
          _builder.append("  ");
          _builder.append("<DESCRIPTION/>");
          _builder.newLine();
          _builder.append("    ");
          _builder.append("</RESOURCE>");
          _builder.newLine();
        }
      }
      _builder.append("  ");
      _builder.append("</RESOURCES>");
      _builder.newLine();
      _builder.append("  ");
      _builder.newLine();
      _builder.append("  ");
      _builder.append("<ADAPTERTYPES>");
      _builder.newLine();
      _builder.append("    ");
      _builder.append("<ADAPTERTYPE FACTORY=\"com.sqlgmbh.tc.adapter.http.AdapterFactory\" ID=\"30\" IMPORT=\"false\"/>");
      _builder.newLine();
      _builder.append("  ");
      _builder.append("</ADAPTERTYPES>");
      _builder.newLine();
      _builder.append("  ");
      _builder.newLine();
      _builder.append("  ");
      _builder.append("<ADAPTERS>");
      _builder.newLine();
      _builder.append("    ");
      _builder.append("<ADAPTER FACTORY=\"com.sqlgmbh.tc.adapter.http.AdapterFactory\" ID=\"80\" IMPORT=\"true\" NAME=\"");
      String _name = api.getName();
      _builder.append(_name, "    ");
      _builder.append("\" OUTBOUND=\"true\" SUBTYPE=\"Default\" TYPE_ID=\"30\">");
      _builder.newLineIfNotEmpty();
      _builder.append("      ");
      _builder.append("<DESCRIPTION/>");
      _builder.newLine();
      _builder.append("      ");
      _builder.append("<PROPERTIES>");
      _builder.newLine();
      _builder.append("        ");
      _builder.append("<PROPERTY NAME=\"connection.maxIdle\">10</PROPERTY>");
      _builder.newLine();
      _builder.append("        ");
      _builder.append("<PROPERTY NAME=\"failed.request.content.length\">100</PROPERTY>");
      _builder.newLine();
      _builder.append("        ");
      _builder.append("<PROPERTY NAME=\"http.statuscode.check\">false</PROPERTY>");
      _builder.newLine();
      _builder.append("        ");
      _builder.append("<PROPERTY NAME=\"lib.log.verbose\">false</PROPERTY>");
      _builder.newLine();
      _builder.append("        ");
      _builder.append("<PROPERTY NAME=\"requestEncoding\">UTF-8</PROPERTY>");
      _builder.newLine();
      _builder.append("        ");
      _builder.append("<PROPERTY NAME=\"responseEncoding\">ISO-8859-1</PROPERTY>");
      _builder.newLine();
      _builder.append("        ");
      _builder.append("<PROPERTY NAME=\"trust.selfsigned\">false</PROPERTY>");
      _builder.newLine();
      _builder.append("        ");
      _builder.append("<PROPERTY NAME=\"url.outbound\">");
      String _url = api.getUrl();
      _builder.append(_url, "        ");
      _builder.append("</PROPERTY>");
      _builder.newLineIfNotEmpty();
      _builder.append("        ");
      _builder.append("<PROPERTY NAME=\"xml.invalidChars\">Error</PROPERTY>");
      _builder.newLine();
      _builder.append("      ");
      _builder.append("</PROPERTIES>");
      _builder.newLine();
      _builder.append("    ");
      _builder.append("</ADAPTER>");
      _builder.newLine();
      _builder.append("  ");
      _builder.append("</ADAPTERS>");
      _builder.newLine();
      _builder.append("</REPOSITORY>");
      _builder.newLine();
      _builder.newLine();
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }

  /**
   * Creates the JSON representation of an ObjectDataType from the API Model.
   */
  public static String resolveObjectDatatypeToJson(final ObjectDataType object) {
    try {
      ObjectMapper mapper = new ObjectMapper();
      Map<String, String> mapOfJsonRepresenation = new LinkedHashMap<String, String>();
      EList<DataType> _datatypes = object.getDatatypes();
      for (final DataType datatype : _datatypes) {
        mapOfJsonRepresenation.put(datatype.getName(), TransconnectRepositoryCreator.getTypeOfData(datatype));
      }
      return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(mapOfJsonRepresenation);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  /**
   * This method returns the associated HTTP-Method for an Operation object.
   */
  public static String getHttpMethodFromOperation(final Operation operation) {
    if ((operation instanceof ReadOperation)) {
      return "GET";
    }
    if ((operation instanceof CreateOperation)) {
      return "POST";
    }
    if ((operation instanceof UpdateOperation)) {
      return "PUT";
    }
    if ((operation instanceof DeleteOperation)) {
      return "DELETE";
    }
    return null;
  }

  /**
   * This methods returns type of the given data.
   */
  public static String getTypeOfData(final DataType datatype) {
    if ((datatype instanceof StringData)) {
      return "string";
    }
    if ((datatype instanceof IntegerData)) {
      return "integer";
    }
    if ((datatype instanceof BooleanData)) {
      return "boolean";
    }
    if ((datatype instanceof FloatData)) {
      return "float";
    }
    if ((datatype instanceof ObjectDataType)) {
      String _name = ((ObjectDataType)datatype).getName();
      return ("object: " + _name);
    }
    if ((datatype instanceof CollectionDataType)) {
      String _name_1 = ((CollectionDataType)datatype).getListItem().getName();
      return ("list of: " + _name_1);
    }
    return null;
  }
}
