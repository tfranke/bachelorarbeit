package org.tfranke.webservice.dsl.generator

import org.eclipse.xtext.generator.IFileSystemAccess2
import webservice.API
import webservice.Operation	
import webservice.ReadOperation
import java.util.Map
import java.util.List
import webservice.Parameter
import webservice.CreateOperation
import webservice.UpdateOperation
import webservice.DeleteOperation
import webservice.DataType
import webservice.StringData
import webservice.IntegerData
import webservice.BooleanData
import webservice.FloatData
import webservice.CollectionDataType
import webservice.ObjectDataType
import com.fasterxml.jackson.databind.ObjectMapper
import java.util.ArrayList
import java.util.LinkedHashMap
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

/**
 * This class contains methods to create a Transconnect repository from a Webservice model.
 */
class TransconnectRepositoryCreator {

	/**
	 * Creates a Transconnect repository for the given Webservice model.
	 */
	def static createRepository(API api, IFileSystemAccess2 fsa) {
		var List<String> fileNames = new ArrayList()
		var folderName = api.name
		//create resource file for each operation
		for (Operation operation : api.operations) { 
			var fileName = getHttpMethodFromOperation(operation) + " " + operation.name + ".xsl";
			fileNames.add(fileName)
			fsa.generateFile( "repositories/" + folderName + "/resources/" + folderName + "/" + fileName , createResourceFile(operation, api));
		}
		//create repositoryXML file
		fsa.generateFile( "repositories/" + folderName + "/repository.xml", createRepositoryXML(api, fileNames, folderName));
	}
	
	/**
	 * Creates the contents of a resource file for the given Operation.
	 */
	def static createResourceFile(Operation operation, API api) {
		
		var List<ObjectDataType> contentObjects = new ArrayList();
		var List<Parameter> queryParams = new ArrayList();
		
		for (Parameter param : operation.parameters) {
			var DataType datatype = param.datatype
			if (datatype instanceof ObjectDataType) {
				contentObjects.add(datatype)
			} else {
				queryParams.add(param)
			}
		}
		
		'''
		<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
		   <xsl:template match="/">
		      <ROOT>
		         <REQUEST CONTENT-TYPE="application/json" EMBED="true" NAME="«operation.name»" OPERATION="«getHttpMethodFromOperation(operation)»" URL="«operation.urlSuffix»">
		            <PARAMS>
		            «FOR Parameter param : queryParams»
		            «IF param.required»
		               <!-- required -->
		            «ENDIF»
		               <PARAM NAME="«param.datatype.name»">«getTypeOfData(param.datatype)»</PARAM>
		                
		            «ENDFOR»
		            </PARAMS>
		            «IF !contentObjects.empty»
		            <CONTENT>
		            «FOR ObjectDataType object : contentObjects SEPARATOR ","»
		            «resolveObjectDatatypeToJson(object)»
		            <!-- required content: «FOR DataType datatype : object.datatypes SEPARATOR ","»«IF datatype.required»«datatype.name» «ENDIF»«ENDFOR» -->
		            «ENDFOR»
		            </CONTENT>
		            
		            «ENDIF»
		         </REQUEST>
		      </ROOT>
		   </xsl:template>
		</xsl:stylesheet>
		
		'''
	}
	
	/**
	 * Creates the contents of the repository.xml file for the given API.
	 */
	def static createRepositoryXML(API api, List<String> fileNames, String folderName) {
		
		var int id = 100
		var LocalDateTime dateTimeNow = LocalDateTime.now()
		var DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss")
		var String dateTimeNowFormatted = dateTimeNow.format(formatter)

		'''
		<?xml version="1.0" encoding="UTF-8" standalone="no"?>
		<REPOSITORY>
		  <RESOURCES>
		    <RESOURCE EMPTYFOLDER="true" FILENAME="«folderName»" ID="«id»" IMPORT="true" NAME="«folderName»">
		      <DESCRIPTION/>
		    </RESOURCE>
		    «FOR String fileName : fileNames»
		    <RESOURCE FILENAME="«folderName»/«fileName»" ID="«id += 1»" IMPORT="true" LASTMODIFIED="«dateTimeNowFormatted»" NAME="«fileName»" TYPE="XsltDocument">
		      <DESCRIPTION/>
		    </RESOURCE>
		    «ENDFOR»
		  </RESOURCES>
		  
		  <ADAPTERTYPES>
		    <ADAPTERTYPE FACTORY="com.sqlgmbh.tc.adapter.http.AdapterFactory" ID="30" IMPORT="false"/>
		  </ADAPTERTYPES>
		  
		  <ADAPTERS>
		    <ADAPTER FACTORY="com.sqlgmbh.tc.adapter.http.AdapterFactory" ID="80" IMPORT="true" NAME="«api.name»" OUTBOUND="true" SUBTYPE="Default" TYPE_ID="30">
		      <DESCRIPTION/>
		      <PROPERTIES>
		        <PROPERTY NAME="connection.maxIdle">10</PROPERTY>
		        <PROPERTY NAME="failed.request.content.length">100</PROPERTY>
		        <PROPERTY NAME="http.statuscode.check">false</PROPERTY>
		        <PROPERTY NAME="lib.log.verbose">false</PROPERTY>
		        <PROPERTY NAME="requestEncoding">UTF-8</PROPERTY>
		        <PROPERTY NAME="responseEncoding">ISO-8859-1</PROPERTY>
		        <PROPERTY NAME="trust.selfsigned">false</PROPERTY>
		        <PROPERTY NAME="url.outbound">«api.url»</PROPERTY>
		        <PROPERTY NAME="xml.invalidChars">Error</PROPERTY>
		      </PROPERTIES>
		    </ADAPTER>
		  </ADAPTERS>
		</REPOSITORY>
		
		'''
	}
	
	/**
	 * Creates the JSON representation of an ObjectDataType from the API Model.
	 */
	def static String resolveObjectDatatypeToJson(ObjectDataType object) {
		var ObjectMapper mapper = new ObjectMapper();
		var Map<String, String> mapOfJsonRepresenation = new LinkedHashMap();
		
		for (DataType datatype : object.datatypes) {
			mapOfJsonRepresenation.put(datatype.name , getTypeOfData(datatype))
		}
		
		//return json
		return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(mapOfJsonRepresenation)
		
	}
		
	
	/**
	 * This method returns the associated HTTP-Method for an Operation object.
	 */
	def static String getHttpMethodFromOperation(Operation operation){
		if(operation instanceof ReadOperation) {
			return "GET"
		}
		if(operation instanceof CreateOperation) {
			return "POST"
		}
		if(operation instanceof UpdateOperation) {
			return "PUT"
		}
		if(operation instanceof DeleteOperation) {
			return "DELETE"
		}
	}
	
	/**
	 * This methods returns type of the given data.
	 */
	def static String getTypeOfData(DataType datatype) {
		if (datatype instanceof StringData) {
			return "string"
		}
		if (datatype instanceof IntegerData) {
			return "integer"
		}
		if (datatype instanceof BooleanData) {
			return "boolean"
		}
		if (datatype instanceof FloatData) {
			return "float"
		}
		if (datatype instanceof ObjectDataType) {
			return "object: " + datatype.name
		}
		if (datatype instanceof CollectionDataType) {
			return "list of: " + datatype.listItem.name
		}
	}
			
}
	


	
