/*
 * generated by Xtext 2.35.0
 */
package org.tfranke.webservice.dsl.formatting2

import com.google.inject.Inject
import org.eclipse.xtext.formatting2.AbstractFormatter2
import org.eclipse.xtext.formatting2.IFormattableDocument
import org.tfranke.webservice.dsl.services.WebserviceDSLGrammarAccess
import webservice.API

class WebserviceDSLFormatter extends AbstractFormatter2 {
	
	@Inject extension WebserviceDSLGrammarAccess

	def dispatch void format(API aPI, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
		for (operation : aPI.operations) {
			operation.format
		}
		for (dataType : aPI.datatypes) {
			dataType.format
		}
	}
	
	// TODO: implement for ReadOperation, CreateOperation, UpdateOperation, DeleteOperation, otherOperation
}
