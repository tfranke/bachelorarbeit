package org.tfranke.webservice.dsl.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.tfranke.webservice.dsl.services.WebserviceDSLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalWebserviceDSLParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'API'", "'{'", "'description'", "'url'", "'datatypes'", "','", "'}'", "'operations'", "'required'", "'Parameter'", "'datatype'", "'ReadOperation'", "'urlSuffix'", "'response'", "'parameters'", "'CreateOperation'", "'UpdateOperation'", "'DeleteOperation'", "'otherOperation'", "'operation'", "'ObjectDataType'", "'('", "')'", "'CollectionDataType'", "'listItem'", "'StringData'", "'IntegerData'", "'FloatData'", "'BooleanData'", "'ByteData'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__37=37;
    public static final int T__16=16;
    public static final int T__38=38;
    public static final int T__17=17;
    public static final int T__39=39;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__40=40;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalWebserviceDSLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalWebserviceDSLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalWebserviceDSLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalWebserviceDSL.g"; }



     	private WebserviceDSLGrammarAccess grammarAccess;

        public InternalWebserviceDSLParser(TokenStream input, WebserviceDSLGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "API";
       	}

       	@Override
       	protected WebserviceDSLGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleAPI"
    // InternalWebserviceDSL.g:64:1: entryRuleAPI returns [EObject current=null] : iv_ruleAPI= ruleAPI EOF ;
    public final EObject entryRuleAPI() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAPI = null;


        try {
            // InternalWebserviceDSL.g:64:44: (iv_ruleAPI= ruleAPI EOF )
            // InternalWebserviceDSL.g:65:2: iv_ruleAPI= ruleAPI EOF
            {
             newCompositeNode(grammarAccess.getAPIRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAPI=ruleAPI();

            state._fsp--;

             current =iv_ruleAPI; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAPI"


    // $ANTLR start "ruleAPI"
    // InternalWebserviceDSL.g:71:1: ruleAPI returns [EObject current=null] : (otherlv_0= 'API' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? otherlv_5= 'url' ( (lv_url_6_0= ruleEString ) ) otherlv_7= 'datatypes' otherlv_8= '{' ( (lv_datatypes_9_0= ruleDataType ) ) (otherlv_10= ',' ( (lv_datatypes_11_0= ruleDataType ) ) )* otherlv_12= '}' otherlv_13= 'operations' otherlv_14= '{' ( (lv_operations_15_0= ruleOperation ) ) (otherlv_16= ',' ( (lv_operations_17_0= ruleOperation ) ) )* otherlv_18= '}' otherlv_19= '}' ) ;
    public final EObject ruleAPI() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        Token otherlv_18=null;
        Token otherlv_19=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        AntlrDatatypeRuleToken lv_description_4_0 = null;

        AntlrDatatypeRuleToken lv_url_6_0 = null;

        EObject lv_datatypes_9_0 = null;

        EObject lv_datatypes_11_0 = null;

        EObject lv_operations_15_0 = null;

        EObject lv_operations_17_0 = null;



        	enterRule();

        try {
            // InternalWebserviceDSL.g:77:2: ( (otherlv_0= 'API' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? otherlv_5= 'url' ( (lv_url_6_0= ruleEString ) ) otherlv_7= 'datatypes' otherlv_8= '{' ( (lv_datatypes_9_0= ruleDataType ) ) (otherlv_10= ',' ( (lv_datatypes_11_0= ruleDataType ) ) )* otherlv_12= '}' otherlv_13= 'operations' otherlv_14= '{' ( (lv_operations_15_0= ruleOperation ) ) (otherlv_16= ',' ( (lv_operations_17_0= ruleOperation ) ) )* otherlv_18= '}' otherlv_19= '}' ) )
            // InternalWebserviceDSL.g:78:2: (otherlv_0= 'API' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? otherlv_5= 'url' ( (lv_url_6_0= ruleEString ) ) otherlv_7= 'datatypes' otherlv_8= '{' ( (lv_datatypes_9_0= ruleDataType ) ) (otherlv_10= ',' ( (lv_datatypes_11_0= ruleDataType ) ) )* otherlv_12= '}' otherlv_13= 'operations' otherlv_14= '{' ( (lv_operations_15_0= ruleOperation ) ) (otherlv_16= ',' ( (lv_operations_17_0= ruleOperation ) ) )* otherlv_18= '}' otherlv_19= '}' )
            {
            // InternalWebserviceDSL.g:78:2: (otherlv_0= 'API' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? otherlv_5= 'url' ( (lv_url_6_0= ruleEString ) ) otherlv_7= 'datatypes' otherlv_8= '{' ( (lv_datatypes_9_0= ruleDataType ) ) (otherlv_10= ',' ( (lv_datatypes_11_0= ruleDataType ) ) )* otherlv_12= '}' otherlv_13= 'operations' otherlv_14= '{' ( (lv_operations_15_0= ruleOperation ) ) (otherlv_16= ',' ( (lv_operations_17_0= ruleOperation ) ) )* otherlv_18= '}' otherlv_19= '}' )
            // InternalWebserviceDSL.g:79:3: otherlv_0= 'API' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? otherlv_5= 'url' ( (lv_url_6_0= ruleEString ) ) otherlv_7= 'datatypes' otherlv_8= '{' ( (lv_datatypes_9_0= ruleDataType ) ) (otherlv_10= ',' ( (lv_datatypes_11_0= ruleDataType ) ) )* otherlv_12= '}' otherlv_13= 'operations' otherlv_14= '{' ( (lv_operations_15_0= ruleOperation ) ) (otherlv_16= ',' ( (lv_operations_17_0= ruleOperation ) ) )* otherlv_18= '}' otherlv_19= '}'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getAPIAccess().getAPIKeyword_0());
            		
            // InternalWebserviceDSL.g:83:3: ( (lv_name_1_0= ruleEString ) )
            // InternalWebserviceDSL.g:84:4: (lv_name_1_0= ruleEString )
            {
            // InternalWebserviceDSL.g:84:4: (lv_name_1_0= ruleEString )
            // InternalWebserviceDSL.g:85:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getAPIAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAPIRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.tfranke.webservice.dsl.WebserviceDSL.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_5); 

            			newLeafNode(otherlv_2, grammarAccess.getAPIAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalWebserviceDSL.g:106:3: (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==13) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalWebserviceDSL.g:107:4: otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,13,FOLLOW_3); 

                    				newLeafNode(otherlv_3, grammarAccess.getAPIAccess().getDescriptionKeyword_3_0());
                    			
                    // InternalWebserviceDSL.g:111:4: ( (lv_description_4_0= ruleEString ) )
                    // InternalWebserviceDSL.g:112:5: (lv_description_4_0= ruleEString )
                    {
                    // InternalWebserviceDSL.g:112:5: (lv_description_4_0= ruleEString )
                    // InternalWebserviceDSL.g:113:6: lv_description_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getAPIAccess().getDescriptionEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_description_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAPIRule());
                    						}
                    						set(
                    							current,
                    							"description",
                    							lv_description_4_0,
                    							"org.tfranke.webservice.dsl.WebserviceDSL.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_5=(Token)match(input,14,FOLLOW_3); 

            			newLeafNode(otherlv_5, grammarAccess.getAPIAccess().getUrlKeyword_4());
            		
            // InternalWebserviceDSL.g:135:3: ( (lv_url_6_0= ruleEString ) )
            // InternalWebserviceDSL.g:136:4: (lv_url_6_0= ruleEString )
            {
            // InternalWebserviceDSL.g:136:4: (lv_url_6_0= ruleEString )
            // InternalWebserviceDSL.g:137:5: lv_url_6_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getAPIAccess().getUrlEStringParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_7);
            lv_url_6_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAPIRule());
            					}
            					set(
            						current,
            						"url",
            						lv_url_6_0,
            						"org.tfranke.webservice.dsl.WebserviceDSL.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_7=(Token)match(input,15,FOLLOW_4); 

            			newLeafNode(otherlv_7, grammarAccess.getAPIAccess().getDatatypesKeyword_6());
            		
            otherlv_8=(Token)match(input,12,FOLLOW_8); 

            			newLeafNode(otherlv_8, grammarAccess.getAPIAccess().getLeftCurlyBracketKeyword_7());
            		
            // InternalWebserviceDSL.g:162:3: ( (lv_datatypes_9_0= ruleDataType ) )
            // InternalWebserviceDSL.g:163:4: (lv_datatypes_9_0= ruleDataType )
            {
            // InternalWebserviceDSL.g:163:4: (lv_datatypes_9_0= ruleDataType )
            // InternalWebserviceDSL.g:164:5: lv_datatypes_9_0= ruleDataType
            {

            					newCompositeNode(grammarAccess.getAPIAccess().getDatatypesDataTypeParserRuleCall_8_0());
            				
            pushFollow(FOLLOW_9);
            lv_datatypes_9_0=ruleDataType();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAPIRule());
            					}
            					add(
            						current,
            						"datatypes",
            						lv_datatypes_9_0,
            						"org.tfranke.webservice.dsl.WebserviceDSL.DataType");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalWebserviceDSL.g:181:3: (otherlv_10= ',' ( (lv_datatypes_11_0= ruleDataType ) ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==16) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalWebserviceDSL.g:182:4: otherlv_10= ',' ( (lv_datatypes_11_0= ruleDataType ) )
            	    {
            	    otherlv_10=(Token)match(input,16,FOLLOW_8); 

            	    				newLeafNode(otherlv_10, grammarAccess.getAPIAccess().getCommaKeyword_9_0());
            	    			
            	    // InternalWebserviceDSL.g:186:4: ( (lv_datatypes_11_0= ruleDataType ) )
            	    // InternalWebserviceDSL.g:187:5: (lv_datatypes_11_0= ruleDataType )
            	    {
            	    // InternalWebserviceDSL.g:187:5: (lv_datatypes_11_0= ruleDataType )
            	    // InternalWebserviceDSL.g:188:6: lv_datatypes_11_0= ruleDataType
            	    {

            	    						newCompositeNode(grammarAccess.getAPIAccess().getDatatypesDataTypeParserRuleCall_9_1_0());
            	    					
            	    pushFollow(FOLLOW_9);
            	    lv_datatypes_11_0=ruleDataType();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAPIRule());
            	    						}
            	    						add(
            	    							current,
            	    							"datatypes",
            	    							lv_datatypes_11_0,
            	    							"org.tfranke.webservice.dsl.WebserviceDSL.DataType");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            otherlv_12=(Token)match(input,17,FOLLOW_10); 

            			newLeafNode(otherlv_12, grammarAccess.getAPIAccess().getRightCurlyBracketKeyword_10());
            		
            otherlv_13=(Token)match(input,18,FOLLOW_4); 

            			newLeafNode(otherlv_13, grammarAccess.getAPIAccess().getOperationsKeyword_11());
            		
            otherlv_14=(Token)match(input,12,FOLLOW_11); 

            			newLeafNode(otherlv_14, grammarAccess.getAPIAccess().getLeftCurlyBracketKeyword_12());
            		
            // InternalWebserviceDSL.g:218:3: ( (lv_operations_15_0= ruleOperation ) )
            // InternalWebserviceDSL.g:219:4: (lv_operations_15_0= ruleOperation )
            {
            // InternalWebserviceDSL.g:219:4: (lv_operations_15_0= ruleOperation )
            // InternalWebserviceDSL.g:220:5: lv_operations_15_0= ruleOperation
            {

            					newCompositeNode(grammarAccess.getAPIAccess().getOperationsOperationParserRuleCall_13_0());
            				
            pushFollow(FOLLOW_9);
            lv_operations_15_0=ruleOperation();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAPIRule());
            					}
            					add(
            						current,
            						"operations",
            						lv_operations_15_0,
            						"org.tfranke.webservice.dsl.WebserviceDSL.Operation");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalWebserviceDSL.g:237:3: (otherlv_16= ',' ( (lv_operations_17_0= ruleOperation ) ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==16) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalWebserviceDSL.g:238:4: otherlv_16= ',' ( (lv_operations_17_0= ruleOperation ) )
            	    {
            	    otherlv_16=(Token)match(input,16,FOLLOW_11); 

            	    				newLeafNode(otherlv_16, grammarAccess.getAPIAccess().getCommaKeyword_14_0());
            	    			
            	    // InternalWebserviceDSL.g:242:4: ( (lv_operations_17_0= ruleOperation ) )
            	    // InternalWebserviceDSL.g:243:5: (lv_operations_17_0= ruleOperation )
            	    {
            	    // InternalWebserviceDSL.g:243:5: (lv_operations_17_0= ruleOperation )
            	    // InternalWebserviceDSL.g:244:6: lv_operations_17_0= ruleOperation
            	    {

            	    						newCompositeNode(grammarAccess.getAPIAccess().getOperationsOperationParserRuleCall_14_1_0());
            	    					
            	    pushFollow(FOLLOW_9);
            	    lv_operations_17_0=ruleOperation();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAPIRule());
            	    						}
            	    						add(
            	    							current,
            	    							"operations",
            	    							lv_operations_17_0,
            	    							"org.tfranke.webservice.dsl.WebserviceDSL.Operation");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            otherlv_18=(Token)match(input,17,FOLLOW_12); 

            			newLeafNode(otherlv_18, grammarAccess.getAPIAccess().getRightCurlyBracketKeyword_15());
            		
            otherlv_19=(Token)match(input,17,FOLLOW_2); 

            			newLeafNode(otherlv_19, grammarAccess.getAPIAccess().getRightCurlyBracketKeyword_16());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAPI"


    // $ANTLR start "entryRuleDataType"
    // InternalWebserviceDSL.g:274:1: entryRuleDataType returns [EObject current=null] : iv_ruleDataType= ruleDataType EOF ;
    public final EObject entryRuleDataType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDataType = null;


        try {
            // InternalWebserviceDSL.g:274:49: (iv_ruleDataType= ruleDataType EOF )
            // InternalWebserviceDSL.g:275:2: iv_ruleDataType= ruleDataType EOF
            {
             newCompositeNode(grammarAccess.getDataTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDataType=ruleDataType();

            state._fsp--;

             current =iv_ruleDataType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDataType"


    // $ANTLR start "ruleDataType"
    // InternalWebserviceDSL.g:281:1: ruleDataType returns [EObject current=null] : (this_ObjectDataType_0= ruleObjectDataType | this_CollectionDataType_1= ruleCollectionDataType | this_StringData_2= ruleStringData | this_IntegerData_3= ruleIntegerData | this_FloatData_4= ruleFloatData | this_BooleanData_5= ruleBooleanData | this_ByteData_6= ruleByteData ) ;
    public final EObject ruleDataType() throws RecognitionException {
        EObject current = null;

        EObject this_ObjectDataType_0 = null;

        EObject this_CollectionDataType_1 = null;

        EObject this_StringData_2 = null;

        EObject this_IntegerData_3 = null;

        EObject this_FloatData_4 = null;

        EObject this_BooleanData_5 = null;

        EObject this_ByteData_6 = null;



        	enterRule();

        try {
            // InternalWebserviceDSL.g:287:2: ( (this_ObjectDataType_0= ruleObjectDataType | this_CollectionDataType_1= ruleCollectionDataType | this_StringData_2= ruleStringData | this_IntegerData_3= ruleIntegerData | this_FloatData_4= ruleFloatData | this_BooleanData_5= ruleBooleanData | this_ByteData_6= ruleByteData ) )
            // InternalWebserviceDSL.g:288:2: (this_ObjectDataType_0= ruleObjectDataType | this_CollectionDataType_1= ruleCollectionDataType | this_StringData_2= ruleStringData | this_IntegerData_3= ruleIntegerData | this_FloatData_4= ruleFloatData | this_BooleanData_5= ruleBooleanData | this_ByteData_6= ruleByteData )
            {
            // InternalWebserviceDSL.g:288:2: (this_ObjectDataType_0= ruleObjectDataType | this_CollectionDataType_1= ruleCollectionDataType | this_StringData_2= ruleStringData | this_IntegerData_3= ruleIntegerData | this_FloatData_4= ruleFloatData | this_BooleanData_5= ruleBooleanData | this_ByteData_6= ruleByteData )
            int alt4=7;
            switch ( input.LA(1) ) {
            case 19:
                {
                switch ( input.LA(2) ) {
                case 39:
                    {
                    alt4=6;
                    }
                    break;
                case 38:
                    {
                    alt4=5;
                    }
                    break;
                case 34:
                    {
                    alt4=2;
                    }
                    break;
                case 37:
                    {
                    alt4=4;
                    }
                    break;
                case 31:
                    {
                    alt4=1;
                    }
                    break;
                case 36:
                    {
                    alt4=3;
                    }
                    break;
                case 40:
                    {
                    alt4=7;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 4, 1, input);

                    throw nvae;
                }

                }
                break;
            case 31:
                {
                alt4=1;
                }
                break;
            case 34:
                {
                alt4=2;
                }
                break;
            case 36:
                {
                alt4=3;
                }
                break;
            case 37:
                {
                alt4=4;
                }
                break;
            case 38:
                {
                alt4=5;
                }
                break;
            case 39:
                {
                alt4=6;
                }
                break;
            case 40:
                {
                alt4=7;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // InternalWebserviceDSL.g:289:3: this_ObjectDataType_0= ruleObjectDataType
                    {

                    			newCompositeNode(grammarAccess.getDataTypeAccess().getObjectDataTypeParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_ObjectDataType_0=ruleObjectDataType();

                    state._fsp--;


                    			current = this_ObjectDataType_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalWebserviceDSL.g:298:3: this_CollectionDataType_1= ruleCollectionDataType
                    {

                    			newCompositeNode(grammarAccess.getDataTypeAccess().getCollectionDataTypeParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_CollectionDataType_1=ruleCollectionDataType();

                    state._fsp--;


                    			current = this_CollectionDataType_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalWebserviceDSL.g:307:3: this_StringData_2= ruleStringData
                    {

                    			newCompositeNode(grammarAccess.getDataTypeAccess().getStringDataParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_StringData_2=ruleStringData();

                    state._fsp--;


                    			current = this_StringData_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalWebserviceDSL.g:316:3: this_IntegerData_3= ruleIntegerData
                    {

                    			newCompositeNode(grammarAccess.getDataTypeAccess().getIntegerDataParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_IntegerData_3=ruleIntegerData();

                    state._fsp--;


                    			current = this_IntegerData_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 5 :
                    // InternalWebserviceDSL.g:325:3: this_FloatData_4= ruleFloatData
                    {

                    			newCompositeNode(grammarAccess.getDataTypeAccess().getFloatDataParserRuleCall_4());
                    		
                    pushFollow(FOLLOW_2);
                    this_FloatData_4=ruleFloatData();

                    state._fsp--;


                    			current = this_FloatData_4;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 6 :
                    // InternalWebserviceDSL.g:334:3: this_BooleanData_5= ruleBooleanData
                    {

                    			newCompositeNode(grammarAccess.getDataTypeAccess().getBooleanDataParserRuleCall_5());
                    		
                    pushFollow(FOLLOW_2);
                    this_BooleanData_5=ruleBooleanData();

                    state._fsp--;


                    			current = this_BooleanData_5;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 7 :
                    // InternalWebserviceDSL.g:343:3: this_ByteData_6= ruleByteData
                    {

                    			newCompositeNode(grammarAccess.getDataTypeAccess().getByteDataParserRuleCall_6());
                    		
                    pushFollow(FOLLOW_2);
                    this_ByteData_6=ruleByteData();

                    state._fsp--;


                    			current = this_ByteData_6;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDataType"


    // $ANTLR start "entryRuleOperation"
    // InternalWebserviceDSL.g:355:1: entryRuleOperation returns [EObject current=null] : iv_ruleOperation= ruleOperation EOF ;
    public final EObject entryRuleOperation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOperation = null;


        try {
            // InternalWebserviceDSL.g:355:50: (iv_ruleOperation= ruleOperation EOF )
            // InternalWebserviceDSL.g:356:2: iv_ruleOperation= ruleOperation EOF
            {
             newCompositeNode(grammarAccess.getOperationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOperation=ruleOperation();

            state._fsp--;

             current =iv_ruleOperation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOperation"


    // $ANTLR start "ruleOperation"
    // InternalWebserviceDSL.g:362:1: ruleOperation returns [EObject current=null] : (this_ReadOperation_0= ruleReadOperation | this_CreateOperation_1= ruleCreateOperation | this_UpdateOperation_2= ruleUpdateOperation | this_DeleteOperation_3= ruleDeleteOperation | this_OtherOperation_4= ruleOtherOperation ) ;
    public final EObject ruleOperation() throws RecognitionException {
        EObject current = null;

        EObject this_ReadOperation_0 = null;

        EObject this_CreateOperation_1 = null;

        EObject this_UpdateOperation_2 = null;

        EObject this_DeleteOperation_3 = null;

        EObject this_OtherOperation_4 = null;



        	enterRule();

        try {
            // InternalWebserviceDSL.g:368:2: ( (this_ReadOperation_0= ruleReadOperation | this_CreateOperation_1= ruleCreateOperation | this_UpdateOperation_2= ruleUpdateOperation | this_DeleteOperation_3= ruleDeleteOperation | this_OtherOperation_4= ruleOtherOperation ) )
            // InternalWebserviceDSL.g:369:2: (this_ReadOperation_0= ruleReadOperation | this_CreateOperation_1= ruleCreateOperation | this_UpdateOperation_2= ruleUpdateOperation | this_DeleteOperation_3= ruleDeleteOperation | this_OtherOperation_4= ruleOtherOperation )
            {
            // InternalWebserviceDSL.g:369:2: (this_ReadOperation_0= ruleReadOperation | this_CreateOperation_1= ruleCreateOperation | this_UpdateOperation_2= ruleUpdateOperation | this_DeleteOperation_3= ruleDeleteOperation | this_OtherOperation_4= ruleOtherOperation )
            int alt5=5;
            switch ( input.LA(1) ) {
            case 22:
                {
                alt5=1;
                }
                break;
            case 26:
                {
                alt5=2;
                }
                break;
            case 27:
                {
                alt5=3;
                }
                break;
            case 28:
                {
                alt5=4;
                }
                break;
            case 29:
                {
                alt5=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // InternalWebserviceDSL.g:370:3: this_ReadOperation_0= ruleReadOperation
                    {

                    			newCompositeNode(grammarAccess.getOperationAccess().getReadOperationParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_ReadOperation_0=ruleReadOperation();

                    state._fsp--;


                    			current = this_ReadOperation_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalWebserviceDSL.g:379:3: this_CreateOperation_1= ruleCreateOperation
                    {

                    			newCompositeNode(grammarAccess.getOperationAccess().getCreateOperationParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_CreateOperation_1=ruleCreateOperation();

                    state._fsp--;


                    			current = this_CreateOperation_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalWebserviceDSL.g:388:3: this_UpdateOperation_2= ruleUpdateOperation
                    {

                    			newCompositeNode(grammarAccess.getOperationAccess().getUpdateOperationParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_UpdateOperation_2=ruleUpdateOperation();

                    state._fsp--;


                    			current = this_UpdateOperation_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalWebserviceDSL.g:397:3: this_DeleteOperation_3= ruleDeleteOperation
                    {

                    			newCompositeNode(grammarAccess.getOperationAccess().getDeleteOperationParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_DeleteOperation_3=ruleDeleteOperation();

                    state._fsp--;


                    			current = this_DeleteOperation_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 5 :
                    // InternalWebserviceDSL.g:406:3: this_OtherOperation_4= ruleOtherOperation
                    {

                    			newCompositeNode(grammarAccess.getOperationAccess().getOtherOperationParserRuleCall_4());
                    		
                    pushFollow(FOLLOW_2);
                    this_OtherOperation_4=ruleOtherOperation();

                    state._fsp--;


                    			current = this_OtherOperation_4;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOperation"


    // $ANTLR start "entryRuleEString"
    // InternalWebserviceDSL.g:418:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalWebserviceDSL.g:418:47: (iv_ruleEString= ruleEString EOF )
            // InternalWebserviceDSL.g:419:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalWebserviceDSL.g:425:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;


        	enterRule();

        try {
            // InternalWebserviceDSL.g:431:2: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // InternalWebserviceDSL.g:432:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // InternalWebserviceDSL.g:432:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==RULE_STRING) ) {
                alt6=1;
            }
            else if ( (LA6_0==RULE_ID) ) {
                alt6=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // InternalWebserviceDSL.g:433:3: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    			current.merge(this_STRING_0);
                    		

                    			newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalWebserviceDSL.g:441:3: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    			current.merge(this_ID_1);
                    		

                    			newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleParameter"
    // InternalWebserviceDSL.g:452:1: entryRuleParameter returns [EObject current=null] : iv_ruleParameter= ruleParameter EOF ;
    public final EObject entryRuleParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParameter = null;


        try {
            // InternalWebserviceDSL.g:452:50: (iv_ruleParameter= ruleParameter EOF )
            // InternalWebserviceDSL.g:453:2: iv_ruleParameter= ruleParameter EOF
            {
             newCompositeNode(grammarAccess.getParameterRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleParameter=ruleParameter();

            state._fsp--;

             current =iv_ruleParameter; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParameter"


    // $ANTLR start "ruleParameter"
    // InternalWebserviceDSL.g:459:1: ruleParameter returns [EObject current=null] : ( ( (lv_required_0_0= 'required' ) )? otherlv_1= 'Parameter' otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? otherlv_5= 'datatype' ( ( ruleEString ) ) otherlv_7= '}' ) ;
    public final EObject ruleParameter() throws RecognitionException {
        EObject current = null;

        Token lv_required_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        AntlrDatatypeRuleToken lv_description_4_0 = null;



        	enterRule();

        try {
            // InternalWebserviceDSL.g:465:2: ( ( ( (lv_required_0_0= 'required' ) )? otherlv_1= 'Parameter' otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? otherlv_5= 'datatype' ( ( ruleEString ) ) otherlv_7= '}' ) )
            // InternalWebserviceDSL.g:466:2: ( ( (lv_required_0_0= 'required' ) )? otherlv_1= 'Parameter' otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? otherlv_5= 'datatype' ( ( ruleEString ) ) otherlv_7= '}' )
            {
            // InternalWebserviceDSL.g:466:2: ( ( (lv_required_0_0= 'required' ) )? otherlv_1= 'Parameter' otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? otherlv_5= 'datatype' ( ( ruleEString ) ) otherlv_7= '}' )
            // InternalWebserviceDSL.g:467:3: ( (lv_required_0_0= 'required' ) )? otherlv_1= 'Parameter' otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? otherlv_5= 'datatype' ( ( ruleEString ) ) otherlv_7= '}'
            {
            // InternalWebserviceDSL.g:467:3: ( (lv_required_0_0= 'required' ) )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==19) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalWebserviceDSL.g:468:4: (lv_required_0_0= 'required' )
                    {
                    // InternalWebserviceDSL.g:468:4: (lv_required_0_0= 'required' )
                    // InternalWebserviceDSL.g:469:5: lv_required_0_0= 'required'
                    {
                    lv_required_0_0=(Token)match(input,19,FOLLOW_13); 

                    					newLeafNode(lv_required_0_0, grammarAccess.getParameterAccess().getRequiredRequiredKeyword_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getParameterRule());
                    					}
                    					setWithLastConsumed(current, "required", lv_required_0_0 != null, "required");
                    				

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,20,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getParameterAccess().getParameterKeyword_1());
            		
            otherlv_2=(Token)match(input,12,FOLLOW_14); 

            			newLeafNode(otherlv_2, grammarAccess.getParameterAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalWebserviceDSL.g:489:3: (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==13) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalWebserviceDSL.g:490:4: otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,13,FOLLOW_3); 

                    				newLeafNode(otherlv_3, grammarAccess.getParameterAccess().getDescriptionKeyword_3_0());
                    			
                    // InternalWebserviceDSL.g:494:4: ( (lv_description_4_0= ruleEString ) )
                    // InternalWebserviceDSL.g:495:5: (lv_description_4_0= ruleEString )
                    {
                    // InternalWebserviceDSL.g:495:5: (lv_description_4_0= ruleEString )
                    // InternalWebserviceDSL.g:496:6: lv_description_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getParameterAccess().getDescriptionEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_15);
                    lv_description_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getParameterRule());
                    						}
                    						set(
                    							current,
                    							"description",
                    							lv_description_4_0,
                    							"org.tfranke.webservice.dsl.WebserviceDSL.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_5=(Token)match(input,21,FOLLOW_3); 

            			newLeafNode(otherlv_5, grammarAccess.getParameterAccess().getDatatypeKeyword_4());
            		
            // InternalWebserviceDSL.g:518:3: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:519:4: ( ruleEString )
            {
            // InternalWebserviceDSL.g:519:4: ( ruleEString )
            // InternalWebserviceDSL.g:520:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getParameterRule());
            					}
            				

            					newCompositeNode(grammarAccess.getParameterAccess().getDatatypeDataTypeCrossReference_5_0());
            				
            pushFollow(FOLLOW_12);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_7=(Token)match(input,17,FOLLOW_2); 

            			newLeafNode(otherlv_7, grammarAccess.getParameterAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParameter"


    // $ANTLR start "entryRuleReadOperation"
    // InternalWebserviceDSL.g:542:1: entryRuleReadOperation returns [EObject current=null] : iv_ruleReadOperation= ruleReadOperation EOF ;
    public final EObject entryRuleReadOperation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReadOperation = null;


        try {
            // InternalWebserviceDSL.g:542:54: (iv_ruleReadOperation= ruleReadOperation EOF )
            // InternalWebserviceDSL.g:543:2: iv_ruleReadOperation= ruleReadOperation EOF
            {
             newCompositeNode(grammarAccess.getReadOperationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleReadOperation=ruleReadOperation();

            state._fsp--;

             current =iv_ruleReadOperation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReadOperation"


    // $ANTLR start "ruleReadOperation"
    // InternalWebserviceDSL.g:549:1: ruleReadOperation returns [EObject current=null] : (otherlv_0= 'ReadOperation' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'urlSuffix' ( (lv_urlSuffix_6_0= ruleEString ) ) )? (otherlv_7= 'response' ( ( ruleEString ) ) )? (otherlv_9= 'parameters' otherlv_10= '{' ( (lv_parameters_11_0= ruleParameter ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) ) )* otherlv_14= '}' )? otherlv_15= '}' ) ;
    public final EObject ruleReadOperation() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        AntlrDatatypeRuleToken lv_description_4_0 = null;

        AntlrDatatypeRuleToken lv_urlSuffix_6_0 = null;

        EObject lv_parameters_11_0 = null;

        EObject lv_parameters_13_0 = null;



        	enterRule();

        try {
            // InternalWebserviceDSL.g:555:2: ( (otherlv_0= 'ReadOperation' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'urlSuffix' ( (lv_urlSuffix_6_0= ruleEString ) ) )? (otherlv_7= 'response' ( ( ruleEString ) ) )? (otherlv_9= 'parameters' otherlv_10= '{' ( (lv_parameters_11_0= ruleParameter ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) ) )* otherlv_14= '}' )? otherlv_15= '}' ) )
            // InternalWebserviceDSL.g:556:2: (otherlv_0= 'ReadOperation' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'urlSuffix' ( (lv_urlSuffix_6_0= ruleEString ) ) )? (otherlv_7= 'response' ( ( ruleEString ) ) )? (otherlv_9= 'parameters' otherlv_10= '{' ( (lv_parameters_11_0= ruleParameter ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) ) )* otherlv_14= '}' )? otherlv_15= '}' )
            {
            // InternalWebserviceDSL.g:556:2: (otherlv_0= 'ReadOperation' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'urlSuffix' ( (lv_urlSuffix_6_0= ruleEString ) ) )? (otherlv_7= 'response' ( ( ruleEString ) ) )? (otherlv_9= 'parameters' otherlv_10= '{' ( (lv_parameters_11_0= ruleParameter ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) ) )* otherlv_14= '}' )? otherlv_15= '}' )
            // InternalWebserviceDSL.g:557:3: otherlv_0= 'ReadOperation' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'urlSuffix' ( (lv_urlSuffix_6_0= ruleEString ) ) )? (otherlv_7= 'response' ( ( ruleEString ) ) )? (otherlv_9= 'parameters' otherlv_10= '{' ( (lv_parameters_11_0= ruleParameter ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) ) )* otherlv_14= '}' )? otherlv_15= '}'
            {
            otherlv_0=(Token)match(input,22,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getReadOperationAccess().getReadOperationKeyword_0());
            		
            // InternalWebserviceDSL.g:561:3: ( (lv_name_1_0= ruleEString ) )
            // InternalWebserviceDSL.g:562:4: (lv_name_1_0= ruleEString )
            {
            // InternalWebserviceDSL.g:562:4: (lv_name_1_0= ruleEString )
            // InternalWebserviceDSL.g:563:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getReadOperationAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getReadOperationRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.tfranke.webservice.dsl.WebserviceDSL.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_16); 

            			newLeafNode(otherlv_2, grammarAccess.getReadOperationAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalWebserviceDSL.g:584:3: (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==13) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalWebserviceDSL.g:585:4: otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,13,FOLLOW_3); 

                    				newLeafNode(otherlv_3, grammarAccess.getReadOperationAccess().getDescriptionKeyword_3_0());
                    			
                    // InternalWebserviceDSL.g:589:4: ( (lv_description_4_0= ruleEString ) )
                    // InternalWebserviceDSL.g:590:5: (lv_description_4_0= ruleEString )
                    {
                    // InternalWebserviceDSL.g:590:5: (lv_description_4_0= ruleEString )
                    // InternalWebserviceDSL.g:591:6: lv_description_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getReadOperationAccess().getDescriptionEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_17);
                    lv_description_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getReadOperationRule());
                    						}
                    						set(
                    							current,
                    							"description",
                    							lv_description_4_0,
                    							"org.tfranke.webservice.dsl.WebserviceDSL.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalWebserviceDSL.g:609:3: (otherlv_5= 'urlSuffix' ( (lv_urlSuffix_6_0= ruleEString ) ) )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==23) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalWebserviceDSL.g:610:4: otherlv_5= 'urlSuffix' ( (lv_urlSuffix_6_0= ruleEString ) )
                    {
                    otherlv_5=(Token)match(input,23,FOLLOW_3); 

                    				newLeafNode(otherlv_5, grammarAccess.getReadOperationAccess().getUrlSuffixKeyword_4_0());
                    			
                    // InternalWebserviceDSL.g:614:4: ( (lv_urlSuffix_6_0= ruleEString ) )
                    // InternalWebserviceDSL.g:615:5: (lv_urlSuffix_6_0= ruleEString )
                    {
                    // InternalWebserviceDSL.g:615:5: (lv_urlSuffix_6_0= ruleEString )
                    // InternalWebserviceDSL.g:616:6: lv_urlSuffix_6_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getReadOperationAccess().getUrlSuffixEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_18);
                    lv_urlSuffix_6_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getReadOperationRule());
                    						}
                    						set(
                    							current,
                    							"urlSuffix",
                    							lv_urlSuffix_6_0,
                    							"org.tfranke.webservice.dsl.WebserviceDSL.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalWebserviceDSL.g:634:3: (otherlv_7= 'response' ( ( ruleEString ) ) )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==24) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalWebserviceDSL.g:635:4: otherlv_7= 'response' ( ( ruleEString ) )
                    {
                    otherlv_7=(Token)match(input,24,FOLLOW_3); 

                    				newLeafNode(otherlv_7, grammarAccess.getReadOperationAccess().getResponseKeyword_5_0());
                    			
                    // InternalWebserviceDSL.g:639:4: ( ( ruleEString ) )
                    // InternalWebserviceDSL.g:640:5: ( ruleEString )
                    {
                    // InternalWebserviceDSL.g:640:5: ( ruleEString )
                    // InternalWebserviceDSL.g:641:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getReadOperationRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getReadOperationAccess().getResponseDataTypeCrossReference_5_1_0());
                    					
                    pushFollow(FOLLOW_19);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalWebserviceDSL.g:656:3: (otherlv_9= 'parameters' otherlv_10= '{' ( (lv_parameters_11_0= ruleParameter ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) ) )* otherlv_14= '}' )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==25) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalWebserviceDSL.g:657:4: otherlv_9= 'parameters' otherlv_10= '{' ( (lv_parameters_11_0= ruleParameter ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) ) )* otherlv_14= '}'
                    {
                    otherlv_9=(Token)match(input,25,FOLLOW_4); 

                    				newLeafNode(otherlv_9, grammarAccess.getReadOperationAccess().getParametersKeyword_6_0());
                    			
                    otherlv_10=(Token)match(input,12,FOLLOW_20); 

                    				newLeafNode(otherlv_10, grammarAccess.getReadOperationAccess().getLeftCurlyBracketKeyword_6_1());
                    			
                    // InternalWebserviceDSL.g:665:4: ( (lv_parameters_11_0= ruleParameter ) )
                    // InternalWebserviceDSL.g:666:5: (lv_parameters_11_0= ruleParameter )
                    {
                    // InternalWebserviceDSL.g:666:5: (lv_parameters_11_0= ruleParameter )
                    // InternalWebserviceDSL.g:667:6: lv_parameters_11_0= ruleParameter
                    {

                    						newCompositeNode(grammarAccess.getReadOperationAccess().getParametersParameterParserRuleCall_6_2_0());
                    					
                    pushFollow(FOLLOW_9);
                    lv_parameters_11_0=ruleParameter();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getReadOperationRule());
                    						}
                    						add(
                    							current,
                    							"parameters",
                    							lv_parameters_11_0,
                    							"org.tfranke.webservice.dsl.WebserviceDSL.Parameter");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalWebserviceDSL.g:684:4: (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) ) )*
                    loop12:
                    do {
                        int alt12=2;
                        int LA12_0 = input.LA(1);

                        if ( (LA12_0==16) ) {
                            alt12=1;
                        }


                        switch (alt12) {
                    	case 1 :
                    	    // InternalWebserviceDSL.g:685:5: otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) )
                    	    {
                    	    otherlv_12=(Token)match(input,16,FOLLOW_20); 

                    	    					newLeafNode(otherlv_12, grammarAccess.getReadOperationAccess().getCommaKeyword_6_3_0());
                    	    				
                    	    // InternalWebserviceDSL.g:689:5: ( (lv_parameters_13_0= ruleParameter ) )
                    	    // InternalWebserviceDSL.g:690:6: (lv_parameters_13_0= ruleParameter )
                    	    {
                    	    // InternalWebserviceDSL.g:690:6: (lv_parameters_13_0= ruleParameter )
                    	    // InternalWebserviceDSL.g:691:7: lv_parameters_13_0= ruleParameter
                    	    {

                    	    							newCompositeNode(grammarAccess.getReadOperationAccess().getParametersParameterParserRuleCall_6_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_9);
                    	    lv_parameters_13_0=ruleParameter();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getReadOperationRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"parameters",
                    	    								lv_parameters_13_0,
                    	    								"org.tfranke.webservice.dsl.WebserviceDSL.Parameter");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop12;
                        }
                    } while (true);

                    otherlv_14=(Token)match(input,17,FOLLOW_12); 

                    				newLeafNode(otherlv_14, grammarAccess.getReadOperationAccess().getRightCurlyBracketKeyword_6_4());
                    			

                    }
                    break;

            }

            otherlv_15=(Token)match(input,17,FOLLOW_2); 

            			newLeafNode(otherlv_15, grammarAccess.getReadOperationAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReadOperation"


    // $ANTLR start "entryRuleCreateOperation"
    // InternalWebserviceDSL.g:722:1: entryRuleCreateOperation returns [EObject current=null] : iv_ruleCreateOperation= ruleCreateOperation EOF ;
    public final EObject entryRuleCreateOperation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCreateOperation = null;


        try {
            // InternalWebserviceDSL.g:722:56: (iv_ruleCreateOperation= ruleCreateOperation EOF )
            // InternalWebserviceDSL.g:723:2: iv_ruleCreateOperation= ruleCreateOperation EOF
            {
             newCompositeNode(grammarAccess.getCreateOperationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCreateOperation=ruleCreateOperation();

            state._fsp--;

             current =iv_ruleCreateOperation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCreateOperation"


    // $ANTLR start "ruleCreateOperation"
    // InternalWebserviceDSL.g:729:1: ruleCreateOperation returns [EObject current=null] : (otherlv_0= 'CreateOperation' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'urlSuffix' ( (lv_urlSuffix_6_0= ruleEString ) ) )? (otherlv_7= 'response' ( ( ruleEString ) ) )? (otherlv_9= 'parameters' otherlv_10= '{' ( (lv_parameters_11_0= ruleParameter ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) ) )* otherlv_14= '}' )? otherlv_15= '}' ) ;
    public final EObject ruleCreateOperation() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        AntlrDatatypeRuleToken lv_description_4_0 = null;

        AntlrDatatypeRuleToken lv_urlSuffix_6_0 = null;

        EObject lv_parameters_11_0 = null;

        EObject lv_parameters_13_0 = null;



        	enterRule();

        try {
            // InternalWebserviceDSL.g:735:2: ( (otherlv_0= 'CreateOperation' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'urlSuffix' ( (lv_urlSuffix_6_0= ruleEString ) ) )? (otherlv_7= 'response' ( ( ruleEString ) ) )? (otherlv_9= 'parameters' otherlv_10= '{' ( (lv_parameters_11_0= ruleParameter ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) ) )* otherlv_14= '}' )? otherlv_15= '}' ) )
            // InternalWebserviceDSL.g:736:2: (otherlv_0= 'CreateOperation' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'urlSuffix' ( (lv_urlSuffix_6_0= ruleEString ) ) )? (otherlv_7= 'response' ( ( ruleEString ) ) )? (otherlv_9= 'parameters' otherlv_10= '{' ( (lv_parameters_11_0= ruleParameter ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) ) )* otherlv_14= '}' )? otherlv_15= '}' )
            {
            // InternalWebserviceDSL.g:736:2: (otherlv_0= 'CreateOperation' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'urlSuffix' ( (lv_urlSuffix_6_0= ruleEString ) ) )? (otherlv_7= 'response' ( ( ruleEString ) ) )? (otherlv_9= 'parameters' otherlv_10= '{' ( (lv_parameters_11_0= ruleParameter ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) ) )* otherlv_14= '}' )? otherlv_15= '}' )
            // InternalWebserviceDSL.g:737:3: otherlv_0= 'CreateOperation' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'urlSuffix' ( (lv_urlSuffix_6_0= ruleEString ) ) )? (otherlv_7= 'response' ( ( ruleEString ) ) )? (otherlv_9= 'parameters' otherlv_10= '{' ( (lv_parameters_11_0= ruleParameter ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) ) )* otherlv_14= '}' )? otherlv_15= '}'
            {
            otherlv_0=(Token)match(input,26,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getCreateOperationAccess().getCreateOperationKeyword_0());
            		
            // InternalWebserviceDSL.g:741:3: ( (lv_name_1_0= ruleEString ) )
            // InternalWebserviceDSL.g:742:4: (lv_name_1_0= ruleEString )
            {
            // InternalWebserviceDSL.g:742:4: (lv_name_1_0= ruleEString )
            // InternalWebserviceDSL.g:743:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getCreateOperationAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCreateOperationRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.tfranke.webservice.dsl.WebserviceDSL.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_16); 

            			newLeafNode(otherlv_2, grammarAccess.getCreateOperationAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalWebserviceDSL.g:764:3: (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==13) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalWebserviceDSL.g:765:4: otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,13,FOLLOW_3); 

                    				newLeafNode(otherlv_3, grammarAccess.getCreateOperationAccess().getDescriptionKeyword_3_0());
                    			
                    // InternalWebserviceDSL.g:769:4: ( (lv_description_4_0= ruleEString ) )
                    // InternalWebserviceDSL.g:770:5: (lv_description_4_0= ruleEString )
                    {
                    // InternalWebserviceDSL.g:770:5: (lv_description_4_0= ruleEString )
                    // InternalWebserviceDSL.g:771:6: lv_description_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getCreateOperationAccess().getDescriptionEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_17);
                    lv_description_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCreateOperationRule());
                    						}
                    						set(
                    							current,
                    							"description",
                    							lv_description_4_0,
                    							"org.tfranke.webservice.dsl.WebserviceDSL.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalWebserviceDSL.g:789:3: (otherlv_5= 'urlSuffix' ( (lv_urlSuffix_6_0= ruleEString ) ) )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==23) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalWebserviceDSL.g:790:4: otherlv_5= 'urlSuffix' ( (lv_urlSuffix_6_0= ruleEString ) )
                    {
                    otherlv_5=(Token)match(input,23,FOLLOW_3); 

                    				newLeafNode(otherlv_5, grammarAccess.getCreateOperationAccess().getUrlSuffixKeyword_4_0());
                    			
                    // InternalWebserviceDSL.g:794:4: ( (lv_urlSuffix_6_0= ruleEString ) )
                    // InternalWebserviceDSL.g:795:5: (lv_urlSuffix_6_0= ruleEString )
                    {
                    // InternalWebserviceDSL.g:795:5: (lv_urlSuffix_6_0= ruleEString )
                    // InternalWebserviceDSL.g:796:6: lv_urlSuffix_6_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getCreateOperationAccess().getUrlSuffixEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_18);
                    lv_urlSuffix_6_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCreateOperationRule());
                    						}
                    						set(
                    							current,
                    							"urlSuffix",
                    							lv_urlSuffix_6_0,
                    							"org.tfranke.webservice.dsl.WebserviceDSL.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalWebserviceDSL.g:814:3: (otherlv_7= 'response' ( ( ruleEString ) ) )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==24) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalWebserviceDSL.g:815:4: otherlv_7= 'response' ( ( ruleEString ) )
                    {
                    otherlv_7=(Token)match(input,24,FOLLOW_3); 

                    				newLeafNode(otherlv_7, grammarAccess.getCreateOperationAccess().getResponseKeyword_5_0());
                    			
                    // InternalWebserviceDSL.g:819:4: ( ( ruleEString ) )
                    // InternalWebserviceDSL.g:820:5: ( ruleEString )
                    {
                    // InternalWebserviceDSL.g:820:5: ( ruleEString )
                    // InternalWebserviceDSL.g:821:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getCreateOperationRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getCreateOperationAccess().getResponseDataTypeCrossReference_5_1_0());
                    					
                    pushFollow(FOLLOW_19);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalWebserviceDSL.g:836:3: (otherlv_9= 'parameters' otherlv_10= '{' ( (lv_parameters_11_0= ruleParameter ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) ) )* otherlv_14= '}' )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==25) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalWebserviceDSL.g:837:4: otherlv_9= 'parameters' otherlv_10= '{' ( (lv_parameters_11_0= ruleParameter ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) ) )* otherlv_14= '}'
                    {
                    otherlv_9=(Token)match(input,25,FOLLOW_4); 

                    				newLeafNode(otherlv_9, grammarAccess.getCreateOperationAccess().getParametersKeyword_6_0());
                    			
                    otherlv_10=(Token)match(input,12,FOLLOW_20); 

                    				newLeafNode(otherlv_10, grammarAccess.getCreateOperationAccess().getLeftCurlyBracketKeyword_6_1());
                    			
                    // InternalWebserviceDSL.g:845:4: ( (lv_parameters_11_0= ruleParameter ) )
                    // InternalWebserviceDSL.g:846:5: (lv_parameters_11_0= ruleParameter )
                    {
                    // InternalWebserviceDSL.g:846:5: (lv_parameters_11_0= ruleParameter )
                    // InternalWebserviceDSL.g:847:6: lv_parameters_11_0= ruleParameter
                    {

                    						newCompositeNode(grammarAccess.getCreateOperationAccess().getParametersParameterParserRuleCall_6_2_0());
                    					
                    pushFollow(FOLLOW_9);
                    lv_parameters_11_0=ruleParameter();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCreateOperationRule());
                    						}
                    						add(
                    							current,
                    							"parameters",
                    							lv_parameters_11_0,
                    							"org.tfranke.webservice.dsl.WebserviceDSL.Parameter");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalWebserviceDSL.g:864:4: (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) ) )*
                    loop17:
                    do {
                        int alt17=2;
                        int LA17_0 = input.LA(1);

                        if ( (LA17_0==16) ) {
                            alt17=1;
                        }


                        switch (alt17) {
                    	case 1 :
                    	    // InternalWebserviceDSL.g:865:5: otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) )
                    	    {
                    	    otherlv_12=(Token)match(input,16,FOLLOW_20); 

                    	    					newLeafNode(otherlv_12, grammarAccess.getCreateOperationAccess().getCommaKeyword_6_3_0());
                    	    				
                    	    // InternalWebserviceDSL.g:869:5: ( (lv_parameters_13_0= ruleParameter ) )
                    	    // InternalWebserviceDSL.g:870:6: (lv_parameters_13_0= ruleParameter )
                    	    {
                    	    // InternalWebserviceDSL.g:870:6: (lv_parameters_13_0= ruleParameter )
                    	    // InternalWebserviceDSL.g:871:7: lv_parameters_13_0= ruleParameter
                    	    {

                    	    							newCompositeNode(grammarAccess.getCreateOperationAccess().getParametersParameterParserRuleCall_6_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_9);
                    	    lv_parameters_13_0=ruleParameter();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getCreateOperationRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"parameters",
                    	    								lv_parameters_13_0,
                    	    								"org.tfranke.webservice.dsl.WebserviceDSL.Parameter");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop17;
                        }
                    } while (true);

                    otherlv_14=(Token)match(input,17,FOLLOW_12); 

                    				newLeafNode(otherlv_14, grammarAccess.getCreateOperationAccess().getRightCurlyBracketKeyword_6_4());
                    			

                    }
                    break;

            }

            otherlv_15=(Token)match(input,17,FOLLOW_2); 

            			newLeafNode(otherlv_15, grammarAccess.getCreateOperationAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCreateOperation"


    // $ANTLR start "entryRuleUpdateOperation"
    // InternalWebserviceDSL.g:902:1: entryRuleUpdateOperation returns [EObject current=null] : iv_ruleUpdateOperation= ruleUpdateOperation EOF ;
    public final EObject entryRuleUpdateOperation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUpdateOperation = null;


        try {
            // InternalWebserviceDSL.g:902:56: (iv_ruleUpdateOperation= ruleUpdateOperation EOF )
            // InternalWebserviceDSL.g:903:2: iv_ruleUpdateOperation= ruleUpdateOperation EOF
            {
             newCompositeNode(grammarAccess.getUpdateOperationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleUpdateOperation=ruleUpdateOperation();

            state._fsp--;

             current =iv_ruleUpdateOperation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUpdateOperation"


    // $ANTLR start "ruleUpdateOperation"
    // InternalWebserviceDSL.g:909:1: ruleUpdateOperation returns [EObject current=null] : (otherlv_0= 'UpdateOperation' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'urlSuffix' ( (lv_urlSuffix_6_0= ruleEString ) ) )? (otherlv_7= 'response' ( ( ruleEString ) ) )? (otherlv_9= 'parameters' otherlv_10= '{' ( (lv_parameters_11_0= ruleParameter ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) ) )* otherlv_14= '}' )? otherlv_15= '}' ) ;
    public final EObject ruleUpdateOperation() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        AntlrDatatypeRuleToken lv_description_4_0 = null;

        AntlrDatatypeRuleToken lv_urlSuffix_6_0 = null;

        EObject lv_parameters_11_0 = null;

        EObject lv_parameters_13_0 = null;



        	enterRule();

        try {
            // InternalWebserviceDSL.g:915:2: ( (otherlv_0= 'UpdateOperation' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'urlSuffix' ( (lv_urlSuffix_6_0= ruleEString ) ) )? (otherlv_7= 'response' ( ( ruleEString ) ) )? (otherlv_9= 'parameters' otherlv_10= '{' ( (lv_parameters_11_0= ruleParameter ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) ) )* otherlv_14= '}' )? otherlv_15= '}' ) )
            // InternalWebserviceDSL.g:916:2: (otherlv_0= 'UpdateOperation' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'urlSuffix' ( (lv_urlSuffix_6_0= ruleEString ) ) )? (otherlv_7= 'response' ( ( ruleEString ) ) )? (otherlv_9= 'parameters' otherlv_10= '{' ( (lv_parameters_11_0= ruleParameter ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) ) )* otherlv_14= '}' )? otherlv_15= '}' )
            {
            // InternalWebserviceDSL.g:916:2: (otherlv_0= 'UpdateOperation' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'urlSuffix' ( (lv_urlSuffix_6_0= ruleEString ) ) )? (otherlv_7= 'response' ( ( ruleEString ) ) )? (otherlv_9= 'parameters' otherlv_10= '{' ( (lv_parameters_11_0= ruleParameter ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) ) )* otherlv_14= '}' )? otherlv_15= '}' )
            // InternalWebserviceDSL.g:917:3: otherlv_0= 'UpdateOperation' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'urlSuffix' ( (lv_urlSuffix_6_0= ruleEString ) ) )? (otherlv_7= 'response' ( ( ruleEString ) ) )? (otherlv_9= 'parameters' otherlv_10= '{' ( (lv_parameters_11_0= ruleParameter ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) ) )* otherlv_14= '}' )? otherlv_15= '}'
            {
            otherlv_0=(Token)match(input,27,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getUpdateOperationAccess().getUpdateOperationKeyword_0());
            		
            // InternalWebserviceDSL.g:921:3: ( (lv_name_1_0= ruleEString ) )
            // InternalWebserviceDSL.g:922:4: (lv_name_1_0= ruleEString )
            {
            // InternalWebserviceDSL.g:922:4: (lv_name_1_0= ruleEString )
            // InternalWebserviceDSL.g:923:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getUpdateOperationAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getUpdateOperationRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.tfranke.webservice.dsl.WebserviceDSL.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_16); 

            			newLeafNode(otherlv_2, grammarAccess.getUpdateOperationAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalWebserviceDSL.g:944:3: (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==13) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // InternalWebserviceDSL.g:945:4: otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,13,FOLLOW_3); 

                    				newLeafNode(otherlv_3, grammarAccess.getUpdateOperationAccess().getDescriptionKeyword_3_0());
                    			
                    // InternalWebserviceDSL.g:949:4: ( (lv_description_4_0= ruleEString ) )
                    // InternalWebserviceDSL.g:950:5: (lv_description_4_0= ruleEString )
                    {
                    // InternalWebserviceDSL.g:950:5: (lv_description_4_0= ruleEString )
                    // InternalWebserviceDSL.g:951:6: lv_description_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getUpdateOperationAccess().getDescriptionEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_17);
                    lv_description_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getUpdateOperationRule());
                    						}
                    						set(
                    							current,
                    							"description",
                    							lv_description_4_0,
                    							"org.tfranke.webservice.dsl.WebserviceDSL.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalWebserviceDSL.g:969:3: (otherlv_5= 'urlSuffix' ( (lv_urlSuffix_6_0= ruleEString ) ) )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==23) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // InternalWebserviceDSL.g:970:4: otherlv_5= 'urlSuffix' ( (lv_urlSuffix_6_0= ruleEString ) )
                    {
                    otherlv_5=(Token)match(input,23,FOLLOW_3); 

                    				newLeafNode(otherlv_5, grammarAccess.getUpdateOperationAccess().getUrlSuffixKeyword_4_0());
                    			
                    // InternalWebserviceDSL.g:974:4: ( (lv_urlSuffix_6_0= ruleEString ) )
                    // InternalWebserviceDSL.g:975:5: (lv_urlSuffix_6_0= ruleEString )
                    {
                    // InternalWebserviceDSL.g:975:5: (lv_urlSuffix_6_0= ruleEString )
                    // InternalWebserviceDSL.g:976:6: lv_urlSuffix_6_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getUpdateOperationAccess().getUrlSuffixEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_18);
                    lv_urlSuffix_6_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getUpdateOperationRule());
                    						}
                    						set(
                    							current,
                    							"urlSuffix",
                    							lv_urlSuffix_6_0,
                    							"org.tfranke.webservice.dsl.WebserviceDSL.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalWebserviceDSL.g:994:3: (otherlv_7= 'response' ( ( ruleEString ) ) )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==24) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalWebserviceDSL.g:995:4: otherlv_7= 'response' ( ( ruleEString ) )
                    {
                    otherlv_7=(Token)match(input,24,FOLLOW_3); 

                    				newLeafNode(otherlv_7, grammarAccess.getUpdateOperationAccess().getResponseKeyword_5_0());
                    			
                    // InternalWebserviceDSL.g:999:4: ( ( ruleEString ) )
                    // InternalWebserviceDSL.g:1000:5: ( ruleEString )
                    {
                    // InternalWebserviceDSL.g:1000:5: ( ruleEString )
                    // InternalWebserviceDSL.g:1001:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getUpdateOperationRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getUpdateOperationAccess().getResponseDataTypeCrossReference_5_1_0());
                    					
                    pushFollow(FOLLOW_19);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalWebserviceDSL.g:1016:3: (otherlv_9= 'parameters' otherlv_10= '{' ( (lv_parameters_11_0= ruleParameter ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) ) )* otherlv_14= '}' )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==25) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // InternalWebserviceDSL.g:1017:4: otherlv_9= 'parameters' otherlv_10= '{' ( (lv_parameters_11_0= ruleParameter ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) ) )* otherlv_14= '}'
                    {
                    otherlv_9=(Token)match(input,25,FOLLOW_4); 

                    				newLeafNode(otherlv_9, grammarAccess.getUpdateOperationAccess().getParametersKeyword_6_0());
                    			
                    otherlv_10=(Token)match(input,12,FOLLOW_20); 

                    				newLeafNode(otherlv_10, grammarAccess.getUpdateOperationAccess().getLeftCurlyBracketKeyword_6_1());
                    			
                    // InternalWebserviceDSL.g:1025:4: ( (lv_parameters_11_0= ruleParameter ) )
                    // InternalWebserviceDSL.g:1026:5: (lv_parameters_11_0= ruleParameter )
                    {
                    // InternalWebserviceDSL.g:1026:5: (lv_parameters_11_0= ruleParameter )
                    // InternalWebserviceDSL.g:1027:6: lv_parameters_11_0= ruleParameter
                    {

                    						newCompositeNode(grammarAccess.getUpdateOperationAccess().getParametersParameterParserRuleCall_6_2_0());
                    					
                    pushFollow(FOLLOW_9);
                    lv_parameters_11_0=ruleParameter();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getUpdateOperationRule());
                    						}
                    						add(
                    							current,
                    							"parameters",
                    							lv_parameters_11_0,
                    							"org.tfranke.webservice.dsl.WebserviceDSL.Parameter");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalWebserviceDSL.g:1044:4: (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) ) )*
                    loop22:
                    do {
                        int alt22=2;
                        int LA22_0 = input.LA(1);

                        if ( (LA22_0==16) ) {
                            alt22=1;
                        }


                        switch (alt22) {
                    	case 1 :
                    	    // InternalWebserviceDSL.g:1045:5: otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) )
                    	    {
                    	    otherlv_12=(Token)match(input,16,FOLLOW_20); 

                    	    					newLeafNode(otherlv_12, grammarAccess.getUpdateOperationAccess().getCommaKeyword_6_3_0());
                    	    				
                    	    // InternalWebserviceDSL.g:1049:5: ( (lv_parameters_13_0= ruleParameter ) )
                    	    // InternalWebserviceDSL.g:1050:6: (lv_parameters_13_0= ruleParameter )
                    	    {
                    	    // InternalWebserviceDSL.g:1050:6: (lv_parameters_13_0= ruleParameter )
                    	    // InternalWebserviceDSL.g:1051:7: lv_parameters_13_0= ruleParameter
                    	    {

                    	    							newCompositeNode(grammarAccess.getUpdateOperationAccess().getParametersParameterParserRuleCall_6_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_9);
                    	    lv_parameters_13_0=ruleParameter();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getUpdateOperationRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"parameters",
                    	    								lv_parameters_13_0,
                    	    								"org.tfranke.webservice.dsl.WebserviceDSL.Parameter");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop22;
                        }
                    } while (true);

                    otherlv_14=(Token)match(input,17,FOLLOW_12); 

                    				newLeafNode(otherlv_14, grammarAccess.getUpdateOperationAccess().getRightCurlyBracketKeyword_6_4());
                    			

                    }
                    break;

            }

            otherlv_15=(Token)match(input,17,FOLLOW_2); 

            			newLeafNode(otherlv_15, grammarAccess.getUpdateOperationAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUpdateOperation"


    // $ANTLR start "entryRuleDeleteOperation"
    // InternalWebserviceDSL.g:1082:1: entryRuleDeleteOperation returns [EObject current=null] : iv_ruleDeleteOperation= ruleDeleteOperation EOF ;
    public final EObject entryRuleDeleteOperation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDeleteOperation = null;


        try {
            // InternalWebserviceDSL.g:1082:56: (iv_ruleDeleteOperation= ruleDeleteOperation EOF )
            // InternalWebserviceDSL.g:1083:2: iv_ruleDeleteOperation= ruleDeleteOperation EOF
            {
             newCompositeNode(grammarAccess.getDeleteOperationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDeleteOperation=ruleDeleteOperation();

            state._fsp--;

             current =iv_ruleDeleteOperation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDeleteOperation"


    // $ANTLR start "ruleDeleteOperation"
    // InternalWebserviceDSL.g:1089:1: ruleDeleteOperation returns [EObject current=null] : (otherlv_0= 'DeleteOperation' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'urlSuffix' ( (lv_urlSuffix_6_0= ruleEString ) ) )? (otherlv_7= 'response' ( ( ruleEString ) ) )? (otherlv_9= 'parameters' otherlv_10= '{' ( (lv_parameters_11_0= ruleParameter ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) ) )* otherlv_14= '}' )? otherlv_15= '}' ) ;
    public final EObject ruleDeleteOperation() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        AntlrDatatypeRuleToken lv_description_4_0 = null;

        AntlrDatatypeRuleToken lv_urlSuffix_6_0 = null;

        EObject lv_parameters_11_0 = null;

        EObject lv_parameters_13_0 = null;



        	enterRule();

        try {
            // InternalWebserviceDSL.g:1095:2: ( (otherlv_0= 'DeleteOperation' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'urlSuffix' ( (lv_urlSuffix_6_0= ruleEString ) ) )? (otherlv_7= 'response' ( ( ruleEString ) ) )? (otherlv_9= 'parameters' otherlv_10= '{' ( (lv_parameters_11_0= ruleParameter ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) ) )* otherlv_14= '}' )? otherlv_15= '}' ) )
            // InternalWebserviceDSL.g:1096:2: (otherlv_0= 'DeleteOperation' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'urlSuffix' ( (lv_urlSuffix_6_0= ruleEString ) ) )? (otherlv_7= 'response' ( ( ruleEString ) ) )? (otherlv_9= 'parameters' otherlv_10= '{' ( (lv_parameters_11_0= ruleParameter ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) ) )* otherlv_14= '}' )? otherlv_15= '}' )
            {
            // InternalWebserviceDSL.g:1096:2: (otherlv_0= 'DeleteOperation' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'urlSuffix' ( (lv_urlSuffix_6_0= ruleEString ) ) )? (otherlv_7= 'response' ( ( ruleEString ) ) )? (otherlv_9= 'parameters' otherlv_10= '{' ( (lv_parameters_11_0= ruleParameter ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) ) )* otherlv_14= '}' )? otherlv_15= '}' )
            // InternalWebserviceDSL.g:1097:3: otherlv_0= 'DeleteOperation' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'urlSuffix' ( (lv_urlSuffix_6_0= ruleEString ) ) )? (otherlv_7= 'response' ( ( ruleEString ) ) )? (otherlv_9= 'parameters' otherlv_10= '{' ( (lv_parameters_11_0= ruleParameter ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) ) )* otherlv_14= '}' )? otherlv_15= '}'
            {
            otherlv_0=(Token)match(input,28,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getDeleteOperationAccess().getDeleteOperationKeyword_0());
            		
            // InternalWebserviceDSL.g:1101:3: ( (lv_name_1_0= ruleEString ) )
            // InternalWebserviceDSL.g:1102:4: (lv_name_1_0= ruleEString )
            {
            // InternalWebserviceDSL.g:1102:4: (lv_name_1_0= ruleEString )
            // InternalWebserviceDSL.g:1103:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getDeleteOperationAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDeleteOperationRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.tfranke.webservice.dsl.WebserviceDSL.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_16); 

            			newLeafNode(otherlv_2, grammarAccess.getDeleteOperationAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalWebserviceDSL.g:1124:3: (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==13) ) {
                alt24=1;
            }
            switch (alt24) {
                case 1 :
                    // InternalWebserviceDSL.g:1125:4: otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,13,FOLLOW_3); 

                    				newLeafNode(otherlv_3, grammarAccess.getDeleteOperationAccess().getDescriptionKeyword_3_0());
                    			
                    // InternalWebserviceDSL.g:1129:4: ( (lv_description_4_0= ruleEString ) )
                    // InternalWebserviceDSL.g:1130:5: (lv_description_4_0= ruleEString )
                    {
                    // InternalWebserviceDSL.g:1130:5: (lv_description_4_0= ruleEString )
                    // InternalWebserviceDSL.g:1131:6: lv_description_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getDeleteOperationAccess().getDescriptionEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_17);
                    lv_description_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getDeleteOperationRule());
                    						}
                    						set(
                    							current,
                    							"description",
                    							lv_description_4_0,
                    							"org.tfranke.webservice.dsl.WebserviceDSL.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalWebserviceDSL.g:1149:3: (otherlv_5= 'urlSuffix' ( (lv_urlSuffix_6_0= ruleEString ) ) )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==23) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // InternalWebserviceDSL.g:1150:4: otherlv_5= 'urlSuffix' ( (lv_urlSuffix_6_0= ruleEString ) )
                    {
                    otherlv_5=(Token)match(input,23,FOLLOW_3); 

                    				newLeafNode(otherlv_5, grammarAccess.getDeleteOperationAccess().getUrlSuffixKeyword_4_0());
                    			
                    // InternalWebserviceDSL.g:1154:4: ( (lv_urlSuffix_6_0= ruleEString ) )
                    // InternalWebserviceDSL.g:1155:5: (lv_urlSuffix_6_0= ruleEString )
                    {
                    // InternalWebserviceDSL.g:1155:5: (lv_urlSuffix_6_0= ruleEString )
                    // InternalWebserviceDSL.g:1156:6: lv_urlSuffix_6_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getDeleteOperationAccess().getUrlSuffixEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_18);
                    lv_urlSuffix_6_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getDeleteOperationRule());
                    						}
                    						set(
                    							current,
                    							"urlSuffix",
                    							lv_urlSuffix_6_0,
                    							"org.tfranke.webservice.dsl.WebserviceDSL.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalWebserviceDSL.g:1174:3: (otherlv_7= 'response' ( ( ruleEString ) ) )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==24) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // InternalWebserviceDSL.g:1175:4: otherlv_7= 'response' ( ( ruleEString ) )
                    {
                    otherlv_7=(Token)match(input,24,FOLLOW_3); 

                    				newLeafNode(otherlv_7, grammarAccess.getDeleteOperationAccess().getResponseKeyword_5_0());
                    			
                    // InternalWebserviceDSL.g:1179:4: ( ( ruleEString ) )
                    // InternalWebserviceDSL.g:1180:5: ( ruleEString )
                    {
                    // InternalWebserviceDSL.g:1180:5: ( ruleEString )
                    // InternalWebserviceDSL.g:1181:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getDeleteOperationRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getDeleteOperationAccess().getResponseDataTypeCrossReference_5_1_0());
                    					
                    pushFollow(FOLLOW_19);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalWebserviceDSL.g:1196:3: (otherlv_9= 'parameters' otherlv_10= '{' ( (lv_parameters_11_0= ruleParameter ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) ) )* otherlv_14= '}' )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==25) ) {
                alt28=1;
            }
            switch (alt28) {
                case 1 :
                    // InternalWebserviceDSL.g:1197:4: otherlv_9= 'parameters' otherlv_10= '{' ( (lv_parameters_11_0= ruleParameter ) ) (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) ) )* otherlv_14= '}'
                    {
                    otherlv_9=(Token)match(input,25,FOLLOW_4); 

                    				newLeafNode(otherlv_9, grammarAccess.getDeleteOperationAccess().getParametersKeyword_6_0());
                    			
                    otherlv_10=(Token)match(input,12,FOLLOW_20); 

                    				newLeafNode(otherlv_10, grammarAccess.getDeleteOperationAccess().getLeftCurlyBracketKeyword_6_1());
                    			
                    // InternalWebserviceDSL.g:1205:4: ( (lv_parameters_11_0= ruleParameter ) )
                    // InternalWebserviceDSL.g:1206:5: (lv_parameters_11_0= ruleParameter )
                    {
                    // InternalWebserviceDSL.g:1206:5: (lv_parameters_11_0= ruleParameter )
                    // InternalWebserviceDSL.g:1207:6: lv_parameters_11_0= ruleParameter
                    {

                    						newCompositeNode(grammarAccess.getDeleteOperationAccess().getParametersParameterParserRuleCall_6_2_0());
                    					
                    pushFollow(FOLLOW_9);
                    lv_parameters_11_0=ruleParameter();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getDeleteOperationRule());
                    						}
                    						add(
                    							current,
                    							"parameters",
                    							lv_parameters_11_0,
                    							"org.tfranke.webservice.dsl.WebserviceDSL.Parameter");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalWebserviceDSL.g:1224:4: (otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) ) )*
                    loop27:
                    do {
                        int alt27=2;
                        int LA27_0 = input.LA(1);

                        if ( (LA27_0==16) ) {
                            alt27=1;
                        }


                        switch (alt27) {
                    	case 1 :
                    	    // InternalWebserviceDSL.g:1225:5: otherlv_12= ',' ( (lv_parameters_13_0= ruleParameter ) )
                    	    {
                    	    otherlv_12=(Token)match(input,16,FOLLOW_20); 

                    	    					newLeafNode(otherlv_12, grammarAccess.getDeleteOperationAccess().getCommaKeyword_6_3_0());
                    	    				
                    	    // InternalWebserviceDSL.g:1229:5: ( (lv_parameters_13_0= ruleParameter ) )
                    	    // InternalWebserviceDSL.g:1230:6: (lv_parameters_13_0= ruleParameter )
                    	    {
                    	    // InternalWebserviceDSL.g:1230:6: (lv_parameters_13_0= ruleParameter )
                    	    // InternalWebserviceDSL.g:1231:7: lv_parameters_13_0= ruleParameter
                    	    {

                    	    							newCompositeNode(grammarAccess.getDeleteOperationAccess().getParametersParameterParserRuleCall_6_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_9);
                    	    lv_parameters_13_0=ruleParameter();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getDeleteOperationRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"parameters",
                    	    								lv_parameters_13_0,
                    	    								"org.tfranke.webservice.dsl.WebserviceDSL.Parameter");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop27;
                        }
                    } while (true);

                    otherlv_14=(Token)match(input,17,FOLLOW_12); 

                    				newLeafNode(otherlv_14, grammarAccess.getDeleteOperationAccess().getRightCurlyBracketKeyword_6_4());
                    			

                    }
                    break;

            }

            otherlv_15=(Token)match(input,17,FOLLOW_2); 

            			newLeafNode(otherlv_15, grammarAccess.getDeleteOperationAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDeleteOperation"


    // $ANTLR start "entryRuleOtherOperation"
    // InternalWebserviceDSL.g:1262:1: entryRuleOtherOperation returns [EObject current=null] : iv_ruleOtherOperation= ruleOtherOperation EOF ;
    public final EObject entryRuleOtherOperation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOtherOperation = null;


        try {
            // InternalWebserviceDSL.g:1262:55: (iv_ruleOtherOperation= ruleOtherOperation EOF )
            // InternalWebserviceDSL.g:1263:2: iv_ruleOtherOperation= ruleOtherOperation EOF
            {
             newCompositeNode(grammarAccess.getOtherOperationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOtherOperation=ruleOtherOperation();

            state._fsp--;

             current =iv_ruleOtherOperation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOtherOperation"


    // $ANTLR start "ruleOtherOperation"
    // InternalWebserviceDSL.g:1269:1: ruleOtherOperation returns [EObject current=null] : (otherlv_0= 'otherOperation' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'urlSuffix' ( (lv_urlSuffix_6_0= ruleEString ) ) )? (otherlv_7= 'operation' ( (lv_operation_8_0= ruleEString ) ) )? (otherlv_9= 'response' ( ( ruleEString ) ) )? (otherlv_11= 'parameters' otherlv_12= '{' ( (lv_parameters_13_0= ruleParameter ) ) (otherlv_14= ',' ( (lv_parameters_15_0= ruleParameter ) ) )* otherlv_16= '}' ) otherlv_17= '}' ) ;
    public final EObject ruleOtherOperation() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        AntlrDatatypeRuleToken lv_description_4_0 = null;

        AntlrDatatypeRuleToken lv_urlSuffix_6_0 = null;

        AntlrDatatypeRuleToken lv_operation_8_0 = null;

        EObject lv_parameters_13_0 = null;

        EObject lv_parameters_15_0 = null;



        	enterRule();

        try {
            // InternalWebserviceDSL.g:1275:2: ( (otherlv_0= 'otherOperation' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'urlSuffix' ( (lv_urlSuffix_6_0= ruleEString ) ) )? (otherlv_7= 'operation' ( (lv_operation_8_0= ruleEString ) ) )? (otherlv_9= 'response' ( ( ruleEString ) ) )? (otherlv_11= 'parameters' otherlv_12= '{' ( (lv_parameters_13_0= ruleParameter ) ) (otherlv_14= ',' ( (lv_parameters_15_0= ruleParameter ) ) )* otherlv_16= '}' ) otherlv_17= '}' ) )
            // InternalWebserviceDSL.g:1276:2: (otherlv_0= 'otherOperation' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'urlSuffix' ( (lv_urlSuffix_6_0= ruleEString ) ) )? (otherlv_7= 'operation' ( (lv_operation_8_0= ruleEString ) ) )? (otherlv_9= 'response' ( ( ruleEString ) ) )? (otherlv_11= 'parameters' otherlv_12= '{' ( (lv_parameters_13_0= ruleParameter ) ) (otherlv_14= ',' ( (lv_parameters_15_0= ruleParameter ) ) )* otherlv_16= '}' ) otherlv_17= '}' )
            {
            // InternalWebserviceDSL.g:1276:2: (otherlv_0= 'otherOperation' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'urlSuffix' ( (lv_urlSuffix_6_0= ruleEString ) ) )? (otherlv_7= 'operation' ( (lv_operation_8_0= ruleEString ) ) )? (otherlv_9= 'response' ( ( ruleEString ) ) )? (otherlv_11= 'parameters' otherlv_12= '{' ( (lv_parameters_13_0= ruleParameter ) ) (otherlv_14= ',' ( (lv_parameters_15_0= ruleParameter ) ) )* otherlv_16= '}' ) otherlv_17= '}' )
            // InternalWebserviceDSL.g:1277:3: otherlv_0= 'otherOperation' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'urlSuffix' ( (lv_urlSuffix_6_0= ruleEString ) ) )? (otherlv_7= 'operation' ( (lv_operation_8_0= ruleEString ) ) )? (otherlv_9= 'response' ( ( ruleEString ) ) )? (otherlv_11= 'parameters' otherlv_12= '{' ( (lv_parameters_13_0= ruleParameter ) ) (otherlv_14= ',' ( (lv_parameters_15_0= ruleParameter ) ) )* otherlv_16= '}' ) otherlv_17= '}'
            {
            otherlv_0=(Token)match(input,29,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getOtherOperationAccess().getOtherOperationKeyword_0());
            		
            // InternalWebserviceDSL.g:1281:3: ( (lv_name_1_0= ruleEString ) )
            // InternalWebserviceDSL.g:1282:4: (lv_name_1_0= ruleEString )
            {
            // InternalWebserviceDSL.g:1282:4: (lv_name_1_0= ruleEString )
            // InternalWebserviceDSL.g:1283:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getOtherOperationAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getOtherOperationRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.tfranke.webservice.dsl.WebserviceDSL.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_21); 

            			newLeafNode(otherlv_2, grammarAccess.getOtherOperationAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalWebserviceDSL.g:1304:3: (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==13) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // InternalWebserviceDSL.g:1305:4: otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,13,FOLLOW_3); 

                    				newLeafNode(otherlv_3, grammarAccess.getOtherOperationAccess().getDescriptionKeyword_3_0());
                    			
                    // InternalWebserviceDSL.g:1309:4: ( (lv_description_4_0= ruleEString ) )
                    // InternalWebserviceDSL.g:1310:5: (lv_description_4_0= ruleEString )
                    {
                    // InternalWebserviceDSL.g:1310:5: (lv_description_4_0= ruleEString )
                    // InternalWebserviceDSL.g:1311:6: lv_description_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getOtherOperationAccess().getDescriptionEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_22);
                    lv_description_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getOtherOperationRule());
                    						}
                    						set(
                    							current,
                    							"description",
                    							lv_description_4_0,
                    							"org.tfranke.webservice.dsl.WebserviceDSL.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalWebserviceDSL.g:1329:3: (otherlv_5= 'urlSuffix' ( (lv_urlSuffix_6_0= ruleEString ) ) )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==23) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // InternalWebserviceDSL.g:1330:4: otherlv_5= 'urlSuffix' ( (lv_urlSuffix_6_0= ruleEString ) )
                    {
                    otherlv_5=(Token)match(input,23,FOLLOW_3); 

                    				newLeafNode(otherlv_5, grammarAccess.getOtherOperationAccess().getUrlSuffixKeyword_4_0());
                    			
                    // InternalWebserviceDSL.g:1334:4: ( (lv_urlSuffix_6_0= ruleEString ) )
                    // InternalWebserviceDSL.g:1335:5: (lv_urlSuffix_6_0= ruleEString )
                    {
                    // InternalWebserviceDSL.g:1335:5: (lv_urlSuffix_6_0= ruleEString )
                    // InternalWebserviceDSL.g:1336:6: lv_urlSuffix_6_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getOtherOperationAccess().getUrlSuffixEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_23);
                    lv_urlSuffix_6_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getOtherOperationRule());
                    						}
                    						set(
                    							current,
                    							"urlSuffix",
                    							lv_urlSuffix_6_0,
                    							"org.tfranke.webservice.dsl.WebserviceDSL.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalWebserviceDSL.g:1354:3: (otherlv_7= 'operation' ( (lv_operation_8_0= ruleEString ) ) )?
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==30) ) {
                alt31=1;
            }
            switch (alt31) {
                case 1 :
                    // InternalWebserviceDSL.g:1355:4: otherlv_7= 'operation' ( (lv_operation_8_0= ruleEString ) )
                    {
                    otherlv_7=(Token)match(input,30,FOLLOW_3); 

                    				newLeafNode(otherlv_7, grammarAccess.getOtherOperationAccess().getOperationKeyword_5_0());
                    			
                    // InternalWebserviceDSL.g:1359:4: ( (lv_operation_8_0= ruleEString ) )
                    // InternalWebserviceDSL.g:1360:5: (lv_operation_8_0= ruleEString )
                    {
                    // InternalWebserviceDSL.g:1360:5: (lv_operation_8_0= ruleEString )
                    // InternalWebserviceDSL.g:1361:6: lv_operation_8_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getOtherOperationAccess().getOperationEStringParserRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_24);
                    lv_operation_8_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getOtherOperationRule());
                    						}
                    						set(
                    							current,
                    							"operation",
                    							lv_operation_8_0,
                    							"org.tfranke.webservice.dsl.WebserviceDSL.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalWebserviceDSL.g:1379:3: (otherlv_9= 'response' ( ( ruleEString ) ) )?
            int alt32=2;
            int LA32_0 = input.LA(1);

            if ( (LA32_0==24) ) {
                alt32=1;
            }
            switch (alt32) {
                case 1 :
                    // InternalWebserviceDSL.g:1380:4: otherlv_9= 'response' ( ( ruleEString ) )
                    {
                    otherlv_9=(Token)match(input,24,FOLLOW_3); 

                    				newLeafNode(otherlv_9, grammarAccess.getOtherOperationAccess().getResponseKeyword_6_0());
                    			
                    // InternalWebserviceDSL.g:1384:4: ( ( ruleEString ) )
                    // InternalWebserviceDSL.g:1385:5: ( ruleEString )
                    {
                    // InternalWebserviceDSL.g:1385:5: ( ruleEString )
                    // InternalWebserviceDSL.g:1386:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getOtherOperationRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getOtherOperationAccess().getResponseDataTypeCrossReference_6_1_0());
                    					
                    pushFollow(FOLLOW_25);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalWebserviceDSL.g:1401:3: (otherlv_11= 'parameters' otherlv_12= '{' ( (lv_parameters_13_0= ruleParameter ) ) (otherlv_14= ',' ( (lv_parameters_15_0= ruleParameter ) ) )* otherlv_16= '}' )
            // InternalWebserviceDSL.g:1402:4: otherlv_11= 'parameters' otherlv_12= '{' ( (lv_parameters_13_0= ruleParameter ) ) (otherlv_14= ',' ( (lv_parameters_15_0= ruleParameter ) ) )* otherlv_16= '}'
            {
            otherlv_11=(Token)match(input,25,FOLLOW_4); 

            				newLeafNode(otherlv_11, grammarAccess.getOtherOperationAccess().getParametersKeyword_7_0());
            			
            otherlv_12=(Token)match(input,12,FOLLOW_20); 

            				newLeafNode(otherlv_12, grammarAccess.getOtherOperationAccess().getLeftCurlyBracketKeyword_7_1());
            			
            // InternalWebserviceDSL.g:1410:4: ( (lv_parameters_13_0= ruleParameter ) )
            // InternalWebserviceDSL.g:1411:5: (lv_parameters_13_0= ruleParameter )
            {
            // InternalWebserviceDSL.g:1411:5: (lv_parameters_13_0= ruleParameter )
            // InternalWebserviceDSL.g:1412:6: lv_parameters_13_0= ruleParameter
            {

            						newCompositeNode(grammarAccess.getOtherOperationAccess().getParametersParameterParserRuleCall_7_2_0());
            					
            pushFollow(FOLLOW_9);
            lv_parameters_13_0=ruleParameter();

            state._fsp--;


            						if (current==null) {
            							current = createModelElementForParent(grammarAccess.getOtherOperationRule());
            						}
            						add(
            							current,
            							"parameters",
            							lv_parameters_13_0,
            							"org.tfranke.webservice.dsl.WebserviceDSL.Parameter");
            						afterParserOrEnumRuleCall();
            					

            }


            }

            // InternalWebserviceDSL.g:1429:4: (otherlv_14= ',' ( (lv_parameters_15_0= ruleParameter ) ) )*
            loop33:
            do {
                int alt33=2;
                int LA33_0 = input.LA(1);

                if ( (LA33_0==16) ) {
                    alt33=1;
                }


                switch (alt33) {
            	case 1 :
            	    // InternalWebserviceDSL.g:1430:5: otherlv_14= ',' ( (lv_parameters_15_0= ruleParameter ) )
            	    {
            	    otherlv_14=(Token)match(input,16,FOLLOW_20); 

            	    					newLeafNode(otherlv_14, grammarAccess.getOtherOperationAccess().getCommaKeyword_7_3_0());
            	    				
            	    // InternalWebserviceDSL.g:1434:5: ( (lv_parameters_15_0= ruleParameter ) )
            	    // InternalWebserviceDSL.g:1435:6: (lv_parameters_15_0= ruleParameter )
            	    {
            	    // InternalWebserviceDSL.g:1435:6: (lv_parameters_15_0= ruleParameter )
            	    // InternalWebserviceDSL.g:1436:7: lv_parameters_15_0= ruleParameter
            	    {

            	    							newCompositeNode(grammarAccess.getOtherOperationAccess().getParametersParameterParserRuleCall_7_3_1_0());
            	    						
            	    pushFollow(FOLLOW_9);
            	    lv_parameters_15_0=ruleParameter();

            	    state._fsp--;


            	    							if (current==null) {
            	    								current = createModelElementForParent(grammarAccess.getOtherOperationRule());
            	    							}
            	    							add(
            	    								current,
            	    								"parameters",
            	    								lv_parameters_15_0,
            	    								"org.tfranke.webservice.dsl.WebserviceDSL.Parameter");
            	    							afterParserOrEnumRuleCall();
            	    						

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop33;
                }
            } while (true);

            otherlv_16=(Token)match(input,17,FOLLOW_12); 

            				newLeafNode(otherlv_16, grammarAccess.getOtherOperationAccess().getRightCurlyBracketKeyword_7_4());
            			

            }

            otherlv_17=(Token)match(input,17,FOLLOW_2); 

            			newLeafNode(otherlv_17, grammarAccess.getOtherOperationAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOtherOperation"


    // $ANTLR start "entryRuleObjectDataType"
    // InternalWebserviceDSL.g:1467:1: entryRuleObjectDataType returns [EObject current=null] : iv_ruleObjectDataType= ruleObjectDataType EOF ;
    public final EObject entryRuleObjectDataType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleObjectDataType = null;


        try {
            // InternalWebserviceDSL.g:1467:55: (iv_ruleObjectDataType= ruleObjectDataType EOF )
            // InternalWebserviceDSL.g:1468:2: iv_ruleObjectDataType= ruleObjectDataType EOF
            {
             newCompositeNode(grammarAccess.getObjectDataTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleObjectDataType=ruleObjectDataType();

            state._fsp--;

             current =iv_ruleObjectDataType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleObjectDataType"


    // $ANTLR start "ruleObjectDataType"
    // InternalWebserviceDSL.g:1474:1: ruleObjectDataType returns [EObject current=null] : ( ( (lv_required_0_0= 'required' ) )? otherlv_1= 'ObjectDataType' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'datatypes' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' otherlv_10= '}' ) ;
    public final EObject ruleObjectDataType() throws RecognitionException {
        EObject current = null;

        Token lv_required_0_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;



        	enterRule();

        try {
            // InternalWebserviceDSL.g:1480:2: ( ( ( (lv_required_0_0= 'required' ) )? otherlv_1= 'ObjectDataType' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'datatypes' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' otherlv_10= '}' ) )
            // InternalWebserviceDSL.g:1481:2: ( ( (lv_required_0_0= 'required' ) )? otherlv_1= 'ObjectDataType' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'datatypes' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' otherlv_10= '}' )
            {
            // InternalWebserviceDSL.g:1481:2: ( ( (lv_required_0_0= 'required' ) )? otherlv_1= 'ObjectDataType' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'datatypes' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' otherlv_10= '}' )
            // InternalWebserviceDSL.g:1482:3: ( (lv_required_0_0= 'required' ) )? otherlv_1= 'ObjectDataType' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'datatypes' otherlv_5= '(' ( ( ruleEString ) ) (otherlv_7= ',' ( ( ruleEString ) ) )* otherlv_9= ')' otherlv_10= '}'
            {
            // InternalWebserviceDSL.g:1482:3: ( (lv_required_0_0= 'required' ) )?
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==19) ) {
                alt34=1;
            }
            switch (alt34) {
                case 1 :
                    // InternalWebserviceDSL.g:1483:4: (lv_required_0_0= 'required' )
                    {
                    // InternalWebserviceDSL.g:1483:4: (lv_required_0_0= 'required' )
                    // InternalWebserviceDSL.g:1484:5: lv_required_0_0= 'required'
                    {
                    lv_required_0_0=(Token)match(input,19,FOLLOW_26); 

                    					newLeafNode(lv_required_0_0, grammarAccess.getObjectDataTypeAccess().getRequiredRequiredKeyword_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getObjectDataTypeRule());
                    					}
                    					setWithLastConsumed(current, "required", lv_required_0_0 != null, "required");
                    				

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,31,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getObjectDataTypeAccess().getObjectDataTypeKeyword_1());
            		
            // InternalWebserviceDSL.g:1500:3: ( (lv_name_2_0= ruleEString ) )
            // InternalWebserviceDSL.g:1501:4: (lv_name_2_0= ruleEString )
            {
            // InternalWebserviceDSL.g:1501:4: (lv_name_2_0= ruleEString )
            // InternalWebserviceDSL.g:1502:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getObjectDataTypeAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getObjectDataTypeRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.tfranke.webservice.dsl.WebserviceDSL.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_7); 

            			newLeafNode(otherlv_3, grammarAccess.getObjectDataTypeAccess().getLeftCurlyBracketKeyword_3());
            		
            otherlv_4=(Token)match(input,15,FOLLOW_27); 

            			newLeafNode(otherlv_4, grammarAccess.getObjectDataTypeAccess().getDatatypesKeyword_4());
            		
            otherlv_5=(Token)match(input,32,FOLLOW_3); 

            			newLeafNode(otherlv_5, grammarAccess.getObjectDataTypeAccess().getLeftParenthesisKeyword_5());
            		
            // InternalWebserviceDSL.g:1531:3: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:1532:4: ( ruleEString )
            {
            // InternalWebserviceDSL.g:1532:4: ( ruleEString )
            // InternalWebserviceDSL.g:1533:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getObjectDataTypeRule());
            					}
            				

            					newCompositeNode(grammarAccess.getObjectDataTypeAccess().getDatatypesDataTypeCrossReference_6_0());
            				
            pushFollow(FOLLOW_28);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalWebserviceDSL.g:1547:3: (otherlv_7= ',' ( ( ruleEString ) ) )*
            loop35:
            do {
                int alt35=2;
                int LA35_0 = input.LA(1);

                if ( (LA35_0==16) ) {
                    alt35=1;
                }


                switch (alt35) {
            	case 1 :
            	    // InternalWebserviceDSL.g:1548:4: otherlv_7= ',' ( ( ruleEString ) )
            	    {
            	    otherlv_7=(Token)match(input,16,FOLLOW_3); 

            	    				newLeafNode(otherlv_7, grammarAccess.getObjectDataTypeAccess().getCommaKeyword_7_0());
            	    			
            	    // InternalWebserviceDSL.g:1552:4: ( ( ruleEString ) )
            	    // InternalWebserviceDSL.g:1553:5: ( ruleEString )
            	    {
            	    // InternalWebserviceDSL.g:1553:5: ( ruleEString )
            	    // InternalWebserviceDSL.g:1554:6: ruleEString
            	    {

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getObjectDataTypeRule());
            	    						}
            	    					

            	    						newCompositeNode(grammarAccess.getObjectDataTypeAccess().getDatatypesDataTypeCrossReference_7_1_0());
            	    					
            	    pushFollow(FOLLOW_28);
            	    ruleEString();

            	    state._fsp--;


            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop35;
                }
            } while (true);

            otherlv_9=(Token)match(input,33,FOLLOW_12); 

            			newLeafNode(otherlv_9, grammarAccess.getObjectDataTypeAccess().getRightParenthesisKeyword_8());
            		
            otherlv_10=(Token)match(input,17,FOLLOW_2); 

            			newLeafNode(otherlv_10, grammarAccess.getObjectDataTypeAccess().getRightCurlyBracketKeyword_9());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleObjectDataType"


    // $ANTLR start "entryRuleCollectionDataType"
    // InternalWebserviceDSL.g:1581:1: entryRuleCollectionDataType returns [EObject current=null] : iv_ruleCollectionDataType= ruleCollectionDataType EOF ;
    public final EObject entryRuleCollectionDataType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCollectionDataType = null;


        try {
            // InternalWebserviceDSL.g:1581:59: (iv_ruleCollectionDataType= ruleCollectionDataType EOF )
            // InternalWebserviceDSL.g:1582:2: iv_ruleCollectionDataType= ruleCollectionDataType EOF
            {
             newCompositeNode(grammarAccess.getCollectionDataTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCollectionDataType=ruleCollectionDataType();

            state._fsp--;

             current =iv_ruleCollectionDataType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCollectionDataType"


    // $ANTLR start "ruleCollectionDataType"
    // InternalWebserviceDSL.g:1588:1: ruleCollectionDataType returns [EObject current=null] : ( ( (lv_required_0_0= 'required' ) )? otherlv_1= 'CollectionDataType' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'listItem' ( ( ruleEString ) ) otherlv_6= '}' ) ;
    public final EObject ruleCollectionDataType() throws RecognitionException {
        EObject current = null;

        Token lv_required_0_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;



        	enterRule();

        try {
            // InternalWebserviceDSL.g:1594:2: ( ( ( (lv_required_0_0= 'required' ) )? otherlv_1= 'CollectionDataType' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'listItem' ( ( ruleEString ) ) otherlv_6= '}' ) )
            // InternalWebserviceDSL.g:1595:2: ( ( (lv_required_0_0= 'required' ) )? otherlv_1= 'CollectionDataType' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'listItem' ( ( ruleEString ) ) otherlv_6= '}' )
            {
            // InternalWebserviceDSL.g:1595:2: ( ( (lv_required_0_0= 'required' ) )? otherlv_1= 'CollectionDataType' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'listItem' ( ( ruleEString ) ) otherlv_6= '}' )
            // InternalWebserviceDSL.g:1596:3: ( (lv_required_0_0= 'required' ) )? otherlv_1= 'CollectionDataType' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'listItem' ( ( ruleEString ) ) otherlv_6= '}'
            {
            // InternalWebserviceDSL.g:1596:3: ( (lv_required_0_0= 'required' ) )?
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==19) ) {
                alt36=1;
            }
            switch (alt36) {
                case 1 :
                    // InternalWebserviceDSL.g:1597:4: (lv_required_0_0= 'required' )
                    {
                    // InternalWebserviceDSL.g:1597:4: (lv_required_0_0= 'required' )
                    // InternalWebserviceDSL.g:1598:5: lv_required_0_0= 'required'
                    {
                    lv_required_0_0=(Token)match(input,19,FOLLOW_29); 

                    					newLeafNode(lv_required_0_0, grammarAccess.getCollectionDataTypeAccess().getRequiredRequiredKeyword_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getCollectionDataTypeRule());
                    					}
                    					setWithLastConsumed(current, "required", lv_required_0_0 != null, "required");
                    				

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,34,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getCollectionDataTypeAccess().getCollectionDataTypeKeyword_1());
            		
            // InternalWebserviceDSL.g:1614:3: ( (lv_name_2_0= ruleEString ) )
            // InternalWebserviceDSL.g:1615:4: (lv_name_2_0= ruleEString )
            {
            // InternalWebserviceDSL.g:1615:4: (lv_name_2_0= ruleEString )
            // InternalWebserviceDSL.g:1616:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getCollectionDataTypeAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCollectionDataTypeRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.tfranke.webservice.dsl.WebserviceDSL.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_30); 

            			newLeafNode(otherlv_3, grammarAccess.getCollectionDataTypeAccess().getLeftCurlyBracketKeyword_3());
            		
            otherlv_4=(Token)match(input,35,FOLLOW_3); 

            			newLeafNode(otherlv_4, grammarAccess.getCollectionDataTypeAccess().getListItemKeyword_4());
            		
            // InternalWebserviceDSL.g:1641:3: ( ( ruleEString ) )
            // InternalWebserviceDSL.g:1642:4: ( ruleEString )
            {
            // InternalWebserviceDSL.g:1642:4: ( ruleEString )
            // InternalWebserviceDSL.g:1643:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getCollectionDataTypeRule());
            					}
            				

            					newCompositeNode(grammarAccess.getCollectionDataTypeAccess().getListItemDataTypeCrossReference_5_0());
            				
            pushFollow(FOLLOW_12);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_6=(Token)match(input,17,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getCollectionDataTypeAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCollectionDataType"


    // $ANTLR start "entryRuleStringData"
    // InternalWebserviceDSL.g:1665:1: entryRuleStringData returns [EObject current=null] : iv_ruleStringData= ruleStringData EOF ;
    public final EObject entryRuleStringData() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStringData = null;


        try {
            // InternalWebserviceDSL.g:1665:51: (iv_ruleStringData= ruleStringData EOF )
            // InternalWebserviceDSL.g:1666:2: iv_ruleStringData= ruleStringData EOF
            {
             newCompositeNode(grammarAccess.getStringDataRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStringData=ruleStringData();

            state._fsp--;

             current =iv_ruleStringData; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringData"


    // $ANTLR start "ruleStringData"
    // InternalWebserviceDSL.g:1672:1: ruleStringData returns [EObject current=null] : ( () ( (lv_required_1_0= 'required' ) )? otherlv_2= 'StringData' ( (lv_name_3_0= ruleEString ) ) ) ;
    public final EObject ruleStringData() throws RecognitionException {
        EObject current = null;

        Token lv_required_1_0=null;
        Token otherlv_2=null;
        AntlrDatatypeRuleToken lv_name_3_0 = null;



        	enterRule();

        try {
            // InternalWebserviceDSL.g:1678:2: ( ( () ( (lv_required_1_0= 'required' ) )? otherlv_2= 'StringData' ( (lv_name_3_0= ruleEString ) ) ) )
            // InternalWebserviceDSL.g:1679:2: ( () ( (lv_required_1_0= 'required' ) )? otherlv_2= 'StringData' ( (lv_name_3_0= ruleEString ) ) )
            {
            // InternalWebserviceDSL.g:1679:2: ( () ( (lv_required_1_0= 'required' ) )? otherlv_2= 'StringData' ( (lv_name_3_0= ruleEString ) ) )
            // InternalWebserviceDSL.g:1680:3: () ( (lv_required_1_0= 'required' ) )? otherlv_2= 'StringData' ( (lv_name_3_0= ruleEString ) )
            {
            // InternalWebserviceDSL.g:1680:3: ()
            // InternalWebserviceDSL.g:1681:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getStringDataAccess().getStringDataAction_0(),
            					current);
            			

            }

            // InternalWebserviceDSL.g:1687:3: ( (lv_required_1_0= 'required' ) )?
            int alt37=2;
            int LA37_0 = input.LA(1);

            if ( (LA37_0==19) ) {
                alt37=1;
            }
            switch (alt37) {
                case 1 :
                    // InternalWebserviceDSL.g:1688:4: (lv_required_1_0= 'required' )
                    {
                    // InternalWebserviceDSL.g:1688:4: (lv_required_1_0= 'required' )
                    // InternalWebserviceDSL.g:1689:5: lv_required_1_0= 'required'
                    {
                    lv_required_1_0=(Token)match(input,19,FOLLOW_31); 

                    					newLeafNode(lv_required_1_0, grammarAccess.getStringDataAccess().getRequiredRequiredKeyword_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getStringDataRule());
                    					}
                    					setWithLastConsumed(current, "required", lv_required_1_0 != null, "required");
                    				

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,36,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getStringDataAccess().getStringDataKeyword_2());
            		
            // InternalWebserviceDSL.g:1705:3: ( (lv_name_3_0= ruleEString ) )
            // InternalWebserviceDSL.g:1706:4: (lv_name_3_0= ruleEString )
            {
            // InternalWebserviceDSL.g:1706:4: (lv_name_3_0= ruleEString )
            // InternalWebserviceDSL.g:1707:5: lv_name_3_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getStringDataAccess().getNameEStringParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_3_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getStringDataRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_3_0,
            						"org.tfranke.webservice.dsl.WebserviceDSL.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringData"


    // $ANTLR start "entryRuleIntegerData"
    // InternalWebserviceDSL.g:1728:1: entryRuleIntegerData returns [EObject current=null] : iv_ruleIntegerData= ruleIntegerData EOF ;
    public final EObject entryRuleIntegerData() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerData = null;


        try {
            // InternalWebserviceDSL.g:1728:52: (iv_ruleIntegerData= ruleIntegerData EOF )
            // InternalWebserviceDSL.g:1729:2: iv_ruleIntegerData= ruleIntegerData EOF
            {
             newCompositeNode(grammarAccess.getIntegerDataRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleIntegerData=ruleIntegerData();

            state._fsp--;

             current =iv_ruleIntegerData; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerData"


    // $ANTLR start "ruleIntegerData"
    // InternalWebserviceDSL.g:1735:1: ruleIntegerData returns [EObject current=null] : ( () ( (lv_required_1_0= 'required' ) )? otherlv_2= 'IntegerData' ( (lv_name_3_0= ruleEString ) ) ) ;
    public final EObject ruleIntegerData() throws RecognitionException {
        EObject current = null;

        Token lv_required_1_0=null;
        Token otherlv_2=null;
        AntlrDatatypeRuleToken lv_name_3_0 = null;



        	enterRule();

        try {
            // InternalWebserviceDSL.g:1741:2: ( ( () ( (lv_required_1_0= 'required' ) )? otherlv_2= 'IntegerData' ( (lv_name_3_0= ruleEString ) ) ) )
            // InternalWebserviceDSL.g:1742:2: ( () ( (lv_required_1_0= 'required' ) )? otherlv_2= 'IntegerData' ( (lv_name_3_0= ruleEString ) ) )
            {
            // InternalWebserviceDSL.g:1742:2: ( () ( (lv_required_1_0= 'required' ) )? otherlv_2= 'IntegerData' ( (lv_name_3_0= ruleEString ) ) )
            // InternalWebserviceDSL.g:1743:3: () ( (lv_required_1_0= 'required' ) )? otherlv_2= 'IntegerData' ( (lv_name_3_0= ruleEString ) )
            {
            // InternalWebserviceDSL.g:1743:3: ()
            // InternalWebserviceDSL.g:1744:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getIntegerDataAccess().getIntegerDataAction_0(),
            					current);
            			

            }

            // InternalWebserviceDSL.g:1750:3: ( (lv_required_1_0= 'required' ) )?
            int alt38=2;
            int LA38_0 = input.LA(1);

            if ( (LA38_0==19) ) {
                alt38=1;
            }
            switch (alt38) {
                case 1 :
                    // InternalWebserviceDSL.g:1751:4: (lv_required_1_0= 'required' )
                    {
                    // InternalWebserviceDSL.g:1751:4: (lv_required_1_0= 'required' )
                    // InternalWebserviceDSL.g:1752:5: lv_required_1_0= 'required'
                    {
                    lv_required_1_0=(Token)match(input,19,FOLLOW_32); 

                    					newLeafNode(lv_required_1_0, grammarAccess.getIntegerDataAccess().getRequiredRequiredKeyword_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getIntegerDataRule());
                    					}
                    					setWithLastConsumed(current, "required", lv_required_1_0 != null, "required");
                    				

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,37,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getIntegerDataAccess().getIntegerDataKeyword_2());
            		
            // InternalWebserviceDSL.g:1768:3: ( (lv_name_3_0= ruleEString ) )
            // InternalWebserviceDSL.g:1769:4: (lv_name_3_0= ruleEString )
            {
            // InternalWebserviceDSL.g:1769:4: (lv_name_3_0= ruleEString )
            // InternalWebserviceDSL.g:1770:5: lv_name_3_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getIntegerDataAccess().getNameEStringParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_3_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getIntegerDataRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_3_0,
            						"org.tfranke.webservice.dsl.WebserviceDSL.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerData"


    // $ANTLR start "entryRuleFloatData"
    // InternalWebserviceDSL.g:1791:1: entryRuleFloatData returns [EObject current=null] : iv_ruleFloatData= ruleFloatData EOF ;
    public final EObject entryRuleFloatData() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFloatData = null;


        try {
            // InternalWebserviceDSL.g:1791:50: (iv_ruleFloatData= ruleFloatData EOF )
            // InternalWebserviceDSL.g:1792:2: iv_ruleFloatData= ruleFloatData EOF
            {
             newCompositeNode(grammarAccess.getFloatDataRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFloatData=ruleFloatData();

            state._fsp--;

             current =iv_ruleFloatData; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFloatData"


    // $ANTLR start "ruleFloatData"
    // InternalWebserviceDSL.g:1798:1: ruleFloatData returns [EObject current=null] : ( () ( (lv_required_1_0= 'required' ) )? otherlv_2= 'FloatData' ( (lv_name_3_0= ruleEString ) ) ) ;
    public final EObject ruleFloatData() throws RecognitionException {
        EObject current = null;

        Token lv_required_1_0=null;
        Token otherlv_2=null;
        AntlrDatatypeRuleToken lv_name_3_0 = null;



        	enterRule();

        try {
            // InternalWebserviceDSL.g:1804:2: ( ( () ( (lv_required_1_0= 'required' ) )? otherlv_2= 'FloatData' ( (lv_name_3_0= ruleEString ) ) ) )
            // InternalWebserviceDSL.g:1805:2: ( () ( (lv_required_1_0= 'required' ) )? otherlv_2= 'FloatData' ( (lv_name_3_0= ruleEString ) ) )
            {
            // InternalWebserviceDSL.g:1805:2: ( () ( (lv_required_1_0= 'required' ) )? otherlv_2= 'FloatData' ( (lv_name_3_0= ruleEString ) ) )
            // InternalWebserviceDSL.g:1806:3: () ( (lv_required_1_0= 'required' ) )? otherlv_2= 'FloatData' ( (lv_name_3_0= ruleEString ) )
            {
            // InternalWebserviceDSL.g:1806:3: ()
            // InternalWebserviceDSL.g:1807:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getFloatDataAccess().getFloatDataAction_0(),
            					current);
            			

            }

            // InternalWebserviceDSL.g:1813:3: ( (lv_required_1_0= 'required' ) )?
            int alt39=2;
            int LA39_0 = input.LA(1);

            if ( (LA39_0==19) ) {
                alt39=1;
            }
            switch (alt39) {
                case 1 :
                    // InternalWebserviceDSL.g:1814:4: (lv_required_1_0= 'required' )
                    {
                    // InternalWebserviceDSL.g:1814:4: (lv_required_1_0= 'required' )
                    // InternalWebserviceDSL.g:1815:5: lv_required_1_0= 'required'
                    {
                    lv_required_1_0=(Token)match(input,19,FOLLOW_33); 

                    					newLeafNode(lv_required_1_0, grammarAccess.getFloatDataAccess().getRequiredRequiredKeyword_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getFloatDataRule());
                    					}
                    					setWithLastConsumed(current, "required", lv_required_1_0 != null, "required");
                    				

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,38,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getFloatDataAccess().getFloatDataKeyword_2());
            		
            // InternalWebserviceDSL.g:1831:3: ( (lv_name_3_0= ruleEString ) )
            // InternalWebserviceDSL.g:1832:4: (lv_name_3_0= ruleEString )
            {
            // InternalWebserviceDSL.g:1832:4: (lv_name_3_0= ruleEString )
            // InternalWebserviceDSL.g:1833:5: lv_name_3_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getFloatDataAccess().getNameEStringParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_3_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getFloatDataRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_3_0,
            						"org.tfranke.webservice.dsl.WebserviceDSL.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFloatData"


    // $ANTLR start "entryRuleBooleanData"
    // InternalWebserviceDSL.g:1854:1: entryRuleBooleanData returns [EObject current=null] : iv_ruleBooleanData= ruleBooleanData EOF ;
    public final EObject entryRuleBooleanData() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanData = null;


        try {
            // InternalWebserviceDSL.g:1854:52: (iv_ruleBooleanData= ruleBooleanData EOF )
            // InternalWebserviceDSL.g:1855:2: iv_ruleBooleanData= ruleBooleanData EOF
            {
             newCompositeNode(grammarAccess.getBooleanDataRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBooleanData=ruleBooleanData();

            state._fsp--;

             current =iv_ruleBooleanData; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanData"


    // $ANTLR start "ruleBooleanData"
    // InternalWebserviceDSL.g:1861:1: ruleBooleanData returns [EObject current=null] : ( () ( (lv_required_1_0= 'required' ) )? otherlv_2= 'BooleanData' ( (lv_name_3_0= ruleEString ) ) ) ;
    public final EObject ruleBooleanData() throws RecognitionException {
        EObject current = null;

        Token lv_required_1_0=null;
        Token otherlv_2=null;
        AntlrDatatypeRuleToken lv_name_3_0 = null;



        	enterRule();

        try {
            // InternalWebserviceDSL.g:1867:2: ( ( () ( (lv_required_1_0= 'required' ) )? otherlv_2= 'BooleanData' ( (lv_name_3_0= ruleEString ) ) ) )
            // InternalWebserviceDSL.g:1868:2: ( () ( (lv_required_1_0= 'required' ) )? otherlv_2= 'BooleanData' ( (lv_name_3_0= ruleEString ) ) )
            {
            // InternalWebserviceDSL.g:1868:2: ( () ( (lv_required_1_0= 'required' ) )? otherlv_2= 'BooleanData' ( (lv_name_3_0= ruleEString ) ) )
            // InternalWebserviceDSL.g:1869:3: () ( (lv_required_1_0= 'required' ) )? otherlv_2= 'BooleanData' ( (lv_name_3_0= ruleEString ) )
            {
            // InternalWebserviceDSL.g:1869:3: ()
            // InternalWebserviceDSL.g:1870:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getBooleanDataAccess().getBooleanDataAction_0(),
            					current);
            			

            }

            // InternalWebserviceDSL.g:1876:3: ( (lv_required_1_0= 'required' ) )?
            int alt40=2;
            int LA40_0 = input.LA(1);

            if ( (LA40_0==19) ) {
                alt40=1;
            }
            switch (alt40) {
                case 1 :
                    // InternalWebserviceDSL.g:1877:4: (lv_required_1_0= 'required' )
                    {
                    // InternalWebserviceDSL.g:1877:4: (lv_required_1_0= 'required' )
                    // InternalWebserviceDSL.g:1878:5: lv_required_1_0= 'required'
                    {
                    lv_required_1_0=(Token)match(input,19,FOLLOW_34); 

                    					newLeafNode(lv_required_1_0, grammarAccess.getBooleanDataAccess().getRequiredRequiredKeyword_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getBooleanDataRule());
                    					}
                    					setWithLastConsumed(current, "required", lv_required_1_0 != null, "required");
                    				

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,39,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getBooleanDataAccess().getBooleanDataKeyword_2());
            		
            // InternalWebserviceDSL.g:1894:3: ( (lv_name_3_0= ruleEString ) )
            // InternalWebserviceDSL.g:1895:4: (lv_name_3_0= ruleEString )
            {
            // InternalWebserviceDSL.g:1895:4: (lv_name_3_0= ruleEString )
            // InternalWebserviceDSL.g:1896:5: lv_name_3_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getBooleanDataAccess().getNameEStringParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_3_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getBooleanDataRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_3_0,
            						"org.tfranke.webservice.dsl.WebserviceDSL.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanData"


    // $ANTLR start "entryRuleByteData"
    // InternalWebserviceDSL.g:1917:1: entryRuleByteData returns [EObject current=null] : iv_ruleByteData= ruleByteData EOF ;
    public final EObject entryRuleByteData() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleByteData = null;


        try {
            // InternalWebserviceDSL.g:1917:49: (iv_ruleByteData= ruleByteData EOF )
            // InternalWebserviceDSL.g:1918:2: iv_ruleByteData= ruleByteData EOF
            {
             newCompositeNode(grammarAccess.getByteDataRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleByteData=ruleByteData();

            state._fsp--;

             current =iv_ruleByteData; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleByteData"


    // $ANTLR start "ruleByteData"
    // InternalWebserviceDSL.g:1924:1: ruleByteData returns [EObject current=null] : ( () ( (lv_required_1_0= 'required' ) )? otherlv_2= 'ByteData' ( (lv_name_3_0= ruleEString ) ) ) ;
    public final EObject ruleByteData() throws RecognitionException {
        EObject current = null;

        Token lv_required_1_0=null;
        Token otherlv_2=null;
        AntlrDatatypeRuleToken lv_name_3_0 = null;



        	enterRule();

        try {
            // InternalWebserviceDSL.g:1930:2: ( ( () ( (lv_required_1_0= 'required' ) )? otherlv_2= 'ByteData' ( (lv_name_3_0= ruleEString ) ) ) )
            // InternalWebserviceDSL.g:1931:2: ( () ( (lv_required_1_0= 'required' ) )? otherlv_2= 'ByteData' ( (lv_name_3_0= ruleEString ) ) )
            {
            // InternalWebserviceDSL.g:1931:2: ( () ( (lv_required_1_0= 'required' ) )? otherlv_2= 'ByteData' ( (lv_name_3_0= ruleEString ) ) )
            // InternalWebserviceDSL.g:1932:3: () ( (lv_required_1_0= 'required' ) )? otherlv_2= 'ByteData' ( (lv_name_3_0= ruleEString ) )
            {
            // InternalWebserviceDSL.g:1932:3: ()
            // InternalWebserviceDSL.g:1933:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getByteDataAccess().getByteDataAction_0(),
            					current);
            			

            }

            // InternalWebserviceDSL.g:1939:3: ( (lv_required_1_0= 'required' ) )?
            int alt41=2;
            int LA41_0 = input.LA(1);

            if ( (LA41_0==19) ) {
                alt41=1;
            }
            switch (alt41) {
                case 1 :
                    // InternalWebserviceDSL.g:1940:4: (lv_required_1_0= 'required' )
                    {
                    // InternalWebserviceDSL.g:1940:4: (lv_required_1_0= 'required' )
                    // InternalWebserviceDSL.g:1941:5: lv_required_1_0= 'required'
                    {
                    lv_required_1_0=(Token)match(input,19,FOLLOW_35); 

                    					newLeafNode(lv_required_1_0, grammarAccess.getByteDataAccess().getRequiredRequiredKeyword_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getByteDataRule());
                    					}
                    					setWithLastConsumed(current, "required", lv_required_1_0 != null, "required");
                    				

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,40,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getByteDataAccess().getByteDataKeyword_2());
            		
            // InternalWebserviceDSL.g:1957:3: ( (lv_name_3_0= ruleEString ) )
            // InternalWebserviceDSL.g:1958:4: (lv_name_3_0= ruleEString )
            {
            // InternalWebserviceDSL.g:1958:4: (lv_name_3_0= ruleEString )
            // InternalWebserviceDSL.g:1959:5: lv_name_3_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getByteDataAccess().getNameEStringParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_3_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getByteDataRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_3_0,
            						"org.tfranke.webservice.dsl.WebserviceDSL.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleByteData"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000006000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x000001F480080000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000030000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x000000003C400000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000202000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000003822000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000003820000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000003020000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000002020000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000180000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000043802000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000043800000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000043000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000003000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000200010000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000010000000000L});

}