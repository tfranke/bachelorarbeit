/**
 */
package webservice;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Read Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see webservice.WebservicePackage#getReadOperation()
 * @model
 * @generated
 */
public interface ReadOperation extends Operation {
} // ReadOperation
