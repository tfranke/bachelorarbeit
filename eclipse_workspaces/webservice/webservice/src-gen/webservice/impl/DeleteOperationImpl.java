/**
 */
package webservice.impl;

import org.eclipse.emf.ecore.EClass;

import webservice.DeleteOperation;
import webservice.WebservicePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Delete Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DeleteOperationImpl extends OperationImpl implements DeleteOperation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeleteOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebservicePackage.Literals.DELETE_OPERATION;
	}

} //DeleteOperationImpl
