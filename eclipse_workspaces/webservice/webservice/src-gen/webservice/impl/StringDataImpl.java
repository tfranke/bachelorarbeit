/**
 */
package webservice.impl;

import org.eclipse.emf.ecore.EClass;

import webservice.StringData;
import webservice.WebservicePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>String Data</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class StringDataImpl extends DataTypeImpl implements StringData {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StringDataImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebservicePackage.Literals.STRING_DATA;
	}

} //StringDataImpl
