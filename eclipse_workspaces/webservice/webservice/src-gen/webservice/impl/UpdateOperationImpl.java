/**
 */
package webservice.impl;

import org.eclipse.emf.ecore.EClass;

import webservice.UpdateOperation;
import webservice.WebservicePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Update Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class UpdateOperationImpl extends OperationImpl implements UpdateOperation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UpdateOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebservicePackage.Literals.UPDATE_OPERATION;
	}

} //UpdateOperationImpl
