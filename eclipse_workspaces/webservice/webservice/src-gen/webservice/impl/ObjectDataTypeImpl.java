/**
 */
package webservice.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import webservice.DataType;
import webservice.ObjectDataType;
import webservice.WebservicePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Object Data Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link webservice.impl.ObjectDataTypeImpl#getDatatypes <em>Datatypes</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ObjectDataTypeImpl extends DataTypeImpl implements ObjectDataType {
	/**
	 * The cached value of the '{@link #getDatatypes() <em>Datatypes</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDatatypes()
	 * @generated
	 * @ordered
	 */
	protected EList<DataType> datatypes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ObjectDataTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebservicePackage.Literals.OBJECT_DATA_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DataType> getDatatypes() {
		if (datatypes == null) {
			datatypes = new EObjectResolvingEList<DataType>(DataType.class, this,
					WebservicePackage.OBJECT_DATA_TYPE__DATATYPES);
		}
		return datatypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case WebservicePackage.OBJECT_DATA_TYPE__DATATYPES:
			return getDatatypes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case WebservicePackage.OBJECT_DATA_TYPE__DATATYPES:
			getDatatypes().clear();
			getDatatypes().addAll((Collection<? extends DataType>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case WebservicePackage.OBJECT_DATA_TYPE__DATATYPES:
			getDatatypes().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case WebservicePackage.OBJECT_DATA_TYPE__DATATYPES:
			return datatypes != null && !datatypes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ObjectDataTypeImpl
