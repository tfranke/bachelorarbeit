/**
 */
package webservice.impl;

import org.eclipse.emf.ecore.EClass;

import webservice.CreateOperation;
import webservice.WebservicePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Create Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CreateOperationImpl extends OperationImpl implements CreateOperation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CreateOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebservicePackage.Literals.CREATE_OPERATION;
	}

} //CreateOperationImpl
