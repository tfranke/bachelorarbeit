/**
 */
package webservice.impl;

import org.eclipse.emf.ecore.EClass;

import webservice.FloatData;
import webservice.WebservicePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Float Data</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FloatDataImpl extends DataTypeImpl implements FloatData {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FloatDataImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebservicePackage.Literals.FLOAT_DATA;
	}

} //FloatDataImpl
