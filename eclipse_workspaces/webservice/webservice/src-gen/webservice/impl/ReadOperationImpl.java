/**
 */
package webservice.impl;

import org.eclipse.emf.ecore.EClass;

import webservice.ReadOperation;
import webservice.WebservicePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Read Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ReadOperationImpl extends OperationImpl implements ReadOperation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReadOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebservicePackage.Literals.READ_OPERATION;
	}

} //ReadOperationImpl
