/**
 */
package webservice.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import webservice.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class WebserviceFactoryImpl extends EFactoryImpl implements WebserviceFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static WebserviceFactory init() {
		try {
			WebserviceFactory theWebserviceFactory = (WebserviceFactory) EPackage.Registry.INSTANCE
					.getEFactory(WebservicePackage.eNS_URI);
			if (theWebserviceFactory != null) {
				return theWebserviceFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new WebserviceFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WebserviceFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case WebservicePackage.API:
			return createAPI();
		case WebservicePackage.PARAMETER:
			return createParameter();
		case WebservicePackage.OBJECT_DATA_TYPE:
			return createObjectDataType();
		case WebservicePackage.COLLECTION_DATA_TYPE:
			return createCollectionDataType();
		case WebservicePackage.STRING_DATA:
			return createStringData();
		case WebservicePackage.INTEGER_DATA:
			return createIntegerData();
		case WebservicePackage.FLOAT_DATA:
			return createFloatData();
		case WebservicePackage.BOOLEAN_DATA:
			return createBooleanData();
		case WebservicePackage.BYTE_DATA:
			return createByteData();
		case WebservicePackage.READ_OPERATION:
			return createReadOperation();
		case WebservicePackage.CREATE_OPERATION:
			return createCreateOperation();
		case WebservicePackage.UPDATE_OPERATION:
			return createUpdateOperation();
		case WebservicePackage.DELETE_OPERATION:
			return createDeleteOperation();
		case WebservicePackage.OTHER_OPERATION:
			return createOtherOperation();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public API createAPI() {
		APIImpl api = new APIImpl();
		return api;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Parameter createParameter() {
		ParameterImpl parameter = new ParameterImpl();
		return parameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ObjectDataType createObjectDataType() {
		ObjectDataTypeImpl objectDataType = new ObjectDataTypeImpl();
		return objectDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CollectionDataType createCollectionDataType() {
		CollectionDataTypeImpl collectionDataType = new CollectionDataTypeImpl();
		return collectionDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public StringData createStringData() {
		StringDataImpl stringData = new StringDataImpl();
		return stringData;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IntegerData createIntegerData() {
		IntegerDataImpl integerData = new IntegerDataImpl();
		return integerData;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FloatData createFloatData() {
		FloatDataImpl floatData = new FloatDataImpl();
		return floatData;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BooleanData createBooleanData() {
		BooleanDataImpl booleanData = new BooleanDataImpl();
		return booleanData;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ByteData createByteData() {
		ByteDataImpl byteData = new ByteDataImpl();
		return byteData;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ReadOperation createReadOperation() {
		ReadOperationImpl readOperation = new ReadOperationImpl();
		return readOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CreateOperation createCreateOperation() {
		CreateOperationImpl createOperation = new CreateOperationImpl();
		return createOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UpdateOperation createUpdateOperation() {
		UpdateOperationImpl updateOperation = new UpdateOperationImpl();
		return updateOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DeleteOperation createDeleteOperation() {
		DeleteOperationImpl deleteOperation = new DeleteOperationImpl();
		return deleteOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public OtherOperation createOtherOperation() {
		OtherOperationImpl otherOperation = new OtherOperationImpl();
		return otherOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public WebservicePackage getWebservicePackage() {
		return (WebservicePackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static WebservicePackage getPackage() {
		return WebservicePackage.eINSTANCE;
	}

} //WebserviceFactoryImpl
