/**
 */
package webservice.impl;

import org.eclipse.emf.ecore.EClass;

import webservice.IntegerData;
import webservice.WebservicePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Integer Data</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IntegerDataImpl extends DataTypeImpl implements IntegerData {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IntegerDataImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebservicePackage.Literals.INTEGER_DATA;
	}

} //IntegerDataImpl
