/**
 */
package webservice.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import webservice.CollectionDataType;
import webservice.DataType;
import webservice.WebservicePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Collection Data Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link webservice.impl.CollectionDataTypeImpl#getListItem <em>List Item</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CollectionDataTypeImpl extends DataTypeImpl implements CollectionDataType {
	/**
	 * The cached value of the '{@link #getListItem() <em>List Item</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getListItem()
	 * @generated
	 * @ordered
	 */
	protected DataType listItem;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CollectionDataTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebservicePackage.Literals.COLLECTION_DATA_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DataType getListItem() {
		if (listItem != null && listItem.eIsProxy()) {
			InternalEObject oldListItem = (InternalEObject) listItem;
			listItem = (DataType) eResolveProxy(oldListItem);
			if (listItem != oldListItem) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							WebservicePackage.COLLECTION_DATA_TYPE__LIST_ITEM, oldListItem, listItem));
			}
		}
		return listItem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataType basicGetListItem() {
		return listItem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setListItem(DataType newListItem) {
		DataType oldListItem = listItem;
		listItem = newListItem;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebservicePackage.COLLECTION_DATA_TYPE__LIST_ITEM,
					oldListItem, listItem));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case WebservicePackage.COLLECTION_DATA_TYPE__LIST_ITEM:
			if (resolve)
				return getListItem();
			return basicGetListItem();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case WebservicePackage.COLLECTION_DATA_TYPE__LIST_ITEM:
			setListItem((DataType) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case WebservicePackage.COLLECTION_DATA_TYPE__LIST_ITEM:
			setListItem((DataType) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case WebservicePackage.COLLECTION_DATA_TYPE__LIST_ITEM:
			return listItem != null;
		}
		return super.eIsSet(featureID);
	}

} //CollectionDataTypeImpl
