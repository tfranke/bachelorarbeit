/**
 */
package webservice.impl;

import org.eclipse.emf.ecore.EClass;

import webservice.ByteData;
import webservice.WebservicePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Byte Data</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ByteDataImpl extends DataTypeImpl implements ByteData {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ByteDataImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebservicePackage.Literals.BYTE_DATA;
	}

} //ByteDataImpl
