/**
 */
package webservice.impl;

import org.eclipse.emf.ecore.EClass;

import webservice.BooleanData;
import webservice.WebservicePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Boolean Data</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BooleanDataImpl extends DataTypeImpl implements BooleanData {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BooleanDataImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebservicePackage.Literals.BOOLEAN_DATA;
	}

} //BooleanDataImpl
