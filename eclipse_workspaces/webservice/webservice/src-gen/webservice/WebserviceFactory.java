/**
 */
package webservice;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see webservice.WebservicePackage
 * @generated
 */
public interface WebserviceFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	WebserviceFactory eINSTANCE = webservice.impl.WebserviceFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>API</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>API</em>'.
	 * @generated
	 */
	API createAPI();

	/**
	 * Returns a new object of class '<em>Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Parameter</em>'.
	 * @generated
	 */
	Parameter createParameter();

	/**
	 * Returns a new object of class '<em>Object Data Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Object Data Type</em>'.
	 * @generated
	 */
	ObjectDataType createObjectDataType();

	/**
	 * Returns a new object of class '<em>Collection Data Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Collection Data Type</em>'.
	 * @generated
	 */
	CollectionDataType createCollectionDataType();

	/**
	 * Returns a new object of class '<em>String Data</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>String Data</em>'.
	 * @generated
	 */
	StringData createStringData();

	/**
	 * Returns a new object of class '<em>Integer Data</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Integer Data</em>'.
	 * @generated
	 */
	IntegerData createIntegerData();

	/**
	 * Returns a new object of class '<em>Float Data</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Float Data</em>'.
	 * @generated
	 */
	FloatData createFloatData();

	/**
	 * Returns a new object of class '<em>Boolean Data</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Boolean Data</em>'.
	 * @generated
	 */
	BooleanData createBooleanData();

	/**
	 * Returns a new object of class '<em>Byte Data</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Byte Data</em>'.
	 * @generated
	 */
	ByteData createByteData();

	/**
	 * Returns a new object of class '<em>Read Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Read Operation</em>'.
	 * @generated
	 */
	ReadOperation createReadOperation();

	/**
	 * Returns a new object of class '<em>Create Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Create Operation</em>'.
	 * @generated
	 */
	CreateOperation createCreateOperation();

	/**
	 * Returns a new object of class '<em>Update Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Update Operation</em>'.
	 * @generated
	 */
	UpdateOperation createUpdateOperation();

	/**
	 * Returns a new object of class '<em>Delete Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Delete Operation</em>'.
	 * @generated
	 */
	DeleteOperation createDeleteOperation();

	/**
	 * Returns a new object of class '<em>Other Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Other Operation</em>'.
	 * @generated
	 */
	OtherOperation createOtherOperation();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	WebservicePackage getWebservicePackage();

} //WebserviceFactory
