/**
 */
package webservice.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import webservice.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see webservice.WebservicePackage
 * @generated
 */
public class WebserviceAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static WebservicePackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WebserviceAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = WebservicePackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WebserviceSwitch<Adapter> modelSwitch = new WebserviceSwitch<Adapter>() {
		@Override
		public Adapter caseAPI(API object) {
			return createAPIAdapter();
		}

		@Override
		public Adapter caseOperation(Operation object) {
			return createOperationAdapter();
		}

		@Override
		public Adapter caseParameter(Parameter object) {
			return createParameterAdapter();
		}

		@Override
		public Adapter caseDataType(DataType object) {
			return createDataTypeAdapter();
		}

		@Override
		public Adapter caseObjectDataType(ObjectDataType object) {
			return createObjectDataTypeAdapter();
		}

		@Override
		public Adapter caseCollectionDataType(CollectionDataType object) {
			return createCollectionDataTypeAdapter();
		}

		@Override
		public Adapter caseStringData(StringData object) {
			return createStringDataAdapter();
		}

		@Override
		public Adapter caseIntegerData(IntegerData object) {
			return createIntegerDataAdapter();
		}

		@Override
		public Adapter caseFloatData(FloatData object) {
			return createFloatDataAdapter();
		}

		@Override
		public Adapter caseBooleanData(BooleanData object) {
			return createBooleanDataAdapter();
		}

		@Override
		public Adapter caseByteData(ByteData object) {
			return createByteDataAdapter();
		}

		@Override
		public Adapter caseReadOperation(ReadOperation object) {
			return createReadOperationAdapter();
		}

		@Override
		public Adapter caseCreateOperation(CreateOperation object) {
			return createCreateOperationAdapter();
		}

		@Override
		public Adapter caseUpdateOperation(UpdateOperation object) {
			return createUpdateOperationAdapter();
		}

		@Override
		public Adapter caseDeleteOperation(DeleteOperation object) {
			return createDeleteOperationAdapter();
		}

		@Override
		public Adapter caseOtherOperation(OtherOperation object) {
			return createOtherOperationAdapter();
		}

		@Override
		public Adapter defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link webservice.API <em>API</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see webservice.API
	 * @generated
	 */
	public Adapter createAPIAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link webservice.Operation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see webservice.Operation
	 * @generated
	 */
	public Adapter createOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link webservice.Parameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see webservice.Parameter
	 * @generated
	 */
	public Adapter createParameterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link webservice.DataType <em>Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see webservice.DataType
	 * @generated
	 */
	public Adapter createDataTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link webservice.ObjectDataType <em>Object Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see webservice.ObjectDataType
	 * @generated
	 */
	public Adapter createObjectDataTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link webservice.CollectionDataType <em>Collection Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see webservice.CollectionDataType
	 * @generated
	 */
	public Adapter createCollectionDataTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link webservice.StringData <em>String Data</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see webservice.StringData
	 * @generated
	 */
	public Adapter createStringDataAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link webservice.IntegerData <em>Integer Data</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see webservice.IntegerData
	 * @generated
	 */
	public Adapter createIntegerDataAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link webservice.FloatData <em>Float Data</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see webservice.FloatData
	 * @generated
	 */
	public Adapter createFloatDataAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link webservice.BooleanData <em>Boolean Data</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see webservice.BooleanData
	 * @generated
	 */
	public Adapter createBooleanDataAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link webservice.ByteData <em>Byte Data</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see webservice.ByteData
	 * @generated
	 */
	public Adapter createByteDataAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link webservice.ReadOperation <em>Read Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see webservice.ReadOperation
	 * @generated
	 */
	public Adapter createReadOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link webservice.CreateOperation <em>Create Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see webservice.CreateOperation
	 * @generated
	 */
	public Adapter createCreateOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link webservice.UpdateOperation <em>Update Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see webservice.UpdateOperation
	 * @generated
	 */
	public Adapter createUpdateOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link webservice.DeleteOperation <em>Delete Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see webservice.DeleteOperation
	 * @generated
	 */
	public Adapter createDeleteOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link webservice.OtherOperation <em>Other Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see webservice.OtherOperation
	 * @generated
	 */
	public Adapter createOtherOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //WebserviceAdapterFactory
