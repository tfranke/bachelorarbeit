/**
 */
package webservice;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Delete Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see webservice.WebservicePackage#getDeleteOperation()
 * @model
 * @generated
 */
public interface DeleteOperation extends Operation {
} // DeleteOperation
