/**
 */
package webservice;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Float Data</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see webservice.WebservicePackage#getFloatData()
 * @model
 * @generated
 */
public interface FloatData extends DataType {
} // FloatData
