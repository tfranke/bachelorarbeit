/**
 */
package webservice;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Integer Data</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see webservice.WebservicePackage#getIntegerData()
 * @model
 * @generated
 */
public interface IntegerData extends DataType {
} // IntegerData
