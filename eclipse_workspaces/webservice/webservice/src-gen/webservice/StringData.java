/**
 */
package webservice;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>String Data</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see webservice.WebservicePackage#getStringData()
 * @model
 * @generated
 */
public interface StringData extends DataType {
} // StringData
