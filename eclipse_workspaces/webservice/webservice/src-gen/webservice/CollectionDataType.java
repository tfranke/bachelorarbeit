/**
 */
package webservice;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Collection Data Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link webservice.CollectionDataType#getListItem <em>List Item</em>}</li>
 * </ul>
 *
 * @see webservice.WebservicePackage#getCollectionDataType()
 * @model
 * @generated
 */
public interface CollectionDataType extends DataType {
	/**
	 * Returns the value of the '<em><b>List Item</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>List Item</em>' reference.
	 * @see #setListItem(DataType)
	 * @see webservice.WebservicePackage#getCollectionDataType_ListItem()
	 * @model required="true"
	 * @generated
	 */
	DataType getListItem();

	/**
	 * Sets the value of the '{@link webservice.CollectionDataType#getListItem <em>List Item</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>List Item</em>' reference.
	 * @see #getListItem()
	 * @generated
	 */
	void setListItem(DataType value);

} // CollectionDataType
