/**
 */
package webservice;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Boolean Data</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see webservice.WebservicePackage#getBooleanData()
 * @model
 * @generated
 */
public interface BooleanData extends DataType {
} // BooleanData
