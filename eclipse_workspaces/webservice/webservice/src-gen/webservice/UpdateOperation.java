/**
 */
package webservice;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Update Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see webservice.WebservicePackage#getUpdateOperation()
 * @model
 * @generated
 */
public interface UpdateOperation extends Operation {
} // UpdateOperation
