/**
 */
package webservice;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Create Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see webservice.WebservicePackage#getCreateOperation()
 * @model
 * @generated
 */
public interface CreateOperation extends Operation {
} // CreateOperation
