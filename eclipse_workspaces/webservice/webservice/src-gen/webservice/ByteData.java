/**
 */
package webservice;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Byte Data</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see webservice.WebservicePackage#getByteData()
 * @model
 * @generated
 */
public interface ByteData extends DataType {
} // ByteData
