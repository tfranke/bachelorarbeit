/**
 */
package webservice;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Object Data Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link webservice.ObjectDataType#getDatatypes <em>Datatypes</em>}</li>
 * </ul>
 *
 * @see webservice.WebservicePackage#getObjectDataType()
 * @model
 * @generated
 */
public interface ObjectDataType extends DataType {
	/**
	 * Returns the value of the '<em><b>Datatypes</b></em>' reference list.
	 * The list contents are of type {@link webservice.DataType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Datatypes</em>' reference list.
	 * @see webservice.WebservicePackage#getObjectDataType_Datatypes()
	 * @model required="true"
	 * @generated
	 */
	EList<DataType> getDatatypes();

} // ObjectDataType
