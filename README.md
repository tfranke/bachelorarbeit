# Bachelorarbeit

Dieses Repository gehört zur Bachelorarbeit "Entwicklung eines abstrakten Servicemodells zur Vereinfachung der Plattformintegration" und beinhaltet ein mit dem Eclipse Modelling Framework erstelltes Metamodell zur Beschreibung unterschiedlicher Service-Arten, wie REST, SOAP und GraphQL. Zusätzlich liefert es eine DSL zur Beschreibung von Metamodell-Instanzen, zusammen mit einem OpenAPI-Generator und Transconnect-Repository-Generator. 

### Installation und Einrichtung

**Vorraussetzungen:**
- Eclipse EMF 2024-03 oder neuer (https://www.eclipse.org/downloads/packages/release/2024-06/r/eclipse-modeling-tools)

**Benutzung:**
Die Funktionalität des Projekts wurde bisher ausschließlich unter Windows getestet, daher kann die Kompatibilität mit anderen Betriebssystemen nicht garantiert werden.

- öffne Eclipse und wähle den Workspace "<Pfad_zum_heruntergeladenen_Repository>\eclipse_workspaces\webservice" aus
- unter Help > Eclipse Marketplace ... installiere zusätzlich Eclipse Xtext und Eclipse Xtend

Anschließend kann der Workspace benutzt werden. Im Projekt ***webservice*** ist das Metamodell und alle von EMF generierten Java-Klassen zu finden. 
Die Xtext Grammatik ist unter org.tfranke.webservice.dsl.WebserviceDSL zu finden.
Der OpenAPI-Generator ist unter org.tfranke.webservice.dsl.generator.WebserviceDSLGenerator zu finden.
Der Transconnect-Repository-Generator ist unter org.tfranke.webservice.dsl.generator.TransconnectRepositoryCreator zu finden.

**Benutzung der DSL**
- Rechtsklick auf org.tfranke.webservice.dsl.GenerateWebserviceDSL.mwe2 > Run As > MWE2 Workflow
- Rechtsklick auf das Top-Level Eclipse Projekt ***org.tfranke.webservice.dsl*** > Run As > Eclipse Application

Es öffnet sich eine neue Workbench (das kann Anfangs etwas länger dauern), in der die DSL benutzt werden kann. Es existiert bereits eine Beispiel DSl src/petstore.dsl . Eigene DSLs können durch anlegen einer neuen Datei mit der Endung .dsl erstellt werden. Für valide DSLs wird automatisch nach jeder Änderung der OpenAPI-Generator und Transconnect-Generator durchlaufen. Die erstellten Artefakte sind im src-gen Ordner zu finden.

